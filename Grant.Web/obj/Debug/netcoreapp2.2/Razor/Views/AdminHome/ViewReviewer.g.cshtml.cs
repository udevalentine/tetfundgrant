#pragma checksum "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReviewer.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7d7d02ce6a907e74d4e99452acaa1b7ad350996b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_AdminHome_ViewReviewer), @"mvc.1.0.view", @"/Views/AdminHome/ViewReviewer.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/AdminHome/ViewReviewer.cshtml", typeof(AspNetCore.Views_AdminHome_ViewReviewer))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\_ViewImports.cshtml"
using Grant.Web;

#line default
#line hidden
#line 2 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\_ViewImports.cshtml"
using Grant.Web.Models;

#line default
#line hidden
#line 3 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\_ViewImports.cshtml"
using System.Security.Claims;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7d7d02ce6a907e74d4e99452acaa1b7ad350996b", @"/Views/AdminHome/ViewReviewer.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ee57882324b1c76fbe9e059654765310563444e3", @"/Views/_ViewImports.cshtml")]
    public class Views_AdminHome_ViewReviewer : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Grant.Web.Models.ViewModels.AdminViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReviewer.cshtml"
  
    ViewData["Title"] = "Assessor Detail";
    Layout = "~/Views/Shared/_AdminLayout.cshtml";

#line default
#line hidden
            BeginContext(154, 90, true);
            WriteLiteral("<section class=\"content-header bd-top\" style=\"padding-top: 0; background-color: #fff\">\r\n\r\n");
            EndContext();
#line 8 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReviewer.cshtml"
     if (@TempData["Message"] != null)
    {

#line default
#line hidden
            BeginContext(291, 67, true);
            WriteLiteral("        <div class=\"callout \" style=\"color: white\">\r\n\r\n            ");
            EndContext();
            BeginContext(359, 45, false);
#line 12 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReviewer.cshtml"
       Write(Html.Partial("_Message", TempData["Message"]));

#line default
#line hidden
            EndContext();
            BeginContext(404, 20, true);
            WriteLiteral("\r\n\r\n        </div>\r\n");
            EndContext();
#line 15 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReviewer.cshtml"
    }

#line default
#line hidden
            BeginContext(431, 1732, true);
            WriteLiteral(@"</section>
<section class=""content"">

    <div class=""ct-page-title"">
        <h1 class=""ct-title"" id=""content"">Assessors  <span class=""text-muted text-sm"">/ View Assessors </span></h1>
        <hr class=""mt-0 mb-2"" />
    </div>

    <div class=""row justify-content-center"" style=""margin-bottom: 30px"">
        <div class=""col-md-10"">
            <!-- Horizontal Form -->
            <div class=""card"" style="""">
                
                <div class=""card-body"">
                    <div class=""table-responsive"">
                    <table id=""modelDataTable"" class=""table table-bordered table-hover"">
                        <thead>
                            <tr>
                                <th>S/N</th>
                                <th>
                                    Title
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
        ");
            WriteLiteral(@"                            Phone
                                </th>
                                <th>
                                    Institution
                                </th>
                                <th>
                                    Email
                                </th>
                                <th>
                                    Active
                                </th>
                                <th>
                                    Assigned ConceptNote
                                </th>
                                <th>
                                    Assigned Proposal
                                </th>
");
            EndContext();
            BeginContext(2222, 104, true);
            WriteLiteral("\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody>\r\n");
            EndContext();
#line 66 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReviewer.cshtml"
                               int index = 0; 

#line default
#line hidden
            BeginContext(2375, 28, true);
            WriteLiteral("                            ");
            EndContext();
#line 67 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReviewer.cshtml"
                             foreach (var item in Model.ReviewerDetails)
                            {
                                index += 1;

#line default
#line hidden
            BeginContext(2525, 70, true);
            WriteLiteral("                            <tr>\r\n                                <td>");
            EndContext();
            BeginContext(2596, 5, false);
#line 71 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReviewer.cshtml"
                               Write(index);

#line default
#line hidden
            EndContext();
            BeginContext(2601, 81, true);
            WriteLiteral("</td>\r\n                                <td>\r\n                                    ");
            EndContext();
            BeginContext(2683, 54, false);
#line 73 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReviewer.cshtml"
                               Write(Html.DisplayFor(modelItem => item.Reviewer.Title.Name));

#line default
#line hidden
            EndContext();
            BeginContext(2737, 115, true);
            WriteLiteral("\r\n                                </td>\r\n                                <td>\r\n                                    ");
            EndContext();
            BeginContext(2853, 48, false);
#line 76 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReviewer.cshtml"
                               Write(Html.DisplayFor(modelItem => item.Reviewer.Name));

#line default
#line hidden
            EndContext();
            BeginContext(2901, 115, true);
            WriteLiteral("\r\n                                </td>\r\n                                <td>\r\n                                    ");
            EndContext();
            BeginContext(3017, 40, false);
#line 79 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReviewer.cshtml"
                               Write(Html.DisplayFor(modelItem => item.Phone));

#line default
#line hidden
            EndContext();
            BeginContext(3057, 115, true);
            WriteLiteral("\r\n                                </td>\r\n                                <td>\r\n                                    ");
            EndContext();
            BeginContext(3173, 60, false);
#line 82 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReviewer.cshtml"
                               Write(Html.DisplayFor(modelItem => item.Reviewer.Institution.Name));

#line default
#line hidden
            EndContext();
            BeginContext(3233, 115, true);
            WriteLiteral("\r\n                                </td>\r\n                                <td>\r\n                                    ");
            EndContext();
            BeginContext(3349, 45, false);
#line 85 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReviewer.cshtml"
                               Write(Html.DisplayFor(modelItem => item.User.Email));

#line default
#line hidden
            EndContext();
            BeginContext(3394, 117, true);
            WriteLiteral("\r\n                                </td>\r\n\r\n                                <td>\r\n                                    ");
            EndContext();
            BeginContext(3512, 46, false);
#line 89 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReviewer.cshtml"
                               Write(Html.DisplayFor(modelItem => item.User.Active));

#line default
#line hidden
            EndContext();
            BeginContext(3558, 119, true);
            WriteLiteral("\r\n\r\n                                </td>\r\n                                <td>\r\n\r\n                                    ");
            EndContext();
            BeginContext(3678, 49, false);
#line 94 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReviewer.cshtml"
                               Write(Html.DisplayFor(modelItem => item.ApplicantCount));

#line default
#line hidden
            EndContext();
            BeginContext(3727, 119, true);
            WriteLiteral("\r\n\r\n                                </td>\r\n                                <td>\r\n\r\n                                    ");
            EndContext();
            BeginContext(3847, 48, false);
#line 99 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReviewer.cshtml"
                               Write(Html.DisplayFor(modelItem => item.ProposalCount));

#line default
#line hidden
            EndContext();
            BeginContext(3895, 43, true);
            WriteLiteral("\r\n\r\n                                </td>\r\n");
            EndContext();
            BeginContext(4103, 37, true);
            WriteLiteral("\r\n                            </tr>\r\n");
            EndContext();
#line 107 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReviewer.cshtml"
                            }

#line default
#line hidden
            BeginContext(4171, 127, true);
            WriteLiteral("\r\n                    </table>\r\n                        </div>\r\n                    <div class=\"box-footer mt-3 text-center\">\r\n");
            EndContext();
            BeginContext(4438, 118, true);
            WriteLiteral("                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n\r\n\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Grant.Web.Models.ViewModels.AdminViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
