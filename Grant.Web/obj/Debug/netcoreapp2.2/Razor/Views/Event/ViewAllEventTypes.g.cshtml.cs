#pragma checksum "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Event\ViewAllEventTypes.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c334ac019c8f903fc41ba4ba2533a42e20373897"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Event_ViewAllEventTypes), @"mvc.1.0.view", @"/Views/Event/ViewAllEventTypes.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Event/ViewAllEventTypes.cshtml", typeof(AspNetCore.Views_Event_ViewAllEventTypes))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\_ViewImports.cshtml"
using Grant.Web;

#line default
#line hidden
#line 2 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\_ViewImports.cshtml"
using Grant.Web.Models;

#line default
#line hidden
#line 3 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\_ViewImports.cshtml"
using System.Security.Claims;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c334ac019c8f903fc41ba4ba2533a42e20373897", @"/Views/Event/ViewAllEventTypes.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ee57882324b1c76fbe9e059654765310563444e3", @"/Views/_ViewImports.cshtml")]
    public class Views_Event_ViewAllEventTypes : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Grant.Web.Models.ViewModels.EventViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("name", "_Message", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "EditEventType", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Event", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-primary"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-info pull-right"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "AddEventType", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.PartialTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Event\ViewAllEventTypes.cshtml"
  
    ViewData["Title"] = "View All Event Types";
    Layout = "~/Views/Shared/_AdminLayout.cshtml";

#line default
#line hidden
            BeginContext(159, 277, true);
            WriteLiteral(@"
<section class=""content-header bd-top"" style=""padding-top: 0; background-color: #fff"">

    <div class=""pg-head"">
        <h3 class=""monts text-center"" style=""font-weight: 300; margin-top: 10px"">Event Type / <span class=""small"">View Event Types</span></h3>
    </div>

");
            EndContext();
#line 13 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Event\ViewAllEventTypes.cshtml"
     if (@TempData["Message"] != null)
    {

#line default
#line hidden
            BeginContext(483, 65, true);
            WriteLiteral("        <div class=\"callout \" style=\"color: white\">\r\n            ");
            EndContext();
            BeginContext(548, 56, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("partial", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "c334ac019c8f903fc41ba4ba2533a42e203738976529", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.PartialTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper.Name = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
#line 16 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Event\ViewAllEventTypes.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper.Model = TempData["Message"];

#line default
#line hidden
            __tagHelperExecutionContext.AddTagHelperAttribute("model", __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper.Model, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(604, 18, true);
            WriteLiteral("\r\n        </div>\r\n");
            EndContext();
#line 18 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Event\ViewAllEventTypes.cshtml"
    }

#line default
#line hidden
            BeginContext(629, 1011, true);
            WriteLiteral(@"</section>

<div class="""" style=""margin-left: 3px !important; margin-bottom: 20px"">
    <div class=""container"">
        <div class=""col-md-12"">
            <!-- Horizontal Form -->
            <div class=""box box-danger"" style=""border-top-color: #fff; border-radius: 0px"">
                <div class=""box-body"">
                    <div class=""table-responsive"">
                        <table id=""modelDataTable"" class=""table table-bordered table-striped"">
                            <thead>
                                <tr>
                                    <th>
                                        SN
                                    </th>
                                    <th>
                                        Name
                                    </th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
");
            EndContext();
#line 42 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Event\ViewAllEventTypes.cshtml"
                                   int index = 0; 

#line default
#line hidden
            BeginContext(1693, 32, true);
            WriteLiteral("                                ");
            EndContext();
#line 43 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Event\ViewAllEventTypes.cshtml"
                                 foreach (var item in Model.EventTypes)
                                {
                                    index += 1;

#line default
#line hidden
            BeginContext(1850, 132, true);
            WriteLiteral("                                    <tr>\r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(1983, 5, false);
#line 48 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Event\ViewAllEventTypes.cshtml"
                                       Write(index);

#line default
#line hidden
            EndContext();
            BeginContext(1988, 163, true);
            WriteLiteral("\r\n                                        </td>\r\n                                        <td class=\"text-capitalize\">\r\n                                            ");
            EndContext();
            BeginContext(2152, 9, false);
#line 51 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Event\ViewAllEventTypes.cshtml"
                                       Write(item.Name);

#line default
#line hidden
            EndContext();
            BeginContext(2161, 95, true);
            WriteLiteral("\r\n                                        </td>\r\n                                        <td>\r\n");
            EndContext();
#line 54 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Event\ViewAllEventTypes.cshtml"
                                             if (item.Active)
                                            {

#line default
#line hidden
            BeginContext(2366, 78, true);
            WriteLiteral("                                                <input type=\"checkbox\" checked");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 2444, "\"", 2483, 3);
            WriteAttributeValue("", 2454, "deactivateEventType(", 2454, 20, true);
#line 56 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Event\ViewAllEventTypes.cshtml"
WriteAttributeValue("", 2474, item.Id, 2474, 8, false);

#line default
#line hidden
            WriteAttributeValue("", 2482, ")", 2482, 1, true);
            EndWriteAttribute();
            BeginContext(2484, 5, true);
            WriteLiteral(" />\r\n");
            EndContext();
#line 57 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Event\ViewAllEventTypes.cshtml"
                                            }
                                            else
                                            {

#line default
#line hidden
            BeginContext(2633, 70, true);
            WriteLiteral("                                                <input type=\"checkbox\"");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 2703, "\"", 2742, 3);
            WriteAttributeValue("", 2713, "deactivateEventType(", 2713, 20, true);
#line 60 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Event\ViewAllEventTypes.cshtml"
WriteAttributeValue("", 2733, item.Id, 2733, 8, false);

#line default
#line hidden
            WriteAttributeValue("", 2741, ")", 2741, 1, true);
            EndWriteAttribute();
            BeginContext(2743, 5, true);
            WriteLiteral(" />\r\n");
            EndContext();
#line 61 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Event\ViewAllEventTypes.cshtml"
                                            }

#line default
#line hidden
            BeginContext(2795, 137, true);
            WriteLiteral("                                        </td>\r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(2932, 109, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "c334ac019c8f903fc41ba4ba2533a42e2037389714035", async() => {
                BeginContext(3033, 4, true);
                WriteLiteral("Edit");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 64 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Event\ViewAllEventTypes.cshtml"
                                                                                                                           WriteLiteral(item.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3041, 92, true);
            WriteLiteral("\r\n                                        </td>\r\n                                    </tr>\r\n");
            EndContext();
#line 67 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Event\ViewAllEventTypes.cshtml"
                                }

#line default
#line hidden
            BeginContext(3168, 181, true);
            WriteLiteral("                        </table>\r\n                    </div>\r\n                </div>\r\n                <div class=\"box-body\" style=\"border-top:1px solid #ddd;\">\r\n                    ");
            EndContext();
            BeginContext(3349, 102, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "c334ac019c8f903fc41ba4ba2533a42e2037389717352", async() => {
                BeginContext(3433, 14, true);
                WriteLiteral("Add Event Type");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_5.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_5);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3451, 253, true);
            WriteLiteral("\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n\r\n<script type=\"text/javascript\">\r\n    function deactivateEventType(id) {\r\n        if (id) {\r\n            $.ajax({\r\n                type: \"POST\",\r\n                url: \"");
            EndContext();
            BeginContext(3705, 42, false);
#line 85 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Event\ViewAllEventTypes.cshtml"
                 Write(Url.Action("DeactivateEventType", "Event"));

#line default
#line hidden
            EndContext();
            BeginContext(3747, 317, true);
            WriteLiteral(@""",
                data: { id },
                dataType: ""json"",
                success: function (data) {
                    alert(data);
                },
                error: function (error) {
                    console.error(error);
                }
            });
        }
    }
</script>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Grant.Web.Models.ViewModels.EventViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
