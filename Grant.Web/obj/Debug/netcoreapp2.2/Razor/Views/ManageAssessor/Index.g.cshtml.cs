#pragma checksum "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\ManageAssessor\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "74a3a62e06a44a3a101ab8be7208c623e3a76143"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_ManageAssessor_Index), @"mvc.1.0.view", @"/Views/ManageAssessor/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/ManageAssessor/Index.cshtml", typeof(AspNetCore.Views_ManageAssessor_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\_ViewImports.cshtml"
using Grant.Web;

#line default
#line hidden
#line 2 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\_ViewImports.cshtml"
using Grant.Web.Models;

#line default
#line hidden
#line 3 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\_ViewImports.cshtml"
using System.Security.Claims;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"74a3a62e06a44a3a101ab8be7208c623e3a76143", @"/Views/ManageAssessor/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ee57882324b1c76fbe9e059654765310563444e3", @"/Views/_ViewImports.cshtml")]
    public class Views_ManageAssessor_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Grant.Web.Models.ViewModels.AdminViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "GetAssessorExpertiseDetail", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-primary"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\ManageAssessor\Index.cshtml"
  
    ViewData["Title"] = "Assessor Detail";
    Layout = "~/Views/Shared/_AdminLayout.cshtml";

#line default
#line hidden
            BeginContext(154, 90, true);
            WriteLiteral("<section class=\"content-header bd-top\" style=\"padding-top: 0; background-color: #fff\">\r\n\r\n");
            EndContext();
#line 8 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\ManageAssessor\Index.cshtml"
     if (@TempData["Message"] != null)
    {

#line default
#line hidden
            BeginContext(291, 67, true);
            WriteLiteral("        <div class=\"callout \" style=\"color: white\">\r\n\r\n            ");
            EndContext();
            BeginContext(359, 45, false);
#line 12 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\ManageAssessor\Index.cshtml"
       Write(Html.Partial("_Message", TempData["Message"]));

#line default
#line hidden
            EndContext();
            BeginContext(404, 20, true);
            WriteLiteral("\r\n\r\n        </div>\r\n");
            EndContext();
#line 15 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\ManageAssessor\Index.cshtml"
    }

#line default
#line hidden
            BeginContext(431, 1462, true);
            WriteLiteral(@"</section>
<section class=""content"">

    <div class=""ct-page-title"">
        <h1 class=""ct-title"" id=""content"">Assessors  <span class=""text-muted text-sm"">/ View Assessor(s) Thematic Category </span></h1>
        <hr class=""mt-0 mb-2"" />
    </div>

    <div class=""row justify-content-center"" style=""margin-bottom: 30px"">
        <div class=""col-md-10"">
            <!-- Horizontal Form -->
            <div class=""card"" style="""">

                <div class=""card-body"">
                    <div class=""table-responsive"">
                        <table id=""modelDataTable"" class=""table table-bordered table-hover"">
                            <thead>
                                <tr>
                                    <th>S/N</th>
                                    <th>
                                        Title
                                    </th>
                                    <th>
                                        Name
                                    </th>
  ");
            WriteLiteral(@"                                  <th>
                                        Phone
                                    </th>
                                    <th>
                                        Thematic Category
                                    </th>
                                   
                                    <th>
                                        
                                    </th>
");
            EndContext();
            BeginContext(1988, 116, true);
            WriteLiteral("\r\n                                </tr>\r\n                            </thead>\r\n                            <tbody>\r\n");
            EndContext();
#line 58 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\ManageAssessor\Index.cshtml"
                                   int index = 0; 

#line default
#line hidden
            BeginContext(2157, 32, true);
            WriteLiteral("                                ");
            EndContext();
#line 59 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\ManageAssessor\Index.cshtml"
                                 foreach (var item in Model.ReviewerDetails)
                                {
                                    index += 1;

#line default
#line hidden
            BeginContext(2319, 86, true);
            WriteLiteral("                                    <tr>\r\n                                        <td>");
            EndContext();
            BeginContext(2406, 5, false);
#line 63 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\ManageAssessor\Index.cshtml"
                                       Write(index);

#line default
#line hidden
            EndContext();
            BeginContext(2411, 97, true);
            WriteLiteral("</td>\r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(2509, 54, false);
#line 65 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\ManageAssessor\Index.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.Reviewer.Title.Name));

#line default
#line hidden
            EndContext();
            BeginContext(2563, 139, true);
            WriteLiteral("\r\n                                        </td>\r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(2703, 48, false);
#line 68 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\ManageAssessor\Index.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.Reviewer.Name));

#line default
#line hidden
            EndContext();
            BeginContext(2751, 139, true);
            WriteLiteral("\r\n                                        </td>\r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(2891, 40, false);
#line 71 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\ManageAssessor\Index.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.Phone));

#line default
#line hidden
            EndContext();
            BeginContext(2931, 95, true);
            WriteLiteral("\r\n                                        </td>\r\n                                        <td>\r\n");
            EndContext();
#line 74 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\ManageAssessor\Index.cshtml"
                                             if (item.Theme != null && item.Theme.Id > 0)
                                            {
                                                

#line default
#line hidden
            BeginContext(3213, 54, false);
#line 76 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\ManageAssessor\Index.cshtml"
                                           Write(Html.DisplayFor(modelItem => item.Theme.Category.Name));

#line default
#line hidden
            EndContext();
#line 76 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\ManageAssessor\Index.cshtml"
                                                                                                       
                                            }
                                            else
                                            {
                                                
                                            }

#line default
#line hidden
            BeginContext(3510, 185, true);
            WriteLiteral("\r\n                                        </td>\r\n                                        \r\n                                        <td>\r\n                                                ");
            EndContext();
            BeginContext(3695, 118, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "74a3a62e06a44a3a101ab8be7208c623e3a7614311796", async() => {
                BeginContext(3795, 14, true);
                WriteLiteral("View Expertise");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 86 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\ManageAssessor\Index.cshtml"
                                                                                                                     WriteLiteral(item.Reviewer.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3813, 98, true);
            WriteLiteral("\r\n                                            </td>\r\n\r\n                                    </tr>\r\n");
            EndContext();
#line 90 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\ManageAssessor\Index.cshtml"
                                }

#line default
#line hidden
            BeginContext(3946, 64, true);
            WriteLiteral("\r\n                        </table>\r\n                    </div>\r\n");
            EndContext();
            BeginContext(4241, 90, true);
            WriteLiteral("                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n\r\n\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Grant.Web.Models.ViewModels.AdminViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
