#pragma checksum "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d797b647ac433acfd87211bfdb17110d105ec9ed"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Application_MySubmissions), @"mvc.1.0.view", @"/Views/Application/MySubmissions.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Application/MySubmissions.cshtml", typeof(AspNetCore.Views_Application_MySubmissions))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\_ViewImports.cshtml"
using Grant.Web;

#line default
#line hidden
#line 2 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\_ViewImports.cshtml"
using Grant.Web.Models;

#line default
#line hidden
#line 3 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\_ViewImports.cshtml"
using System.Security.Claims;

#line default
#line hidden
#line 2 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml"
using Grant.Web.Infrastructure;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d797b647ac433acfd87211bfdb17110d105ec9ed", @"/Views/Application/MySubmissions.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ee57882324b1c76fbe9e059654765310563444e3", @"/Views/_ViewImports.cshtml")]
    public class Views_Application_MySubmissions : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Grant.Web.Models.ViewModels.UserViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "ConceptNotePreview", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Application", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-primary"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "ProposalPreview", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 3 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml"
  
    ViewData["Title"] = "My Submissions";
    Layout = "~/Views/Shared/_UserLayout.cshtml";

#line default
#line hidden
            BeginContext(184, 94, true);
            WriteLiteral("\r\n<section class=\"content-header bd-top\" style=\"padding-top: 0; background-color: #fff\">\r\n\r\n\r\n");
            EndContext();
#line 11 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml"
     if (@TempData["Message"] != null)
    {

#line default
#line hidden
            BeginContext(325, 67, true);
            WriteLiteral("        <div class=\"callout \" style=\"color: white\">\r\n\r\n            ");
            EndContext();
            BeginContext(393, 45, false);
#line 15 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml"
       Write(Html.Partial("_Message", TempData["Message"]));

#line default
#line hidden
            EndContext();
            BeginContext(438, 20, true);
            WriteLiteral("\r\n\r\n        </div>\r\n");
            EndContext();
#line 18 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml"
    }

#line default
#line hidden
            BeginContext(465, 2876, true);
            WriteLiteral(@"</section>



<div class=""ct-page-title"">
    <h1 class=""ct-title"" id=""content"">My Submissions <span class=""text-muted text-sm""> </span></h1>
    <hr class=""mt-0 mb-2"" />
</div>
                    <div class=""row justify-content-center"" style=""margin-bottom: 30px"">

                        <div class=""col-md-12"">

                            <div class=""card animated fadeInUp delay-1s"" style="""">

                                <div class=""card-body"">
                                    <div class="""" style=""margin-left: 3px !important; margin-bottom: 20px"">


                                        <!-- Horizontal Form -->
                                        <div class=""box box-danger"" style=""border-top-color: #fff; border-radius: 0px"">

                                            <div class=""box-body"">




                                                <div class=""table-responsive"">
                                                    <table id=""modelDataTable"" class=""table ta");
            WriteLiteral(@"ble-bordered table-striped"">
                                                        <thead>
                                                            <tr>
                                                                <th>
                                                                    S/N
                                                                </th>
                                                                <th>
                                                                    Research Title
                                                                </th>
                                                                <th>
                                                                    Research Thematic Area
                                                                </th>
                                                                <th>
                                                                    Research Application No
                            ");
            WriteLiteral(@"                                    </th>
                                                                <th>
                                                                    Research Cycle
                                                                </th>
                                                                <th>
                                                                    Type
                                                                </th>

                                                                <th>

                                                                </th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
");
            EndContext();
#line 75 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml"
                                                              
                                                                int count = 0;
                                                            

#line default
#line hidden
            BeginContext(3548, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 79 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml"
                                                             foreach (var item in Model.ApplicantSubmissions)
                                                            {
                                                                count++;

#line default
#line hidden
            BeginContext(3798, 142, true);
            WriteLiteral("                                                                <tr>\r\n                                                                    <td>");
            EndContext();
            BeginContext(3941, 5, false);
#line 83 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml"
                                                                   Write(count);

#line default
#line hidden
            EndContext();
            BeginContext(3946, 153, true);
            WriteLiteral("</td>\r\n                                                                    <td>\r\n                                                                        ");
            EndContext();
            BeginContext(4100, 58, false);
#line 85 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml"
                                                                   Write(Html.DisplayFor(modelItem => item.ApplicantThematic.Title));

#line default
#line hidden
            EndContext();
            BeginContext(4158, 223, true);
            WriteLiteral("\r\n                                                                    </td>\r\n                                                                    <td>\r\n                                                                        ");
            EndContext();
            BeginContext(4382, 72, false);
#line 88 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml"
                                                                   Write(Html.DisplayFor(modelItem => item.ApplicantThematic.Theme.Category.Name));

#line default
#line hidden
            EndContext();
            BeginContext(4454, 1, true);
            WriteLiteral("-");
            EndContext();
            BeginContext(4456, 63, false);
#line 88 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml"
                                                                                                                                             Write(Html.DisplayFor(modelItem => item.ApplicantThematic.Theme.Name));

#line default
#line hidden
            EndContext();
            BeginContext(4519, 223, true);
            WriteLiteral("\r\n                                                                    </td>\r\n                                                                    <td>\r\n                                                                        ");
            EndContext();
            BeginContext(4743, 59, false);
#line 91 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml"
                                                                   Write(Html.DisplayFor(modelItem => item.ApplicantThematic.FileNo));

#line default
#line hidden
            EndContext();
            BeginContext(4802, 223, true);
            WriteLiteral("\r\n                                                                    </td>\r\n                                                                    <td>\r\n                                                                        ");
            EndContext();
            BeginContext(5026, 65, false);
#line 94 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml"
                                                                   Write(Html.DisplayFor(modelItem => item.ApplicantThematic.Session.Name));

#line default
#line hidden
            EndContext();
            BeginContext(5091, 79, true);
            WriteLiteral("\r\n                                                                    </td>\r\n\r\n");
            EndContext();
#line 97 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml"
                                                                     if (@item.TypeId == (int)Types.ConceptNote)
                                                                    {

#line default
#line hidden
            BeginContext(5355, 247, true);
            WriteLiteral("                                                                        <td>\r\n                                                                            Concept Note\r\n                                                                        </td>\r\n");
            EndContext();
#line 102 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml"
                                                                    }
                                                                    else
                                                                    {

#line default
#line hidden
            BeginContext(5818, 248, true);
            WriteLiteral("                                                                        <td>\r\n                                                                            Full Proposal\r\n                                                                        </td>\r\n");
            EndContext();
#line 108 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml"
                                                                    }

#line default
#line hidden
            BeginContext(6137, 68, true);
            WriteLiteral("                                                                    ");
            EndContext();
#line 109 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml"
                                                                     if (@item.TypeId == (int)Types.ConceptNote)
                                                                    {

#line default
#line hidden
            BeginContext(6322, 154, true);
            WriteLiteral("                                                                        <td>\r\n                                                                            ");
            EndContext();
            BeginContext(6476, 138, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d797b647ac433acfd87211bfdb17110d105ec9ed16767", async() => {
                BeginContext(6606, 4, true);
                WriteLiteral("View");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 112 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml"
                                                                                                                                                                      WriteLiteral(item.ApplicantThematic.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(6614, 81, true);
            WriteLiteral("\r\n                                                                        </td>\r\n");
            EndContext();
#line 114 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml"
                                                                    }
                                                                    else
                                                                    {

#line default
#line hidden
            BeginContext(6911, 154, true);
            WriteLiteral("                                                                        <td>\r\n                                                                            ");
            EndContext();
            BeginContext(7065, 135, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d797b647ac433acfd87211bfdb17110d105ec9ed20282", async() => {
                BeginContext(7192, 4, true);
                WriteLiteral("View");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 118 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml"
                                                                                                                                                                   WriteLiteral(item.ApplicantThematic.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(7200, 81, true);
            WriteLiteral("\r\n                                                                        </td>\r\n");
            EndContext();
#line 120 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml"
                                                                    }

#line default
#line hidden
            BeginContext(7352, 73, true);
            WriteLiteral("\r\n                                                                </tr>\r\n");
            EndContext();
#line 123 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\Application\MySubmissions.cshtml"
                                                            }

#line default
#line hidden
            BeginContext(7488, 306, true);
            WriteLiteral(@"
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

");
            EndContext();
            BeginContext(7842, 116, true);
            WriteLiteral("\r\n\r\n\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Grant.Web.Models.ViewModels.UserViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
