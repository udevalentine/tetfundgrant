#pragma checksum "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\GetAllApprovedConceptNote.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c48f4873c1889417ec0feb60250f7b625b10f346"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_AdminHome_GetAllApprovedConceptNote), @"mvc.1.0.view", @"/Views/AdminHome/GetAllApprovedConceptNote.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/AdminHome/GetAllApprovedConceptNote.cshtml", typeof(AspNetCore.Views_AdminHome_GetAllApprovedConceptNote))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\_ViewImports.cshtml"
using Grant.Web;

#line default
#line hidden
#line 2 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\_ViewImports.cshtml"
using Grant.Web.Models;

#line default
#line hidden
#line 3 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\_ViewImports.cshtml"
using System.Security.Claims;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c48f4873c1889417ec0feb60250f7b625b10f346", @"/Views/AdminHome/GetAllApprovedConceptNote.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ee57882324b1c76fbe9e059654765310563444e3", @"/Views/_ViewImports.cshtml")]
    public class Views_AdminHome_GetAllApprovedConceptNote : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Grant.Web.Models.ViewModels.AdminViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\GetAllApprovedConceptNote.cshtml"
  
    ViewData["Title"] = "GetAllApprovedConceptNote";
    Layout = "~/Views/Shared/_AdminLayout.cshtml";

#line default
#line hidden
            BeginContext(164, 286, true);
            WriteLiteral(@"
<section class=""content-header bd-top"" style=""padding-top: 0; background-color: #fff"">

    <div class=""pg-head"">
        <h3 class=""monts text-center"" style=""font-weight: 300; margin-top: 10px"">Concept Notes / <span class=""small"">Approved Concept Notes</span></h3>
    </div>

");
            EndContext();
#line 13 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\GetAllApprovedConceptNote.cshtml"
     if (@TempData["Message"] != null)
            {

#line default
#line hidden
            BeginContext(505, 67, true);
            WriteLiteral("        <div class=\"callout \" style=\"color: white\">\r\n\r\n            ");
            EndContext();
            BeginContext(573, 45, false);
#line 17 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\GetAllApprovedConceptNote.cshtml"
       Write(Html.Partial("_Message", TempData["Message"]));

#line default
#line hidden
            EndContext();
            BeginContext(618, 20, true);
            WriteLiteral("\r\n\r\n        </div>\r\n");
            EndContext();
#line 20 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\GetAllApprovedConceptNote.cshtml"
    }

#line default
#line hidden
            BeginContext(645, 1531, true);
            WriteLiteral(@"</section>
<section class=""content"">
    <div class=""container"" style=""margin-bottom: 30px"">
        <div class=""col-md-12"">
            <!-- Horizontal Form -->
            <div class=""box box-danger"" style=""border-top-color: #fff; border-radius: 0"">
                
                <div class=""box-body"">
                    <div class=""table-responsive"">
                    <table id=""modelDataTable"" class=""table table-bordered table-striped"">
                        <thead>
                            <tr>
                                <th>
                                    Principal Investigator(PI)
                                </th>
                                <th>
                                    Phone No
                                </th>
                                <th>
                                    E-mail
                                </th>
                                <th>
                                    Title
                               ");
            WriteLiteral(@" </th>
                                <th>
                                    Category-(Thematic Area)
                                </th>
                                <th>
                                    Assessor(s)
                                </th>

                                <th>
                                    Average Score
                                </th>

                            </tr>
                        </thead>
                        <tbody>
");
            EndContext();
#line 59 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\GetAllApprovedConceptNote.cshtml"
                             foreach (var item in Model.ApplicationList)
                                {

#line default
#line hidden
            BeginContext(2285, 134, true);
            WriteLiteral("                                    <tr>\r\n\r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(2420, 46, false);
#line 64 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\GetAllApprovedConceptNote.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.Person.Name));

#line default
#line hidden
            EndContext();
            BeginContext(2466, 139, true);
            WriteLiteral("\r\n                                        </td>\r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(2606, 49, false);
#line 67 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\GetAllApprovedConceptNote.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.Person.PhoneNo));

#line default
#line hidden
            EndContext();
            BeginContext(2655, 139, true);
            WriteLiteral("\r\n                                        </td>\r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(2795, 47, false);
#line 70 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\GetAllApprovedConceptNote.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.Person.Email));

#line default
#line hidden
            EndContext();
            BeginContext(2842, 139, true);
            WriteLiteral("\r\n                                        </td>\r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(2982, 58, false);
#line 73 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\GetAllApprovedConceptNote.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.ApplicantThematic.Title));

#line default
#line hidden
            EndContext();
            BeginContext(3040, 139, true);
            WriteLiteral("\r\n                                        </td>\r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(3180, 48, false);
#line 76 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\GetAllApprovedConceptNote.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.Category.Name));

#line default
#line hidden
            EndContext();
            BeginContext(3228, 2, true);
            WriteLiteral("-(");
            EndContext();
            BeginContext(3231, 45, false);
#line 76 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\GetAllApprovedConceptNote.cshtml"
                                                                                          Write(Html.DisplayFor(modelItem => item.Theme.Name));

#line default
#line hidden
            EndContext();
            BeginContext(3276, 96, true);
            WriteLiteral(")\r\n                                        </td>\r\n                                        <td>\r\n");
            EndContext();
#line 79 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\GetAllApprovedConceptNote.cshtml"
                                             if (item.ReviewerScores.Count > 0)
                                            {
                                                for (int t = 0; t < item.ReviewerScores.Count; t++)
                                                {
                                                    

#line default
#line hidden
            BeginContext(3705, 66, false);
#line 83 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\GetAllApprovedConceptNote.cshtml"
                                               Write(Html.DisplayFor(modelItem => item.ReviewerScores[t].Reviewer.Name));

#line default
#line hidden
            EndContext();
            BeginContext(3776, 57, false);
#line 83 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\GetAllApprovedConceptNote.cshtml"
                                                                                                                      Write(Html.DisplayFor(modelItem => item.ReviewerScores[t].Mark));

#line default
#line hidden
            EndContext();
            BeginContext(3835, 60, true);
            WriteLiteral("                                                    <br />\r\n");
            EndContext();
#line 85 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\GetAllApprovedConceptNote.cshtml"


                                                }

                                            }

#line default
#line hidden
            BeginContext(3999, 137, true);
            WriteLiteral("                                        </td>\r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(4137, 42, false);
#line 92 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\GetAllApprovedConceptNote.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.Average));

#line default
#line hidden
            EndContext();
            BeginContext(4179, 51, true);
            WriteLiteral("\r\n                                        </td>\r\n\r\n");
            EndContext();
            BeginContext(4544, 45, true);
            WriteLiteral("\r\n                                    </tr>\r\n");
            EndContext();
#line 100 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\GetAllApprovedConceptNote.cshtml"
                                }

#line default
#line hidden
            BeginContext(4624, 164, true);
            WriteLiteral("\r\n                        </table>\r\n                        </div>\r\n                    </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n</section>\r\n\r\n\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Grant.Web.Models.ViewModels.AdminViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
