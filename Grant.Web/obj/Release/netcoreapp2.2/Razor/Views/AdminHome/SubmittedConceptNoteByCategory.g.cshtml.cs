#pragma checksum "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\SubmittedConceptNoteByCategory.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2d715b072d3e8ed77771131aee6fa3898ce79530"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_AdminHome_SubmittedConceptNoteByCategory), @"mvc.1.0.view", @"/Views/AdminHome/SubmittedConceptNoteByCategory.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/AdminHome/SubmittedConceptNoteByCategory.cshtml", typeof(AspNetCore.Views_AdminHome_SubmittedConceptNoteByCategory))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\_ViewImports.cshtml"
using Grant.Web;

#line default
#line hidden
#line 2 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\_ViewImports.cshtml"
using Grant.Web.Models;

#line default
#line hidden
#line 3 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\_ViewImports.cshtml"
using System.Security.Claims;

#line default
#line hidden
#line 2 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\SubmittedConceptNoteByCategory.cshtml"
using Grant.Web.Infrastructure;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2d715b072d3e8ed77771131aee6fa3898ce79530", @"/Views/AdminHome/SubmittedConceptNoteByCategory.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ee57882324b1c76fbe9e059654765310563444e3", @"/Views/_ViewImports.cshtml")]
    public class Views_AdminHome_SubmittedConceptNoteByCategory : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Grant.Web.Models.ViewModels.AdminViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/theme/bower_components/jquery/dist/jquery.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 3 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\SubmittedConceptNoteByCategory.cshtml"
  
    ViewData["Title"] = "Submitted Concept Note";
    Layout = "~/Views/Shared/_AdminLayout.cshtml";

#line default
#line hidden
            BeginContext(194, 92, true);
            WriteLiteral("\r\n<section class=\"content-header bd-top\" style=\"padding-top: 0; background-color: #fff\">\r\n\r\n");
            EndContext();
#line 10 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\SubmittedConceptNoteByCategory.cshtml"
     if (@TempData["Message"] != null)
    {

#line default
#line hidden
            BeginContext(333, 67, true);
            WriteLiteral("        <div class=\"callout \" style=\"color: white\">\r\n\r\n            ");
            EndContext();
            BeginContext(401, 45, false);
#line 14 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\SubmittedConceptNoteByCategory.cshtml"
       Write(Html.Partial("_Message", TempData["Message"]));

#line default
#line hidden
            EndContext();
            BeginContext(446, 20, true);
            WriteLiteral("\r\n\r\n        </div>\r\n");
            EndContext();
#line 17 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\SubmittedConceptNoteByCategory.cshtml"
    }

#line default
#line hidden
            BeginContext(473, 2188, true);
            WriteLiteral(@"
</section>
<section class=""content"">

    <div class=""ct-page-title"">
        <h1 class=""ct-title"" id=""content"">Concept Notes <span class=""text-muted text-sm"">/ Submitted Concept Notes By Category</span></h1>
        <hr class=""mt-0 mb-2"" />
    </div>


    <div class=""row justify-content-center"" style=""margin-bottom: 30px"">

        <div class=""col-md-12"">

            <div class=""card animated fadeInUp delay-1s"" style="""">

                <div class=""card-body"">
                    <div class="""" style=""margin-left: 3px !important; margin-bottom: 20px"">


                        <!-- Horizontal Form -->
                        <div class=""box box-danger"" style=""border-top-color: #fff; border-radius: 0px"">

                            <div class=""box-body"">
                                <div class=""table-responsive"">



                                    <table id=""modelDataTable"" class=""table table-bordered table-hover"">
                                        <thead>
     ");
            WriteLiteral(@"                                       <tr>
                                                <th>S/N</th>
                                                <th>
                                                    Principal Investigator(PI)
                                                </th>

                                                <th>
                                                    Concept Note Title
                                                </th>
                                                <th>
                                                    File-No
                                                </th>
                                                <th>
                                                    Category-(Thematic Area)
                                                </th>

                                                <th>
                                                    Assessor(s)
                                                </th>

          ");
            WriteLiteral("                                  </tr>\r\n                                        </thead>\r\n                                        <tbody>\r\n");
            EndContext();
#line 71 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\SubmittedConceptNoteByCategory.cshtml"
                                               int index = 0; 

#line default
#line hidden
            BeginContext(2726, 44, true);
            WriteLiteral("                                            ");
            EndContext();
#line 72 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\SubmittedConceptNoteByCategory.cshtml"
                                             foreach (var item in Model.PIAccessorSummary)
                                            {

                                                index += 1;

#line default
#line hidden
            BeginContext(2928, 110, true);
            WriteLiteral("                                                <tr>\r\n                                                    <td>");
            EndContext();
            BeginContext(3039, 5, false);
#line 77 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\SubmittedConceptNoteByCategory.cshtml"
                                                   Write(index);

#line default
#line hidden
            EndContext();
            BeginContext(3044, 121, true);
            WriteLiteral("</td>\r\n                                                    <td>\r\n                                                        ");
            EndContext();
            BeginContext(3166, 74, false);
#line 79 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\SubmittedConceptNoteByCategory.cshtml"
                                                   Write(Html.DisplayFor(modelItem => item.ApplicantThematic.Applicant.Person.Name));

#line default
#line hidden
            EndContext();
            BeginContext(3240, 175, true);
            WriteLiteral("\r\n                                                    </td>\r\n                                                    <td>\r\n                                                        ");
            EndContext();
            BeginContext(3416, 58, false);
#line 82 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\SubmittedConceptNoteByCategory.cshtml"
                                                   Write(Html.DisplayFor(modelItem => item.ApplicantThematic.Title));

#line default
#line hidden
            EndContext();
            BeginContext(3474, 175, true);
            WriteLiteral("\r\n                                                    </td>\r\n                                                    <td>\r\n                                                        ");
            EndContext();
            BeginContext(3650, 59, false);
#line 85 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\SubmittedConceptNoteByCategory.cshtml"
                                                   Write(Html.DisplayFor(modelItem => item.ApplicantThematic.FileNo));

#line default
#line hidden
            EndContext();
            BeginContext(3709, 175, true);
            WriteLiteral("\r\n                                                    </td>\r\n                                                    <td>\r\n                                                        ");
            EndContext();
            BeginContext(3885, 72, false);
#line 88 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\SubmittedConceptNoteByCategory.cshtml"
                                                   Write(Html.DisplayFor(modelItem => item.ApplicantThematic.Theme.Category.Name));

#line default
#line hidden
            EndContext();
            BeginContext(3957, 2, true);
            WriteLiteral("-(");
            EndContext();
            BeginContext(3960, 63, false);
#line 88 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\SubmittedConceptNoteByCategory.cshtml"
                                                                                                                              Write(Html.DisplayFor(modelItem => item.ApplicantThematic.Theme.Name));

#line default
#line hidden
            EndContext();
            BeginContext(4023, 120, true);
            WriteLiteral(")\r\n                                                    </td>\r\n                                                    <td>\r\n");
            EndContext();
#line 91 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\SubmittedConceptNoteByCategory.cshtml"
                                                         if (item.Reviewers.Count > 0)
                                                        {
                                                            for (int t = 0; t < item.Reviewers.Count; t++)
                                                            {
                                                                

#line default
#line hidden
            BeginContext(4526, 58, false);
#line 95 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\SubmittedConceptNoteByCategory.cshtml"
                                                           Write(Html.DisplayFor(modelItem => item.Reviewers[t].Title.Name));

#line default
#line hidden
            EndContext();
            BeginContext(4586, 52, false);
#line 95 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\SubmittedConceptNoteByCategory.cshtml"
                                                                                                                       Write(Html.DisplayFor(modelItem => item.Reviewers[t].Name));

#line default
#line hidden
            EndContext();
            BeginContext(4640, 144, true);
            WriteLiteral("                                                                <br />\r\n                                                                <hr />\r\n");
            EndContext();
#line 98 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\SubmittedConceptNoteByCategory.cshtml"



                                                            }

                                                        }

#line default
#line hidden
            BeginContext(4914, 122, true);
            WriteLiteral("\r\n\r\n\r\n                                                    </td>\r\n\r\n                                                </tr>\r\n");
            EndContext();
#line 110 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\SubmittedConceptNoteByCategory.cshtml"
                                            }

#line default
#line hidden
            BeginContext(5083, 582, true);
            WriteLiteral(@"
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=""box-footer"">
                        <div class=""row justify-content-between mt-3 px-4"">
                            <button onclick=""goBack()"" type=""button"" class=""save btn btn-outline-success"">Back</button>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>



</section>
");
            EndContext();
            BeginContext(5665, 70, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "2d715b072d3e8ed77771131aee6fa3898ce7953015744", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(5735, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(5754, 125, true);
                WriteLiteral("\r\n    <script>\r\n        //go back\r\n        function goBack() {\r\n            window.history.back()\r\n        }\r\n    </script>\r\n");
                EndContext();
            }
            );
            BeginContext(5882, 30, true);
            WriteLiteral("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Grant.Web.Models.ViewModels.AdminViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
