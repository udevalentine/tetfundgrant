#pragma checksum "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0850a3f64b7ee225673a02420b01fe15aab23df9"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_AdminHome_ViewReconciliation), @"mvc.1.0.view", @"/Views/AdminHome/ViewReconciliation.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/AdminHome/ViewReconciliation.cshtml", typeof(AspNetCore.Views_AdminHome_ViewReconciliation))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\_ViewImports.cshtml"
using Grant.Web;

#line default
#line hidden
#line 2 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\_ViewImports.cshtml"
using Grant.Web.Models;

#line default
#line hidden
#line 3 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\_ViewImports.cshtml"
using System.Security.Claims;

#line default
#line hidden
#line 2 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
using Grant.Web.Infrastructure;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0850a3f64b7ee225673a02420b01fe15aab23df9", @"/Views/AdminHome/ViewReconciliation.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ee57882324b1c76fbe9e059654765310563444e3", @"/Views/_ViewImports.cshtml")]
    public class Views_AdminHome_ViewReconciliation : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Grant.Web.Models.ViewModels.AdminViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Reconcile", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "AdminHome", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-primary float-right"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/theme/bower_components/jquery/dist/jquery.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 3 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
  
    ViewData["Title"] = "Reconciliation";
    Layout = "~/Views/Shared/_AdminLayout.cshtml";

#line default
#line hidden
            BeginContext(186, 92, true);
            WriteLiteral("\r\n<section class=\"content-header bd-top\" style=\"padding-top: 0; background-color: #fff\">\r\n\r\n");
            EndContext();
#line 10 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
     if (@TempData["Message"] != null)
    {

#line default
#line hidden
            BeginContext(325, 67, true);
            WriteLiteral("        <div class=\"callout \" style=\"color: white\">\r\n\r\n            ");
            EndContext();
            BeginContext(393, 45, false);
#line 14 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
       Write(Html.Partial("_Message", TempData["Message"]));

#line default
#line hidden
            EndContext();
            BeginContext(438, 20, true);
            WriteLiteral("\r\n\r\n        </div>\r\n");
            EndContext();
#line 17 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
    }

#line default
#line hidden
            BeginContext(465, 463, true);
            WriteLiteral(@"
</section>
<section class=""content"">

    <div class=""ct-page-title"">
        <h1 class=""ct-title"" id=""content"">Concept Notes <span class=""text-muted text-sm"">/ Reconciliation</span></h1>
        <hr class=""mt-0 mb-2"" />
    </div>


    <div class=""row justify-content-center"" style=""margin-bottom: 30px"">
        <div class=""col-md-12"">

            <div class=""card animated fadeInUp delay-1s"" style="""">
                <div class=""card-body"">
");
            EndContext();
#line 33 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
                     if (Model.Reconciliation?.Id > 0)
                    {

#line default
#line hidden
            BeginContext(1007, 946, true);
            WriteLiteral(@"                        <div class=""table-responsive"">
                            <table class=""table table-bordered table-hover"">
                                <thead>
                                    <tr>
                                        <th>
                                            Is Reconciled
                                        </th>
                                        <th>Reconcile average</th>
                                        <th>By</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            True
                                        </td>
                                        <td>
                                            ");
            EndContext();
            BeginContext(1954, 48, false);
#line 53 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
                                       Write(Model.Reconciliation.ReconcilliationAverageScore);

#line default
#line hidden
            EndContext();
            BeginContext(2002, 93, true);
            WriteLiteral("\r\n                                        </td>\r\n                                        <td>");
            EndContext();
            BeginContext(2096, 43, false);
#line 55 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
                                       Write(Model.Reconciliation.User.Reviewer.FullName);

#line default
#line hidden
            EndContext();
            BeginContext(2139, 51, true);
            WriteLiteral("</td>\r\n                                        <td>");
            EndContext();
            BeginContext(2191, 55, false);
#line 56 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
                                       Write(Model.Reconciliation.DateReconcilled.ToLongDateString());

#line default
#line hidden
            EndContext();
            BeginContext(2246, 162, true);
            WriteLiteral("</td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n");
            EndContext();
#line 61 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
                    }
                    else
                    {
                        var id = Utility.Encrypt(Model.ApplicantReviewerViewModel.FirstOrDefault().ApplicantThematic.Id.ToString());
                        var tid = Utility.Encrypt("1");

#line default
#line hidden
            BeginContext(2671, 24, true);
            WriteLiteral("                        ");
            EndContext();
            BeginContext(2695, 138, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0850a3f64b7ee225673a02420b01fe15aab23df910672", async() => {
                BeginContext(2820, 9, true);
                WriteLiteral("Reconcile");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 66 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
                                                                                                                  WriteLiteral(id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#line 66 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
                                                                                                                                    WriteLiteral(tid);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["tid"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-tid", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["tid"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2833, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 67 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
                    }

#line default
#line hidden
            BeginContext(2858, 2363, true);
            WriteLiteral(@"
                </div>
            </div>
        </div>
                <div class=""col-md-12"">

                    <div class=""card animated fadeInUp delay-1s"" style="""">
                        
                        <div class=""card-body"">
                            <div class="""" style=""margin-left: 3px !important; margin-bottom: 20px"">


                                <!-- Horizontal Form -->
                                <div class=""box box-danger"" style=""border-top-color: #fff; border-radius: 0px"">

                                    <div class=""box-body"">
                                        <div class=""table-responsive"">



                                            <table id=""modelDataTable"" class=""table table-bordered table-hover"">
                                                <thead>
                                                    <tr>
                                                        <th>S/N</th>
                                                     ");
            WriteLiteral(@"   <th>
                                                            Accessor Name
                                                        </th>
                                                        <th>
                                                            Phone No.
                                                        </th>
                                                        <th>
                                                            Email Address
                                                        </th>
                                                        <th>
                                                            Principal Investigator(PI)
                                                        </th>
                                                        <th>
                                                            Concept Note Title
                                                        </th>
                                                        <th>
 ");
            WriteLiteral(@"                                                           Total Score
                                                        </th>



                                                    </tr>
                                                </thead>
                                                <tbody>
");
            EndContext();
#line 116 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
                                                       int index = 0; 

#line default
#line hidden
            BeginContext(5294, 52, true);
            WriteLiteral("                                                    ");
            EndContext();
#line 117 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
                                                     foreach (var item in Model.ApplicantReviewerViewModel)
                                                    {

                                                        index += 1;

#line default
#line hidden
            BeginContext(5529, 118, true);
            WriteLiteral("                                                    <tr>\r\n                                                        <td>");
            EndContext();
            BeginContext(5648, 5, false);
#line 122 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
                                                       Write(index);

#line default
#line hidden
            EndContext();
            BeginContext(5653, 129, true);
            WriteLiteral("</td>\r\n                                                        <td>\r\n                                                            ");
            EndContext();
            BeginContext(5783, 48, false);
#line 124 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
                                                       Write(Html.DisplayFor(modelItem => item.Reviewer.Name));

#line default
#line hidden
            EndContext();
            BeginContext(5831, 187, true);
            WriteLiteral("\r\n                                                        </td>\r\n                                                        <td>\r\n                                                            ");
            EndContext();
            BeginContext(6019, 51, false);
#line 127 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
                                                       Write(Html.DisplayFor(modelItem => item.Reviewer.PhoneNo));

#line default
#line hidden
            EndContext();
            BeginContext(6070, 187, true);
            WriteLiteral("\r\n                                                        </td>\r\n                                                        <td>\r\n                                                            ");
            EndContext();
            BeginContext(6258, 49, false);
#line 130 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
                                                       Write(Html.DisplayFor(modelItem => item.Reviewer.Email));

#line default
#line hidden
            EndContext();
            BeginContext(6307, 187, true);
            WriteLiteral("\r\n                                                        </td>\r\n                                                        <td>\r\n                                                            ");
            EndContext();
            BeginContext(6495, 87, false);
#line 133 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
                                                       Write(Html.DisplayFor(modelItem => item.ApplicantThematic.Applicant.Person.ApplicantFullName));

#line default
#line hidden
            EndContext();
            BeginContext(6582, 187, true);
            WriteLiteral("\r\n                                                        </td>\r\n                                                        <td>\r\n                                                            ");
            EndContext();
            BeginContext(6770, 58, false);
#line 136 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
                                                       Write(Html.DisplayFor(modelItem => item.ApplicantThematic.Title));

#line default
#line hidden
            EndContext();
            BeginContext(6828, 187, true);
            WriteLiteral("\r\n                                                        </td>\r\n                                                        <td>\r\n                                                            ");
            EndContext();
            BeginContext(7016, 50, false);
#line 139 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
                                                       Write(Html.DisplayFor(modelItem => item.ConceptNoteMark));

#line default
#line hidden
            EndContext();
            BeginContext(7066, 128, true);
            WriteLiteral("\r\n                                                        </td>\r\n\r\n\r\n                                                    </tr>\r\n");
            EndContext();
#line 144 "C:\Users\udevalentine\Documents\Office Doc\New folder\tetfundgrant\Grant.Web\Views\AdminHome\ViewReconciliation.cshtml"
                                                    }

#line default
#line hidden
            BeginContext(7249, 694, true);
            WriteLiteral(@"
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=""box-footer"">
                                <div class=""row justify-content-between mt-3 px-4"">
                                    <button onclick=""goBack()"" type=""button"" class=""save btn btn-outline-success"">Back</button>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>



</section>
");
            EndContext();
            BeginContext(7943, 70, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0850a3f64b7ee225673a02420b01fe15aab23df923355", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(8013, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(8032, 125, true);
                WriteLiteral("\r\n    <script>\r\n        //go back\r\n        function goBack() {\r\n            window.history.back()\r\n        }\r\n    </script>\r\n");
                EndContext();
            }
            );
            BeginContext(8160, 30, true);
            WriteLiteral("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Grant.Web.Models.ViewModels.AdminViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
