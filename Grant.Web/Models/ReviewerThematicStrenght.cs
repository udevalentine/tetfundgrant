﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class ReviewerThematicStrenght
    {
        public int Id { get; set; }
        public int ReviewerId { get; set; }
        public int ThemeId { get; set; }
        public int Strenght { get; set; }
        public virtual Theme Theme { get; set; }
        public virtual Reviewer Reviewer { get; set; }
    }
}
