﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class ResearchCycleEvent
    {
        public int Id { get; set; }

        public int EventTypeId { get; set; }

        public virtual EventType EventType { get; set; }

        public int SessionId { get; set; }

        public virtual Session Session { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool Active { get; set; }
    }
}