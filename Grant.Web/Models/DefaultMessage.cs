﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class DefaultMessage
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public bool Active { get; set; }
        public bool ProposalApprove { get; set; }
        public bool isProposal { get; set; }
    }
}
