﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class Reviewer
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string PhoneNo { get; set; }
        public bool Active { get; set; }
        public int TitleId { get; set; }
        public int RoleId { get; set; }
        public virtual Role Role { get; set; }
        public virtual Title Title { get; set; }
        [DefaultValue(1)]
        public int InstitutionId { get; set; }
        public virtual Institution Institution { get; set; }
        [NotMapped]
        [Display(Name = "Name")]
        public string FullName
        {
            //get { return Title.Name + " " + Name; }
            get { return Title != null ? Title.Name + " " + Name : " " + " " + Name; }
            set { FullName = value; }
        }
        public virtual ICollection<ProposalScoring> ProposalScoring { get; set; }
        public virtual ICollection<ProposalAssessment> ProposalAssessment { get; set; }

    }
}
