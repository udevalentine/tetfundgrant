﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class GeneralAudit
    {
        public Int64 Id { get; set; }
        public string TableName { get; set; }
        public string InitialValue { get; set; }
        public string CurrentValue { get; set; }
        public string Operation { get; set; }
        public string Action { get; set; }
        public int UserId { get; set; }
        public DateTime ActionDate { get; set; }
        public string Client { get; set; }
        public virtual User User {get;set;}
    }
}
