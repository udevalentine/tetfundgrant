﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class Reconciliation
    {
        public int Id { get; set; }
        public int ApplicantThematicId { get; set; }
        public int UserId { get; set; }
        public DateTime DateReconcilled { get; set; }
        public virtual User User { get; set; }
        public virtual ApplicantThematic ApplicantThematic { get; set; }
        public decimal? ReconcilliationAverageScore { get; set; }
        public int Type { get; set; }
    }
}
