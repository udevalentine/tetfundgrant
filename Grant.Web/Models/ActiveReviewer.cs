﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class ActiveReviewer
    {
        public int Id { get; set; }
        public int SessionId { get; set; }
        public int ReviewerId { get; set; }
        public int Type { get; set; }
        public bool Active { get; set; }
        public virtual Session Session { get; set; }
        public virtual Reviewer Reviewer { get; set; }

    }
}
