﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class BulletinResponse
    {
        public long Id { get; set; }
        public long BulletinTopicId { get; set; }
        public int PersonId { get; set; }
        public virtual Person Person { get; set; }
        public string ResponseMessage { get; set; }
        public string Upload { get; set; }
        public string FileName { get; set; }
        public DateTime ResponseDate { get; set; }
        public bool Active { get; set; }
        public virtual BulletinTopic BulletinTopic { get; set; }
    }
}
