﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class CategoryAdministrator
    {
        public long Id { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public DateTime DateTime { get; set; }
        public bool Active { get; set; }
        public  int CategoryId { get; set; }
        public virtual Category Category { get; set; }
    }
}
