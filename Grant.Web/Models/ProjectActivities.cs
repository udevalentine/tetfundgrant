﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class ProjectActivities
    {
        public int Id { get; set; }
        public string Activity { get; set; }
        public string Outcome { get; set; }
        public int ApplicantThematicId { get; set; }
        public virtual ApplicantThematic ApplicantThematic { get; set; }


    }
}
