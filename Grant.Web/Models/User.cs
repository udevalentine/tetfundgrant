﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool Active { get; set; }
        public int RoleId { get; set; }
        public DateTime LastLogin { get; set; }
        public int ReviewerId { get; set; }
        public virtual Role Role { get; set; }
        public virtual Reviewer Reviewer { get; set; }

        public string OneTimeValidator { get; set; }
        public string Token { get; set; }
        public bool IsLoggedIn { get; set; }

        public virtual ICollection<FinalProposalAproval> FinalProposalAproval { get; set; }

    }

}
