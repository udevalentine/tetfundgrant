﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class Proposal
    {
        public int Id { get; set; }
        public string ProposalUrl { get; set; }
        public int ApplicantThemeId { get; set; }
        public decimal SubmittedBudget { get; set; }
        public bool Submitted { get; set; }
        [ForeignKey("ApplicantThemeId")]
        public virtual ApplicantThematic ApplicantThematic { get; set; }
        public virtual ICollection<ProposalAssessment> ProposalAssessment { get; set; }
    }

}
