﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class BudgetDetail
    {
        public int Id { get; set; }
        public int ApplicantThematicId { get; set; }
        public int BudgetItemId { get; set; }
        //public int FundingSourceId { get; set; }
        public decimal? TetFundAmount { get; set; }
        public decimal? InstitutionFundingAmount { get; set; }
        public decimal? OtherFunding { get; set; }
        public string Description { get; set; }
        public virtual BudgetItem BudgetItem { get; set; }
        //public virtual FundingSource FundingSource { get; set; }
        public int TypeId { get; set; }
        public virtual Type Type { get; set; }
        public decimal Total { get; set; }
        public virtual ApplicantThematic ApplicantThematic { get; set; }
    }
}
