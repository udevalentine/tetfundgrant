﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class Institution
    {
        public int Id { get; set; }
        public string Name { get; set;}
        public bool Active { get; set; }
        public bool IsPublic { get; set; }
        public int InstitutionCategoryId { get; set; }
        public virtual InstitutionCategory InstitutionCategory { get; set; }
        public virtual ICollection<Person> Person { get; set; }
        public int StateId { get; set; }
        public virtual State State { get; set; }

    }
}
