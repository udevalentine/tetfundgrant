﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class AdminSettings
    {
        public int Id { get; set; }
        public bool ViewReview { get; set; }
    }
}
