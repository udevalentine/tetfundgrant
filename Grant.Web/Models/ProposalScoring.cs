﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class ProposalScoring
    {
        public int Id { get; set; }
        public int ApplicantThematicId { get; set; }
        public int ReviewerId { get; set; }
        public int TypeId { get; set; }
        public int? TitleScore { get; set; }
        public int? ExecutiveSummaryScore { get; set; }
        public int? IntroductionBackgroundScore { get; set; }
        public int? AimsScore { get; set; }
        public int? StatementOfProblemScore { get; set; }
        public int? ConceptualFrameworkScore { get; set; }
        public int? ProjectGoalScore { get; set; }
        public int? ProjectImpactScore { get; set; }
        public int? LiteratureReviewScore { get; set; }
        public int? ResearchMethodolyScore { get; set; }
        public int? ProjectActivities { get; set; }
        public int? TimeFrameScore { get; set; }
        public int? ActivityIndicatorScore { get; set; }
        public int? StudyLocationScore { get; set; }
        public int? DataManagementScore { get; set; }
        public int? EthicalScore { get; set; }
        public int? MonitoringScore { get; set; }
        public int? DisseminationStrategiesScore { get; set; }
        public int? ResearchWorkToDateScore { get; set; }
        public int? PreviousGrantScore { get; set; }
        public int? GroupResearchScore { get; set; }
        public int? BudgetJustificationScore { get; set; }
        public int? AdditionalSourcesScore { get; set; }
        public DateTime DateReviewed { get; set; }
        public virtual ApplicantThematic ApplicantThematic { get; set; }
        public virtual Reviewer Reviewer { get; set; }
        public virtual ICollection<ProposalAssessment> ProposalAssessment { get; set; }
        public string Remark { get; set; }
        public bool IsSubmitted { get; set; }
    }
}
