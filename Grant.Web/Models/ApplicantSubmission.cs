﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class ApplicantSubmission
    {
        public int Id { get; set; }
        public int ApplicantThematicId { get; set; }
        public int TypeId { get; set; }
        public string Title { get; set; }
        public string ProposalTitle { get; set; }
        public string ExecutiveSummary { get; set; }
        public string IntroductionBackground { get; set; }
        public string Aims { get; set; }
        public string StatementOfProblem { get; set; }
        public string ConceptualFramework { get; set; }
        public string ProjectGoal { get; set; }
        public string ProjectImpact { get; set; }
        public string LiteratureReview { get; set; }
        public string ResearchMethodoly { get; set; }
        public string ProjectActivities { get; set; }
        public string TimeFrame { get; set; }
        public string ActivityIndicator { get; set; }
        public string StudyLocation { get; set; }
        public string DataManagement { get; set; }
        public string Ethical { get; set; }
        public string Monitoring { get; set; }
        public string DisseminationStrategies { get; set; }
        public string ResearchWorkToDate { get; set; }
        public string PreviousGrant { get; set; }
        public string GroupResearch { get; set; }
        public string BudgetJustification { get; set; }
        public string AdditionalSources { get; set; }
        public string ResearchQuestions { get; set; }
        public string ExpectedResult { get; set; }
        public string Innovations { get; set; }
        public string References { get; set; }
        public string TeamMates { get; set; }
        //public DateTime DateSubmited { get; set; }
        public virtual ApplicantThematic ApplicantThematic { get; set; }

    }
}