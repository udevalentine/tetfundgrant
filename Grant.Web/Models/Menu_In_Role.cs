﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class Menu_In_Role
    {
        public int Id { get; set; }

        public int RoleId { get; set; }

        public virtual Role Role { get; set; }

        public int MenuId { get; set; }

        public virtual Menu Menu { get; set; }
    }
}
