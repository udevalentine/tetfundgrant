﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class ApplicantReviewer
    {
        public int Id { get; set; }
        public int ReviewerId { get; set; }
        public int ApplicantThematicId { get; set; }
        public DateTime DateAssigned { get; set; }
        public int? ScoreId { get; set; }
        public DateTime? DateReviewed { get; set; }
        public decimal? ConceptNoteMark { get; set; }
        public int TypeId { get; set; }
        public virtual Type Type { get; set; }
        public virtual ICollection<Reviewer> Reviewer { get; set; }
        public virtual ApplicantThematic ApplicantThematic { get; set; }
        public bool IsReconciled { get; set; }

    }
}
