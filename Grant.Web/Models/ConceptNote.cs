﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class ConceptNote
    {
        public int Id { get; set; }
        public string NoteUrl { get; set; }
        public int ApplicantThemeId { get; set; }
        public decimal ConceptNoteSubmittedBudget { get; set; }
        [ForeignKey("ApplicantThemeId")]
        public virtual ApplicantThematic ApplicantThematic { get;set;}
        public string ConceptNoteSummary { get; set; }
        public bool Submitted { get; set; }
        public string AttachmentOne { get; set; }
        public string AttachmentTwo { get; set; }
        public string AttachmentThree { get; set; }
        public string AttachmentFour { get; set; }
        public string AttachmentFive { get; set; }
        public string NameOne { get; set; }
        public string NameTwo { get; set; }
        public string NameThree { get; set; }
        public string NameFour { get; set; }
        public string NameFive { get; set; }

    }
}
