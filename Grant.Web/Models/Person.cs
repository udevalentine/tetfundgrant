﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class Person
    {
        public int Id { get; set; }
        public string Email { get; set; }
        [Required]
        [Compare("Email", ErrorMessage = "Email did not match.")]
        [NotMapped]
        public string ConfirmEmail { get; set; }
        public string Name { get; set; }
        public string PhoneNo { get; set; }
        public DateTime DateCreated { get; set; }
        public int QualificationId { get; set; }
        public int InstitutionId { get; set; }
        public string PassportUrl { get; set; }
        public int TitleId { get; set; }
        public int SexId { get; set; }
        public int Age { get; set; }
        [DefaultValue("true")]
        public DateTime DOB { get; set; }


        public  int StateId { get; set; }
        public int RankId { get; set; }
        public virtual Qualification Qualification { get; set; }
        public virtual Institution Institution { get; set; }
        public virtual Title Title { get; set; }
        public virtual Sex Sex { get; set; }
        public virtual Rank Rank { get;set;}
        public virtual State State { get; set; }
        [NotMapped]
        [Display(Name = "Name")]
        public string ApplicantFullName
        {
            get { return Title!=null? Title.Name + " " + Name: " " + " " + Name; }
            set { ApplicantFullName = value; }
        }
        public virtual ICollection<BulletinTopic> BulletinTopic { get; set; }


    }
}
