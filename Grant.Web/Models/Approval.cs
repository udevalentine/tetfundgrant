﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class Approval
    {
        public int Id { get; set; }
        public int ApplicantThematicId { get; set; }
        public decimal AverageScore { get; set; }
        public bool Approved { get; set; }
        public int ReviewerId { get; set; }
        public bool IsProposal { get; set; }
        public virtual ICollection<ApplicantThematic> ApplicantThematic { get; set; }
        public virtual Reviewer Reviewer { get; set; }
    }
}
