﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class Applicant
    {
        public int Id { get; set; }
        public string ApplicationNo { get; set; }
        public int PersonId { get; set; }
        public int RoleId { get; set; }
        public DateTime LastLogin { get; set; }
        public bool Active { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password, ErrorMessage = "Password did not match.")]
        [Compare("Password")]
        [NotMapped]
        public string ConfirmPassword { get; set; }
        public bool IsVerified { get; set; }
        public DateTime? DateVerified { get; set; }
        public string OneTimeValidator { get; set; }
        public bool IsLoggedIn { get; set; }
        public string DeviceName { get; set; }
        public string DeviceMacAddress { get; set; }
        public virtual Role Role { get; set; }

        public virtual Person Person { get; set; }


    }
}
