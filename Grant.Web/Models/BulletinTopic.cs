﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class BulletinTopic
    {
        public long Id { get; set; }
        public string Topic { get; set; }
        public DateTime DateCreated { get; set; }
        public int PersonId { get; set; }
        public int ApplicantThematicId { get; set; }
        public virtual ApplicantThematic ApplicantThematic { get; set; }
        public virtual Person Person { get; set; }
        public bool Active { get; set; }
        

    }
}
