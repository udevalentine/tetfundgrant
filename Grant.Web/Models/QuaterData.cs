﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class QuaterData
    {
        public int Id { get; set; }
        public int QuarterId { get; set; }
        public string Year { get; set; }
        public int TimeFrameId { get; set; }
        public virtual TimeFrame TimeFrame { get; set; }


    }
}
