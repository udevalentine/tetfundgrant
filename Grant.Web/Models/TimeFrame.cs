﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class TimeFrame
    {
        public int Id { get; set; }
        public int ApplicantThematicId { get; set; }
        public string Name { get; set; }
        public string Year { get; set; }
        public string Duration { get; set; }
        public virtual ApplicantThematic ApplicantThematic { get; set; }
        public virtual ICollection<QuaterData> QuaterData { get; set; }




    }
}
