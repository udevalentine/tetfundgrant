﻿using Grant.Web.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class MenuDisplay: IMenuDisplay
    {
        private DataBaseContext _context;
        public MenuDisplay(DataBaseContext context)
        {
            _context = context;
        }
        public string GetUserRole(string userName,bool applicantSession)
        {

            string roleName = "";
            User user = new User();
            Applicant applicant = new Applicant();
            try
            {
                if (applicantSession)
                {
                    applicant = _context.Applicant.Where(d => d.ApplicationNo == userName).FirstOrDefault();
                    roleName = applicant.Role.Name;
                }
                else
                {
                    user = _context.User.Where(d => d.Email == userName).FirstOrDefault();
                    roleName = user.Role.Name;
                }
                
            }
            catch (Exception)
            {
                throw;
            }

            return roleName;
        }

        public List<Menu> GetMenuList(string role)
        {
            var menuList = new List<Menu>();
            try
            {

                var menuInRoleList = _context.MenuInRole.Where(r => r.Role.Name == role).ToList();

                for (int i = 0; i < menuInRoleList.Count; i++)
                {
                    Menu_In_Role thisMenuInRole = menuInRoleList[i];
                    menuList.Add(thisMenuInRole.Menu);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return menuList;
        }
    }
}
