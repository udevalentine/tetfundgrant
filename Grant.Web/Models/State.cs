﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class State
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public int GeoPoliticalZoneId { get; set; }
        public virtual GeoPoliticalZone GeoPoliticalZone { get; set; }
        public virtual ICollection<Person> Person { get; set; }


    }
}
