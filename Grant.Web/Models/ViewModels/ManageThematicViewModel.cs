﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models.ViewModels
{
    public class ManageThematicViewModel
    {
        public int ThematicId { get; set; }//pk

        public string Name { get; set; }

        public string Description { get; set; }

        public string Code { get; set; }

        [Display(Name = "Select Thematic Category")]
        public int CategoryId { get; set; }

        [Display(Name = "Category Name")]
        public string CategoryName { get; set; }

        [Display(Name = "Category Description")]
        public string CategoryDescription { get; set; }

        public List<SelectListItem> CategorySL { get; set; }

        public List<Theme> Themes { get; set; } = new List<Theme>();

        public List<Category> Categories { get; set; } = new List<Category>();
    }
}