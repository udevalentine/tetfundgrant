﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models.ViewModels
{
    public class EventWithoutStartDateViewModel
    {
        public string EventName { get; set; }

        public string EventStartDate { get; set; }

        public string EventEndDate { get; set; }
    }

    public class CountDownViewModel
    {
        public int Year { get; set; }

        public int Month { get; set; }

        public int Day { get; set; }

        public int Hour { get; set; }

        public int Second { get; set; }
        public int Minuite { get; set; }

        public string Type { get; set; }
    }
    public class SubmissionCount
    {
        public int ConceptNote { get; set; }
        public int Proposal { get; set; }
        public int ApprovedConceptNote { get; set; }
        public int ApprovedProposal { get; set; }
        public int NoOfAccessor { get; set; }
        public int NoOfAssessedConceptNote { get; set; }
        public int NoOfAssessedProposal { get; set; }
        public int PICount { get; set; }

    }
    public class ReviewDashboardModel
    {
        public ReviewerConceptNoteCount ReviewerConceptNoteCount { get; set; } = new ReviewerConceptNoteCount();
        public ReviewerProposalCount ReviewerProposalCount { get; set; } = new ReviewerProposalCount();
    }
    public class ReviewerConceptNoteCount
    {
        public int ConceptNote { get; set; }

        public int ApprovedConceptNote { get; set; }
        public int NoOfDiscodanceConceptnote { get; set; }
        public int NoOfAssessedConceptNote { get; set; }
        public int SavedConceptNoteAssessment { get; set; }


    }
    public class ReviewerProposalCount
    {
        public int Proposal { get; set; }
        public int ApprovedProposal { get; set; }
        public int NoOfDiscodanceProposal { get; set; }
        public int NoOfAssessedProposal { get; set; }
        public int SavedProposalAssessment { get; set; }

    }
}
