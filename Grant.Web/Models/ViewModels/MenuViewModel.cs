﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models.ViewModels
{
    public class MenuViewModel
    {
        private DataBaseContext _context;

        public void PopulateAllDropdown(DataBaseContext context)
        {
            _context = context;
            PopulateMenuGroupDropDownList();
            PopulateRoleDropdown();
            PopulateMenuDropdown();
            SetMenuList();

        }
        [Display(Name = ("Controller Name"))]
        public string ControllerName { get; set; }

        [Display(Name = ("Action Name"))]
        public string ActionName { get; set; }

        [Display(Name = "Display Name")]
        public string DisplayName { get; set; }

        [Display(Name = "Select Menu Group")]
        public int MenuGroupId { get; set; }

        [Display(Name = "Select Menu Group Name")]
        public string MenuGroupName { get; set; }

        public int RoleId { get; set; }

        [Display(Name = "Name Of Role")]
        public string RoleName { get; set; }

        public int MenuId { get; set; }

        public int MenuInRoleId { get; set; }

        [Display(Name = "Name of Menu")]
        public string MenuName { get; set; }

        public SelectList MenuGroupSL { get; set; }

        public SelectList MenuSL { get; set; }

        public SelectList RoleSL { get; set; }

        public void PopulateRoleDropdown(object selectedRole = null)
        {
            try
            {
                var roleQuery = from d in _context.Role.Where(a => a.Active == true)
                                orderby d.Name // Sort by Name.
                                select d;
                if(roleQuery != null)
                {
                    RoleSL = new SelectList(roleQuery.AsNoTracking(),
                           "Id", "Name", selectedRole);
                }
            }
            catch(Exception ex) { throw ex; }
        }

        public void PopulateMenuDropdown(object selectedMenu = null)
        {
            try
            {
                var menuQuery = from d in _context.Menu.Where(a => a.Active == true)
                                     orderby d.DisplayName + "-"+ d.MenuGroup.Name // Sort by displayName.
                                     select d;

                if (menuQuery != null)
                {
                     MenuSL = new SelectList(menuQuery.AsNoTracking(),
                            "Id", "DisplayName", selectedMenu);
                }
            }
            catch(Exception ex) { throw ex; }
        }

        public void PopulateMenuGroupDropDownList(object selectedMenuGroup = null)
        {
            var menuGroupQuery = from d in _context.MenuGroup.Where(a => a.Active == true)
                                orderby d.Name // Sort by name.
                                select d;

            if (menuGroupQuery != null)
            {
                MenuGroupSL = new SelectList(menuGroupQuery.AsNoTracking(),
                        "Id", "Name", selectedMenuGroup);
            }
        }

        public List<MenuListItem> Menu_List { get; set; } = new List<MenuListItem>();

        public bool ShowList { get; set; }
      
        public void SetMenuList()
        {
            try
            {

                var menus = _context.Menu.ToList();
                if (menus?.Count() > 0)
                {
                    Menu_List = new List<MenuListItem>();

                    int i = 0;
                    foreach (var item in menus)
                    {
                        i += 1;
                        var menu = new MenuListItem()
                        {
                            SerialNumber = i,
                            Id = item.Id,
                            ActionName = item.ActionName,
                            ControllerName=item.Controller,
                            DisplayName=item.DisplayName,
                            GroupName = item.MenuGroup.Name,
                            Status=item.Active
                        };

                        Menu_List.Add(menu);
                    }
                }


            }
            catch (Exception ex) { throw ex; }
        }
        public List<Menu_In_Role> MenuInRoles { get; set; }
    }

    public class MenuListItem
    {
        public int SerialNumber { get; set; }

        public int Id { get; set; }

        public string DisplayName { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        
        public string GroupName { get; set; }
        public bool Status { get; set; }
    }
}
