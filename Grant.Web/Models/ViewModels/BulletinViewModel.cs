﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models.ViewModels
{
    public class BulletinViewModel
    {
        private DataBaseContext _context;
        public void SetViewModelProperties(DataBaseContext context)
        {
            _context = context;

        }

        public Person Person { get; set; }
        public Applicant Applicant { get; set; }
        public BulletinTopic BulletinTopic { get; set; }
        public List<BulletinTopic> BulletinTopics { get; set; }
        public BulletinResponse BulletinResponse { get; set; }
        public List<BulletinResponse> BulletinResponses { get; set; }
        public List<BulletinTopicList> bulletinTopicLists { get; set; }
        public List<BulletinChatResponse> BulletinChatResponses { get; set; }
        public long ChatTopicId { get; set; }
        public void SetBulletinTopics(ApplicantThematic applicantThematic)
        {
            bulletinTopicLists = new List<BulletinTopicList>();
            var allActiveTopic=_context.BulletinTopic.Where(d => d.ApplicantThematicId == applicantThematic.Id && d.Active).ToList();
            int count = 0;
            if (allActiveTopic?.Count > 0)
            {
                count += 1;
                foreach(var item in allActiveTopic)
                {

                    var response=_context.BulletinResponse.Where(f => f.BulletinTopicId == item.Id).ToList();
                    BulletinTopicList bulletinTopicList = new BulletinTopicList();
                    bulletinTopicList.Author = item.Person.ApplicantFullName;
                    bulletinTopicList.Topic = item.Topic;
                    bulletinTopicList.SerialNumber = count;
                    bulletinTopicList.DateCreated = item.DateCreated.ToLongDateString();
                    bulletinTopicList.BulletinTopicId = item.Id;
                    bulletinTopicList.NumberOfResponse = response.Count();
                    bulletinTopicLists.Add(bulletinTopicList);

                }
            }

        }
        public void SetBulletinChatBoard(Person person, long topicId)
        {
            BulletinChatResponses = new List<BulletinChatResponse>();
            try
            {
               var responses = _context.BulletinResponse
                                       .Where(f => f.BulletinTopicId == topicId)
                                       .OrderBy(g => g.ResponseDate)
                                       .Select(g => new BulletinChatResponse()
                                       {
                                           Sender = g.Person.ApplicantFullName,
                                           Response = g.ResponseMessage,
                                           DateSent = g.ResponseDate.ToLongTimeString(),
                                           ActiveSender = g.PersonId == person.Id ? true : false,
                                           Topic = g.BulletinTopic.Topic,
                                           FilePath = g.Upload
                                       })
                                       .ToList();
                if (responses?.Count > 0)
                {
                    BulletinChatResponses.AddRange(responses);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
    public class BulletinTopicList
    {
        public string Author { get; set; }
        public string DateCreated { get; set; }
        public string Topic { get; set; }
        public int NumberOfResponse { get; set; }
        public int SerialNumber { get; set; }
        public long BulletinTopicId { get; set; }
    }
    public class BulletinChatResponse
    {
        public string Sender { get; set; }
        public string DateSent { get; set; }
        public string Response { get; set; }
        public bool ActiveSender { get; set; }
        public string Topic { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
    }
}
