﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models.ViewModels
{
    public class StatisticsViewModel
    {
        private DataBaseContext _context;

        public void PopulateAllDropdown(DataBaseContext context)
        {
            _context = context;
            PopulateSessionDropDownList();
            PopulateAllSessionDropDownList();
        }
        //Clasification for TETFund Members
        public List<StatisticsValueObj> MembersByStateOfOriginCount { get; set; } = new List<StatisticsValueObj>();

        public List<StatisticsValueObj> MembersByStateOfInstitutionCount { get; set; } = new List<StatisticsValueObj>();

        public List<StatisticsValueObj> GenderCount { get; set; } = new List<StatisticsValueObj>();

        public List<StatisticsValueObj> AgeCount { get; set; } = new List<StatisticsValueObj>();

        public List<StatisticsValueObj> GeoPoliticalZoneCount { get; set; } = new List<StatisticsValueObj>();

        public List<StatisticsValueObj> TypeOfGrantCount { get; set; } = new List<StatisticsValueObj>();

        public List<StatisticsValueObj> InstitutionAttendedCount { get; set; } = new List<StatisticsValueObj>();

        public List<StatisticsValueObj> TotalAndApprovedApplicantThematicCount { get; set; } = new List<StatisticsValueObj>();

        public List<StatisticsValueObj> ConceptNoteByThematicAreasCount { get; set; } = new List<StatisticsValueObj>();

        public List<StatisticsValueObj> TotalConceptCount { get; set; } = new List<StatisticsValueObj>();
        public List<SubmissionSummary> SubmissionSummary { get; set; }
        public SelectList SessionNameSL { get; set; }
        public SelectList AllSessionNameSL { get; set; }
        [Display(Name = "Select Research Cycle")]
        public int SessionId { get; set; }
        public string SessionName { get; set; }
        public void PopulateSessionDropDownList(object selectedSession = null)
        {
            var sessionQuery = from d in _context.Session.Where(f => f.Active)
                               orderby d.Name // Sort by name.
                               select d;

            SessionNameSL = new SelectList(sessionQuery.AsNoTracking(),
                        "Id", "Name", selectedSession);
        }
        public void PopulateAllSessionDropDownList(object selectedSession = null)
        {
            var sessionQuery = from d in _context.Session
                               orderby d.Name // Sort by name.
                               select d;

            AllSessionNameSL = new SelectList(sessionQuery.AsNoTracking(),
                        "Id", "Name", selectedSession);
        }
    }

    public class Value
    {
        public string HospitalName { get; set; }

        public int Count { get; set; }
    }

    public class StatisticsValueObj
    {
        public string Name { get; set; }
        public double Count { get; set; }
    }
    public class SubmissionSummary
    {
        public Category Category { get; set; }
        public List<ThematicArea> ThematicArea { get; set; }
        public int TotalCount { get; set; }
    }
    public class ThematicArea
    {
        public string ThemeName { get; set; }
        public int Count { get; set; }
    }
}