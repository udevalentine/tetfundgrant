﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models.ViewModels
{
    public class EventViewModel
    {
        private DataBaseContext _context;

        public void PopulateDropdown(DataBaseContext context)
        {
            _context = context;
        }

        [Display(Name = "Select Event Type")]
        public int EventTypeId { get; set; }

        [Display(Name = "Event Type Name")]
        public string EventTypeName { get; set; }

        public int ReserchCycleEventId { get; set; }

        [Display(Name = "Start Date")]
        public DateTime EventStartDate { get; set; }

        [Display(Name = "End Date")]
        public DateTime EventEndDate { get; set; }

        [Display(Name = "Select Research Cycle")]
        public int SessionId { get; set; }

        public List<SelectListItem> SessionList { get; set; }

        public List<SelectListItem> EventTypeList { get; set; }

        public List<ResearchCycleEvent> ResearchCycleEventList { get; set; }

        public List<EventType> EventTypes { get; set; }
        public DateTime StartDate { get; set; }
        [Display(Name = "Start Time")]
        public TimeSpan StartTime { get; set; }
        public DateTime EndDate { get; set; }
        [Display(Name = "End Time")]
        public TimeSpan EndTime { get; set; }
        public List<ResearchCycleEvent> SetResearchCycleEventList()
        {
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("W. Central Africa Standard Time");
            try
            {
                return _context.ResearchCycleEvent
                               .Include(r => r.Session)
                               .Include(r => r.EventType)
                               .Select(r => new ResearchCycleEvent() {
                                       EndDate = TimeZoneInfo.ConvertTimeFromUtc(r.EndDate,cstZone),
                                       StartDate = TimeZoneInfo.ConvertTimeFromUtc(r.StartDate, cstZone),
                                        EventTypeName = r.EventType.Name,
                                       SessionName = r.Session.Name,
                                       ResearchCycleEventId = r.Id,
                                       SessionId = r.Session.Id,
                                       Active = r.Active
                               })
                               .ToList();
            }
            catch (Exception ex) { throw ex; }
        }
    }

    public class ResearchCycleEvent
    {
        public int ResearchCycleEventId { get; set; }

        public int SessionId { get; set; }

        public string SessionName { get; set; }

        public string EventTypeName { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool Active { get; set; }
    }
}
