﻿using Grant.Web.Infrastructure;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models.ViewModels
{
    public class SetupViewModel
    {
        private DataBaseContext _context;

        public void PopulateAllDropdown(DataBaseContext context)
        {
            _context = context;
            PopulateCategoryDropDownList();
            PopulateCoAdministratorsDropDownList();


        }
        public List<Institution> Institutions { get; set; } = new List<Institution>();

        public List<Role> Roles { get; set; } = new List<Role>();

        public List<User> AdminUsers { get; set; } = new List<User>();

        public List<SelectListItem> StateSL { get; set; } = new List<SelectListItem>();

        public List<SelectListItem> RoleSL { get; set; } = new List<SelectListItem>();

        public List<SelectListItem> TitleSL { get; set; } = new List<SelectListItem>();

        public List<SelectListItem> InstitutionCategorySL { get; set; } = new List<SelectListItem>();

        [Display(Name = "Institution Name")]
        public string InstitutionName { get; set; }

        [Display(Name ="Is the Institution public?")]
        public bool IsPublic { get; set; }

        [Display(Name ="Select State")]
        public int StateId { get; set; }

        [Display(Name ="Select Institution Id")]
        public int InstitutionId { get; set; }

        [Display(Name = "Select Institution Category")]
        public int InstitutionCategoryId { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Phone Number")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [Display(Name = "Name")]
        public string FullName { get; set; }

        [Display(Name = "Select Role")]
        public int RoleId { get; set; }

        [Display(Name = "Select Title")]
        public int TitleId { get; set; }
        public CategoryAdministrator CategoryAdministrator { get; set; }
        public List<CategoryAdministrator> CategoryAdministrators { get; set; }
        public SelectList CategoryNameSL { get; set; }
        public SelectList CoAdminNameSL { get; set; }
        public int CategoryId { get; set; }
        public int CoAdminId { get; set; }
        public void PopulateCategoryDropDownList(object selectedCategory = null)
        {
            var categoryQuery = from d in _context.Category.Where(a => a.Active == true)
                                orderby d.Name // Sort by name.
                                select d;

            CategoryNameSL = new SelectList(categoryQuery.AsNoTracking(),
                        "Id", "Name", selectedCategory);
        }
        public void PopulateCoAdministratorsDropDownList(object selectedCoAdmin = null)
        {
            var coAdminQuery = from d in _context.Reviewer.Where(a => a.Active == true && a.RoleId==(int)RoleType.CoAdmin)
                                orderby d.Name // Sort by name.
                                select d;

            CoAdminNameSL = new SelectList(coAdminQuery.AsNoTracking(),
                        "Id", "Name", selectedCoAdmin);
        }
    }
}