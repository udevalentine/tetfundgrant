﻿using Grant.Web.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models.ViewModels
{
    public class UserViewModel
    {
        private DataBaseContext _context;
        

        public void PopulateAllDropdown(DataBaseContext context)
        {
            _context = context;
            PopulateCategoryDropDownList();
            PopulatePopulateThemeDropDownList();
            PopulatePopulateInstitutionDropDownList();
            PopulatePopulateQualificationDropDownList();
            PopulateTitleDropDownList();
            PopulateSexDropDownList();
            PopulateStateDropDownList();
            PopulateRankDropDownList();
            PopulateAllRankDropDownList();

        }


       
        public SelectList CategoryNameSL { get; set; }
        public SelectList ThemeNameSL { get; set; }
        public SelectList TitleNameSL { get; set; }
        public int TitleId { get; set; }
        public SelectList InstitutionCategoryNameSL { get; set; }
        public SelectList QualificationNameSL { get; set; }
        public int CategoryId { get; set; }
        public int InstitutionCategoryId { get; set; }
        
        public int InstitutionId { get; set; }
        public int ThemeId { get; set; }
        public Category Category { get; set; }
        public Person Person { get; set; }
        public ApplicantThematic ApplicantThematic { get; set; }
        public Theme Theme { get; set; }
        public Team Team { get; set; }
        public List<Person>TeamMember { get; set; }
        public ConceptNote ConceptNote { get; set; }
        public Proposal Proposal { get; set; }
        public List<ReviewerAssignedScore> ReviewerAssignedScoreList { get; set; }
        public ApplicantReviewer ApplicantReviewer { get; set; }
        public List<ApplicantReviewer> ApplicantReviewers { get; set; }
        public IFormFile UploadFile { get; set; }
        public string Title { get; set; }
        public Approval Approval { get; set; }
        public Message Message { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public Institution Institution { get; set; }
        public Qualification Qualification { get; set; }
        public int QualificationId { get; set; }
        public bool ProposalIsApproved { get; set; }
        public ApplicantSubmission ApplicantSubmission { get; set; }
        public List<Team> Teams { get; set; }
        public bool ViewReview { get; set; }
        public ProposalScoring ProposalScoring { get; set; }
        public string PassportUrl { get; set; }
        public string ConceptNoteSummary { get; set; }
        public List<BudgetDetail> BudgetDetails { get; set; }
        public List<BudgetDetail> BudgetDetailsSectionOne { get; set; }
        public List<BudgetDetail> BudgetDetailsSectionTwo { get; set; }
        public List<BudgetDetail> BudgetDetailsSectionThree { get; set; }
        public List<BudgetDetail> BudgetDetailsSectionFour { get; set; }
        public List<BudgetDetail> BudgetDetailsSectionFive { get; set; }
        public List<BudgetDetail> BudgetDetailsSectionSix { get; set; }
        public List<BudgetDetail> BudgetDetailsSectionSeven { get; set; }
        public List<Team> TeamMemberList { get; set; }
        public FinanceTable FinanceTable { get; set; }
        public decimal ProposedBudget { get; set; }
        public string OtherQualification { get; set; }
        public bool HideButton { get; set; }
        public string SectionPercentage1 { get; set; }
        public string SectionPercentage2 { get; set; }
        public string SectionPercentage3 { get; set; }
        public string SectionPercentage4 { get; set; }
        public string SectionPercentage5 { get; set; }
        public string SectionPercentage6 { get; set; }
        public string SectionPercentage7 { get; set; }
        public int SexId { get; set; }
        
        public SelectList SexNameSL { get; set; }
        public int StateId { get; set; }
        public SelectList StateNameSL { get; set; }
        public PICronologicalOrder PICronologicalOrder { get; set; }
        public SelectList RankNameSL { get; set; }
        public SelectList RankNameSLAll { get; set; }
        public List<ApplicantSubmission> ApplicantSubmissions { get; set; }
        public PITimeLine PITimeLine { get; set; }
        public Applicant Applicant { get; set; }
        public int ApplicantThematicId { get; set; }
        public string OtherRank { get; set; }
        //public ApplicationSubmission ApplicationSubmission { get; set; }
        public void PopulateCategoryDropDownList(object selectedCategory = null)
        {
            var categoryQuery = from d in _context.Category.Where(a => a.Active == true)
                                   orderby d.Name // Sort by name.
                                   select d;

            CategoryNameSL = new SelectList(categoryQuery.AsNoTracking(),
                        "Id", "Name", selectedCategory);
        }
        public void PopulatePopulateThemeDropDownList( object selectedTheme = null)
        {
            var themeQuery = from d in _context.Theme.Where(a => a.Active == true)
                             orderby d.Name // Sort by name.
                             select d;

            ThemeNameSL = new SelectList(themeQuery.AsNoTracking(),
                        "Id", "Name", selectedTheme);
        }
        public void PopulatePopulateInstitutionDropDownList(object selectedInstitution = null)
        {
            var institutionQuery = from d in _context.InstitutionCategory.Where(a => a.Active == true)
                             orderby d.Name // Sort by name.
                             select d;

            InstitutionCategoryNameSL = new SelectList(institutionQuery.AsNoTracking(),
                        "Id", "Name", selectedInstitution);
        }
        public void PopulatePopulateQualificationDropDownList(object selectedQualification = null)
        {
            var qualificationQuery = from d in _context.Qualification.Where(a => a.Active == true)
                                   orderby d.Name == "Others."  // Sort by name.
                                     select d;

            QualificationNameSL = new SelectList(qualificationQuery.AsNoTracking(),
                        "Id", "Name", selectedQualification);
        }
        public void PopulateTitleDropDownList(object selectedTitle = null)
        {
            var titleQuery = from d in _context.Title.Where(a => a.Active == true)
                                     orderby d.Name// Sort by name.
                                     select d;

            TitleNameSL = new SelectList(titleQuery.AsNoTracking(),
                        "Id", "Name", selectedTitle);
        }
        public void PopulateSexDropDownList(object selectedSex = null)
        {
            var sexQuery = from d in _context.Sex.Where(a => a.Active == true)
                           orderby d.Name // Sort by name.
                           select d;

            SexNameSL = new SelectList(sexQuery.AsNoTracking(),
                        "Id", "Name", selectedSex);
        }
        public void PopulateStateDropDownList(object selectedState = null)
        {
            var stateQuery = from d in _context.State.Where(a => a.Active == true)
                             orderby d.Name // Sort by name.
                             select d;

            StateNameSL = new SelectList(stateQuery.AsNoTracking(),
                        "Id", "Name", selectedState);
        }
        public void PopulateRankDropDownList(object selectedRank = null)
        {
            var rankQuery = from d in _context.Rank.Where(a => a.Active == true)
                            orderby d.Name // Sort by name.
                            select d;

            RankNameSL = new SelectList(rankQuery.AsNoTracking(),
                        "Id", "Name", selectedRank);
        }
        public void PopulateAllRankDropDownList(object selectedRank = null)
        {
            var rankQuery = from d in _context.Rank.Where(a => a.Active == true || a.Active==false)
                            orderby d.Name == "Others." // Sort by name.
                            select d;

            RankNameSLAll = new SelectList(rankQuery.AsNoTracking(),
                        "Id", "Name", selectedRank);
        }
    }
    public class ReviewerAssignedScore
    {
        public Reviewer Reviewer { get; set; }
        public string Score { get; set; }
    }
    public class JsonResultModel
    {
        public bool IsError { get; set; }
        public string Message { get; set; }
        public List<BudgetDetail> PersonnelBudgetDetails { get; set; }
        public ApplicationSubmission ApplicationSubmission { get; set; }
        public int ApplicantThematicId { get; set; }
        public string ConceptNoteApprove { get; set; }
        public string ProposalApprove { get; set; }
        public decimal Tetfund { get; set; }
        public decimal Institution { get; set; }
        public decimal Others { get; set; }
        public List<string> Zones { get; set; }
        public List<int> PICount { get; set; }
        public Session Session { get; set; }
        public List<BulletinChatResponse> BulletinChatResponses { get; set; }
        public string FileName { get; set; }
    }
    public class ApplicationSubmission
    {
        public string Title { get; set; }
        public string ExecutiveSummary { get; set; }
        public string IntroductionBackground { get; set; }
        public string Aims { get; set; }
        public string StatementOfProblem { get; set; }
        public string ConceptualFramework { get; set; }
        public string ProjectGoal { get; set; }
        public string ProjectImpact { get; set; }
        public string LiteratureReview { get; set; }
        public string ResearchMethodoly { get; set; }
        public string ProjectActivities { get; set; }
        public string TimeFrame { get; set; }
        public string ActivityIndicator { get; set; }
        public string StudyLocation { get; set; }
        public string DataManagement { get; set; }
        public string Ethical { get; set; }
        public string Monitoring { get; set; }
        public string DisseminationStrategies { get; set; }
        public string ResearchWorkToDate { get; set; }
        public string PreviousGrant { get; set; }
        public string GroupResearch { get; set; }
        public string BudgetJustification { get; set; }
        public string AdditionalSources { get; set; }
        public string ProposalTitle { get; set; }
        public string ResearchQuestions { get; set; }
        public string ExpectedResult { get; set; }
        public string Innovations { get; set; }
        public string References { get; set; }
        public string ResearchTeam { get; set; }


    }
    public class FinanceTable
    {
        public List<BudgetDetail> BudgetDetail { get; set; }
        public decimal Total { get; set; }
    }
    public class AttachmentUpload
    {
        public int applicantThematicId { get; set; }
        public string attachmentNo { get; set; }
        public IFormFile AttachmentFile { get; set; }
    }

}
