﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models.ViewModels
{
    public class UtilityViewModel
    {
        private DataBaseContext _context;
        public void PopulateAllDropdown(DataBaseContext context)
        {
            _context = context;
            PopulateUserDropDownList();
        }
        public SelectList UserSL { get; set; }
        public List<ApplicantThematic> ApplicantThematics { get; set; }
        public List<Applicant> Applicants { get; set; }
        public List<Reviewer> Reviewers { get; set; }
        public void PopulateUserDropDownList(object selectedTitle = null)
        {
            var userQuery = from d in _context.User.Where(a => a.Active == true)
                             orderby d.UserName // Sort by name.
                             select d;

            UserSL = new SelectList(userQuery.AsNoTracking(),
                        "Id", "UserName", selectedTitle);
        }
    }
}
