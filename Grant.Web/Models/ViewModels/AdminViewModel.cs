﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models.ViewModels
{
    public class AdminViewModel
    {
        private  DataBaseContext _context;

        public void PopulateAllDropdown(DataBaseContext context)
        {
            _context = context;
            PopulateCategoryDropDownList();
            //PopulatePopulateThemeDropDownList();
            PopulateReviewerDropDownList();
            PopulateTitleDropDownList();
            PopulateSessionDropDownList();
            PopulateInstitutionCategoryDropDownList();
            PopulateGeoZoneDropDownList();
            PopulateStateDropDownList();
            PopulateAllSessionDropDownList();
            PopulateRearchcycleReports();
        }

        public decimal TotalAllocatedAmountForThematicCategory { get; set; }
        public decimal UsedAmountFromAllocatedAmount { get; set; }

        public SelectList CategoryNameSL { get; set; }
        public SelectList ThemeNameSL { get; set; }
        public SelectList ReviewerNameSL { get; set; }
        public SelectList SessionNameSL { get; set; }
        public SelectList AllSessionNameSL { get; set; }
        public List<SelectListItem> SessionSL { get; set; }
        
        public SelectList ReviewerHighStrengthNameSL { get; set; }
        public int CategoryId { get; set; }
        public int ThemeId { get; set; }

        [Display(Name = "Select Research Cycle")]
        public int SessionId { get; set; }

        public Category Category { get; set; }
        public Person Person { get; set; }
        public ApplicantThematic ApplicantThematic { get; set; }

        public List<ApplicantThematic> ApplicantThematicList { get; set; } = new List<ApplicantThematic>();
        public List<Proposal> ProposalList { get; set; } = new List<Proposal>();

        public List<SelectListItem> UserGuideList { get; set; } = new List<SelectListItem>();

        public string UserGuideFile { get; set; }

        public Theme Theme { get; set; }
        public Team Team { get; set; }
        public List<Person> TeamMember { get; set; }
        public ConceptNote ConceptNote { get; set; }
        public Proposal Proposal { get; set; }
        public List<ReviewerAssignedScore> ReviewerAssignedScoreList { get; set; }
        public List<ApplicationList> ApplicationList { get; set; }
        public ApplicantReviewer ApplicantReviewer { get; set; }
        public List<ApplicantReviewer> ApplicantReviewers { get; set; }
        public List<Reviewer> AllReviewers { get; set; }
        public Reviewer Reviewer { get; set; }
        public IFormFile UploadFile { get; set; }
        public string Title { get; set; }
        public bool ShowPanel { get; set; }
        public int ReviewerId { get; set; }
        public int scoreId { get; set; }
        public decimal? mark {get;set;}
        public Message Message { get; set; }
        public Approval Approval { get; set; }
        public User User { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public List<ReviewerDetail> ReviewerDetails { get; set; }
        public List<AssessmentGuide> AssessmentGuides { get; set; }
        public List<ScoreAssessment> ScoreAssessments { get; set; }
        public decimal ApprovedBudget { get; set; }
        public decimal ProposedBudget { get; set; }
        public bool Approved { get; set; }
        public ProposalAssessment ProposalAssessment { get; set; }
        public List<ProposalAssessment> ProposalAssessments { get; set; }
        public List<BudgetSum> BudgetSums { get; set; }

        public List<ReasearchCycleReport> ReasearchCycleReportList { get; set; } = new List<ReasearchCycleReport>();
        public ApplicantSubmission ApplicantSubmission { get; set; }

        public ProposalScoring ProposalScoring { get; set; }
        public List<ReviewerThematicStrenght> ReviewerThematicStrenght { get; set; }
        public ReviewerThematicStrenght ThematicStrenght { get; set; }
        public int Strength { get; set; }
        public List<ScoreRanking> ScoreRankings { get; set; }
        public string Remark { get; set; }
        public string ReasonForRejection { get; set; }
        public int ApplicantCount { get; set; }
        public decimal AverageApprovedBudget { get; set; }
        public SelectList TitleNameSL { get; set; }
        public int TitleId { get; set; }
        
        public SelectList GeoZoneNameSL { get; set; }
        public int GeoPoliticalZoneId { get; set; }
        public SelectList InstitutionCategoryNameSL { get; set; }
        public int GInstitutionCategoryId { get; set; }
        public List<ApplicantThematic> ApplicantThematics { get; set; }
        public List<BudgetDetail> BudgetDetails { get; set; }
        public List<BudgetDetail> BudgetDetailsSectionOne { get; set; }
        public List<BudgetDetail> BudgetDetailsSectionTwo { get; set; }
        public List<BudgetDetail> BudgetDetailsSectionThree { get; set; }
        public List<BudgetDetail> BudgetDetailsSectionFour { get; set; }
        public List<BudgetDetail> BudgetDetailsSectionFive { get; set; }
        public List<BudgetDetail> BudgetDetailsSectionSix { get; set; }
        public List<BudgetDetail> BudgetDetailsSectionSeven { get; set; }
        public int ConceptNoteScore { get; set; }
        public string OtherQualification { get; set; }

        public FinanceTable FinanceTable { get; set; }
        public bool HideButton { get; set; }
        public string SectionPercentage1 { get; set; }
        public string SectionPercentage2 { get; set; }
        public string SectionPercentage3 { get; set; }
        public string SectionPercentage4 { get; set; }
        public string SectionPercentage5 { get; set; }
        public string SectionPercentage6 { get; set; }
        public string SectionPercentage7 { get; set; }
        public List<AssessmentReport> AssessmentReports { get; set; }
        public int budgetScore { get; set; }
        public List<Session> SessionList { get; set; }
        public List<DefaultMessage> DefaultMessageList { get; set; }
        public List<Title> TitleList { get; set; }
        public List<Institution> InstitutionList { get; set; }
        public Institution Institution { get; set; }
        public InstitutionCategory InstitutionCategory { get; set; }
        public GeoPoliticalZone GeoPoliticalZone { get; set; }
        public int StateId { get; set; }
        public SelectList StateNameSL { get; set; }
        public SelectList ExpertCategoryNameSL { get; set; }
        
        public State State { get; set; }
        public decimal TotalBudgetPerResearchCycle { get; set; }
        public bool isPublic { get; set; }
        public int InstitutionId { get; set; }
        public int InstitutionCategoryId { get; set; }
        public List<PrincipalInvestigator> PrincipalInvestigators { get; set; }
        public List<Team> TeamMembers { get; set; }
        public Applicant Applicant { get; set; }
        public List<ReviewerScore> ReviewerScores { get; set; }
        public CategoryAdministrator CategoryAdministrator { get; set; }
        public List<CategoryAdministrator> CategoryAdministrators { get; set; }
        public decimal Score { get; set; }
        public List<GroupSubmissionByTHematicArea> GroupSubmissionByTHematicAreas { get; set; }
        public List<ReadExcel> ReadExcelList { get; set; }
        public List<AccessorDistributionCount> AccessorDistributionCount { get; set; }
        public List<AccessorActivation> AccessorActivationList { get; set; }
        public List<PIAccessorSummary> PIAccessorSummary { get; set; }
        public List<ApplicantReviewerViewModel> ApplicantReviewerViewModel { get; set; }
        public List<Reconciliation> Reconciliations { get; set; }
        public Reconciliation Reconciliation { get; set; }
        public void PopulateCategoryDropDownList(object selectedCategory = null)
        {
            var categoryQuery = from d in _context.Category.Where(a => a.Active == true)
                                orderby d.Name // Sort by name.
                                select d;

            CategoryNameSL = new SelectList(categoryQuery.AsNoTracking(),
                        "Id", "Name", selectedCategory);
        }
        public void PopulatePopulateThemeDropDownList(object selectedTheme = null)
        {
            var themeQuery = from d in _context.Theme.Where(a => a.Active == true)
                             orderby d.Name // Sort by name.
                             select d;

            ThemeNameSL = new SelectList(themeQuery.AsNoTracking(),
                        "Id", "Name", selectedTheme);
        }
        public void PopulateReviewerDropDownList(object selectedReviewer = null)
        {
            var reviewerQuery = from d in _context.Reviewer.Where(g => g.Active && g.RoleId == 1)
                                orderby d.Name // Sort by name.
                                select d;

            ReviewerNameSL = new SelectList(reviewerQuery.AsNoTracking(),
                        "Id", "Name", selectedReviewer);
        }
        public void PopulateTitleDropDownList(object selectedTitle = null)
        {
            var titleQuery = from d in _context.Title.Where(a => a.Active == true)
                             orderby d.Name // Sort by name.
                             select d;

            TitleNameSL = new SelectList(titleQuery.AsNoTracking(),
                        "Id", "Name", selectedTitle);
        }
        public void PopulatePopulateThemeDropDownListByCategory(int CategoryId, object selectedTheme = null)
        {
            var themeQuery = from d in _context.Theme.Where(a => a.Active == true && a.CategoryId==CategoryId)
                             orderby d.Name // Sort by name.
                             select d;

            ThemeNameSL = new SelectList(themeQuery.AsNoTracking(),
                        "Id", "Name", selectedTheme);
        }
        public void PopulateSessionDropDownList(object selectedSession = null)
        {
            var sessionQuery = from d in _context.Session.Where(f=>f.Active)
                                orderby d.Name // Sort by name.
                                select d;

            SessionNameSL = new SelectList(sessionQuery.AsNoTracking(),
                        "Id", "Name", selectedSession);
        }
        public void PopulateAllSessionDropDownList(object selectedSession = null)
        {
            var sessionQuery = from d in _context.Session
                               orderby d.Name // Sort by name.
                               select d;

            AllSessionNameSL = new SelectList(sessionQuery.AsNoTracking(),
                        "Id", "Name", selectedSession);
        }

        public void PopulateGeoZoneDropDownList(object selectedGeoZone = null)
        {
            var geoZoneQuery = from d in _context.GeoPoliticalZone.Where(f => f.Active)
                               orderby d.Name // Sort by name.
                               select d;

            GeoZoneNameSL = new SelectList(geoZoneQuery.AsNoTracking(),
                        "Id", "Name", selectedGeoZone);
        }
        public void PopulateInstitutionCategoryDropDownList(object selectedInstitutionCategory = null)
        {
            var institutionCategoryQuery = from d in _context.InstitutionCategory.Where(f => f.Active)
                               orderby d.Name // Sort by name.
                               select d;

            InstitutionCategoryNameSL = new SelectList(institutionCategoryQuery.AsNoTracking(),
                        "Id", "Name", selectedInstitutionCategory);
        }
        public void PopulateStateDropDownList(object selectedState = null)
        {
            var stateQuery = from d in _context.State.Where(a => a.Active == true)
                             orderby d.Name // Sort by name.
                             select d;

            StateNameSL = new SelectList(stateQuery.AsNoTracking(),
                        "Id", "Name", selectedState);
        }
        public void PopulateReviewerExpertriateCategory(int CategoryId, object selectedCategory = null)
        {
            IQueryable<Category> categoryQuery;
            if (CategoryId > 0)
            {
                 categoryQuery = from d in _context.Category.Where(a => a.Active && a.Id == CategoryId)
                                 orderby d.Name // Sort by name.
                                 select d;
            }
            else
            {
                categoryQuery = from d in _context.Category.Where(a => a.Active)
                                 orderby d.Name // Sort by name.
                                 select d;
            }
            

            ExpertCategoryNameSL = new SelectList(categoryQuery.AsNoTracking(),
                        "Id", "Name", selectedCategory);
        }

        public void PopulateRearchcycleReports()
        {
            try
            {
                var researchCycles = _context.Session.Where(s => s.Active).ToList();
                if (researchCycles?.Count() > 0)
                {
                    foreach (var researchCycle in researchCycles)
                    {
                        var budget = _context.Budget.Where(s => s.SessionId == researchCycle.Id);
                        ReasearchCycleReport reasearchCycleReport = new ReasearchCycleReport();
                        reasearchCycleReport.ReaserchCycleName = researchCycle.Name;
                        reasearchCycleReport.ResearchCycleId = researchCycle.Id;
                        reasearchCycleReport.AmountAllocated = budget.Sum(s => s.Amount);
                        reasearchCycleReport.CategoryCountPerCycle = budget.Count();
                        reasearchCycleReport.ShowEdit = budget.Count() > 0 ? true : false;
                        ReasearchCycleReportList = new List<ReasearchCycleReport>();
                        ReasearchCycleReportList.Add(reasearchCycleReport);
                    }
                }
            }
            catch (Exception ex){ throw ex; }
        }

    }
    public class ApplicationList
    {
        public Person Person { get; set; }
        public ApplicantThematic ApplicantThematic { get; set; }
        public List<Reviewer> Reviewers { get; set; }
        public Category Category { get; set; }
        public Theme Theme { get; set; }
        public int ReviewerId { get; set; }
        public ApplicantReviewer ApplicantReviewer { get; set; }
        public decimal Average { get; set; }
        public List<ApplicantReviewer> ApplicantReviewers { get; set; }
        public List<ReviewerScore> ReviewerScores { get; set; }
        public Approval Approval { get; set; }
        public List<AssessmentGuide> AssessmentGuides { get; set; }
        public ScoreAssessment ScoreAssessment { get; set; }
        public List<ScoreAssessment> ScoreAssessments { get; set; }
        public decimal? ProposalScore { get; set; }
        public ApplicantSubmission ApplicantSubmission { get; set; }
        public ProposalScoring proposalScoring { get; set; }
        public SelectList ReviewerHighStrengthNameSL { get; set; }


    }
    public class ReviewerScore
    {
        public Reviewer Reviewer { get; set; }
        public string Score { get; set; }
        public string Mark { get; set; }
        public decimal proposalScore { get; set; }
        public decimal ProposedBudget { get; set; }
        public Person Person { get; set; }
        public ApplicantThematic ApplicantThematic { get; set; }
        public string Comment { get; set; }
    }
    public class ReviewerDetail
    {
        public Reviewer Reviewer { get; set; }
        public string Phone { get; set; }
        public int ApplicantCount { get; set; }
        public int ProposalCount { get; set; }
        public User User { get; set; }
        public Theme Theme { get; set; }
        public List<ReviewerThematicStrenght> ReviewerThematicStrenghts { get; set; }
    }
    public class ScoreAssessment
    {
        public AssessmentGuide AssessmentGuide { get; set; }
        public decimal Score { get; set; }
    }
    public class BudgetSum
    {
        public Category Category { get; set; }
        public decimal Amount { get; set; }
        public decimal? UsedAmount { get; set; }
        public decimal Balance { get; set; }
    }
    public class ScoreRanking
    {
        public decimal AverageScore { get; set; }
        public string CategoryName { get; set; }
        public string ThematicAreaName { get; set; }
        public int ReviewerCount { get; set; }
        public decimal AverageApprovedBudget { get; set; }
        public ApplicantThematic ApplicantThematic { get; set; }
        public bool iSApproved { get; set; }
        public string Title { get; set; }
    }
    public class ReconcilliationAssessment
    {
        public ApplicantThematic ApplicantThematic { get; set; }
        public Reviewer Reviewer { get; set; }
    }
    public class AssessmentReport
    {
        public ApplicantThematic ApplicantThematic { get; set; }
        public decimal AverageBudget { get; set; }
    }

    public class ReasearchCycleReport
    {
        public int ResearchCycleId { get; set; }

        public int CategoryCountPerCycle { get; set; }

        public decimal AmountAllocated { get; set; }

        public string ReaserchCycleName { get; set; }
        public bool ShowEdit { get; set; }
    }
    public class AssessorDropdown
    {
        public int ReviewerId { get; set; }
        public string Name { get; set; }
    }
    public class PrincipalInvestigator
    {
        public Applicant Applicant { get; set; }
        public ApplicantThematic ApplicantThematic { get; set; }
        public ApplicantReviewer ApplicantReviewer { get; set; }
        
    }
    public class AccessorDistributionModel
    {
        public int ReviewerId { get; set; }
        public int ReviewerStrenght { get; set; }
        public int ReviewerThemeId { get; set; }
        public int NoOfAssignment { get; set; }
        public Reviewer Reviewer { get; set; }
    }
    public class GroupSubmissionByTHematicArea
    {
        public int ThemeId { get; set; }
        public string ThematicName { get; set; }
        public int Count { get; set; }
        public int ReviewerId { get; set; }
    }
    public class ReadExcel
    {
        public int SerialNumber { get; set; }
        public string FullName { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string guid { get; set; }

    }
    public class FileNoText
    {
        public string TextFile { get; set; }
        public string FileNo { get; set; }
    }
    public class AccessorDistributionCount
    {
        public int TotalSubmission { get; set; }
        public int TotalAssigned { get; set; }
        public int TotalUnassigned { get; set; }
        public Category Category { get; set; }

    }
    public class AccessorActivation
    {
        public bool Active { get; set; }
        public Reviewer Reviewer { get; set; }
    }
    public class ReviewerJsonObj
    {
        public bool Status { get; set; }
        public int Id { get; set; }
    }
    public class PIAccessorSummary
    {
        public ApplicantThematic ApplicantThematic { get; set; }
        public List<Reviewer> Reviewers { get; set; }
    }
    public class AssigedCategory
    {
        public Category Category { get; set; }
        public bool IsAdmin { get; set; }
    }
    public class ApplicantReviewerViewModel
    {
        public int Id { get; set; }
        public DateTime DateAssigned { get; set; }
        public int? ScoreId { get; set; }
        public DateTime? DateReviewed { get; set; }
        public decimal? ConceptNoteMark { get; set; }
        public Type Type { get; set; }
        public Reviewer Reviewer { get; set; }
        public ApplicantThematic ApplicantThematic { get; set; }
    }

}

