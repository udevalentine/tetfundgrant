﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models.ViewModels
{
    public class RegistrationViewModel
    {
        private DataBaseContext _context;
        public void PopulateAllDropdown(DataBaseContext context)
        {
            _context = context;
            PopulatePopulateInstitutionDropDownList();
            PopulatePopulateQualificationDropDownList();
            PopulateTitleDropDownList();
            PopulateSexDropDownList();
            PopulateGeoPoliticalZoneDropDownList();
            PopulateStateDropDownList();
            PopulateRankDropDownList();

        }
        public Applicant Applicant { get; set; }
        public Person Person { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "New Password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        public string ConfirmNewPassword { get; set; }

        public string OneTimeValidator { get; set; }

        [Required]
        [DataType(DataType.Password,ErrorMessage = "Password did not match.")]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
        public SelectList InstitutionCategoryNameSL { get; set; }
        public SelectList QualificationNameSL { get; set; }
        public int InstitutionCategoryId { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "Your Email")]
        public string Email { get; set; }

        public int InstitutionId { get; set; }
        public int QualificationId { get; set; }
        public SelectList TitleNameSL { get; set; }
        public int StateId { get; set; }
        public SelectList StateNameSL { get; set; }
        public int RankId { get; set; }
        public SelectList RankNameSL { get; set; }
        public int GeoPoliticalZoneId { get; set; }
        public SelectList GeoPoliticalZoneNameSL { get; set; }
        public int SexId { get; set; }
        public SelectList SexNameSL { get; set; }
        public int TitleId { get; set; }
        public string OtherQualification { get; set; }
        public bool ShowModal { get; set; }
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string Othername { get; set; }
        public List<Applicant> Applicants { get; set; }
        public bool IsPI { get; set; }
        public int ReturnInstitutionId { get; set; }
        public User User { get; set; }

        public List<ResearcherLogin> ResearcherLogins { get; set; }
        public List<CoReseacherLoginDetail> CoReseacherLoginDetails { get; set; }
        public void PopulatePopulateInstitutionDropDownList(object selectedInstitution = null)
        {
            var institutionQuery = from d in _context.InstitutionCategory.Where(a => a.Active == true)
                                   orderby d.Name // Sort by name.
                                   select d;

            InstitutionCategoryNameSL = new SelectList(institutionQuery.AsNoTracking(),
                        "Id", "Name", selectedInstitution);
        }
        public void PopulatePopulateQualificationDropDownList(object selectedQualification = null)
        {
            var qualificationQuery = from d in _context.Qualification.Where(a => a.Active == true)
                                     orderby  d.Name == "Others." // Sort by name.
                                     select d;
            QualificationNameSL = new SelectList(qualificationQuery,
                        "Id", "Name", selectedQualification);
        }


        public void PopulateTitleDropDownList(object selectedTitle = null)
        {
            var titleQuery = from d in _context.Title.Where(a => a.Active == true)
                             orderby d.Name // Sort by name.
                             select d;

            TitleNameSL = new SelectList(titleQuery.AsNoTracking(),
                        "Id", "Name", selectedTitle);
        }
        public void PopulateStateDropDownList(object selectedState = null)
        {
            var stateQuery = from d in _context.State.Where(a => a.Active == true)
                             orderby d.Name // Sort by name.
                             select d;

            StateNameSL = new SelectList(stateQuery.AsNoTracking(),
                        "Id", "Name", selectedState);
        }
        public void PopulateRankDropDownList(object selectedRank = null)
        {
            var rankQuery = from d in _context.Rank.Where(a => a.Active == true)
                             orderby d.Name // Sort by name.
                             select d;

            RankNameSL = new SelectList(rankQuery.AsNoTracking(),
                        "Id", "Name", selectedRank);
        }
        public void PopulateGeoPoliticalZoneDropDownList(object selectedGeoPolicalZone = null)
        {
            var geoPoliticalZoneQuery = from d in _context.GeoPoliticalZone.Where(a => a.Active == true)
                             orderby d.Name // Sort by name.
                             select d;

            GeoPoliticalZoneNameSL = new SelectList(geoPoliticalZoneQuery.AsNoTracking(),
                        "Id", "Name", selectedGeoPolicalZone);
        }
        public void PopulateSexDropDownList(object selectedSex = null)
        {
            var sexQuery = from d in _context.Sex.Where(a => a.Active == true)
                             orderby d.Name // Sort by name.
                             select d;

            SexNameSL = new SelectList(sexQuery.AsNoTracking(),
                        "Id", "Name", selectedSex);
        }
    }
    public class CoReseacherLoginDetail
    {
        public long ApplicantId { get; set; }
        public long ApplicantThematicId { get; set; }
        public string Password { get; set; }
        public bool IsChecked { get; set; }
        public string Title { get; set; }
    }
    public class ResearcherLogin
    {
        public long ApplicantId { get; set; }
        public string Password { get; set; }
        public bool IsChecked { get; set; }
        public int RoleId { get; set; }
    }
    public class UserLogin
    {
        public string username { get; set; }
        public string password { get; set; }
    }
    public class UserModel
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
    }
}
