﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class ProposalAssessment
    {
        public int Id { get; set; }
        public int ApplicantThematicId { get; set; }
        public int ReviewerId { get; set; }
        //public int AssessmentGuideId { get; set; }
        public decimal? ApprovedBudget { get; set; }
        public bool Approved { get; set; }
        public decimal Score { get; set; }
        public int ProposalId { get; set; }
        public string ReasonForDissapproval { get; set; }
        public string Remark { get; set; }
        public int ProposalScoringId { get; set; }
        public virtual ProposalScoring ProposalScoring { get; set; }
        public virtual ApplicantThematic ApplicantThematic { get; set; }
        public virtual Reviewer Reviewer { get; set; }
        public virtual ICollection<AssessmentGuide> AssessmentGuide { get; set; }
        public virtual Proposal Proposal { get; set; }
        public bool IsReconcilled { get; set; }

    }
}
