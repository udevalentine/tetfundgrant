﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class AssessmentGuide
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal MaxScore { get; set; }
        public bool Active { get; set; }
    }
}
