﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class FinalProposalAproval
    {
        public int Id { get; set; }
        public decimal? ApprovedAmount { get; set; }
        public int ApplicantThematicId { get; set; }
        public int UserId { get; set; }
        public bool IsApproved { get; set; }
        public string Remark { get; set; }
        public virtual User User { get; set; }
        public virtual ApplicantThematic ApplicantThematic { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
