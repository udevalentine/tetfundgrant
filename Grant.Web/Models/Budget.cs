﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class Budget
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public int SessionId { get; set; }
        public int UserId { get; set; }
        public decimal Amount { get; set; }
        public decimal? AmountDispensed { get; set; }
        public virtual Category Category { get; set; }
        public virtual Session Session { get; set; }
        public virtual User User { get; set; }
    }
}
