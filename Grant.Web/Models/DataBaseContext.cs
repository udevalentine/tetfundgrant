﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class DataBaseContext : DbContext
    {
        public DataBaseContext(DbContextOptions<DataBaseContext> options) : base(options)
        {

        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(f => f.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            base.OnModelCreating(modelBuilder);
            //modelBuilder.Entity<Team>()
            //.HasOne<ApplicantThematic>(s => s.ApplicantThematic)
            //.WithMany(ad => ad.Teams).OnDelete(DeleteBehavior.Restrict);

            // modelBuilder.Entity<ProposalScoring>()
            // .HasOne<Reviewer>(s => s.Reviewer)
            // .WithMany(ad => ad.ProposalScoring).OnDelete(DeleteBehavior.Restrict);

            // modelBuilder.Entity<ProposalAssessment>()
            //.HasOne<Proposal>(s => s.Proposal)
            //.WithMany(ad => ad.ProposalAssessment)
            //.OnDelete(DeleteBehavior.Restrict);

            // modelBuilder.Entity<User>()
            //.HasOne<Role>(s => s.Role)
            //.WithMany(ad => ad.User)
            //.OnDelete(DeleteBehavior.Restrict);


            // modelBuilder.Entity<ProposalAssessment>()
            //.HasOne<ProposalScoring>(s => s.ProposalScoring)
            //.WithMany(ad => ad.ProposalAssessment)
            //.OnDelete(DeleteBehavior.Restrict);

            // modelBuilder.Entity<ProposalAssessment>()
            //.HasOne<Reviewer>(s => s.Reviewer)
            //.WithMany(ad => ad.ProposalAssessment)
            //.OnDelete(DeleteBehavior.Restrict);

            // modelBuilder.Entity<Reviewer>()
            //.HasOne<Title>(s => s.Title)
            //.WithMany(ad => ad.Reviewer)
            //.OnDelete(DeleteBehavior.Restrict);

            // modelBuilder.Entity<Person>()
            //.HasOne<Title>(s => s.Title)
            //.WithMany(ad => ad.Person)
            //.OnDelete(DeleteBehavior.Restrict);

            // modelBuilder.Entity<Person>()
            //.HasOne<State>(s => s.State)
            //.WithMany(ad => ad.Person)
            //.OnDelete(DeleteBehavior.Restrict);

            // modelBuilder.Entity<FinalProposalAproval>()
            //.HasOne<User>(s => s.User)
            //.WithMany(ad => ad.FinalProposalAproval)
            //.OnDelete(DeleteBehavior.Restrict);


            // modelBuilder.Entity<BulletinTopic>()
            // .HasOne(s => s.Applicant)
            // .WithMany(d => d.BulletinTopic).OnDelete(DeleteBehavior.Restrict);
            // modelBuilder.Entity<BulletinTopic>()
            // .HasOne(s => s.Person)
            // .WithMany(d => d.BulletinTopic).OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<User>().HasData(new User()
            {
                Id = 1,
                Active = true,
                Email = "Admin@gmail.com",
                Password = "1234567",
                ReviewerId = 1,
                RoleId = 6,
                LastLogin = DateTime.UtcNow,
                UserName = "Admin@gmail.com"

            },
            new User()
            {
                Id = 2,
                Active = true,
                Email = "SuperAdmin@gmail.com",
                Password = "1234567",
                ReviewerId = 2,
                RoleId = 2,
                LastLogin = DateTime.UtcNow,
                UserName = "SuperAdmin@gmail.com"

            }
            );
            modelBuilder.Entity<FundingSource>().HasData(
               new FundingSource() { Id = 1, Active = true, Name = "TetFund" },
               new FundingSource() { Id = 2, Active = true, Name = "Institution" },
               new FundingSource() { Id = 3, Active = true, Name = "Others" });

            modelBuilder.Entity<BudgetItem>().HasData(
               new BudgetItem() { Id = 1, Active = true, Name = "Personnel/Allowances" },
               new BudgetItem() { Id = 2, Active = true, Name = "Equipment(List and Specify)" },
               new BudgetItem() { Id = 3, Active = true, Name = "Supplies/Consumables" },
            new BudgetItem() { Id = 4, Active = true, Name = "Data Collection and Analysis" },
               new BudgetItem() { Id = 5, Active = true, Name = "Travels" },
               new BudgetItem() { Id = 6, Active = true, Name = "Dissemination" },
               new BudgetItem() { Id = 7, Active = true, Name = "Others/Misc" });

            modelBuilder.Entity<Role>().HasData(new Role()
            {
                Id = 1,
                Active = true,
                Name = "Reviewer",
            },
            new Role()
            {
                Id = 2,
                Active = true,
                Name = "Super-Admin",
            },
            new Role()
            {
                Id = 3,
                Active = true,
                Name = "Applicant",
            },
            new Role()
            {
                Id = 4,
                Active = true,
                Name = "Co-Applicant",
            },
             new Role()
             {
                 Id = 5,
                 Active = true,
                 Name = "Co-Admin",
             },
               new Role()
               {
                   Id = 6,
                   Active = true,
                   Name = "Admin",
               }
            );

            modelBuilder.Entity<Reviewer>().HasData(new Reviewer()
            {
                Id = 1,
                Email = "Admin@gmail.com",
                Name = "Tetfund ICT",
                RoleId = 6,
                Active = true,
                PhoneNo = "08067486937",
                TitleId=1,
                InstitutionId=1

            },
            new Reviewer()
            {
                Id = 2,
                Email = "SuperAdmin@gmail.com",
                Name = "Director Tetfund ICT",
                RoleId = 2,
                Active = true,
                PhoneNo = "09906784223",
                TitleId = 1,
                InstitutionId = 1

            }
            );

            modelBuilder.Entity<Category>().HasData(new Category()
            {
                Id = 1,
                Active = true,
                Name = "Humanities and Social Sciences (HSS)",
                Description = "Humanities and Social Sciences (HSS)",
                Code="HSS"
            },
            new Category()
            {
                Id = 2,
                Active = true,
                Name = "Science, Technology and Innovation (SETI)",
                Description = "Science, Technology and Innovation (SETI)",
                Code="SETI"
            },
            new Category()
            {
                Id = 3,
                Active = true,
                Name = "Cross-Cutting (CC)",
                Description = "Cross-Cutting (CC)",
                Code="CC"
            });

            modelBuilder.Entity<Theme>().HasData(new Theme()
            {
                Id = 1,
                Active = true,
                Name = "National Integration, Conflict, Defence and Security",
                CategoryId = 1,
                Description = "National Integration, Conflict, Defence and Security",
                Code = "CDS"

            },
            new Theme()
            {
                Id = 2,
                Active = true,
                Name = "Economic Development and Globalisation",
                CategoryId = 1,
                Description = "Economic Development and Globalisation",
                Code = "EDG"

            },

            new Theme()
            {
                Id = 3,
                Active = true,
                Name = "Resource Governance",
                CategoryId = 3,
                Description = "Resource Governance",
                Code = "REG"

            },
            new Theme()
            {
                Id = 4,
                Active = true,
                Name = "Entrepreneurship & Wealth Creation",
                CategoryId = 3,
                Description = "Entrepreneurship & Wealth Creation",
                Code = "EWC"

            },
             new Theme()
             {
                 Id = 5,
                 Active = true,
                 Name = "Humanities, Social Sciences, Technology and Business Interface",
                 CategoryId = 1,
                 Description = "Humanities, Social Sciences, Technology and Business Interface",
                 Code = "HST"

             },
              new Theme()
              {
                  Id = 6,
                  Active = true,
                  Name = "Languages, Literatures and Media",
                  CategoryId = 1,
                  Description = "Languages, Literatures and Media",
                  Code = "LLM"
              },
               new Theme()
               {
                   Id = 7,
                   Active = true,
                   Name = "History, Culture and Identities",
                   CategoryId = 1,
                   Description = "History, Culture and Identities",
                   Code = "HCI"
               },
                new Theme()
                {
                    Id = 8,
                    Active = true,
                    Name = "Population and Migration",
                    CategoryId = 1,
                    Description = "Population and Migration",
                    Code = "PAM"
                },

                 new Theme()
                 {
                     Id = 9,
                     Active = true,
                     Name = "Tourism, Sports and Recreation",
                     CategoryId = 1,
                     Description = "Tourism, Sports and Recreation",
                     Code = "TSR"
                 },
                  new Theme()
                  {
                      Id = 10,
                      Active = true,
                      Name = "Education and Human Capital",
                      CategoryId = 1,
                      Description = "Education and Human Capital",
                      Code = "EHC"
                  },

                   new Theme()
                   {
                       Id = 11,
                       Active = true,
                       Name = "Water & Sanitation",
                       CategoryId = 2,
                       Description = "Water & Sanitation",
                       Code = "WAS"
                   },
                   new Theme()
                   {
                       Id = 12,
                       Active = true,
                       Name = "Geosciences",
                       CategoryId = 2,
                       Description = "Geosciences",
                       Code = "GEO"
                   },
                   new Theme()
                   {
                       Id = 13,
                       Active = true,
                       Name = "Space Science and Technology",
                       CategoryId = 2,
                       Description = "Space Science and Technology",
                       Code = "SST"
                   },
                   new Theme()
                   {
                       Id = 14,
                       Active = true,
                       Name = "Power and Energy",
                       CategoryId = 2,
                       Description = "Power and Energy",
                       Code = "PAE"
                   },
                    new Theme()
                    {
                        Id = 15,
                        Active = true,
                        Name = "Transport",
                        CategoryId = 2,
                        Description = "Transport",
                        Code = "TRT"
                    },
                     new Theme()
                     {
                         Id = 16,
                         Active = true,
                         Name = "Health and Social Welfare",
                         CategoryId = 2,
                         Description = "Health and Social Welfare",
                         Code = "HSW"
                     },
                     new Theme()
                     {
                         Id = 17,
                         Active = true,
                         Name = "IT, Computing & Telecommunications",
                         CategoryId = 2,
                         Description = "IT, Computing & Telecommunications",
                         Code = "ICT"
                     },
                     new Theme()
                     {
                         Id = 18,
                         Active = true,
                         Name = "Blue Economy",
                         CategoryId = 3,
                         Description = "Blue Economy",
                         Code = "BEC"
                     },
                     new Theme()
                     {
                         Id = 19,
                         Active = true,
                         Name = "Science,Technology and  Innovation System Management",
                         CategoryId = 3,
                         Description = "Science,Technology and  Innovation System Management",
                         Code = "STI"
                     },
                      new Theme()
                      {
                          Id = 20,
                          Active = true,
                          Name = "Clean and Affordable Energy",
                          CategoryId = 3,
                          Description = "Clean and Affordable Energy",
                          Code = "CAE"
                      },
                      new Theme()
                      {
                          Id = 21,
                          Active = true,
                          Name = "Environment, Housing & Urban and Regional Development",
                          CategoryId = 3,
                          Description = "Environment, Housing & Urban and Regional Development",
                          Code = "EHU"
                      },
                    new Theme()
                    {
                        Id = 22,
                        Active = true,
                        Name = "Science & Engineering",
                        CategoryId = 2,
                        Description = "Science & Engineering",
                        Code = "SAE"
                    },
                 new Theme()
                 {
                     Id = 23,
                     Active = true,
                     Name = "Governance, Politics, Law and Ethics",
                     CategoryId = 1,
                     Description = "Governance, Politics, Law and Ethics",
                     Code = "GPL"

                 },
                 new Theme()
                 {
                     Id = 24,
                     Active = true,
                     Name = "Gender, Equity and Social Inclusion",
                     CategoryId = 1,
                     Description = "Gender, Equity and Social Inclusion",
                     Code = "GES"
                 },
                  new Theme()
                  {
                      Id = 25,
                      Active = true,
                      Name = "Social Development and Welfare",
                      CategoryId = 1,
                      Description = "Social Development and Welfare",
                      Code = "SDW"
                  },
                 new Theme()
                 {
                     Id = 26,
                     Active = true,
                     Name = "Agriculture and Food Security",
                     CategoryId = 2,
                     Description = "Agriculture and Food Security",
                     Code = "AFS"
                 },
                  new Theme()
                  {
                      Id = 27,
                      Active = true,
                      Name = "Industry, Innovation and Infrastructure",
                      CategoryId = 2,
                      Description = "Industry, Innovation and Infrastructure",
                      Code = "III"
                  },
                  new Theme()
                  {
                      Id = 28,
                      Active = true,
                      Name = "Sustainable Use of Natural Resources and Terrestrial Ecosystems",
                      CategoryId = 2,
                      Description = "Sustainable Use of Natural Resources and Terrestrial Ecosystems",
                      Code = "NRT"
                  }

             );
            modelBuilder.Entity<Score>().HasData(
                new Score() { Id = 1, Active = true, Name = "Pass" },
                new Score() { Id = 2, Active = true, Name = "Fail" },
                new Score() { Id = 3, Active = true, Name = "To Be Reconciled" });
            modelBuilder.Entity<Institution>().HasData(
                            new Institution() { Id = 1, Active = true, Name = "University of Abuja", InstitutionCategoryId = 1, IsPublic = true, StateId = 15 },
                            new Institution() { Id = 2, Active = true, Name = "University of Nigeria,Nsukka", InstitutionCategoryId = 1, IsPublic = true, StateId = 14 },
                            new Institution() { Id = 3, Active = true, Name = "University of Portharcourt", InstitutionCategoryId = 1, IsPublic = true, StateId = 33 },
                        new Institution() { Id = 4, Active = true, Name = "Federal Polytechnic Ilaro", InstitutionCategoryId = 2, IsPublic = true, StateId = 28 },
                        new Institution() { Id = 5, Active = true, Name = "Federal Polytechnic Nekede", InstitutionCategoryId = 2, IsPublic = true, StateId = 17 },
                        new Institution() { Id = 6, Active = true, Name = "Federal Polytechnic, Oko", InstitutionCategoryId = 2, IsPublic = true, StateId = 4 },
                        new Institution() { Id = 7, Active = true, Name = "Federal Polytechnic, Ado", InstitutionCategoryId = 2, IsPublic = true, StateId = 13 },
                        new Institution() { Id = 8, Active = true, Name = "Rivers State University", InstitutionCategoryId = 1, IsPublic = true, StateId = 33 },
                        new Institution() { Id = 9, Active = true, Name = "Federal University Otuoke", InstitutionCategoryId = 1, IsPublic = true, StateId = 6 },
                        new Institution() { Id = 10, Active = true, Name = "Micheal Okpara University", InstitutionCategoryId = 1, IsPublic = true, StateId = 1 },
                        new Institution() { Id = 11, Active = true, Name = "Badcock University", InstitutionCategoryId = 1, IsPublic = false, StateId = 28 },
                        new Institution() { Id = 12, Active = true, Name = "Benson Idahosa University", InstitutionCategoryId = 1, IsPublic = false, StateId = 12 },
                        new Institution() { Id = 13, Active = true, Name = "Adeleke University", InstitutionCategoryId = 1, IsPublic = false, StateId = 30 },
                        new Institution() { Id = 14, Active = true, Name = "Caritas University", InstitutionCategoryId = 1, IsPublic = false, StateId = 14 },
                        new Institution() { Id = 15, Active = true, Name = "Elizade University", InstitutionCategoryId = 1, IsPublic = false, StateId = 29 },
                        new Institution() { Id = 16, Active = true, Name = "Federal University of Agricultutre, Abeokuta", InstitutionCategoryId = 1, IsPublic = true, StateId = 28 },
                        new Institution() { Id = 17, Active = true, Name = "Adamawa State Polytechnic", InstitutionCategoryId = 2, IsPublic = true, StateId = 2 },
                        new Institution() { Id = 18, Active = true, Name = "Federal Polytechnic, Mubi", InstitutionCategoryId = 2, IsPublic = true, StateId = 2 },
                        new Institution() { Id = 19, Active = true, Name = "Federal Polytechnic,Bauchi", InstitutionCategoryId = 2, IsPublic = true, StateId = 5 },
                        new Institution() { Id = 20, Active = true, Name = "Akwa-Ibom State Polytechnic", InstitutionCategoryId = 2, IsPublic = true, StateId = 3 },
                        new Institution() { Id = 21, Active = true, Name = "Benue State Polytechnic", InstitutionCategoryId = 2, IsPublic = true, StateId = 7 },
                        new Institution() { Id = 22, Active = true, Name = "Ramata Polytechnic", InstitutionCategoryId = 2, IsPublic = false, StateId = 8 },
                        new Institution() { Id = 23, Active = true, Name = "The Polytechnic,Calabar", InstitutionCategoryId = 2, IsPublic = true, StateId = 9 },
                        new Institution() { Id = 24, Active = true, Name = "Delta State Polytechnic", InstitutionCategoryId = 2, IsPublic = true, StateId = 10 },
                        new Institution() { Id = 25, Active = true, Name = "Imo State Polytechnic", InstitutionCategoryId = 2, IsPublic = true, StateId = 17 },
                        new Institution() { Id = 26, Active = true, Name = "Temple Gate Polytechnic", InstitutionCategoryId = 2, IsPublic = false, StateId = 1 },
                        new Institution() { Id = 27, Active = true, Name = "Federal College of Education, Oyo ", InstitutionCategoryId = 3, IsPublic = true, StateId = 31 },
                        new Institution() { Id = 28, Active = true, Name = "Federal College of Education, Asaba ", InstitutionCategoryId = 3, IsPublic = true, StateId = 10 },
                        new Institution() { Id = 29, Active = true, Name = "Federal College of Education, Kano ", InstitutionCategoryId = 3, IsPublic = true, StateId = 20 },
                        new Institution() { Id = 30, Active = true, Name = "Federal College of Education, Eha-Amufu ", InstitutionCategoryId = 3, IsPublic = true, StateId = 14 },
                        new Institution() { Id = 31, Active = true, Name = "Federal College of Education, Okene ", InstitutionCategoryId = 3, IsPublic = true, StateId = 23 },
                        new Institution() { Id = 32, Active = true, Name = "Federal College of Education, Zaria ", InstitutionCategoryId = 3, IsPublic = true, StateId = 19 },
                        new Institution() { Id = 33, Active = true, Name = "Piaget College of Education, Abeokuta ", InstitutionCategoryId = 3, IsPublic = false, StateId = 28 },
                        new Institution() { Id = 34, Active = true, Name = "Our Saviour Institute of Science and Technology, Enugu ", InstitutionCategoryId = 3, IsPublic = false, StateId = 14 },
                        new Institution() { Id = 35, Active = true, Name = "Federal University of Petroleum, Resources", InstitutionCategoryId = 1, IsPublic = true, StateId = 10 },
                        new Institution() { Id = 36, Active = true, Name = "Ahmadu Bello University,zaria	", InstitutionCategoryId = 1, IsPublic = true, StateId = 19 },
                        new Institution() { Id = 37, Active = true, Name = "University of Lagos,Lagos ", InstitutionCategoryId = 1, IsPublic = true, StateId = 25 },
                        new Institution() { Id = 38, Active = true, Name = "University of Ibadan,Ibadan ", InstitutionCategoryId = 1, IsPublic = true, StateId = 31 },
                        new Institution() { Id = 39, Active = true, Name = "Obafemi Awolowo University,Ile-Ife ", InstitutionCategoryId = 1, IsPublic = true, StateId = 30 },
                        new Institution() { Id = 40, Active = true, Name = "University of Ilorin,Ilorin", InstitutionCategoryId = 1, IsPublic = true, StateId = 24 },
                        new Institution() { Id = 41, Active = true, Name = "Federal University of Technology, Akure", InstitutionCategoryId = 1, IsPublic = true, StateId = 29 },
                        new Institution() { Id = 42, Active = true, Name = "University of Jos, Jos", InstitutionCategoryId = 1, IsPublic = true, StateId = 32 },
                        new Institution() { Id = 43, Active = true, Name = "University of Benin, Benin City ", InstitutionCategoryId = 1, IsPublic = true, StateId = 12 },
                        new Institution() { Id = 44, Active = true, Name = "Federal University, Oye-Ekiti", InstitutionCategoryId = 1, IsPublic = true, StateId = 13 },
                        new Institution() { Id = 45, Active = true, Name = "Olabisi Onabanjo University, Ago Iwoye ", InstitutionCategoryId = 1, IsPublic = true, StateId = 28 },
                        new Institution() { Id = 46, Active = true, Name = "Lagos State University, Ojo ", InstitutionCategoryId = 1, IsPublic = true, StateId = 25 },
                        new Institution() { Id = 47, Active = true, Name = "Ladoke Akintola University of Technology, Ogbomoso ", InstitutionCategoryId = 1, IsPublic = true, StateId = 31 },
                        new Institution() { Id = 48, Active = true, Name = "Rivers State University, Port Harcourt ", InstitutionCategoryId = 1, IsPublic = true, StateId = 33 },
                        new Institution() { Id = 49, Active = true, Name = "Federal University of Technology, Owerri", InstitutionCategoryId = 1, IsPublic = true, StateId = 17 },
                        new Institution() { Id = 50, Active = true, Name = "Federal University of Technology, Minna", InstitutionCategoryId = 1, IsPublic = true, StateId = 27 },
                        new Institution() { Id = 51, Active = true, Name = "Abubakar Tafawa Balewa University,Bauchi ", InstitutionCategoryId = 1, IsPublic = true, StateId = 5 },
                        new Institution() { Id = 52, Active = true, Name = "Kwara State University, Ilorin ", InstitutionCategoryId = 1, IsPublic = true, StateId = 24 },
                        new Institution() { Id = 53, Active = true, Name = "Bayero University Kano, Kano ", InstitutionCategoryId = 1, IsPublic = true, StateId = 20 },
                        new Institution() { Id = 54, Active = true, Name = "Ekiti State University, Ado Ekiti ", InstitutionCategoryId = 1, IsPublic = true, StateId = 13 },
                        new Institution() { Id = 55, Active = true, Name = "University of Abuja,Abuja ", InstitutionCategoryId = 1, IsPublic = true, StateId = 15 },
                        new Institution() { Id = 56, Active = true, Name = "Adekunle Ajasin University, Akungba Akoko ", InstitutionCategoryId = 1, IsPublic = true, StateId = 29 },
                        new Institution() { Id = 57, Active = true, Name = "University of Uyo, Uyo ", InstitutionCategoryId = 1, IsPublic = true, StateId = 3 },
                        new Institution() { Id = 58, Active = true, Name = "Tai Solarin University of Education, Ijebu-Ode ", InstitutionCategoryId = 1, IsPublic = true, StateId = 28 },
                        new Institution() { Id = 59, Active = true, Name = "Benue State University, Makurdi ", InstitutionCategoryId = 1, IsPublic = true, StateId = 7 },
                        new Institution() { Id = 60, Active = true, Name = "Nnamdi Azikiwe University, Awka ", InstitutionCategoryId = 1, IsPublic = true, StateId = 4 },
                        new Institution() { Id = 61, Active = true, Name = "Usmanu Danfodio University, Sokoto ", InstitutionCategoryId = 1, IsPublic = true, StateId = 34 },
                        new Institution() { Id = 62, Active = true, Name = "Umaru Musa Yar'Adua University, Katsina ", InstitutionCategoryId = 1, IsPublic = true, StateId = 21 },
                        new Institution() { Id = 63, Active = true, Name = "Alex Ekwueme Federal University, Ndufu-Alike", InstitutionCategoryId = 1, IsPublic = true, StateId = 11 },
                        new Institution() { Id = 64, Active = true, Name = "Michael Okpara University of Agriculture, Umuahia ", InstitutionCategoryId = 1, IsPublic = true, StateId = 1 },
                        new Institution() { Id = 65, Active = true, Name = "Nasarawa State University, Keffi ", InstitutionCategoryId = 1, IsPublic = true, StateId = 26 },
                        new Institution() { Id = 66, Active = true, Name = "University of Maiduguri, Maiduguri ", InstitutionCategoryId = 1, IsPublic = true, StateId = 8 },
                        new Institution() { Id = 67, Active = true, Name = "University of Agriculture, Makurdi", InstitutionCategoryId = 1, IsPublic = true, StateId = 7 },
                        new Institution() { Id = 68, Active = true, Name = "Osun State University, Oshogbo ", InstitutionCategoryId = 1, IsPublic = true, StateId = 30 },
                        new Institution() { Id = 69, Active = true, Name = "Abia State University, Uturu ", InstitutionCategoryId = 1, IsPublic = true, StateId = 1 },
                        new Institution() { Id = 70, Active = true, Name = "Enugu State University of Science and Technology, Enugu ", InstitutionCategoryId = 1, IsPublic = true, StateId = 14 },
                        new Institution() { Id = 71, Active = true, Name = "Federal University, Dutsin-Ma", InstitutionCategoryId = 1, IsPublic = true, StateId = 21 },
                        new Institution() { Id = 72, Active = true, Name = "Kaduna State University, Kaduna ", InstitutionCategoryId = 1, IsPublic = true, StateId = 19 },
                        new Institution() { Id = 73, Active = true, Name = "Imo State University, Owerri ", InstitutionCategoryId = 1, IsPublic = true, StateId = 17 },
                        new Institution() { Id = 74, Active = true, Name = "Federal University, Dutse ", InstitutionCategoryId = 1, IsPublic = true, StateId = 18 },
                        new Institution() { Id = 75, Active = true, Name = "Kano University of Science and Technology, Wudil ", InstitutionCategoryId = 1, IsPublic = true, StateId = 20 },
                        new Institution() { Id = 76, Active = true, Name = "The Technical University, Ibadan ", InstitutionCategoryId = 1, IsPublic = true, StateId = 31 },
                        new Institution() { Id = 77, Active = true, Name = "Ignatius Ajuru University of Education, Port Harcourt ", InstitutionCategoryId = 1, IsPublic = true, StateId = 33 },
                        new Institution() { Id = 78, Active = true, Name = "University of Calabar, Calabar ", InstitutionCategoryId = 1, IsPublic = true, StateId = 9 },
                        new Institution() { Id = 79, Active = true, Name = "Yusuf Maitama Sule University Kano, Kano ", InstitutionCategoryId = 1, IsPublic = true, StateId = 20 },
                        new Institution() { Id = 80, Active = true, Name = "Ambrose Alli University, Ekpoma ", InstitutionCategoryId = 1, IsPublic = true, StateId = 12 },
                        new Institution() { Id = 81, Active = true, Name = "Ibrahim Badamasi Babangida University, Lapai ", InstitutionCategoryId = 1, IsPublic = true, StateId = 27 },
                        new Institution() { Id = 82, Active = true, Name = "Federal University, Lokoja", InstitutionCategoryId = 1, IsPublic = true, StateId = 23 },
                        new Institution() { Id = 83, Active = true, Name = "Chukwuemeka Odumegwu Ojukwu University, Uli ", InstitutionCategoryId = 1, IsPublic = true, StateId = 4 },
                        new Institution() { Id = 84, Active = true, Name = "Ebonyi State University, Abakaliki ", InstitutionCategoryId = 1, IsPublic = true, StateId = 11 },
                        new Institution() { Id = 85, Active = true, Name = "Delta State University, Abraka ", InstitutionCategoryId = 1, IsPublic = true, StateId = 10 },
                        new Institution() { Id = 86, Active = true, Name = "Akwa Ibom State University,Ikot Akpad ", InstitutionCategoryId = 1, IsPublic = true, StateId = 3 },
                        new Institution() { Id = 87, Active = true, Name = "Modibbo Adama University of Technology, Yola ", InstitutionCategoryId = 1, IsPublic = true, StateId = 2 },
                        new Institution() { Id = 89, Active = true, Name = "Taraba State University, Jalingo ", InstitutionCategoryId = 1, IsPublic = true, StateId = 35 },
                        new Institution() { Id = 90, Active = true, Name = "Cross River University of Technology, Calabar ", InstitutionCategoryId = 1, IsPublic = true, StateId = 9 },
                        new Institution() { Id = 91, Active = true, Name = "Federal University, Wukari", InstitutionCategoryId = 1, IsPublic = true, StateId = 35 },
                        new Institution() { Id = 92, Active = true, Name = "Edo University, Iyamho ", InstitutionCategoryId = 1, IsPublic = true, StateId = 12 },
                        new Institution() { Id = 93, Active = true, Name = "Bauchi State University,Gadau ", InstitutionCategoryId = 1, IsPublic = true, StateId = 5 },
                        new Institution() { Id = 94, Active = true, Name = "Kogi State University, Anyigba ", InstitutionCategoryId = 1, IsPublic = true, StateId = 23 },
                        new Institution() { Id = 95, Active = true, Name = "Niger Delta University, Wilberforce Island ", InstitutionCategoryId = 1, IsPublic = true, StateId = 6 },
                        new Institution() { Id = 96, Active = true, Name = "Federal University, Birnin Kebbi ", InstitutionCategoryId = 1, IsPublic = true, StateId = 22 },
                        new Institution() { Id = 97, Active = true, Name = "University of Medical Sciences, Ondo City ", InstitutionCategoryId = 1, IsPublic = true, StateId = 29 },
                        new Institution() { Id = 98, Active = true, Name = "Plateau State University, Bokkos ", InstitutionCategoryId = 1, IsPublic = true, StateId = 32 },
                        new Institution() { Id = 99, Active = true, Name = "Federal University, Lafia ", InstitutionCategoryId = 1, IsPublic = true, StateId = 26 },
                        new Institution() { Id = 100, Active = true, Name = "Sokoto State University, Sokoto ", InstitutionCategoryId = 1, IsPublic = true, StateId = 34 },
                        new Institution() { Id = 101, Active = true, Name = "Federal University, Kashere ", InstitutionCategoryId = 1, IsPublic = true, StateId = 16 },
                        new Institution() { Id = 102, Active = true, Name = "Ondo State University of Science and Technology, Okitipupa ", InstitutionCategoryId = 1, IsPublic = true, StateId = 29 },
                        new Institution() { Id = 103, Active = true, Name = "Yobe State University, Damaturu ", InstitutionCategoryId = 1, IsPublic = true, StateId = 36 },
                        new Institution() { Id = 104, Active = true, Name = "Adamawa State University, Mubi ", InstitutionCategoryId = 1, IsPublic = true, StateId = 2 },
                        new Institution() { Id = 105, Active = true, Name = "Gombe State University, Gombe ", InstitutionCategoryId = 1, IsPublic = true, StateId = 16 },
                        new Institution() { Id = 106, Active = true, Name = "Sule Lamido University, Kafin Hausa ", InstitutionCategoryId = 1, IsPublic = true, StateId = 18 },
                        new Institution() { Id = 107, Active = true, Name = "University of Africa, Toru-Orua ", InstitutionCategoryId = 1, IsPublic = true, StateId = 6 },
                        new Institution() { Id = 108, Active = true, Name = "Federal University, Gusau ", InstitutionCategoryId = 1, IsPublic = true, StateId = 37 },
                        new Institution() { Id = 109, Active = true, Name = "Federal University, Gashua ", InstitutionCategoryId = 1, IsPublic = true, StateId = 36 },
                        new Institution() { Id = 110, Active = true, Name = "Kebbi State University of Science and Technology, Aliero ", InstitutionCategoryId = 1, IsPublic = true, StateId = 22 },
                        new Institution() { Id = 111, Active = true, Name = "Eastern Palm University, Ogboko ", InstitutionCategoryId = 1, IsPublic = true, StateId = 17 },
                        new Institution() { Id = 112, Active = true, Name = "Nigerian Maritime University, Okerenkoko ", InstitutionCategoryId = 1, IsPublic = true, StateId = 10 },
                        new Institution() { Id = 113, Active = true, Name = "Moshood Abiola University of Science and Technology, Abeokuta ", InstitutionCategoryId = 1, IsPublic = true, StateId = 28 },
                        new Institution() { Id = 114, Active = true, Name = "Borno State University, Maiduguri ", InstitutionCategoryId = 1, IsPublic = true, StateId = 8 },
                        new Institution() { Id = 115, Active = true, Name = "Zamfara State University, Talata Mafara ", InstitutionCategoryId = 1, IsPublic = true, StateId = 37 },
                        new Institution() { Id = 116, Active = true, Name = "Gombe State University of Science and Technology ", InstitutionCategoryId = 1, IsPublic = true, StateId = 16 },
                        new Institution() { Id = 117, Active = true, Name = "Covenant University, Ota ", InstitutionCategoryId = 1, IsPublic = false, StateId = 28 },
                        new Institution() { Id = 118, Active = true, Name = "Afe Babalola University, Ado-Ekiti ", InstitutionCategoryId = 1, IsPublic = false, StateId = 13 },
                        new Institution() { Id = 119, Active = true, Name = "Landmark University, Omu-Aran ", InstitutionCategoryId = 1, IsPublic = false, StateId = 24 },
                        new Institution() { Id = 120, Active = true, Name = "Babcock University, Ilishan-Remo ", InstitutionCategoryId = 1, IsPublic = false, StateId = 28 },
                        new Institution() { Id = 121, Active = true, Name = "Nile University of Nigeria, Abuja ", InstitutionCategoryId = 1, IsPublic = false, StateId = 15 },
                        new Institution() { Id = 122, Active = true, Name = "American University of Nigeria, Yola ", InstitutionCategoryId = 1, IsPublic = false, StateId = 2 },
                        new Institution() { Id = 123, Active = true, Name = "Redeemer's University, Ede ", InstitutionCategoryId = 1, IsPublic = false, StateId = 30 },
                        new Institution() { Id = 124, Active = true, Name = "Al-Hikmah University, Ilorin ", InstitutionCategoryId = 1, IsPublic = false, StateId = 24 },
                        new Institution() { Id = 125, Active = true, Name = "Pan-Atlantic University, Lagos ", InstitutionCategoryId = 1, IsPublic = false, StateId = 25 },
                        new Institution() { Id = 126, Active = true, Name = "Bowen University, Iwo ", InstitutionCategoryId = 1, IsPublic = false, StateId = 30 },
                        new Institution() { Id = 127, Active = true, Name = "African University of Science and Technology, Abuja ", InstitutionCategoryId = 1, IsPublic = false, StateId = 15 },
                        new Institution() { Id = 128, Active = true, Name = "Godfrey Okoye University, Ugwuomu-Nike ", InstitutionCategoryId = 1, IsPublic = false, StateId = 14 },
                        new Institution() { Id = 129, Active = true, Name = "Bells University of Technology, Ota ", InstitutionCategoryId = 1, IsPublic = false, StateId = 28 },
                        new Institution() { Id = 130, Active = true, Name = "Baze University, Abuja ", InstitutionCategoryId = 1, IsPublic = false, StateId = 15 },
                        new Institution() { Id = 131, Active = true, Name = "Veritas University, Abuja ", InstitutionCategoryId = 1, IsPublic = false, StateId = 15 },
                        new Institution() { Id = 132, Active = true, Name = "Mountain Top University, Makogi Oba ", InstitutionCategoryId = 1, IsPublic = false, StateId = 28 },
                        new Institution() { Id = 133, Active = true, Name = "Caleb University, Imota ", InstitutionCategoryId = 1, IsPublic = false, StateId = 25 },
                        new Institution() { Id = 134, Active = true, Name = "Ajayi Crowther University, Oyo Town ", InstitutionCategoryId = 1, IsPublic = false, StateId = 31 },
                        new Institution() { Id = 135, Active = true, Name = "Lead City University, Ibadan ", InstitutionCategoryId = 1, IsPublic = false, StateId = 31 },
                        new Institution() { Id = 136, Active = true, Name = "Igbinedion University Okada ", InstitutionCategoryId = 1, IsPublic = false, StateId = 12 },
                        new Institution() { Id = 137, Active = true, Name = "Madonna University, Okija ", InstitutionCategoryId = 1, IsPublic = false, StateId = 4 },
                        new Institution() { Id = 138, Active = true, Name = "Bingham University,Auta Balifi ", InstitutionCategoryId = 1, IsPublic = false, StateId = 26 },
                        new Institution() { Id = 139, Active = true, Name = "Fountain University, Osogbo ", InstitutionCategoryId = 1, IsPublic = false, StateId = 30 },
                        new Institution() { Id = 140, Active = true, Name = "Joseph Ayo Babalola University, Ikeji-Arakeji ", InstitutionCategoryId = 1, IsPublic = false, StateId = 30 },
                        new Institution() { Id = 141, Active = true, Name = "Achievers University, Owo ", InstitutionCategoryId = 1, IsPublic = false, StateId = 29 },
                        new Institution() { Id = 142, Active = true, Name = "University of Mkar, Mkar ", InstitutionCategoryId = 1, IsPublic = false, StateId = 7 },
                        new Institution() { Id = 143, Active = true, Name = "Anchor University, Lagos ", InstitutionCategoryId = 1, IsPublic = false, StateId = 25 },
                        new Institution() { Id = 144, Active = true, Name = "Ritman University, Ikot Ekpene ", InstitutionCategoryId = 1, IsPublic = false, StateId = 3 },
                        new Institution() { Id = 145, Active = true, Name = "Crescent University, Abeokuta ", InstitutionCategoryId = 1, IsPublic = false, StateId = 28 },
                        new Institution() { Id = 146, Active = true, Name = "Obong University, Obong Ntak ", InstitutionCategoryId = 1, IsPublic = false, StateId = 3 },
                        new Institution() { Id = 147, Active = true, Name = "Oduduwa University, Ile Ife ", InstitutionCategoryId = 1, IsPublic = false, StateId = 30 },
                        new Institution() { Id = 148, Active = true, Name = "Al-Qalam University, Katsina ", InstitutionCategoryId = 1, IsPublic = false, StateId = 21 },
                        new Institution() { Id = 149, Active = true, Name = "Samuel Adegboyega University, Ogwa ", InstitutionCategoryId = 1, IsPublic = false, StateId = 12 },
                        new Institution() { Id = 150, Active = true, Name = "Crawford University, Faith City ", InstitutionCategoryId = 1, IsPublic = false, StateId = 28 },
                        new Institution() { Id = 151, Active = true, Name = "Michael and Cecilia Ibru University, Agbara-Otor ", InstitutionCategoryId = 1, IsPublic = false, StateId = 10 },
                        new Institution() { Id = 152, Active = true, Name = "Paul University, Awka ", InstitutionCategoryId = 1, IsPublic = false, StateId = 4 },
                        new Institution() { Id = 153, Active = true, Name = "Kola Daisi University, Ibadan ", InstitutionCategoryId = 1, IsPublic = false, StateId = 31 },
                        new Institution() { Id = 154, Active = true, Name = "Gregory University, Uturu ", InstitutionCategoryId = 1, IsPublic = false, StateId = 1 },
                        new Institution() { Id = 155, Active = true, Name = "Tansian University, Umunya ", InstitutionCategoryId = 1, IsPublic = false, StateId = 4 },
                        new Institution() { Id = 156, Active = true, Name = "Western Delta University, Oghara ", InstitutionCategoryId = 1, IsPublic = false, StateId = 10 },
                        new Institution() { Id = 157, Active = true, Name = "Salem University, Lokoja ", InstitutionCategoryId = 1, IsPublic = false, StateId = 23 },
                        new Institution() { Id = 158, Active = true, Name = "Novena University, Ogume ", InstitutionCategoryId = 1, IsPublic = false, StateId = 10 },
                        new Institution() { Id = 159, Active = true, Name = "PAMO University of Medical Sciences, Port-Harcourt ", InstitutionCategoryId = 1, IsPublic = false, StateId = 33 },
                        new Institution() { Id = 160, Active = true, Name = "Kings University, Odeomu ", InstitutionCategoryId = 1, IsPublic = false, StateId = 30 },
                        new Institution() { Id = 161, Active = true, Name = "Renaissance University, Enugu ", InstitutionCategoryId = 1, IsPublic = false, StateId = 14 },
                        new Institution() { Id = 162, Active = true, Name = "Admiralty University of Nigeria, Ibusa ", InstitutionCategoryId = 1, IsPublic = false, StateId = 10 },
                        new Institution() { Id = 163, Active = true, Name = "Wesley University of Science and Technology, Ondo City ", InstitutionCategoryId = 1, IsPublic = false, StateId = 29 },
                        new Institution() { Id = 164, Active = true, Name = "Kwararafa University, Wukari ", InstitutionCategoryId = 1, IsPublic = false, StateId = 35 },
                        new Institution() { Id = 165, Active = true, Name = "Rhema University, Aba ", InstitutionCategoryId = 1, IsPublic = false, StateId = 1 },
                        new Institution() { Id = 166, Active = true, Name = "Wellspring University, Benin City ", InstitutionCategoryId = 1, IsPublic = false, StateId = 12 },
                        new Institution() { Id = 167, Active = true, Name = "Christopher University, Mowe ", InstitutionCategoryId = 1, IsPublic = false, StateId = 28 },
                        new Institution() { Id = 168, Active = true, Name = "Mcpherson University, Seriki-Sotayo ", InstitutionCategoryId = 1, IsPublic = false, StateId = 28 },
                        new Institution() { Id = 169, Active = true, Name = "Edwin Clark University, Kiagbodo ", InstitutionCategoryId = 1, IsPublic = false, StateId = 10 },
                        new Institution() { Id = 170, Active = true, Name = "Chrisland University, Abeokuta ", InstitutionCategoryId = 1, IsPublic = false, StateId = 28 },
                        new Institution() { Id = 171, Active = true, Name = "Crown Hill University, Ilorin ", InstitutionCategoryId = 1, IsPublic = false, StateId = 24 },
                        new Institution() { Id = 172, Active = true, Name = "Augustine University, Ilara ", InstitutionCategoryId = 1, IsPublic = false, StateId = 29 },
                        new Institution() { Id = 173, Active = true, Name = "Southwestern University, Nigeria ", InstitutionCategoryId = 1, IsPublic = false, StateId = 28 },
                        new Institution() { Id = 174, Active = true, Name = "Coal City University, Enugu ", InstitutionCategoryId = 1, IsPublic = false, StateId = 14 },
                        new Institution() { Id = 175, Active = true, Name = "Evangel University, Akaeze Enugu ", InstitutionCategoryId = 1, IsPublic = false, StateId = 14 },
                        new Institution() { Id = 176, Active = true, Name = "Hezekiah University, Umudi ", InstitutionCategoryId = 1, IsPublic = false, StateId = 17 },
                        new Institution() { Id = 177, Active = true, Name = "Summit University Offa,Offa ", InstitutionCategoryId = 1, IsPublic = false, StateId = 24 },
                        new Institution() { Id = 178, Active = true, Name = "Hallmark University, Ijebu-Itele ", InstitutionCategoryId = 1, IsPublic = false, StateId = 28 },
                        new Institution() { Id = 179, Active = true, Name = "Dominican University, Ibadan ", InstitutionCategoryId = 1, IsPublic = false, StateId = 31 },
                        new Institution() { Id = 180, Active = true, Name = "Clifford University, Ihie ", InstitutionCategoryId = 1, IsPublic = false, StateId = 1 },
                        new Institution() { Id = 181, Active = true, Name = "Arthur Jarvis University, Calabar ", InstitutionCategoryId = 1, IsPublic = false, StateId = 9 },
                        new Institution() { Id = 182, Active = true, Name = "Eko University of Medical and Health Sciences, Ijanikin ", InstitutionCategoryId = 1, IsPublic = false, StateId = 25 },
                        new Institution() { Id = 183, Active = true, Name = "Spiritan University, Nneochi ", InstitutionCategoryId = 1, IsPublic = false, StateId = 1 },
                        new Institution() { Id = 184, Active = true, Name = "Legacy University, Okija ", InstitutionCategoryId = 1, IsPublic = false, StateId = 4 },
                        new Institution() { Id = 185, Active = true, Name = "Atiba University, Oyo Town ", InstitutionCategoryId = 1, IsPublic = false, StateId = 31 },
                        new Institution() { Id = 186, Active = true, Name = "Skyline University Nigeria,Kano ", InstitutionCategoryId = 1, IsPublic = false, StateId = 20 },
                        new Institution() { Id = 187, Active = true, Name = "Precious Cornerstone University, Ibadan ", InstitutionCategoryId = 1, IsPublic = false, StateId = 31 },
                        new Institution() { Id = 188, Active = true, Name = "Abdu Gusau Polytechnic, Talata Mafara ", InstitutionCategoryId = 2, IsPublic = true, StateId = 37 },
                        new Institution() { Id = 189, Active = true, Name = "Abia State Polytechnic, Aba ", InstitutionCategoryId = 2, IsPublic = true, StateId = 1 },
                        new Institution() { Id = 190, Active = true, Name = "Abraham Adesanya Polytechnic, Dogbolu/Akanran Ibadan Road, Atikori, Ijebu Igbo ", InstitutionCategoryId = 2, IsPublic = true, StateId = 31 },
                        new Institution() { Id = 191, Active = true, Name = "Abubakar Tatari Ali Polytechnic, Bauchi ", InstitutionCategoryId = 2, IsPublic = true, StateId = 5 },
                        new Institution() { Id = 192, Active = true, Name = "Adamawa State Polytechnic, Yola ", InstitutionCategoryId = 2, IsPublic = true, StateId = 2 },
                        new Institution() { Id = 193, Active = true, Name = "Akwa Ibom State College of Art & Science, Nung Ukim ", InstitutionCategoryId = 2, IsPublic = true, StateId = 3 },
                        new Institution() { Id = 194, Active = true, Name = "Akwa Ibom State Polytechnic,Ikot-Ekpene ", InstitutionCategoryId = 2, IsPublic = true, StateId = 3 },
                        new Institution() { Id = 195, Active = true, Name = "Bayelsa State College of Arts and Science, Elebele ", InstitutionCategoryId = 2, IsPublic = true, StateId = 6 },
                        new Institution() { Id = 196, Active = true, Name = "Benue State Polytechnic, Ugbokolo ", InstitutionCategoryId = 2, IsPublic = true, StateId = 7 },
                        new Institution() { Id = 197, Active = true, Name = "Binyaminu Usman Polytechnic, Hadejia,Jigawa State ", InstitutionCategoryId = 2, IsPublic = true, StateId = 18 },
                        new Institution() { Id = 198, Active = true, Name = "D.S. Adegbenro ICT Polytechnic, Itori-Ewekoro ", InstitutionCategoryId = 2, IsPublic = true, StateId = 28 },
                        new Institution() { Id = 199, Active = true, Name = "Delta State Polytechnic, Ogwashi-Uku ", InstitutionCategoryId = 2, IsPublic = true, StateId = 10 },
                        new Institution() { Id = 200, Active = true, Name = "Delta State School of Marine Technology, Burutu ", InstitutionCategoryId = 2, IsPublic = true, StateId = 10 },
                        new Institution() { Id = 201, Active = true, Name = "Edo State Institute of Technology and Management, Usen ", InstitutionCategoryId = 2, IsPublic = true, StateId = 12 },
                        new Institution() { Id = 202, Active = true, Name = "Enugu State Polytechnic, Iwollo ", InstitutionCategoryId = 2, IsPublic = true, StateId = 14 },
                        new Institution() { Id = 203, Active = true, Name = "Gateway Polytechnic, Saapade ", InstitutionCategoryId = 2, IsPublic = true, StateId = 28 },
                        new Institution() { Id = 204, Active = true, Name = "Hassan Usman Katsina Polytechnic, Katsina ", InstitutionCategoryId = 2, IsPublic = true, StateId = 21 },
                        new Institution() { Id = 205, Active = true, Name = "Ibarapa Polytechnic, Eruwa Oyo ", InstitutionCategoryId = 2, IsPublic = true, StateId = 31 },
                        new Institution() { Id = 206, Active = true, Name = "Institute of Management and Technology, Enugu ", InstitutionCategoryId = 2, IsPublic = true, StateId = 14 },
                        new Institution() { Id = 207, Active = true, Name = "Institute of Technology and Management (ITM),Ikom-Calabar ", InstitutionCategoryId = 2, IsPublic = true, StateId = 9 },
                        new Institution() { Id = 208, Active = true, Name = "Jigawa State Polytechnic, Dutse ", InstitutionCategoryId = 2, IsPublic = true, StateId = 18 },
                        new Institution() { Id = 209, Active = true, Name = "Kano State Polytechnic, Kano ", InstitutionCategoryId = 2, IsPublic = true, StateId = 20 },
                        new Institution() { Id = 210, Active = true, Name = "Ken Sarowiwa Polytechnic, Bori ", InstitutionCategoryId = 2, IsPublic = true, StateId = 33 },
                        new Institution() { Id = 211, Active = true, Name = "Kogi State Polytechnic, Lokoja ", InstitutionCategoryId = 2, IsPublic = true, StateId = 23 },
                        new Institution() { Id = 212, Active = true, Name = "Kwara State Polytechnic, Ilorin ", InstitutionCategoryId = 2, IsPublic = true, StateId = 24 },
                        new Institution() { Id = 213, Active = true, Name = "Lagos State Polytechnic, Ikorodu ", InstitutionCategoryId = 2, IsPublic = true, StateId = 25 },
                        new Institution() { Id = 214, Active = true, Name = "Mai-Idris Alooma Polytechnic, Geidam ", InstitutionCategoryId = 2, IsPublic = true, StateId = 36 },
                        new Institution() { Id = 215, Active = true, Name = "Moshood Abiola Polytechnic, Abeokuta ", InstitutionCategoryId = 2, IsPublic = true, StateId = 28 },
                        new Institution() { Id = 216, Active = true, Name = "Nasarawa State Polytechnic, Nasarawa State. ", InstitutionCategoryId = 2, IsPublic = true, StateId = 26 },
                        new Institution() { Id = 217, Active = true, Name = "Niger State Polytechnic, Zungeru ", InstitutionCategoryId = 2, IsPublic = true, StateId = 27 },
                        new Institution() { Id = 218, Active = true, Name = "Nuhu Bamalli Polytechnic, Zaria ", InstitutionCategoryId = 2, IsPublic = true, StateId = 19 },
                        new Institution() { Id = 219, Active = true, Name = "Ogun State Institute of Technology, IgbesaOba ", InstitutionCategoryId = 2, IsPublic = true, StateId = 28 },
                        new Institution() { Id = 220, Active = true, Name = "Ogun State Polytechnic, Ipokia ", InstitutionCategoryId = 2, IsPublic = true, StateId = 28 },
                        new Institution() { Id = 221, Active = true, Name = "Oke-Ogun Polytechnic, Shaki,Oyo State ", InstitutionCategoryId = 2, IsPublic = true, StateId = 31 },
                        new Institution() { Id = 222, Active = true, Name = "Osun State College of Technology, Osun State ", InstitutionCategoryId = 2, IsPublic = true, StateId = 30 },
                        new Institution() { Id = 223, Active = true, Name = "Osun State Polytechnic, Osun State ", InstitutionCategoryId = 2, IsPublic = true, StateId = 30 },
                        new Institution() { Id = 224, Active = true, Name = "Oyo State College of Agriculture and Technology, Igbo Ora ", InstitutionCategoryId = 2, IsPublic = true, StateId = 31 },
                        new Institution() { Id = 225, Active = true, Name = "Plateau State Polytechnic, Barkin-Ladi ", InstitutionCategoryId = 2, IsPublic = true, StateId = 32 },
                        new Institution() { Id = 226, Active = true, Name = "Port-Harcourt Polytechnic, Rivers State. ", InstitutionCategoryId = 2, IsPublic = true, StateId = 33 },
                        new Institution() { Id = 227, Active = true, Name = "Ramat Polytechnic, Maiduguri ", InstitutionCategoryId = 2, IsPublic = true, StateId = 8 },
                        new Institution() { Id = 228, Active = true, Name = "Rufus Giwa Polytechnic, Owo ", InstitutionCategoryId = 2, IsPublic = true, StateId = 29 },
                        new Institution() { Id = 229, Active = true, Name = "Taraba State Polytechnic, Suntai ", InstitutionCategoryId = 2, IsPublic = true, StateId = 35 },
                        new Institution() { Id = 230, Active = true, Name = "The Polytechnic Ibadan, Oyo State. ", InstitutionCategoryId = 2, IsPublic = true, StateId = 31 },
                        new Institution() { Id = 231, Active = true, Name = "Umaru Ali Shinkafi Polytechnic, Sokoto ", InstitutionCategoryId = 2, IsPublic = true, StateId = 34 },
                        new Institution() { Id = 232, Active = true, Name = "Zamfara State College of Arts and Science, Gusau ", InstitutionCategoryId = 2, IsPublic = true, StateId = 37 },
                        new Institution() { Id = 233, Active = true, Name = "Al-Hikma Polytechnic, Nasarawa State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 26 },
                        new Institution() { Id = 234, Active = true, Name = "Allover Central Polytechnic, Sango Ota ", InstitutionCategoryId = 2, IsPublic = false, StateId = 28 },
                        new Institution() { Id = 235, Active = true, Name = "Ajayi PolytechnicIkere Ekiti,Ekiti State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 13 },
                        new Institution() { Id = 236, Active = true, Name = "Ashi Polytechnic Anyiin, Benue State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 7 },
                        new Institution() { Id = 237, Active = true, Name = "Best Solution Polytechnic, Akure ", InstitutionCategoryId = 2, IsPublic = false, StateId = 29 },
                        new Institution() { Id = 238, Active = true, Name = "Bolmor Polytechnic, Oyo State. ", InstitutionCategoryId = 2, IsPublic = false, StateId = 31 },
                        new Institution() { Id = 239, Active = true, Name = "Calvary Polytechnic, Owa-OyibuDelta State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 10 },
                        new Institution() { Id = 240, Active = true, Name = "Citi PolytechnicPlot 182, FCT Abuja ", InstitutionCategoryId = 2, IsPublic = false, StateId = 15 },
                        new Institution() { Id = 241, Active = true, Name = "Covenant Polytechnic, Aba ", InstitutionCategoryId = 2, IsPublic = false, StateId = 1 },
                        new Institution() { Id = 242, Active = true, Name = "Crown Polytechnic, Ado-Ekiti ", InstitutionCategoryId = 2, IsPublic = false, StateId = 13 },
                        new Institution() { Id = 243, Active = true, Name = "Dorben Polytechnic, Abuja ", InstitutionCategoryId = 2, IsPublic = false, StateId = 15 },
                        new Institution() { Id = 244, Active = true, Name = "Eastern Polytechnic, Rivers State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 33 },
                        new Institution() { Id = 245, Active = true, Name = "Fidei Polytechnic, Benue State. ", InstitutionCategoryId = 2, IsPublic = false, StateId = 7 },
                        new Institution() { Id = 246, Active = true, Name = "Gboko Polytechnic, Gboko ", InstitutionCategoryId = 2, IsPublic = false, StateId = 7 },
                        new Institution() { Id = 247, Active = true, Name = "Global Polytechnic, Akure,Ondo State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 29 },
                        new Institution() { Id = 248, Active = true, Name = "Grace Polytechnic, Surulere ", InstitutionCategoryId = 2, IsPublic = false, StateId = 25 },
                        new Institution() { Id = 249, Active = true, Name = "Graceland Polytechnic, Kwara State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 24 },
                        new Institution() { Id = 250, Active = true, Name = "Grundtvig Polytechnic, Oba Anambra ", InstitutionCategoryId = 2, IsPublic = false, StateId = 4 },
                        new Institution() { Id = 251, Active = true, Name = "Heritage Polytechnic, Ikot Udota, Eket,Akwa Ibom State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 3 },
                        new Institution() { Id = 252, Active = true, Name = "Ibadan City Polytechnic, Ibadan Oyo State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 31 },
                        new Institution() { Id = 253, Active = true, Name = "Igbajo Polytechnic, Osun State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 30 },
                        new Institution() { Id = 254, Active = true, Name = "Interlink Polytechnic, Ado-Ekiti ", InstitutionCategoryId = 2, IsPublic = false, StateId = 13 },
                        new Institution() { Id = 255, Active = true, Name = "Kalac Christal Polytechnic, Lagos State. ", InstitutionCategoryId = 2, IsPublic = false, StateId = 25 },
                        new Institution() { Id = 256, Active = true, Name = "Kings Polytechnic,Edo State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 12 },
                        new Institution() { Id = 257, Active = true, Name = "Landmark Polytechnic, Ogun State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 28 },
                        new Institution() { Id = 258, Active = true, Name = "Lagos City Polytechnic, Ikeja ", InstitutionCategoryId = 2, IsPublic = false, StateId = 25 },
                        new Institution() { Id = 259, Active = true, Name = "Lens Polytechnic, Kwara State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 24 },
                        new Institution() { Id = 260, Active = true, Name = "Lighthouse Polytechnic, Edo State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 12 },
                        new Institution() { Id = 261, Active = true, Name = "Marist Polytechnic, Umuchigbo, Iji-Nike,Emene Enugu State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 14 },
                        new Institution() { Id = 262, Active = true, Name = "Mater Dei PolytechnicUgwuoba TownOji River LGA Enugu State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 14 },
                        new Institution() { Id = 263, Active = true, Name = "Nacabs Polytechnic, Nasarawa State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 26 },
                        new Institution() { Id = 264, Active = true, Name = "Nogak Polytechnic, Ikom,Cross Rivers State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 9 },
                        new Institution() { Id = 265, Active = true, Name = "Our Saviour Institute of Science, Agriculture & Technology, Enugu State. ", InstitutionCategoryId = 2, IsPublic = false, StateId = 14 },
                        new Institution() { Id = 266, Active = true, Name = "Prime Polytechnic, Jida Bassa, Ajaokuta, Kogi State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 23 },
                        new Institution() { Id = 267, Active = true, Name = "Redeemers College of Technology and Management, Ogun State", InstitutionCategoryId = 2, IsPublic = false, StateId = 28 },
                        new Institution() { Id = 268, Active = true, Name = "Ronik Polytechnic, Lagos State. ", InstitutionCategoryId = 2, IsPublic = false, StateId = 25 },
                        new Institution() { Id = 269, Active = true, Name = "Saf Polytechnic,P.O. Box 55 Iseyin Oyo State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 31 },
                        new Institution() { Id = 270, Active = true, Name = "Savanah Institute of Technology, Ebonyi State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 11 },
                        new Institution() { Id = 271, Active = true, Name = "Shaka Polytechnic, Edo State. ", InstitutionCategoryId = 2, IsPublic = false, StateId = 12 },
                        new Institution() { Id = 272, Active = true, Name = "Speedway Polytechnic, Ogun State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 28 },
                        new Institution() { Id = 273, Active = true, Name = "St. Mary Polytechnic, Niger State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 27 },
                        new Institution() { Id = 274, Active = true, Name = "Sure Foundation Polytechnic, Akwa Ibom State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 3 },
                        new Institution() { Id = 275, Active = true, Name = "Temple Gate Polytechnic, Abia State. ", InstitutionCategoryId = 2, IsPublic = false, StateId = 1 },
                        new Institution() { Id = 276, Active = true, Name = "The Polytechnic, Kwara State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 24 },
                        new Institution() { Id = 277, Active = true, Name = "The Polytechnic, Osun State. ", InstitutionCategoryId = 2, IsPublic = false, StateId = 30 },
                        new Institution() { Id = 278, Active = true, Name = "The Polytechnic Iresi, Osun State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 30 },
                        new Institution() { Id = 279, Active = true, Name = "The Polytechnic, Benue State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 7 },
                        new Institution() { Id = 280, Active = true, Name = "Timeon Kairos Polytechnic, Lagos ", InstitutionCategoryId = 2, IsPublic = false, StateId = 25 },
                        new Institution() { Id = 281, Active = true, Name = "Tower Polytechnic, Ibadan. ", InstitutionCategoryId = 2, IsPublic = false, StateId = 31 },
                        new Institution() { Id = 282, Active = true, Name = "Trinity Polytechnic Uyo,Akwa Ibom State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 3 },
                        new Institution() { Id = 283, Active = true, Name = "Uma Ukpai Polytechnic, Abia State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 1 },
                        new Institution() { Id = 284, Active = true, Name = "Uyo City Polytechnic, Akwa Ibom State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 3 },
                        new Institution() { Id = 285, Active = true, Name = "Valley View Polytechnic, OhafiaAbia State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 1 },
                        new Institution() { Id = 286, Active = true, Name = "Villanova Polytechnic, Osun State. ", InstitutionCategoryId = 2, IsPublic = false, StateId = 30 },
                        new Institution() { Id = 287, Active = true, Name = "Wolex Polytechnic, Osun State ", InstitutionCategoryId = 2, IsPublic = false, StateId = 30 },
                        new Institution() { Id = 288, Active = true, Name = "Adeyemi College of Education, Ondo State ", InstitutionCategoryId = 3, IsPublic = true, StateId = 29 },
                        new Institution() { Id = 289, Active = true, Name = "Alvan Ikoku Federal College of Education, Owerri ", InstitutionCategoryId = 3, IsPublic = true, StateId = 17 },
                        new Institution() { Id = 290, Active = true, Name = "Federal College of Education, Ogun State ", InstitutionCategoryId = 3, IsPublic = true, StateId = 28 },
                        new Institution() { Id = 291, Active = true, Name = "Federal College of Education., Enugu State ", InstitutionCategoryId = 3, IsPublic = true, StateId = 14 },
                        new Institution() { Id = 292, Active = true, Name = "Federal College of Education, Kano ", InstitutionCategoryId = 3, IsPublic = true, StateId = 20 },
                        new Institution() { Id = 293, Active = true, Name = "Federal College of Education, Katsina ", InstitutionCategoryId = 3, IsPublic = true, StateId = 21 },
                        new Institution() { Id = 294, Active = true, Name = "Federal College of Education, Kontagora Niger State. ", InstitutionCategoryId = 3, IsPublic = true, StateId = 27 },
                        new Institution() { Id = 295, Active = true, Name = "Federal College of Education, Obudu, Cross River State ", InstitutionCategoryId = 3, IsPublic = true, StateId = 9 },
                        new Institution() { Id = 296, Active = true, Name = "Federal College of Education, Okene, Kogi State ", InstitutionCategoryId = 3, IsPublic = true, StateId = 23 },
                        new Institution() { Id = 297, Active = true, Name = "Federal College of Education, Plateau State. ", InstitutionCategoryId = 3, IsPublic = true, StateId = 32 },
                        new Institution() { Id = 298, Active = true, Name = "Fed. College of Educ. Yola ", InstitutionCategoryId = 3, IsPublic = true, StateId = 2 },
                        new Institution() { Id = 299, Active = true, Name = "Fed. College of Educ. Zaria ", InstitutionCategoryId = 3, IsPublic = true, StateId = 19 },
                        new Institution() { Id = 300, Active = true, Name = "Nigerian Army School of Education,Ilorin, Kwara State ", InstitutionCategoryId = 3, IsPublic = true, StateId = 24 },
                        new Institution() { Id = 301, Active = true, Name = "Federal College of Education (Technical), Akoka ", InstitutionCategoryId = 3, IsPublic = true, StateId = 33 },
                        new Institution() { Id = 302, Active = true, Name = "Federal College of Education (Technical), Asaba ", InstitutionCategoryId = 3, IsPublic = true, StateId = 10 },
                        new Institution() { Id = 303, Active = true, Name = "Federal College of Education (Technical), Bichi ", InstitutionCategoryId = 3, IsPublic = true, StateId = 20 },
                        new Institution() { Id = 304, Active = true, Name = "Federal College Education (Technical), Gombe ", InstitutionCategoryId = 3, IsPublic = true, StateId = 16 },
                        new Institution() { Id = 305, Active = true, Name = "Federal College of Education (Technical), Gusau, Zamfara State ", InstitutionCategoryId = 3, IsPublic = true, StateId = 37 },
                        new Institution() { Id = 306, Active = true, Name = "Federal College of Education (Technical), Omoku,, Rivers State ", InstitutionCategoryId = 3, IsPublic = true, StateId = 33 },
                        new Institution() { Id = 307, Active = true, Name = "Federal College of Education (Technical), Potiskum, Yobe State ", InstitutionCategoryId = 3, IsPublic = true, StateId = 36 },
                        new Institution() { Id = 308, Active = true, Name = "Federal College of Education (Technical), Umunze, Anambra State ", InstitutionCategoryId = 3, IsPublic = true, StateId = 4 },
                        new Institution() { Id = 309, Active = true, Name = "Federal College of Education (Special), Oyo ", InstitutionCategoryId = 3, IsPublic = true, StateId = 31 },
                        new Institution() { Id = 310, Active = true, Name = "College of Education,Gindiri, Plateau State. ", InstitutionCategoryId = 3, IsPublic = true, StateId = 32 },
                        new Institution() { Id = 311, Active = true, Name = "Adamawa State College of Education,Yola, Adamawa State ", InstitutionCategoryId = 3, IsPublic = true, StateId = 2 },
                        new Institution() { Id = 312, Active = true, Name = "Tai Solarin College of Education, Ijebu-Ode ", InstitutionCategoryId = 3, IsPublic = true, StateId = 28 },
                        new Institution() { Id = 313, Active = true, Name = "College of Education, Ikere-Ekiti ", InstitutionCategoryId = 3, IsPublic = true, StateId = 13 },
                        new Institution() { Id = 314, Active = true, Name = "Ebonyi State College of Education, Ikwo	Ikwo, Ebonyi State. ", InstitutionCategoryId = 3, IsPublic = true, StateId = 11 },
                        new Institution() { Id = 315, Active = true, Name = "College of Education, Warri ", InstitutionCategoryId = 3, IsPublic = true, StateId = 10 },
                        new Institution() { Id = 316, Active = true, Name = "FCT College of Education Garki, FCT Abuja ", InstitutionCategoryId = 3, IsPublic = true, StateId = 15 },
                        new Institution() { Id = 317, Active = true, Name = "Osisatech College of Education, Enugu ", InstitutionCategoryId = 3, IsPublic = true, StateId = 14 },
                        new Institution() { Id = 318, Active = true, Name = "Nasarrawa State College of Education, Akwanga ", InstitutionCategoryId = 3, IsPublic = true, StateId = 26 },
                        new Institution() { Id = 319, Active = true, Name = "Isa Kaita College of Education, Dutsin-Ma ", InstitutionCategoryId = 3, IsPublic = true, StateId = 21 },
                        new Institution() { Id = 320, Active = true, Name = "College of Education, Ekiadolor-Benin ", InstitutionCategoryId = 3, IsPublic = true, StateId = 12 },
                        new Institution() { Id = 321, Active = true, Name = "College of Education, Gashua, Damaturu	Gashua, Yobe state. ", InstitutionCategoryId = 3, IsPublic = true, StateId = 36 },
                        new Institution() { Id = 322, Active = true, Name = "Kaduna State College of Education, Gidan-Waya, Kafanchan ", InstitutionCategoryId = 3, IsPublic = true, StateId = 19 },
                        new Institution() { Id = 323, Active = true, Name = "Osun State College of Education, Ilesa ", InstitutionCategoryId = 3, IsPublic = true, StateId = 30 },
                        new Institution() { Id = 324, Active = true, Name = "Kwara State College of Education, Ilorin ", InstitutionCategoryId = 3, IsPublic = true, StateId = 24 },
                        new Institution() { Id = 325, Active = true, Name = "College of Education, katsina-Ala ", InstitutionCategoryId = 3, IsPublic = true, StateId = 21 },
                        new Institution() { Id = 326, Active = true, Name = "Sa'adatu Rimi College of Education, Kumbotso, Kano ", InstitutionCategoryId = 3, IsPublic = true, StateId = 20 },
                        new Institution() { Id = 327, Active = true, Name = "College of Education (Technical) Lafiagi, Kwara State. ", InstitutionCategoryId = 3, IsPublic = true, StateId = 24 },
                        new Institution() { Id = 328, Active = true, Name = "Nwafor Orizu College of Education, Nsugbe ", InstitutionCategoryId = 3, IsPublic = true, StateId = 4 },
                        new Institution() { Id = 329, Active = true, Name = "Adeniran Ogunsanya College of Education, Otto/Ijanikin ", InstitutionCategoryId = 3, IsPublic = true, StateId = 25 },
                        new Institution() { Id = 330, Active = true, Name = "Emmanuel Alayande College of Education (EACOED), Oyo ", InstitutionCategoryId = 3, IsPublic = true, StateId = 31 },
                        new Institution() { Id = 331, Active = true, Name = "College of Education,Waka BIU, Borno State ", InstitutionCategoryId = 3, IsPublic = true, StateId = 8 },
                        new Institution() { Id = 332, Active = true, Name = "St. Augustine College of Education (Project Time), Lagos ", InstitutionCategoryId = 3, IsPublic = true, StateId = 25 },
                        new Institution() { Id = 333, Active = true, Name = "Delta State College of Education, Agbor	Agbor, Delta State ", InstitutionCategoryId = 3, IsPublic = true, StateId = 10 },
                        new Institution() { Id = 334, Active = true, Name = "Akwa Ibom State College of Education,Afahansit, Akwa Ibom State. ", InstitutionCategoryId = 3, IsPublic = true, StateId = 3 },
                        new Institution() { Id = 335, Active = true, Name = "Kogi State College of Education, Ankpa	Ankpa, Kogi state. ", InstitutionCategoryId = 3, IsPublic = true, StateId = 23 },
                        new Institution() { Id = 336, Active = true, Name = "Adamu Augie College of Education, Argungu, Kebbi State. ", InstitutionCategoryId = 3, IsPublic = true, StateId = 22 },
                        new Institution() { Id = 337, Active = true, Name = "College of Education, Azare, Bauchi State ", InstitutionCategoryId = 3, IsPublic = true, StateId = 5 },
                        new Institution() { Id = 338, Active = true, Name = "Umar Ibn Ibrahim El-Kanemi College of Education, Science and Technology,Bama, Borno State. ", InstitutionCategoryId = 3, IsPublic = true, StateId = 8 },
                        new Institution() { Id = 339, Active = true, Name = "College of Education,Jalingo, Taraba State. ", InstitutionCategoryId = 3, IsPublic = true, StateId = 35 },
                        new Institution() { Id = 340, Active = true, Name = "Zamfara State College of Education, Maru ", InstitutionCategoryId = 3, IsPublic = true, StateId = 37 },
                        new Institution() { Id = 341, Active = true, Name = "Jigawa State College of Education, Gumel ", InstitutionCategoryId = 3, IsPublic = true, StateId = 18 },
                        new Institution() { Id = 342, Active = true, Name = "Niger State College of Education, Minna ", InstitutionCategoryId = 3, IsPublic = true, StateId = 27 },
                        new Institution() { Id = 343, Active = true, Name = "Rivers College of Education, Rumuolumeni ", InstitutionCategoryId = 3, IsPublic = true, StateId = 33 },
                        new Institution() { Id = 344, Active = true, Name = "Shehu shagari College of Education, Sokoto ", InstitutionCategoryId = 3, IsPublic = true, StateId = 34 },
                        new Institution() { Id = 345, Active = true, Name = "Jama'Atu College of Education (JACE), Kaduna ", InstitutionCategoryId = 3, IsPublic = true, StateId = 19 },
                        new Institution() { Id = 346, Active = true, Name = "College of Education, Arochukwu, Abia ", InstitutionCategoryId = 3, IsPublic = true, StateId = 1 },
                        new Institution() { Id = 347, Active = true, Name = "College of Education, Ila-Orangun, Osun State ", InstitutionCategoryId = 3, IsPublic = true, StateId = 30 },
                        new Institution() { Id = 348, Active = true, Name = "Michael Otedola Coll. of Prim. Education, Lagos ", InstitutionCategoryId = 3, IsPublic = true, StateId = 25 },
                        new Institution() { Id = 349, Active = true, Name = "Kashim Ibrahim College of Educ., Maiduguri ", InstitutionCategoryId = 3, IsPublic = true, StateId = 8 },
                        new Institution() { Id = 350, Active = true, Name = "Delta State Coll. of Physical Education, Mosogar ", InstitutionCategoryId = 3, IsPublic = true, StateId = 10 },
                        new Institution() { Id = 359, Active = true, Name = "Enugu State Coll. of Education (T), Enugu ", InstitutionCategoryId = 3, IsPublic = true, StateId = 14 },
                        new Institution() { Id = 360, Active = true, Name = "Cross River State Coll. of Education, Akampa ", InstitutionCategoryId = 3, IsPublic = true, StateId = 9 },
                        new Institution() { Id = 361, Active = true, Name = "Edo State College of Education, Igueben	Igueben, Edo State ", InstitutionCategoryId = 3, IsPublic = true, StateId = 12 },
                        new Institution() { Id = 362, Active = true, Name = "Isaac Jasper Boro COE, Sagbama	Sagbama, Yenogoa, Bayelsa Stat	 ", InstitutionCategoryId = 3, IsPublic = true, StateId = 6 },
                        new Institution() { Id = 363, Active = true, Name = "Kogi State College of Education, Kabba	Kabba, Kogi State ", InstitutionCategoryId = 3, IsPublic = true, StateId = 23 },
                        new Institution() { Id = 364, Active = true, Name = "Institute of Ecumenical Education, (Thinkers Corner), Enugu ", InstitutionCategoryId = 3, IsPublic = false, StateId = 14 },
                        new Institution() { Id = 365, Active = true, Name = "Delar College of Education Ibadan, Oyo State ", InstitutionCategoryId = 3, IsPublic = false, StateId = 31 },
                        new Institution() { Id = 366, Active = true, Name = "City College of Education, Mararaba,Nasarawa Stat ", InstitutionCategoryId = 3, IsPublic = false, StateId = 26 },
                        new Institution() { Id = 367, Active = true, Name = "Ansar-Ud-Deen College of Education, Isolo, Lagos State. ", InstitutionCategoryId = 3, IsPublic = false, StateId = 25 },
                        new Institution() { Id = 368, Active = true, Name = "Yewa Central College of Education, Ayetoro, Ogun State.	 ", InstitutionCategoryId = 3, IsPublic = false, StateId = 28 },
                        new Institution() { Id = 369, Active = true, Name = "OSISA Tech. Coll. of Education, Enugu ", InstitutionCategoryId = 3, IsPublic = false, StateId = 14 },
                        new Institution() { Id = 370, Active = true, Name = "St. Augustine Coll. of Education,Akoka, Lagos	 ", InstitutionCategoryId = 3, IsPublic = false, StateId = 25 },
                        new Institution() { Id = 371, Active = true, Name = "African Thinkers Community of Inquiry, Enugu ", InstitutionCategoryId = 3, IsPublic = false, StateId = 14 },
                        new Institution() { Id = 372, Active = true, Name = "Muftau Olanihun College of Education, Ibadan ", InstitutionCategoryId = 3, IsPublic = false, StateId = 31 },
                        new Institution() { Id = 373, Active = true, Name = "Havard Wilson College of Education, Aba	, Abia State ", InstitutionCategoryId = 3, IsPublic = false, StateId = 1 },
                        new Institution() { Id = 374, Active = true, Name = "Muhyideen College of Education, Ilorin ", InstitutionCategoryId = 3, IsPublic = false, StateId = 24 },
                        new Institution() { Id = 375, Active = true, Name = "College of Education, Offa, Kwara State	 ", InstitutionCategoryId = 3, IsPublic = false, StateId = 24 },
                        new Institution() { Id = 376, Active = true, Name = "Bauchi Institute of Arabic & Islamic Studies, Bauchi ", InstitutionCategoryId = 3, IsPublic = false, StateId = 5 },
                        new Institution() { Id = 377, Active = true, Name = "Corner Stone College of Education, lagos ", InstitutionCategoryId = 3, IsPublic = false, StateId = 25 },
                        new Institution() { Id = 378, Active = true, Name = "Peaceland College of Education, Enugu	 ", InstitutionCategoryId = 3, IsPublic = false, StateId = 14 },
                        new Institution() { Id = 379, Active = true, Name = "The College of Education, Nsukka, Enugu	 ", InstitutionCategoryId = 3, IsPublic = false, StateId = 14 },
                        new Institution() { Id = 380, Active = true, Name = "Unity College of Education, Auka Adoka, Benue ", InstitutionCategoryId = 3, IsPublic = false, StateId = 7 },
                        new Institution() { Id = 381, Active = true, Name = "DIAMOND COLLEGE OF EDUCATION, ABA ", InstitutionCategoryId = 3, IsPublic = false, StateId = 1 },
                        new Institution() { Id = 382, Active = true, Name = "Kinsey College of Education, Ilorin, Kwara State ", InstitutionCategoryId = 3, IsPublic = false, StateId = 24 },
                        new Institution() { Id = 383, Active = true, Name = "ECWA College of Education, Jos  ", InstitutionCategoryId = 3, IsPublic = false, StateId = 32 },
                        new Institution() { Id = 384, Active = true, Name = "Yaba College of Technology, Lagos", InstitutionCategoryId = 2, IsPublic = true, StateId = 25 },
                        new Institution() { Id = 385, Active = true, Name = "Air Force Institute of Technology, Kaduna", InstitutionCategoryId = 1, IsPublic = true, StateId = 19 }



                        );

            modelBuilder.Entity<InstitutionCategory>().HasData(
                new InstitutionCategory() { Id = 1, Active = true, Name = "University" },
                new InstitutionCategory() { Id = 2, Active = true, Name = "Polytechnic" },
                new InstitutionCategory() { Id = 3, Active = true, Name = "College Of Education" });
            modelBuilder.Entity<Type>().HasData(
                new Type() { Id = 1, Active = true, Name = "ConceptNote" },
                new Type() { Id = 2, Active = true, Name = "Proposal" });
            modelBuilder.Entity<Title>().HasData(
                new Title() { Id = 1, Active = true, Name = "Prof." },
                new Title() { Id = 2, Active = true, Name = "Dr." },
                new Title() { Id = 3, Active = true, Name = "Mr." },
                new Title() { Id = 4, Active = true, Name = "Mrs." },
                new Title() { Id = 5, Active = true, Name = "Ms." },
                new Title() { Id = 6, Active = true, Name = "Miss." });
            modelBuilder.Entity<Qualification>().HasData(
                new Qualification() { Id = 1, Active = true, Name = "B.A" },
                new Qualification() { Id = 2, Active = true, Name = "BSc." },
                new Qualification() { Id = 3, Active = true, Name = "HND." },
                new Qualification() { Id = 4, Active = true, Name = "M.A" },
                new Qualification() { Id = 5, Active = true, Name = "MSc." },
                new Qualification() { Id = 6, Active = true, Name = "OND." },
                new Qualification() { Id = 7, Active = true, Name = "PhD." },
            new Qualification() { Id = 8, Active = true, Name = "Others." });

            modelBuilder.Entity<AssessmentGuide>().HasData(
                new AssessmentGuide() { Id = 1, Active = true, Name = "Title", MaxScore = 1 },
                new AssessmentGuide() { Id = 2, Active = true, Name = "Executive Summary", MaxScore = 4 },
                new AssessmentGuide() { Id = 3, Active = true, Name = "Project Team", MaxScore = 10 },
                new AssessmentGuide() { Id = 4, Active = true, Name = "Project Profile", MaxScore = 15 },
                new AssessmentGuide() { Id = 5, Active = true, Name = "Justification for Project", MaxScore = 10 },
                new AssessmentGuide() { Id = 6, Active = true, Name = "Strategies for Implementation", MaxScore = 25 },
                new AssessmentGuide() { Id = 7, Active = true, Name = "Time Frame (Phases/Time Lines)", MaxScore = 5 },
                new AssessmentGuide() { Id = 8, Active = true, Name = "Environment/Other Social Issues", MaxScore = 5 },
                new AssessmentGuide() { Id = 9, Active = true, Name = "Resource Implications", MaxScore = 22 },
                new AssessmentGuide() { Id = 10, Active = true, Name = "Ethic Committee", MaxScore = 3 });

            //modelBuilder.Entity<Session>().HasData(
            //    new Session() { Id = 1, Active = false, Name = "2017." },
            //    new Session() { Id = 1, Active = false, Name = "2018." },
            //    new Session() { Id = 2, Active = true, Name = "2019." },
            //    new Session() { Id = 1, Active = false, Name = "2020." }
            //    );
            modelBuilder.Entity<Sex>().HasData(
              new Sex() { Id = 1, Active = true, Name = "Male" },
              new Sex() { Id = 2, Active = true, Name = "Female" }
              );
            modelBuilder.Entity<GeoPoliticalZone>().HasData(
              new GeoPoliticalZone() { Id = 1, Active = true, Name = "North Central" },
              new GeoPoliticalZone() { Id = 2, Active = true, Name = "North East" },
              new GeoPoliticalZone() { Id = 3, Active = true, Name = "North West" },
              new GeoPoliticalZone() { Id = 4, Active = true, Name = "South East" },
              new GeoPoliticalZone() { Id = 5, Active = true, Name = "South South" },
              new GeoPoliticalZone() { Id = 6, Active = true, Name = "South West" }

              );
            modelBuilder.Entity<DefaultMessage>().HasData(
              new DefaultMessage() { Id = 1, Active = true, isProposal = true, ProposalApprove=false, Message= "Thank you for applying to the National Research Fund. Unfortunately, your team did not make the cut for this funding round." +
"We received a large number of submissions this year,"+
                  "and we have spent the last few weeks reviewing over 1000 applications from some of the brightest minds across Nigeria.Sadly,"+
                  "taking the availablilty of funds into cognizance we are limited by the number of submissions we can accept.This was a really hard decision,"+
                  "and we hope that you do not get discouraged by it."+

"Thanks again for taking the time out to apply, and we wish your team continued success as you keep building innovative and enterprising solutions to critical problems on the continent." }
              );

            modelBuilder.Entity<State>().HasData(
             new State() { Id = 1, Active = true, Name = "Abia", GeoPoliticalZoneId = 4 },
             new State() { Id = 2, Active = true, Name = "Adamawa", GeoPoliticalZoneId = 2 },
             new State() { Id = 3, Active = true, Name = "Akwa-Ibom", GeoPoliticalZoneId = 5 },
             new State() { Id = 4, Active = true, Name = "Anambra", GeoPoliticalZoneId = 4 },
             new State() { Id = 5, Active = true, Name = "Bauchi", GeoPoliticalZoneId = 2 },
             new State() { Id = 6, Active = true, Name = "Bayelsa", GeoPoliticalZoneId = 5 },
             new State() { Id = 7, Active = true, Name = "Benue", GeoPoliticalZoneId = 1 },
             new State() { Id = 8, Active = true, Name = "Borno", GeoPoliticalZoneId = 2 },
             new State() { Id = 9, Active = true, Name = "Cross-Rivers", GeoPoliticalZoneId = 5 },
             new State() { Id = 10, Active = true, Name = "Delta", GeoPoliticalZoneId = 5 },
             new State() { Id = 11, Active = true, Name = "Ebonyi", GeoPoliticalZoneId = 4 },
             new State() { Id = 12, Active = true, Name = "Edo", GeoPoliticalZoneId = 5 },
             new State() { Id = 13, Active = true, Name = "Ekiti", GeoPoliticalZoneId = 6 },
             new State() { Id = 14, Active = true, Name = "Enugu", GeoPoliticalZoneId = 4 },
             new State() { Id = 15, Active = true, Name = "FCT Abuja", GeoPoliticalZoneId = 1 },
             new State() { Id = 16, Active = true, Name = "Gombe", GeoPoliticalZoneId = 2 },
             new State() { Id = 17, Active = true, Name = "Imo", GeoPoliticalZoneId = 5 },
             new State() { Id = 18, Active = true, Name = "Jigawa", GeoPoliticalZoneId = 3 },
             new State() { Id = 19, Active = true, Name = "Kaduna", GeoPoliticalZoneId = 3 },
             new State() { Id = 20, Active = true, Name = "Kano", GeoPoliticalZoneId = 3 },
             new State() { Id = 21, Active = true, Name = "Katsina", GeoPoliticalZoneId = 3 },
             new State() { Id = 22, Active = true, Name = "Kebbi", GeoPoliticalZoneId = 3 },
             new State() { Id = 23, Active = true, Name = "Kogi", GeoPoliticalZoneId = 1 },
             new State() { Id = 24, Active = true, Name = "Kwara", GeoPoliticalZoneId = 1 },
             new State() { Id = 25, Active = true, Name = "Lagos", GeoPoliticalZoneId = 6 },
             new State() { Id = 26, Active = true, Name = "Nassarawa", GeoPoliticalZoneId = 1 },
             new State() { Id = 27, Active = true, Name = "Niger", GeoPoliticalZoneId = 1 },
             new State() { Id = 28, Active = true, Name = "Ogun", GeoPoliticalZoneId = 6 },
             new State() { Id = 29, Active = true, Name = "Ondo", GeoPoliticalZoneId = 6 },
             new State() { Id = 30, Active = true, Name = "Osun", GeoPoliticalZoneId = 6 },
             new State() { Id = 31, Active = true, Name = "Oyo", GeoPoliticalZoneId = 6 },
             new State() { Id = 32, Active = true, Name = "Plateau", GeoPoliticalZoneId = 1 },
             new State() { Id = 33, Active = true, Name = "Rivers", GeoPoliticalZoneId = 5 },
             new State() { Id = 34, Active = true, Name = "Sokoto", GeoPoliticalZoneId = 3 },
             new State() { Id = 35, Active = true, Name = "Taraba", GeoPoliticalZoneId = 2 },
             new State() { Id = 36, Active = true, Name = "Yobe", GeoPoliticalZoneId = 2 },
             new State() { Id = 37, Active = true, Name = "Zamfara", GeoPoliticalZoneId = 3 },
             new State() { Id = 38, Active = true, Name = "Nigeria", GeoPoliticalZoneId = 3 }
             );

            //EventType
            modelBuilder.Entity<EventType>().HasData(
                new EventType()
                {
                    Id = 1,
                    Name = "Concept Note Submission",
                    Active = true
                },

                new EventType()
                {
                    Id = 2,
                    Name = "Proposal Submission",
                    Active = true
                },
                 new EventType()
                 {
                     Id = 3,
                     Name = "Assign Assessor - Concept Note",
                     Active = true
                 },
                  new EventType()
                  {
                      Id = 4,
                      Name = "Assessment of Concept Note",
                      Active = true
                  },
                   new EventType()
                   {
                       Id = 5,
                       Name = "Approval/Rejection of Concept Note",
                       Active = true
                   },
                    new EventType()
                    {
                        Id = 6,
                        Name = "Set Budget",
                        Active = true
                    },
                    new EventType()
                    {
                        Id = 7,
                        Name = "Assign Assessor - Full Proposal",
                        Active = true
                    },
                    new EventType()
                    {
                        Id = 8,
                        Name = "Assessment of Full Proposal",
                        Active = true
                    },
                    new EventType()
                    {
                        Id = 9,
                        Name = "Approval/Rejection of Full Proposal",
                        Active = true
                    },
                    new EventType()
                    {
                        Id = 10,
                        Name = "System Setup",
                        Active = true
                    }
            );

            //MenuGroup
            modelBuilder.Entity<Menu_Group>().HasData(
                new Menu_Group()
                {
                    Id = 1,
                    Name = "Concept Note",
                    Active = true
                },
                new Menu_Group()
                {
                    Id = 2,
                    Name = "Full Proposal",
                    Active = true
                },
                new Menu_Group()
                {
                    Id = 3,
                    Name = "Assessor",
                    Active = true
                },
                 new Menu_Group()
                 {
                     Id = 4,
                     Name = "Setup",
                     Active = true
                 },
                 new Menu_Group()
                 {
                     Id = 5,
                     Name = "Report",
                     Active = true
                 },
                 new Menu_Group()
                 {
                     Id = 6,
                     Name = "Statistics",
                     Active = true
                 },
                 new Menu_Group()
                 {
                     Id = 7,
                     Name = "Manage Menu",
                     Active = true
                 },
                  new Menu_Group()
                  {
                      Id = 8,
                      Name = "Profile",
                      Active = true
                  },
                   new Menu_Group()
                   {
                       Id = 9,
                       Name = "Budget",
                       Active = true
                   },
                  new Menu_Group()
                  {
                      Id = 10,
                      Name = "Bulletin Board",
                      Active = true
                  },
                   new Menu_Group()
                   {
                       Id = 11,
                       Name = "Researchers",
                       Active = true
                   },
                   new Menu_Group()
                   {
                       Id = 12,
                       Name = "Research Proposals",
                       Active = true
                   },
                    new Menu_Group()
                    {
                        Id = 13,
                        Name = "Dashboard",
                        Active = true
                    },
                    new Menu_Group()
                    {
                        Id = 14,
                        Name = "User Manual",
                        Active = true
                    },
                    new Menu_Group()
                    {
                        Id = 15,
                        Name = "Research Cycle Events",
                        Active = true
                    }
            );

            //Rank
            modelBuilder.Entity<Rank>().HasData(
                new Rank()
                {
                    Id = 1,
                    Name = "Chief Lecturer",
                    Active = true
                },
                new Rank()
                {
                    Id = 2,
                    Name = "Professor",
                    Active = true
                },
                new Rank()
                {
                    Id = 3,
                    Name = "Reader/Associate Prof.",
                    Active = true
                },
                 new Rank()
                 {
                     Id = 4,
                     Name = "Senior Lecturer",
                     Active = true
                 },
                 new Rank()
                 {
                     Id = 5,
                     Name = "Principal Lecturer",
                     Active = true
                 }

            );
        }   

        public DbSet<Applicant> Applicant { get; set; }
        public DbSet<Reviewer> Reviewer { get; set; }
        public DbSet<Team> Team { get; set; }
        public DbSet<Theme> Theme { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<Person> Person { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<ApplicantReviewer> ApplicantReviewer { get; set; }
        public DbSet<ApplicantThematic> ApplicantThematic { get; set; }
        public DbSet<Score> Score { get; set; }
        public DbSet<ConceptNote> ConceptNote { get; set; }
        public DbSet<Proposal> Proposal { get; set; }
        public DbSet<Approval> Approval { get; set; }
        public DbSet<Institution> Institution { get; set; }
        public DbSet<Budget> Budget { get; set; }
        public DbSet<InstitutionCategory> InstitutionCategory { get; set; }
        public DbSet<Qualification> Qualification { get; set; }
        public DbSet<AssessmentGuide> AssessmentGuide { get; set; }
        public DbSet<ProposalAssessment> ProposalAssessment { get; set; }
        public DbSet<ApplicantSubmission> ApplicantSubmission { get; set; }
        public DbSet<ProposalScoring> ProposalScoring { get; set; }
        public DbSet<AdminSettings> AdminSettings { get; set; }
        public DbSet<ReviewerThematicStrenght> ReviewerThematicStrenght { get; set; }
        public DbSet<BudgetItem> BudgetItem { get; set; }
        public DbSet<FundingSource> FundingSource { get; set; }
        public DbSet<Quarter> Quarter { get; set; }
        public DbSet<ProjectActivities> ProjectActivities { get; set; }
        public DbSet<TimeFrame> TimeFrame { get; set; }
        public DbSet<BudgetDetail> BudgetDetail { get; set; }
        public DbSet<QuaterData> QuaterData { get; set; }
        public DbSet<Type> Type { get; set; }
        public DbSet<Title> Title { get; set; }
        public DbSet<Session> Session { get; set; }
        public DbSet<FinalProposalAproval> FinalProposalAproval { get; set; }
        public DbSet<Sex> Sex { get; set; }
        public DbSet<State> State { get; set; }
        public DbSet<GeoPoliticalZone> GeoPoliticalZone { get; set; }
        public DbSet<DefaultMessage> DefaultMessage { get; set; }
        public DbSet<Menu> Menu { get; set; }

        public DbSet<Menu_Group> MenuGroup { get; set; }

        public DbSet<Menu_In_Role> MenuInRole { get; set; }
        public DbSet<BulletinTopic> BulletinTopic { get; set; }
        public DbSet<BulletinResponse> BulletinResponse { get; set; }

        public DbSet<EventType> EventType { get; set; }
        public DbSet<ResearchCycleEvent> ResearchCycleEvent { get; set; }
        public DbSet<CategoryAdministrator> CategoryAdministrator { get; set; }
        public DbSet<Rank> Rank { get; set; }
        public DbSet<ActiveReviewer> ActiveReviewer { get; set; }
        public DbSet<Reconciliation> Reconciliation { get; set; }
    }
}
