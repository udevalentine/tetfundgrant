﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models.Interface
{
    public interface IMenuDisplay
    {
        string GetUserRole(string userName,bool session);
        List<Menu> GetMenuList(string role);
    }
}
