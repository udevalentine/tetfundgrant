﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class ApplicantThematic
    {
        public int Id { get; set; }
        public int ApplicantId { get; set; }
        public int ThemeId { get; set; }
        public string Title { get; set; }
        public int SessionId { get; set; }
        public DateTime DateApplied { get; set; }
        public string FileNo { get; set; }

        public virtual Session Session { get; set; }
        public virtual Theme Theme { get; set; }
        public virtual Applicant Applicant { get; set; }
        public virtual ICollection<ApplicantReviewer> ApplicantReviewer { get; set; }
        public virtual ICollection<TimeFrame> TimeFrame { get; set; }
        public virtual ICollection<ProjectActivities> ProjectActivities { get; set; }
    }
}
