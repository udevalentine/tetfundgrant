﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Models
{
    public class Menu
    {
        public int Id { get; set; }

        public string Controller { get; set; }

        public string ActionName { get; set; }

        public string DisplayName { get; set; }

        public int Menu_GroupId { get; set; }
        public bool Active { get; set; }

        public virtual Menu_Group MenuGroup { get; set; }
    }
}
