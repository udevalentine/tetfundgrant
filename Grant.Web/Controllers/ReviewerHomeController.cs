﻿using Grant.Web.Infrastructure;
using Grant.Web.Models;
using Grant.Web.Models.ViewModels;
using ICSharpCode.SharpZipLib.Zip;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;

namespace Grant.Web.Controllers
{
    [Authorize(Policy = "MustBeReviewer")]
    public class ReviewerHomeController : BaseController
    {
        private readonly DataBaseContext _context;
        AdminViewModel _adminViewModel;
        Utility _utility;
        private const string Value = "Id";
        private const string Text = "Name";
        private IHostingEnvironment _hostingEnvironment;
        private IConfiguration _configuration;
        private string baseUrl;
        public ReviewerHomeController(DataBaseContext context, IHostingEnvironment hostingEnvironment, Utility utility, IConfiguration configuration)
        {
            _context = context;
            _adminViewModel = new AdminViewModel();
            _adminViewModel.PopulateAllDropdown(_context);
            _hostingEnvironment = hostingEnvironment;
            _utility = utility;
            _configuration = configuration;
            baseUrl = _configuration.GetValue<string>("Url:onlineVerificationRoot");


        }
        public IActionResult Index()
        {
            try
            {
                List<ProposalAssessment> proposalAssessment = new List<ProposalAssessment>();
                List<ApplicantReviewer> applicantReviewerList = new List<ApplicantReviewer>();
                string reviewerEmail = User.FindFirstValue(ClaimTypes.Email);
                var reviewer = _context.Reviewer.Where(d => d.Email == reviewerEmail).FirstOrDefault();
                var reviewerProposalAssessment = _context.ProposalAssessment.Where(f => f.ReviewerId == reviewer.Id && f.ApplicantThematic.Session.ActiveForApplication && !f.IsReconcilled).ToList();
                if (reviewerProposalAssessment.Count > 0)
                {
                    for (int i = 0; i < reviewerProposalAssessment.Count; i++)
                    {
                        var isSubmitted = AssessmentIsSubmitted(reviewerProposalAssessment[i].ApplicantThematicId, reviewer.Id, (int)Types.Proposal);
                        //only submitted assessments are considered
                        if (isSubmitted)
                        {
                            int Id = reviewerProposalAssessment[i].ApplicantThematicId;
                            var myProposalAssessment = reviewerProposalAssessment[i];
                            var allMyProposalAssessment = _context.ProposalAssessment.Where(h => h.ApplicantThematicId == Id).ToList();
                            var orderedScore = allMyProposalAssessment.OrderBy(O => O.Score).Select(o => o.Score).ToList();
                            var orderedMax = orderedScore.LastOrDefault();
                            var orderedMin = orderedScore.FirstOrDefault();
                            var scoreDifference = orderedMax - orderedMin;
                            //filter the proposals that require reconcillation
                            if (scoreDifference > 10)
                            {
                                proposalAssessment.Add(myProposalAssessment);
                            }
                        }
                      
                    }
                }
                var applicantReviewer = _context.ApplicantReviewer.Where(d => d.ReviewerId == reviewer.Id && d.TypeId == (int)Types.ConceptNote && d.ApplicantThematic.Session.ActiveForApplication && !d.IsReconciled && d.ConceptNoteMark!=null).ToList();
                if (applicantReviewer?.Count > 0)
                {
                    foreach (var item in applicantReviewer)
                    {

                        var isSubmitted=AssessmentIsSubmitted(item.ApplicantThematicId, reviewer.Id, (int)Types.ConceptNote);
                        //only submitted assessments are considered
                        if (isSubmitted)
                        {
                            var allApplicantThematicRecore = _context.ApplicantReviewer.Where(f => f.ApplicantThematicId == item.ApplicantThematicId && f.TypeId == (int)Types.ConceptNote).ToList();
                            var orderedScore = allApplicantThematicRecore.Where(f => f.ConceptNoteMark != null).OrderBy(f => f.ConceptNoteMark).Select(f => f.ConceptNoteMark).ToList();
                            var orderedMax = orderedScore.LastOrDefault();
                            var orderedMin = orderedScore.FirstOrDefault();
                            var scoreDifference = orderedMax - orderedMin;
                            //filter the Concept Notes that require reconcillation
                            if (scoreDifference > 10)
                            {
                                applicantReviewerList.Add(item);
                            }
                        }
                       
                    }
                }
                _adminViewModel.ProposalAssessments = proposalAssessment;
                _adminViewModel.ApplicantReviewers = applicantReviewerList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult ViewCoReviewer(string id)
        {
            try
            {
                string reviewerEmail = User.FindFirstValue(ClaimTypes.Email);
                int applicantthematicId = Convert.ToInt32(Utility.Decrypt(id));
                var reviewer = _context.Reviewer.Where(d => d.Email == reviewerEmail).FirstOrDefault();
                _adminViewModel.ProposalAssessments = _context.ProposalAssessment.Where(t => t.ApplicantThematicId == applicantthematicId && t.ReviewerId != reviewer.Id).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult ViewCoReviewerConceptNote(string id)
        {
            List<ReviewerScore> reviewerScoreList = new List<ReviewerScore>();
            try
            {
                string reviewerEmail = User.FindFirstValue(ClaimTypes.Email);
                var reviewer = _context.Reviewer.Where(d => d.Email == reviewerEmail).FirstOrDefault();
                int applicantthematicId = Convert.ToInt32(Utility.Decrypt(id));
                _adminViewModel.ApplicantReviewers = _context.ApplicantReviewer.Where(t => t.ApplicantThematicId == applicantthematicId && t.ReviewerId != reviewer.Id && t.TypeId==(int)Types.ConceptNote).ToList();
                if (_adminViewModel.ApplicantReviewers?.Count > 0)
                {
                    for(int i=0;i< _adminViewModel.ApplicantReviewers.Count; i++)
                    {
                        ReviewerScore reviewerScore = new ReviewerScore();

                        var reviewerId = _adminViewModel.ApplicantReviewers[i].ReviewerId;

                        reviewerScore.Reviewer= _context.Reviewer.Where(f => f.Id == reviewerId).FirstOrDefault();
                        reviewerScore.Score = Convert.ToString(_adminViewModel.ApplicantReviewers[i].ConceptNoteMark);
                        reviewerScore.ApplicantThematic = _adminViewModel.ApplicantReviewers[i].ApplicantThematic;
                        reviewerScore.Comment=_context.ProposalScoring.Where(d => d.ApplicantThematicId == applicantthematicId && d.ReviewerId == reviewerId && d.TypeId == (int)Types.ConceptNote).Select(d=>d.Remark).FirstOrDefault();
                        reviewerScoreList.Add(reviewerScore);
                    }
                }
                _adminViewModel.ReviewerScores = reviewerScoreList;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult ViewMyApplicantConceptNote()
        {
            try
            {
                List<ApplicationList> list = new List<ApplicationList>();
                
                string reviewerEmail = User.FindFirstValue(ClaimTypes.Email);
                var reviewer = _context.Reviewer.Where(d => d.Email == reviewerEmail).FirstOrDefault();
                if (reviewer != null)
                {
                    var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                    if (activeSessionForApplication == null)
                    {
                        SetMessage("No Active Research Cycle at the Moment.");
                        return RedirectToAction("Index");
                    }
                    var listOfApplicant = _context.ApplicantReviewer.Where(d => d.ReviewerId == reviewer.Id && d.TypeId == (int)Types.ConceptNote && d.ApplicantThematic.SessionId== activeSessionForApplication.Id).ToList();
                    if (listOfApplicant.Count > 0)
                    {
                        for (int i = 0; i < listOfApplicant.Count; i++)
                        {
                            ApplicationList applicationList = new ApplicationList();
                            Category category = new Category();
                            var applicantthematicId = listOfApplicant[i].ApplicantThematicId;
                            var applicantThematic = _context.ApplicantThematic.Where(g => g.Id == applicantthematicId).FirstOrDefault();
                            var applicant = _context.Applicant.Where(f => f.Id == applicantThematic.ApplicantId).FirstOrDefault();
                            var person = _context.Person.Where(k => k.Id == applicant.PersonId).FirstOrDefault();
                            var theme = _context.Theme.Where(j => j.Id == applicantThematic.ThemeId).FirstOrDefault();
                            applicationList.ApplicantReviewer = _context.ApplicantReviewer.Where(d => d.ReviewerId == reviewer.Id && d.ApplicantThematicId == applicantthematicId && d.TypeId == (int)Types.ConceptNote).FirstOrDefault();
                            if (theme != null)
                            {
                                category = _context.Category.Where(u => u.Id == theme.CategoryId).FirstOrDefault();
                            }
                            applicationList.ApplicantThematic = applicantThematic;
                            applicationList.Person = person;
                            applicationList.Theme = theme;
                            applicationList.Category = category;
                            // Check that application has upto two reviewers
                            if (applicationList.ApplicantReviewer != null)
                            {
                                var applicantReviewer = _context.ApplicantReviewer.Where(f => f.ApplicantThematicId == applicationList.ApplicantReviewer.ApplicantThematicId).ToList();
                                if (applicantReviewer.Count > 1)
                                {
                                    list.Add(applicationList);
                                }


                            }


                            //var proposalScore=_context.ProposalScoring.Where(f => f.ApplicantThematicId == applicantthematicId && f.ReviewerId == reviewer.Id).FirstOrDefault();
                            //if (proposalScore != null)
                            //{
                            //   applicationList.ProposalScore= _utility.SumProposalScore(proposalScore);
                            //}
                            //else
                            //{
                            //    applicationList.ProposalScore = 0;
                            //}

                        }
                    }
                }
                _adminViewModel.ApplicationList = list;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult ViewMyApplicantProposal()
        {
            try
            {
                List<ApplicationList> list = new List<ApplicationList>();
                string reviewerEmail = User.FindFirstValue(ClaimTypes.Email);
                var reviewer = _context.Reviewer.Where(d => d.Email == reviewerEmail).FirstOrDefault();
                if (reviewer != null)
                {
                    var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                    if (activeSessionForApplication == null)
                    {
                        SetMessage("No Active Research Cycle at the Moment.");
                        return RedirectToAction("Index");
                    }
                    var myApplicantList = _context.ApplicantReviewer.Where(f => f.ReviewerId == reviewer.Id && f.TypeId == (int)Types.Proposal && f.ApplicantThematic.Session.Id== activeSessionForApplication.Id).ToList();
                    if (myApplicantList.Count > 0)
                    {
                        for (int i = 0; i < myApplicantList.Count; i++)
                        {
                            ApplicationList applicationList = new ApplicationList();
                            applicationList.ApplicantReviewer = myApplicantList[i];
                            applicationList.ApplicantSubmission = _context.ApplicantSubmission.Where(f => f.ApplicantThematicId == applicationList.ApplicantReviewer.ApplicantThematicId && f.TypeId == (int)Types.Proposal).FirstOrDefault();
                            var proposalScore = _context.ProposalScoring.Where(f => f.ApplicantThematicId == applicationList.ApplicantReviewer.ApplicantThematicId && f.ReviewerId == reviewer.Id && f.TypeId == (int)Types.Proposal).FirstOrDefault();
                            applicationList.proposalScoring = proposalScore;
                            if (proposalScore != null)
                            {
                                applicationList.ProposalScore = _utility.SumProposalScore(proposalScore);
                            }
                            else
                            {
                                applicationList.ProposalScore = 0;
                            }
                            // Check that application has upto two reviewers
                            if (applicationList.ApplicantReviewer != null)
                            {
                                var applicantReviewer = _context.ApplicantReviewer.Where(f => f.ApplicantThematicId == applicationList.ApplicantReviewer.ApplicantThematicId).ToList();
                                if (applicantReviewer != null && applicantReviewer.Count > 1)
                                {
                                    list.Add(applicationList);
                                }


                            }
                            //list.Add(applicationList);
                        }

                        _adminViewModel.ApplicationList = list;
                    }
                    else
                    {
                        _adminViewModel.ApplicationList = new List<ApplicationList>();
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult ReviewConceptNote(string id)
        {
            try
            {
                
                if (!_utility.CheckResearchCycleEvent((int)EventTypes.ReviewConceptNote))
                {
                    SetMessage("Concept Note cannot be assessed at this time.");
                    return RedirectToAction("ViewMyApplicantConceptNote", "ReviewerHome");
                }
                if (!String.IsNullOrEmpty(id))
                {
                    int applicantthematicId = Convert.ToInt32(Utility.Decrypt(id));
                    List<ReviewerAssignedScore> reviewerList = new List<ReviewerAssignedScore>();
                    List<Person> personTeamList = new List<Person>();
                    string reviewerEmail = User.FindFirstValue(ClaimTypes.Email);
                    var reviewer = _context.Reviewer.Where(d => d.Email == reviewerEmail).FirstOrDefault();
                    var applicantThematic = _context.ApplicantThematic.Where(f => f.Id == applicantthematicId).FirstOrDefault();
                    var isSubmitted=_utility.IsAssessmentSubmitted(applicantthematicId, (int)Types.ConceptNote, reviewer.Id);
                    if (isSubmitted)
                    {
                        SetMessage("Can not make modifications to already submitted assessment");
                        return RedirectToAction("ViewMyApplicantConceptNote");
                    }
                    var approval = _context.Approval.Where(y => y.ApplicantThematicId == applicantthematicId && y.IsProposal == false).FirstOrDefault();
                    if (approval?.Approved == true)
                    {
                        SetMessage("Can not assess already approved Concept Note");
                        return RedirectToAction("ViewMyApplicantConceptNote");

                    }
                    if (applicantThematic?.Id > 0)
                    {
                        //var scores = _context.ProposalScoring.Where(f => f.ApplicantThematicId == applicantThematic.Id && f.ReviewerId == reviewer.Id && f.TypeId == (int)Types.ConceptNote).FirstOrDefault();

                        var applicant = _context.Applicant.Where(h => h.Id == applicantThematic.ApplicantId).FirstOrDefault();
                        var person = _context.Person.Where(f => f.Id == applicant.PersonId).FirstOrDefault();

                        var existingConceptNote = _context.ApplicantSubmission.Where(g => g.ApplicantThematicId == applicantThematic.Id && g.TypeId == (int)Types.ConceptNote).FirstOrDefault();
                        if (existingConceptNote != null)
                        {
                            _adminViewModel.ApplicantSubmission = existingConceptNote;
                            _adminViewModel.BudgetDetails = _context.BudgetDetail.Where(f => f.ApplicantThematicId == applicantThematic.Id && f.TypeId == (int)Types.ConceptNote).ToList();
                            if (_adminViewModel.BudgetDetails.Count > 0)
                            {

                                _adminViewModel.BudgetDetailsSectionOne = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.PersonnelAllowances).ToList();


                                _adminViewModel.BudgetDetailsSectionFour = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.DataCollectionandAnalysis).ToList();
                                _adminViewModel.BudgetDetailsSectionThree = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.SuppliesConsumables).ToList();
                                _adminViewModel.BudgetDetailsSectionTwo = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Equipment).ToList();
                                _adminViewModel.BudgetDetailsSectionFive = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Travel).ToList();
                                _adminViewModel.BudgetDetailsSectionSix = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Dissemination).ToList();
                                _adminViewModel.BudgetDetailsSectionSeven = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Other).ToList();
                                _adminViewModel.SectionPercentage1 = Math.Round(((_adminViewModel.BudgetDetailsSectionOne.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                                _adminViewModel.SectionPercentage2 = Math.Round(((_adminViewModel.BudgetDetailsSectionTwo.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                                _adminViewModel.SectionPercentage3 = Math.Round(((_adminViewModel.BudgetDetailsSectionThree.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                                _adminViewModel.SectionPercentage4 = Math.Round(((_adminViewModel.BudgetDetailsSectionFour.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                                _adminViewModel.SectionPercentage5 = Math.Round(((_adminViewModel.BudgetDetailsSectionFive.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                                _adminViewModel.SectionPercentage6 = Math.Round(((_adminViewModel.BudgetDetailsSectionSix.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                                _adminViewModel.SectionPercentage7 = Math.Round(((_adminViewModel.BudgetDetailsSectionSeven.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            }
                            else
                            {

                                _adminViewModel.BudgetDetailsSectionOne = new List<BudgetDetail>();
                                _adminViewModel.BudgetDetailsSectionFour = new List<BudgetDetail>();
                                _adminViewModel.BudgetDetailsSectionThree = new List<BudgetDetail>();
                                _adminViewModel.BudgetDetailsSectionTwo = new List<BudgetDetail>();
                                _adminViewModel.BudgetDetailsSectionFive = new List<BudgetDetail>();
                                _adminViewModel.BudgetDetailsSectionSix = new List<BudgetDetail>();
                                _adminViewModel.BudgetDetailsSectionSeven = new List<BudgetDetail>();
                            }

                        }

                        var myScore = _context.ProposalScoring.Where(f => f.ApplicantThematicId == applicantThematic.Id && f.ReviewerId == reviewer.Id && f.TypeId == (int)Types.ConceptNote).FirstOrDefault();
                        _adminViewModel.ProposalScoring = myScore != null ? myScore : new ProposalScoring();

                        if (applicant != null)
                        {
                            _adminViewModel.ApplicantThematic = _context.ApplicantThematic.Where(d => d.ApplicantId == applicant.Id).FirstOrDefault();
                            _adminViewModel.Theme = _context.Theme.Where(f => f.Id == _adminViewModel.ApplicantThematic.ThemeId).FirstOrDefault();
                            _adminViewModel.Category = _context.Category.Where(f => f.Id == _adminViewModel.Theme.CategoryId).FirstOrDefault();
                            _adminViewModel.Person = person;
                            var teamList = _context.Team.Where(f => f.ApplicantThematicId == applicantThematic.Id).ToList();
                            if (teamList.Count > 0)
                            {
                                for (int j = 0; j < teamList.Count; j++)
                                {
                                    var teamPersonId = teamList[j].PersonId;
                                    var singleTeamPerson = _context.Person.Where(c => c.Id == teamPersonId).FirstOrDefault();
                                    personTeamList.Add(singleTeamPerson);
                                }
                            }
                            _adminViewModel.TeamMember = personTeamList;
                            if (_adminViewModel.ApplicantThematic != null)
                            {
                                _adminViewModel.ConceptNote = _context.ConceptNote.Where(f => f.ApplicantThemeId == _adminViewModel.ApplicantThematic.Id).FirstOrDefault();

                                _adminViewModel.ApplicantReviewer = _context.ApplicantReviewer.Where(f => f.ApplicantThematicId == _adminViewModel.ApplicantThematic.Id && f.ReviewerId == reviewer.Id).FirstOrDefault();

                                //if (_adminViewModel.ApplicantReviewers.Count > 0)
                                //{
                                //    for (int i = 0; i < _adminViewModel.ApplicantReviewers.Count; i++)
                                //    {
                                //        int reviewerId = _adminViewModel.ApplicantReviewers[i].ReviewerId;
                                //        int? scoreId = _adminViewModel.ApplicantReviewers[i].ScoreId;
                                //        var singleReviewer = _context.Reviewer.Where(f => f.Id == reviewerId).FirstOrDefault();
                                //        ReviewerAssignedScore reviewerAssignedScore = new ReviewerAssignedScore();
                                //        reviewerAssignedScore.Reviewer = new Reviewer();
                                //        reviewerAssignedScore.Reviewer.Email = singleReviewer.Email;
                                //        reviewerAssignedScore.Reviewer.Name = singleReviewer.Name;
                                //        reviewerAssignedScore.Score = scoreId == null ? "Not Reviewed Yet" : _context.Score.Where(f => f.Id == scoreId).Select(g => g.Name).FirstOrDefault();

                                //        reviewerList.Add(reviewerAssignedScore);
                                //    }
                                //}
                            }

                            //_adminViewModel.ReviewerAssignedScoreList = reviewerList;

                        }
                        //_adminViewModel.ProposalScoring = scores;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult ScoreConceptNote(AdminViewModel viewModel)
        {
            try
            {
                if (!_utility.CheckResearchCycleEvent((int)EventTypes.ReviewConceptNote))
                {
                    SetMessage("Concept Note cannot be assessed at this time.");
                    return RedirectToAction("ViewMyApplicantConceptNote", "ReviewerHome");
                }

                if (viewModel.ProposalScoring != null && viewModel.ApplicantThematic.Id > 0)
                {
                    //var applicantReview = _context.ApplicantReviewer.Where(y => y.Id == viewModel.ApplicantReviewer.Id).FirstOrDefault();

                    _utility.UpdateConceptNoteAssessment(viewModel.ProposalScoring, viewModel.ApplicantThematic.Id, viewModel.ApplicantReviewer.ReviewerId, (int)Types.ConceptNote, viewModel.ApplicantReviewer.ConceptNoteMark,true);
                    SetMessage("Assessment saved successfully");
                }
                else
                {

                }





            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("ViewMyApplicantConceptNote");
        }
        public IActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        public IActionResult ChangePassword(IFormCollection collection)
        {
            try
            {
                _adminViewModel.CurrentPassword = collection["CurrentPassword"];
                _adminViewModel.NewPassword = collection["NewPassword"];
                _adminViewModel.ConfirmPassword = collection["ConfirmPassword"];
                if (String.IsNullOrEmpty(_adminViewModel.NewPassword) || String.IsNullOrEmpty(_adminViewModel.CurrentPassword) || String.IsNullOrEmpty(_adminViewModel.ConfirmPassword))
                {
                    SetMessage("Please, provide all required fields.");
                    return View();
                }
                string email = User.FindFirstValue(ClaimTypes.Email);
                string role = User.FindFirstValue(ClaimTypes.Role);
                var user = _context.User.Where(d => d.Email == email).FirstOrDefault();
                if (user != null)
                {
                    if (user.Password != _adminViewModel.CurrentPassword)
                    {
                        SetMessage("Your current password dont match, Log out and try again.");
                        return View();
                    }
                    if (_adminViewModel.ConfirmPassword != _adminViewModel.NewPassword)
                    {
                        SetMessage("Your confirm password do not match, try again.");
                        return View();
                    }
                    _utility.ChangePassword(_adminViewModel.CurrentPassword, _adminViewModel.NewPassword, user.UserName, role);
                    SetMessage("Password changed successfully");
                    return RedirectToAction("Logout", "Home");

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View();
        }
        //public IActionResult ProposalAssessment(int applicantthematicId)
        //{
        //    try
        //    {
        //        List<ScoreAssessment> list = new List<ScoreAssessment>();
        //        string reviewerEmail = User.FindFirstValue(ClaimTypes.Email);
        //        var reviewer = _context.Reviewer.Where(d => d.Email == reviewerEmail).FirstOrDefault();
        //        var applicantThematic = _context.ApplicantThematic.Where(f => f.Id == applicantthematicId).FirstOrDefault();
        //        var hasProposal=_context.Proposal.Where(f => f.ApplicantThemeId == applicantthematicId).FirstOrDefault();
        //        if (hasProposal != null)
        //        {
        //            var assessmentguide= _context.AssessmentGuide.Where(f => f.Active).ToList();
        //            if (assessmentguide.Count > 0)
        //            {
        //                for(int i=0; i<assessmentguide.Count; i++)
        //                {
        //                    ScoreAssessment scoreAssessment = new ScoreAssessment();
        //                    var assId = assessmentguide[i].Id;
        //                    scoreAssessment.AssessmentGuide = assessmentguide[i];
        //                    var assessment=_context.ProposalAssessment.Where(g => g.AssessmentGuideId == assId && g.ApplicantThematicId == applicantthematicId && g.ReviewerId==reviewer.Id).FirstOrDefault();
        //                    if (assessment != null)
        //                    {
        //                        scoreAssessment.Score = assessment.Score;
        //                    }
        //                    list.Add(scoreAssessment);
        //                }
        //            }
        //            _adminViewModel.ProposalAssessment= _context.ProposalAssessment.Where(g =>g.ApplicantThematicId == applicantthematicId).FirstOrDefault();
        //            _adminViewModel.ScoreAssessments = list;
        //            _adminViewModel.Proposal = hasProposal;
        //            var applicant= _context.Applicant.Where(g => g.Id == applicantThematic.ApplicantId).FirstOrDefault();
        //            _adminViewModel.Person= _context.Person.Where(f => f.Id == applicant.PersonId).FirstOrDefault();
        //        }
        //        else
        //        {
        //            SetMessage("Applicant has not submitted Proposal");
        //            return RedirectToAction("Index", "ReviewerHome");
        //        }

        //    }
        //    catch(Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return View(_adminViewModel);
        //}

        //[HttpPost]
        //public IActionResult ProposalAssessment(IFormCollection form)
        //{
        //    try
        //    {


        //        var scores = form["item.Score"];
        //        var guideId = form["item.AssessmentGuide.Id"];
        //        var Id=Convert.ToInt32(form["Person.Id"]);

        //        var approve = form["ProposalAssessment.Approved"];
        //        var budget = Convert.ToDecimal(form["ProposalAssessment.ApprovedBudget"]);
        //        var applicant=_context.Applicant.Where(h => h.PersonId == Id).FirstOrDefault();
        //       var applicantThematic =_context.ApplicantThematic.Where(j => j.ApplicantId == applicant.Id).FirstOrDefault();
        //        string reviewerEmail = User.FindFirstValue(ClaimTypes.Email);
        //        var reviewer = _context.Reviewer.Where(d => d.Email == reviewerEmail).FirstOrDefault();
        //        var proposal=_context.Proposal.Where(f => f.ApplicantThemeId == applicantThematic.Id).FirstOrDefault();
        //        for(int i=0; i< guideId.Count; i++)
        //        {
        //            ProposalAssessment assessment = new Models.ProposalAssessment();
        //            assessment.AssessmentGuideId = Convert.ToInt32(guideId[i]);
        //            assessment.Score = Convert.ToDecimal(scores[i]);
        //            var existingAssessment=_context.ProposalAssessment.Where(f => f.AssessmentGuideId == assessment.AssessmentGuideId && f.ApplicantThematicId == applicantThematic.Id && f.ReviewerId == reviewer.Id).FirstOrDefault();
        //            if (existingAssessment != null)
        //            {
        //                existingAssessment.Score = assessment.Score;
        //                existingAssessment.Approved = !string.IsNullOrEmpty(approve)?true:false;
        //                existingAssessment.ApprovedBudget = budget;
        //                existingAssessment.ProposalId = proposal.Id;
        //                _context.Update(existingAssessment);
        //            }
        //            else
        //            {
        //                assessment.ApplicantThematicId = applicantThematic.Id;
        //                assessment.ReviewerId = reviewer.Id;
        //                assessment.ProposalId = proposal.Id;
        //                assessment.Approved = !string.IsNullOrEmpty(approve) ? true : false;
        //                assessment.ApprovedBudget = budget;
        //                _context.Add(assessment);
        //            }



        //        }
        //        _context.SaveChanges();


        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    SetMessage("Proposal Assessment done Successfully");
        //    return RedirectToAction("Index", "ReviewerHome");
        //}
        public IActionResult ProposalReviewAndAssessment(string id)
        {
            try
            {
                if (!_utility.CheckResearchCycleEvent((int)EventTypes.ReviewProposal))
                {
                    SetMessage("Full Proposal cannot be assessed at this time.");
                    return RedirectToAction("ViewMyApplicantProposal", "ReviewerHome");
                }
                if (!String.IsNullOrEmpty(id))
                {
                    int applicantthematicId = Convert.ToInt32(Utility.Decrypt(id));
                    string reviewerEmail = User.FindFirstValue(ClaimTypes.Email);
                    var reviewer = _context.Reviewer.Where(d => d.Email == reviewerEmail).FirstOrDefault();
                    
                    _adminViewModel.ApplicantSubmission = _context.ApplicantSubmission.Where(f => f.ApplicantThematicId == applicantthematicId && f.TypeId == (int)Types.Proposal).FirstOrDefault();
                    _adminViewModel.ProposalScoring = _context.ProposalScoring.Where(f => f.ApplicantThematicId == applicantthematicId && f.ReviewerId == reviewer.Id && f.TypeId==(int)Types.Proposal).FirstOrDefault();
                    if (_adminViewModel.ProposalScoring == null)
                    {
                        _adminViewModel.ProposalScoring = new ProposalScoring();
                    }
                    _adminViewModel.Proposal = _context.Proposal.Where(g => g.ApplicantThemeId == applicantthematicId).FirstOrDefault();
                    _adminViewModel.Reviewer = reviewer;
                    _adminViewModel.BudgetDetails = _context.BudgetDetail.Where(f => f.ApplicantThematicId == applicantthematicId).ToList();
                    _adminViewModel.ProposalAssessment= _context.ProposalAssessment.Where(f => f.ApplicantThematicId == applicantthematicId && f.ReviewerId == reviewer.Id).FirstOrDefault();
                    if (_adminViewModel.ProposalAssessment?.Id>0)
                    {
                        _adminViewModel.ApprovedBudget = _adminViewModel.ProposalAssessment.ApprovedBudget!=null?(decimal)_adminViewModel.ProposalAssessment.ApprovedBudget:0;
                    }
                    if (_adminViewModel.BudgetDetails.Count >= 0)
                    {
                        _adminViewModel.BudgetDetailsSectionOne = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.PersonnelAllowances).ToList();


                        _adminViewModel.BudgetDetailsSectionFour = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.DataCollectionandAnalysis).ToList();
                        _adminViewModel.BudgetDetailsSectionThree = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.SuppliesConsumables).ToList();
                        _adminViewModel.BudgetDetailsSectionTwo = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Equipment).ToList();
                        _adminViewModel.BudgetDetailsSectionFive = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Travel).ToList();
                        _adminViewModel.BudgetDetailsSectionSix = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Dissemination).ToList();
                        _adminViewModel.BudgetDetailsSectionSeven = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Other).ToList();
                        _adminViewModel.SectionPercentage1 = Math.Round(((_adminViewModel.BudgetDetailsSectionOne.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        _adminViewModel.SectionPercentage2 = Math.Round(((_adminViewModel.BudgetDetailsSectionTwo.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        _adminViewModel.SectionPercentage3 = Math.Round(((_adminViewModel.BudgetDetailsSectionThree.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        _adminViewModel.SectionPercentage4 = Math.Round(((_adminViewModel.BudgetDetailsSectionFour.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        _adminViewModel.SectionPercentage5 = Math.Round(((_adminViewModel.BudgetDetailsSectionFive.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        _adminViewModel.SectionPercentage6 = Math.Round(((_adminViewModel.BudgetDetailsSectionSix.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        _adminViewModel.SectionPercentage7 = Math.Round(((_adminViewModel.BudgetDetailsSectionSeven.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                    }
                    else
                    {

                        _adminViewModel.BudgetDetailsSectionOne = new List<BudgetDetail>();
                        _adminViewModel.BudgetDetailsSectionFour = new List<BudgetDetail>();
                        _adminViewModel.BudgetDetailsSectionThree = new List<BudgetDetail>();
                        _adminViewModel.BudgetDetailsSectionTwo = new List<BudgetDetail>();
                        _adminViewModel.BudgetDetailsSectionFive = new List<BudgetDetail>();
                        _adminViewModel.BudgetDetailsSectionSix = new List<BudgetDetail>();
                        _adminViewModel.BudgetDetailsSectionSeven = new List<BudgetDetail>();
                    }




                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        [HttpPost]
        public IActionResult ProposalReviewAndAssessment(AdminViewModel viewModel)
        {
            if (!_utility.CheckResearchCycleEvent((int)EventTypes.ReviewProposal))
            {
                SetMessage("Concept Note cannot be assessed at this time.");
                return RedirectToAction("ViewMyApplicantProposal", "ReviewerHome");
            }
            try
            {
                var existingProposalScoring = _context.ProposalScoring.Where(f => f.ApplicantThematicId == viewModel.ApplicantSubmission.ApplicantThematicId && f.ReviewerId == viewModel.Reviewer.Id).FirstOrDefault();
                var proposal = _context.Proposal.Where(f => f.ApplicantThemeId == viewModel.ApplicantSubmission.ApplicantThematicId).FirstOrDefault();
                _utility.UpdateProposalAssessment(viewModel.ProposalScoring, viewModel.ApplicantSubmission.ApplicantThematicId, viewModel.Reviewer.Id, 2, viewModel.ApprovedBudget, viewModel.Approved, viewModel.Remark, viewModel.ReasonForRejection, proposal.Id);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            SetMessage("Your assessment was successful");
            return RedirectToAction("ViewMyApplicantProposal");
        }
        public IActionResult AddThematicStrenght()
        {
            try
            {
                string reviewerEmail = User.FindFirstValue(ClaimTypes.Email);
                var reviewer = _context.Reviewer.Where(d => d.Email == reviewerEmail).FirstOrDefault();
                if (reviewer?.Id > 0)
                {
                    var existingReviewerStrenght = _utility.GetAssessorsExpertCategory(reviewer.Id);
                    if (existingReviewerStrenght?.Id > 0)
                    {
                        _adminViewModel.PopulateReviewerExpertriateCategory(existingReviewerStrenght.Theme.Category.Id, null);
                    }
                    else
                    {
                        _adminViewModel.PopulateReviewerExpertriateCategory(0, null);
                    }

                }
                _adminViewModel.ReviewerThematicStrenght = _context.ReviewerThematicStrenght.Where(i => i.ReviewerId == reviewer.Id).ToList();



            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult AddStrength(AdminViewModel viewModel)
        {
            try
            {
                string reviewerEmail = User.FindFirstValue(ClaimTypes.Email);
                var reviewer = _context.Reviewer.Where(d => d.Email == reviewerEmail).FirstOrDefault();

                if (viewModel.Strength == 0)
                {
                    SetMessage("Please select valid strenght");
                    return RedirectToAction("AddThematicStrenght");
                }
                var existingStregth = _context.ReviewerThematicStrenght.Where(g => g.ReviewerId == reviewer.Id && g.ThemeId == viewModel.ThemeId).FirstOrDefault();
                if (existingStregth != null)
                {
                      existingStregth.Strenght = viewModel.Strength;
                        _context.Update(existingStregth);
                    
                    _context.Update(existingStregth);
                }
                else
                {
                      ReviewerThematicStrenght reviewerThematicStrenght = new ReviewerThematicStrenght()
                        {
                            ReviewerId = reviewer.Id,
                            Strenght = viewModel.Strength,

                            ThemeId = viewModel.ThemeId
                        };
                        _context.Add(reviewerThematicStrenght);
                    
                }
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("AddThematicStrenght");
        }
        public IActionResult EditStrength(string id)
        {
            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    var reviewerThematicStrenghtId= Convert.ToInt32(Utility.Decrypt(id));
                    //JavaScriptSerializer productModelserializer = new JavaScriptSerializer();
                    //ProductModel productModel = productModelserializer.Deserialize<ProductModel>(productItems);
                    _adminViewModel.ThematicStrenght = _context.ReviewerThematicStrenght.Where(f => f.Id == reviewerThematicStrenghtId).FirstOrDefault();
                    //TempData["strengthObj"] = JsonConvert.SerializeObject(_adminViewModel.ThematicStrenght);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        [HttpPost]
        public IActionResult EditStrength(AdminViewModel viewModel)
        {
            try
            {
                if (viewModel.ThematicStrenght.Id > 0)
                {
                    var existingStrength = _context.ReviewerThematicStrenght.Where(f => f.Id == viewModel.ThematicStrenght.Id).FirstOrDefault();
                    if (existingStrength != null)
                    {
                        existingStrength.Strenght = viewModel.ThematicStrenght.Strenght;
                        if (viewModel.ThematicStrenght.Strenght > 0)
                        {
                            _context.Update(existingStrength);
                        }
                        else
                        {
                            _context.Remove(existingStrength);
                        }
                        
                        _context.SaveChanges();
                        SetMessage("Operation Successful");

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("AddThematicStrenght");
        }
        public IActionResult CoReviewerAssessment(string applicantthematicIdString, string reviewerIdString)
        {
            try
            {
                if (!_utility.CheckResearchCycleEvent((int)EventTypes.ReviewProposal))
                {
                    SetMessage("Full Proposal cannot be assessed at this time.");
                    return RedirectToAction("ViewMyApplicantProposal", "ReviewerHome");
                }
                if (!String.IsNullOrEmpty(applicantthematicIdString) && !String.IsNullOrEmpty(reviewerIdString))
                {
                    var applicantthematicId = Convert.ToInt32(Utility.Decrypt(applicantthematicIdString));
                    var reviewerId = Convert.ToInt32(Utility.Decrypt(reviewerIdString));
                    _adminViewModel.ApplicantSubmission = _context.ApplicantSubmission.Where(f => f.ApplicantThematicId == applicantthematicId && f.TypeId == (int)Types.Proposal).FirstOrDefault();
                    _adminViewModel.ProposalScoring = _context.ProposalScoring.Where(f => f.ApplicantThematicId == applicantthematicId && f.ReviewerId == reviewerId).FirstOrDefault();
                    _adminViewModel.Proposal = _context.Proposal.Where(g => g.ApplicantThemeId == applicantthematicId).FirstOrDefault();
                    //_adminViewModel.Reviewer = reviewer;
                    var reviewer = _context.Reviewer.Where(f => f.Id == reviewerId).FirstOrDefault();
                    _adminViewModel.Reviewer = reviewer;
                    _adminViewModel.ProposalAssessment = _context.ProposalAssessment.Where(f => f.ApplicantThematicId == applicantthematicId && f.ReviewerId == reviewer.Id).FirstOrDefault();
                    if (_adminViewModel.ProposalAssessment?.Id > 0)
                    {
                        _adminViewModel.ApprovedBudget = _adminViewModel.ProposalAssessment.ApprovedBudget != null ? (decimal)_adminViewModel.ProposalAssessment.ApprovedBudget : 0;
                        _adminViewModel.Score = _adminViewModel.ProposalAssessment.Score;
                    }
                    _adminViewModel.BudgetDetails = _context.BudgetDetail.Where(f => f.ApplicantThematicId == applicantthematicId).ToList();
                    if (_adminViewModel.BudgetDetails.Count >= 0)
                    {
                        _adminViewModel.BudgetDetailsSectionOne = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.PersonnelAllowances).ToList();


                        _adminViewModel.BudgetDetailsSectionFour = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.DataCollectionandAnalysis).ToList();
                        _adminViewModel.BudgetDetailsSectionThree = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.SuppliesConsumables).ToList();
                        _adminViewModel.BudgetDetailsSectionTwo = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Equipment).ToList();
                        _adminViewModel.BudgetDetailsSectionFive = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Travel).ToList();
                        _adminViewModel.BudgetDetailsSectionSix = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Dissemination).ToList();
                        _adminViewModel.BudgetDetailsSectionSeven = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Other).ToList();
                        _adminViewModel.SectionPercentage1 = Math.Round(((_adminViewModel.BudgetDetailsSectionOne.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        _adminViewModel.SectionPercentage2 = Math.Round(((_adminViewModel.BudgetDetailsSectionTwo.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        _adminViewModel.SectionPercentage3 = Math.Round(((_adminViewModel.BudgetDetailsSectionThree.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        _adminViewModel.SectionPercentage4 = Math.Round(((_adminViewModel.BudgetDetailsSectionFour.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        _adminViewModel.SectionPercentage5 = Math.Round(((_adminViewModel.BudgetDetailsSectionFive.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        _adminViewModel.SectionPercentage6 = Math.Round(((_adminViewModel.BudgetDetailsSectionSix.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        _adminViewModel.SectionPercentage7 = Math.Round(((_adminViewModel.BudgetDetailsSectionSeven.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                    }
                    else
                    {

                        _adminViewModel.BudgetDetailsSectionOne = new List<BudgetDetail>();
                        _adminViewModel.BudgetDetailsSectionFour = new List<BudgetDetail>();
                        _adminViewModel.BudgetDetailsSectionThree = new List<BudgetDetail>();
                        _adminViewModel.BudgetDetailsSectionTwo = new List<BudgetDetail>();
                        _adminViewModel.BudgetDetailsSectionFive = new List<BudgetDetail>();
                        _adminViewModel.BudgetDetailsSectionSix = new List<BudgetDetail>();
                        _adminViewModel.BudgetDetailsSectionSeven = new List<BudgetDetail>();
                    }



                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult CoReviewerConceptNote(string applicantthematicIdString, string reviewerIdString)
        {
            try
            {
                if (!String.IsNullOrEmpty(applicantthematicIdString) && !String.IsNullOrEmpty(reviewerIdString))
                {
                    var applicantthematicId = Convert.ToInt32(Utility.Decrypt(applicantthematicIdString));
                    var reviewerId = Convert.ToInt32(Utility.Decrypt(reviewerIdString));
                    List<ReviewerAssignedScore> reviewerList = new List<ReviewerAssignedScore>();
                    List<Person> personTeamList = new List<Person>();
                    var applicantThematic = _context.ApplicantThematic.Where(f => f.Id == applicantthematicId).FirstOrDefault();
                    var approval = _context.Approval.Where(y => y.ApplicantThematicId == applicantthematicId && y.IsProposal == false).FirstOrDefault();
                    if (approval?.Approved == true)
                    {
                        SetMessage("Cannot assess already approved Concept Note");
                        return RedirectToAction("ViewMyApplicantConceptNote");

                    }
                    if (applicantThematic != null)
                    {
                        //var scores = _context.ProposalScoring.Where(f => f.ApplicantThematicId == applicantThematic.Id && f.ReviewerId == reviewer.Id && f.TypeId == (int)Types.ConceptNote).FirstOrDefault();

                        var applicant = _context.Applicant.Where(h => h.Id == applicantThematic.ApplicantId).FirstOrDefault();
                        var person = _context.Person.Where(f => f.Id == applicant.PersonId).FirstOrDefault();

                        var existingConceptNote = _context.ApplicantSubmission.Where(g => g.ApplicantThematicId == applicantThematic.Id && g.TypeId == (int)Types.ConceptNote).FirstOrDefault();
                        if (existingConceptNote != null)
                        {
                            _adminViewModel.ApplicantSubmission = existingConceptNote;
                            _adminViewModel.BudgetDetails = _context.BudgetDetail.Where(f => f.ApplicantThematicId == applicantThematic.Id && f.TypeId == (int)Types.ConceptNote).ToList();
                            if (_adminViewModel.BudgetDetails.Count > 0)
                            {

                                _adminViewModel.BudgetDetailsSectionOne = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.PersonnelAllowances).ToList();


                                _adminViewModel.BudgetDetailsSectionFour = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.DataCollectionandAnalysis).ToList();
                                _adminViewModel.BudgetDetailsSectionThree = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.SuppliesConsumables).ToList();
                                _adminViewModel.BudgetDetailsSectionTwo = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Equipment).ToList();
                                _adminViewModel.BudgetDetailsSectionFive = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Travel).ToList();
                                _adminViewModel.BudgetDetailsSectionSix = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Dissemination).ToList();
                                _adminViewModel.BudgetDetailsSectionSeven = _adminViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Other).ToList();
                                _adminViewModel.SectionPercentage1 = Math.Round(((_adminViewModel.BudgetDetailsSectionOne.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                                _adminViewModel.SectionPercentage2 = Math.Round(((_adminViewModel.BudgetDetailsSectionTwo.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                                _adminViewModel.SectionPercentage3 = Math.Round(((_adminViewModel.BudgetDetailsSectionThree.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                                _adminViewModel.SectionPercentage4 = Math.Round(((_adminViewModel.BudgetDetailsSectionFour.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                                _adminViewModel.SectionPercentage5 = Math.Round(((_adminViewModel.BudgetDetailsSectionFive.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                                _adminViewModel.SectionPercentage6 = Math.Round(((_adminViewModel.BudgetDetailsSectionSix.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                                _adminViewModel.SectionPercentage7 = Math.Round(((_adminViewModel.BudgetDetailsSectionSeven.Sum(d => d.Total) / _adminViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            }
                            else
                            {

                                _adminViewModel.BudgetDetailsSectionOne = new List<BudgetDetail>();
                                _adminViewModel.BudgetDetailsSectionFour = new List<BudgetDetail>();
                                _adminViewModel.BudgetDetailsSectionThree = new List<BudgetDetail>();
                                _adminViewModel.BudgetDetailsSectionTwo = new List<BudgetDetail>();
                                _adminViewModel.BudgetDetailsSectionFive = new List<BudgetDetail>();
                                _adminViewModel.BudgetDetailsSectionSix = new List<BudgetDetail>();
                                _adminViewModel.BudgetDetailsSectionSeven = new List<BudgetDetail>();
                            }

                        }

                        var myScore = _context.ProposalScoring.Where(f => f.ApplicantThematicId == applicantThematic.Id && f.ReviewerId == reviewerId && f.TypeId == (int)Types.ConceptNote).FirstOrDefault();
                        _adminViewModel.ProposalScoring = myScore != null ? myScore : new ProposalScoring();

                        if (applicant != null)
                        {
                            _adminViewModel.ApplicantThematic = _context.ApplicantThematic.Where(d => d.ApplicantId == applicant.Id).FirstOrDefault();
                            _adminViewModel.Theme = _context.Theme.Where(f => f.Id == _adminViewModel.ApplicantThematic.ThemeId).FirstOrDefault();
                            _adminViewModel.Category = _context.Category.Where(f => f.Id == _adminViewModel.Theme.CategoryId).FirstOrDefault();
                            _adminViewModel.Person = person;
                            var teamList = _context.Team.Where(f => f.ApplicantThematicId == applicantthematicId).ToList();
                            if (teamList.Count > 0)
                            {
                                for (int j = 0; j < teamList.Count; j++)
                                {
                                    var teamPersonId = teamList[j].PersonId;
                                    var singleTeamPerson = _context.Person.Where(c => c.Id == teamPersonId).FirstOrDefault();
                                    personTeamList.Add(singleTeamPerson);
                                }
                            }
                            _adminViewModel.TeamMember = personTeamList;
                            if (_adminViewModel.ApplicantThematic != null)
                            {
                                _adminViewModel.ConceptNote = _context.ConceptNote.Where(f => f.ApplicantThemeId == _adminViewModel.ApplicantThematic.Id).FirstOrDefault();

                                _adminViewModel.ApplicantReviewer = _context.ApplicantReviewer.Where(f => f.ApplicantThematicId == _adminViewModel.ApplicantThematic.Id && f.ReviewerId == reviewerId).FirstOrDefault();


                            }



                        }

                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        //Non Action Method
        public IActionResult GetTheme(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }
            int categoryId = Convert.ToInt32(value);
            var themecategoryList = _context.Theme.Where(c => c.Active && c.CategoryId == categoryId).ToList();

            return Json(new SelectList(themecategoryList, Value, Text));
        }
        public FileResult DownloadConceptNoteAttachement(string id)
        {
            List<string> documentLink = new List<string>();
            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    var conceptNoteId = Convert.ToInt32(Utility.Decrypt(id));

                    string fileRootPath = Path.Combine(_hostingEnvironment.WebRootPath, "attachments");
                    string fileName = string.Empty;
                    string filePath = string.Empty;
                    var conceptNote = _context.ConceptNote.Where(f => f.Id == conceptNoteId).FirstOrDefault();
                    if (conceptNote != null)
                    {
                        if (conceptNote.AttachmentFive != null && !String.IsNullOrEmpty(conceptNote.AttachmentFive))
                        {
                            fileName=conceptNote.AttachmentFive.Split('\\')[2];
                            filePath = Path.Combine(fileRootPath, fileName);
                            documentLink.Add(filePath);
                        }
                        if (conceptNote.AttachmentFour != null && !String.IsNullOrEmpty(conceptNote.AttachmentFour))
                        {
                            fileName = conceptNote.AttachmentFour.Split('\\')[2];
                            filePath = Path.Combine(fileRootPath, fileName);
                            documentLink.Add(filePath);
                        }
                        if (conceptNote.AttachmentOne != null && !String.IsNullOrEmpty(conceptNote.AttachmentOne))
                        {
                            fileName = conceptNote.AttachmentOne.Split('\\')[2];
                            filePath = Path.Combine(fileRootPath, fileName);
                            documentLink.Add(filePath);
                        }
                        if (conceptNote.AttachmentThree != null && !String.IsNullOrEmpty(conceptNote.AttachmentThree))
                        {
                            fileName = conceptNote.AttachmentThree.Split('\\')[2];
                            filePath = Path.Combine(fileRootPath, fileName);
                            documentLink.Add(filePath);
                        }
                        if (conceptNote.AttachmentTwo != null && !String.IsNullOrEmpty(conceptNote.AttachmentTwo))
                        {
                            fileName = conceptNote.AttachmentTwo.Split('\\')[2];
                            filePath = Path.Combine(fileRootPath, fileName);
                            documentLink.Add(filePath);
                        }

                        if (documentLink?.Count > 0)
                        {
                            foreach (var item in documentLink)
                            {
                                var zipFileName = string.Format("{0}_ConceptNoteAttachment.zip", conceptNote.ApplicantThematic.FileNo.Replace('/', '_'));
                                var newPath = Path.Combine(_hostingEnvironment.WebRootPath, "ZipDownload");
                                var tempOutPutPath = Path.Combine(newPath, zipFileName);

                                using (ZipOutputStream s = new ZipOutputStream(System.IO.File.Create(tempOutPutPath)))
                                {
                                    s.SetLevel(9); // 0-9, 9 being the highest compression  

                                    byte[] buffer = new byte[4096];
                                    //download zip
                                    for (int i = 0; i < documentLink.Count; i++)
                                    {
                                        ZipEntry entry = new ZipEntry(Path.GetFileName(documentLink[i]));
                                        entry.DateTime = DateTime.Now;
                                        entry.IsUnicodeText = true;
                                        s.PutNextEntry(entry);

                                        using (FileStream fs = System.IO.File.OpenRead(documentLink[i]))
                                        {
                                            int sourceBytes;
                                            do
                                            {
                                                sourceBytes = fs.Read(buffer, 0, buffer.Length);
                                                s.Write(buffer, 0, sourceBytes);
                                            } while (sourceBytes > 0);
                                        }
                                    }
                                    s.Finish();
                                    s.Flush();
                                    s.Close();


                                }
                                byte[] finalResult = System.IO.File.ReadAllBytes(tempOutPutPath);
                                if (System.IO.File.Exists(tempOutPutPath))
                                    System.IO.File.Delete(tempOutPutPath);

                                if (finalResult == null || !finalResult.Any())
                                    throw new Exception(String.Format("No Files found"));

                                return File(finalResult, "application/zip", zipFileName);
                            }
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return File("", "application/pdf", "sample.pdf");


        }
        public IActionResult FileDownload(string fileRootPath, string fileName)
        {
            string fullPart = Path.Combine(fileRootPath, fileName);
            //var fullPath = baseUrl + url;
            byte[] fileBytes = System.IO.File.ReadAllBytes(fullPart);
            return File(fileBytes, "application/force-download", fileName);
        }
        public IActionResult GroupedConceptNoteSubmission()
        {
            try
            {

                string reviewerEmail = User.FindFirstValue(ClaimTypes.Email);
                var reviewer = _context.Reviewer.Where(d => d.Email == reviewerEmail).FirstOrDefault();
                if (reviewer != null)
                {
                    var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                    if (activeSessionForApplication == null)
                    {
                        SetMessage("No Active Research Cycle at the Moment.");
                        return RedirectToAction("Index");
                    }
                    _adminViewModel.GroupSubmissionByTHematicAreas = new List<GroupSubmissionByTHematicArea>();
                    var assignedConceptNote=_context.ApplicantReviewer.Where(f => f.ReviewerId == reviewer.Id && f.ApplicantThematic.SessionId == activeSessionForApplication.Id && f.TypeId==(int)Types.ConceptNote).ToList();
                    if (assignedConceptNote?.Count > 0)
                    {
                        var groupedByTheme=assignedConceptNote.GroupBy(f => f.ApplicantThematic.ThemeId).ToList();
                        foreach(var item in groupedByTheme)
                        {
                            var perThemeSubmission=assignedConceptNote.Where(f => f.ApplicantThematic.ThemeId == item.Key).ToList();
                            GroupSubmissionByTHematicArea groupSubmissionByTHematicArea = new GroupSubmissionByTHematicArea()
                            {
                                ReviewerId=reviewer.Id,
                                Count=perThemeSubmission.Count,
                                ThematicName=perThemeSubmission.FirstOrDefault().ApplicantThematic.Theme.Name,
                                ThemeId=item.Key
                               
                            };
                            _adminViewModel.GroupSubmissionByTHematicAreas.Add(groupSubmissionByTHematicArea);

                        }
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
                
        }
        public FileResult DownloadGroupConceptNote(string rId, string id)
        {
            List<FileNoText> fileList = new List<FileNoText>();
            if (!String.IsNullOrEmpty(rId) && !String.IsNullOrEmpty(id))
            {
                var themeId = Convert.ToInt32(Utility.Decrypt(id));
                var reviewerId = Convert.ToInt32(Utility.Decrypt(rId));
                var reviewersList=_context.ApplicantReviewer.Where(f => f.ReviewerId == reviewerId && f.ApplicantThematic.ThemeId == themeId 
                && f.ApplicantThematic.Session.ActiveForApplication).ToList();
                if (reviewersList?.Count > 0)
                {
                    foreach(var item in reviewersList)
                    {
                        FileNoText fileNoText = new FileNoText();
                        var PISubmission=_context.ApplicantSubmission.Where(f => f.ApplicantThematicId == item.ApplicantThematicId && f.TypeId == (int)Types.ConceptNote).FirstOrDefault();
                        var conceptNote=_context.ConceptNote.Where(f => f.ApplicantThemeId == item.ApplicantThematicId).FirstOrDefault();
                        var text=_utility.CreateSubmissionText(PISubmission, string.Format("{0:N}",conceptNote.ConceptNoteSubmittedBudget));
                        fileNoText.FileNo = PISubmission.ApplicantThematic.FileNo.Replace('/', '_');
                        fileNoText.TextFile = text;
                        fileList.Add(fileNoText);
                        
                    }
                }
                List<string> pathToFile = new List<string>();
                if (fileList.Count > 0)
                {
                    for (int i = 0; i < fileList.Count; i++)
                    {
                        List<Paragraph> paragraphs = new List<Paragraph>();
                        using (MemoryStream stream = new System.IO.MemoryStream())
                        {
                            //This code is responsible for initialize the PDF document object.
                            using (Document pdfDoc = new Document(PageSize.A4, 25f, 25f, 25f, 25f))
                            {
                                pdfDoc.AddAuthor("Tetfund");
                                pdfDoc.AddCreator("Tetfund-ConceptNote");
                                pdfDoc.AddHeader("New File Name","Content");
                                pdfDoc.AddTitle("Title");
                                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                                pdfDoc.Open();
                                var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "ApplicationDownload");
                                string filename = "conceptNote_" + fileList[i].FileNo + ".pdf";
                                string filePath = Path.Combine(uploads, filename);
                                var stringArray = fileList[i].TextFile.Split(new string[] { "/*" },StringSplitOptions.None);
                                //This code is responsible for to add the Image file to the PDF document object.
                                var imageDirectory = Path.Combine(_hostingEnvironment.WebRootPath, "images");
                                var imageFilename = "tetfundlogo.png";
                                var imagePath=Path.Combine(imageDirectory, imageFilename);
                                if (stringArray?.Length > 0)
                                {

                                    string[] headerArray = { "File No: ", "Title: ", "Thematic Area: ", "Category: " };
                                    var bold = FontFactory.GetFont("Times New Roman", 14, Font.BOLD);
                                    var regular = FontFactory.GetFont("Times New Roman", 14);
                                    Image img = Image.GetInstance(imagePath);
                                    img.Alignment = Image.ALIGN_CENTER;
                                    img.ScaleAbsolute(100, 30);
                                    img.SpacingAfter = 5f;

                                    pdfDoc.Add(img);
                                    Paragraph header = new Paragraph("CONCEPT NOTE",bold);
                                    header.SetLeading(2f, 2f);
                                    header.Alignment = Image.ALIGN_CENTER;
                                    pdfDoc.Add(header);
                                    for (int j=0;j<stringArray.Length;j++)
                                    {
                                        if (!String.IsNullOrEmpty(stringArray[j]))
                                        {
                                            if (j == 0 || j == 1 || j == 2 || j == 3)
                                            {
                                                Paragraph paragraph = new Paragraph(headerArray[j], bold);
                                                //paragraph.Add(new Paragraph(headerArray[j], FontFactory.GetFont("Times New Roman", 14, Font.BOLD)));
                                                paragraph.Add(new Chunk(stringArray[j], regular));
                                                
                                                paragraph.SetLeading(2f, 2f);
                            
                                                pdfDoc.Add(paragraph);
                                            }
                                            else
                                            {
                                                var paragraphArray = stringArray[j].Split(new string[] { "*/" }, StringSplitOptions.None);


                                                if (j == 4)
                                                {
                                                    Paragraph title = new Paragraph("ESTIMATED BUDGET: " + "N" + paragraphArray[0], FontFactory.GetFont("Times New Roman", 12, Font.BOLD));

                                                    title.SetLeading(2f, 2f);
                                                    title.SpacingBefore = 2f;
                                                    title.SpacingAfter = 2f;
                                                    pdfDoc.Add(title);
                                                }
                                                else
                                                {
                                                    Paragraph title = new Paragraph(paragraphArray[0], FontFactory.GetFont("Times New Roman", 12, Font.BOLD));

                                                    title.SetLeading(2f, 2f);
                                                    title.SpacingBefore = 2f;
                                                    title.SpacingAfter = 2f;
                                                    pdfDoc.Add(title);
                                                }
                                                if (paragraphArray.Length > 1)
                                                {
                                                    Paragraph body = new Paragraph(paragraphArray[1], FontFactory.GetFont("Times New Roman", 10, Font.NORMAL));
                                                    body.Alignment = Element.ALIGN_JUSTIFIED;


                                                    pdfDoc.Add(body);
                                                }
                                                
                                            }
                                        }
                                       
                                    }
                                }
                                pdfDoc.Close();
                                pathToFile.Add(filePath);

                                FileInfo file = new FileInfo(filePath);
                                if (file.Exists)
                                {
                                    file.Delete();
                                }

                                byte[] content = stream.ToArray();

                                // Write out PDF from memory stream.
                                using (FileStream fs = System.IO.File.Create(filePath))
                                {
                                    fs.Write(content, 0, (int)content.Length);
                                }

                                
                            }
                        }
                    }
                    var fileName = string.Format("{0}_ConceptNoteSubmission_{1}.zip", reviewersList.FirstOrDefault().ApplicantThematic.Theme.Name,reviewerId);
                    var newPath = Path.Combine(_hostingEnvironment.WebRootPath, "ZipDownload");
                    var tempOutPutPath = Path.Combine(newPath, fileName);

                    using (ZipOutputStream s = new ZipOutputStream(System.IO.File.Create(tempOutPutPath)))
                    {
                        s.SetLevel(9); // 0-9, 9 being the highest compression  

                        byte[] buffer = new byte[4096];
                        //download zip
                        for (int i = 0; i < pathToFile.Count; i++)
                        {
                            ZipEntry entry = new ZipEntry(Path.GetFileName(pathToFile[i]));
                            entry.DateTime = DateTime.Now;
                            entry.IsUnicodeText = true;
                            s.PutNextEntry(entry);

                            using (FileStream fs = System.IO.File.OpenRead(pathToFile[i]))
                            {
                                int sourceBytes;
                                do
                                {
                                    sourceBytes = fs.Read(buffer, 0, buffer.Length);
                                    s.Write(buffer, 0, sourceBytes);
                                } while (sourceBytes > 0);
                            }
                        }
                        s.Finish();
                        s.Flush();
                        s.Close();


                    }
                    byte[] finalResult = System.IO.File.ReadAllBytes(tempOutPutPath);
                    if (System.IO.File.Exists(tempOutPutPath))
                        System.IO.File.Delete(tempOutPutPath);

                    if (finalResult == null || !finalResult.Any())
                        throw new Exception(String.Format("No Files found"));

                    return File(finalResult, "application/zip", fileName);

                }
                
            }
            return File("", "application/pdf", "sample.pdf");
        }
        public IActionResult DashBoard()
        {

            return View();
        }
        public JsonResult SaveAssessmentScore(string stringifyAssessmentDetail, int ApplicantThematicId, int typeId,decimal? mark)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                if (!string.IsNullOrEmpty(stringifyAssessmentDetail))
                {
                    if (!_utility.CheckResearchCycleEvent((int)EventTypes.ReviewConceptNote))
                    {
                        result.IsError = false;
                        result.Message = "Concept Note cannot be assessed at this time.";
                        return Json(result);
                    }
                    string reviewerEmail = User.FindFirstValue(ClaimTypes.Email);
                    var reviewer = _context.Reviewer.Where(d => d.Email == reviewerEmail).FirstOrDefault();
                    if (reviewer?.Id > 0)
                    {
                        ProposalScoring proposalScoring = JsonConvert.DeserializeObject<ProposalScoring>(stringifyAssessmentDetail);
                        if (typeId == 2)
                        {
                            _utility.UpdateConceptNoteAssessment(proposalScoring, ApplicantThematicId, reviewer.Id, typeId, mark, false);
                        }
                        else if (typeId == 1)
                        {
                            _utility.UpdateConceptNoteAssessment(proposalScoring, ApplicantThematicId, reviewer.Id, typeId, mark, false);
                            

                        }
                        result.IsError = false;
                        result.Message = "Assessment saved successfully";
                        return Json(result);
                    }
                   

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(result);
        }
        public bool AssessmentIsSubmitted(int applicantThematicId, int reviewerId, int type)
        {
            var reviewerAssessment = _context.ProposalScoring.Where(f => f.ApplicantThematicId == applicantThematicId 
            && f.TypeId == type && f.ReviewerId == reviewerId).ToList();
            foreach(var item in reviewerAssessment)
            {
                if (!item.IsSubmitted)
                    return false;
            }
            return true;
        }

    }
}