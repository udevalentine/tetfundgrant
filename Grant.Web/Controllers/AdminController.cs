﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Grant.Web.Infrastructure;
using Grant.Web.Models;
using Grant.Web.Models.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;

namespace Grant.Web.Controllers
{
    public class AdminController : BaseController
    {
        private readonly DataBaseContext _context;
        private readonly Utility _utility;
        RegistrationViewModel _registrationViewModel;

        public AdminController(DataBaseContext context, Utility utility)
        {
            _context = context;
            _utility = utility;
            _registrationViewModel = new RegistrationViewModel();

        }
        public IActionResult Edmslogin()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Edmslogin(string username, string password)
        {
            try
            {
                if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password))
                {
                    SetMessage("Login detail is incorrect!");
                    return RedirectToAction(nameof(Edmslogin));
                }
                var adminDetail = _utility.VerifyAdminLoginDetail(username, password);
                if (adminDetail != null)
                {
                    //check for Admin login elsewhere
                    //if (adminDetail.IsLoggedIn)
                    //{
                    //    SetMessage("Your account is logged in on another device. Please log out on the other device to continue here");
                    //    return RedirectToAction("logout");
                    //}
                    var loginHash = Guid.NewGuid().ToString();
                    var reviewer = _context.Reviewer.Where(p => p.Id == adminDetail.ReviewerId).FirstOrDefault();
                    var identity = new ClaimsIdentity(new[] {
                         new Claim(ClaimTypes.Name, reviewer.FullName),
                         new Claim(ClaimTypes.Email, reviewer.Email),
                         new Claim(ClaimTypes.Role, reviewer.Role.Name),
                         new Claim(ClaimTypes.Hash, loginHash)
                        }, CookieAuthenticationDefaults.AuthenticationScheme);

                    var principal = new ClaimsPrincipal(identity);
                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                    adminDetail.Token = loginHash;
                    adminDetail.IsLoggedIn = true;
                    adminDetail.LastLogin = DateTime.Now;
                    _context.Update(adminDetail);
                    _context.SaveChanges();
                    return RedirectToAction("Index", "AdminHome", new { area = "" });
                }
                else
                {
                    SetMessage("Login detail is incorrect!");
                    return RedirectToAction("Edmslogin");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}