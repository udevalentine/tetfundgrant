﻿using Grant.Web.Infrastructure;
using Grant.Web.Models;
using Grant.Web.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Grant.Web.Controllers
{
    [Authorize(Policy = "MustBeAdmin")]
    public class AdminReportController : BaseController
    {
        private readonly DataBaseContext _context;
        AdminViewModel _adminViewModel;
        Utility _utility;
        private const string Value = "Id";
        private const string Text = "Name";
        private IHostingEnvironment _hostingEnvironment;
        public AdminReportController(DataBaseContext context, IHostingEnvironment hostingEnvironment, Utility utility)
        {
            _context = context;
            _adminViewModel = new AdminViewModel();
            _adminViewModel.PopulateAllDropdown(_context);
            _hostingEnvironment = hostingEnvironment;
            _utility = utility;
        }

        public IActionResult AllProposalRanking()
        {
            if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
            {
                SetMessage(ValidateUserLogin(_context));
                return RedirectToAction("Logout", "Home");
            }
            return View(_adminViewModel);
        }

        [HttpPost]
        public IActionResult AllProposalRanking(AdminViewModel adminViewModel)
        {
            try
            {
                if (adminViewModel.SessionId > 0)
                {
                    _adminViewModel.ShowPanel = true;
                    List<ScoreRanking> scoreRankingList = new List<ScoreRanking>();
                    var allProposalAssessment = _context.ProposalAssessment.Where(g => g.ApplicantThematic.SessionId == adminViewModel.SessionId).ToList();
                    
                    if (allProposalAssessment.Count > 0)
                    {
                        var uniqueApplicant = allProposalAssessment.GroupBy(o => o.ApplicantThematic).ToList();
                        for (int i = 0; i < uniqueApplicant.Count; i++)
                        {
                            ScoreRanking scoreRanking = new ScoreRanking();
                            var totalScore = 0M;
                            var average = 0M;
                            var applicantThematicId = uniqueApplicant[i].Key.Id;
                            var allApplicantReviewedScore = allProposalAssessment.Where(f => f.ApplicantThematicId == applicantThematicId).ToList();
                            if (allApplicantReviewedScore?.Count > 0)
                            {
                                var orderedMax = allApplicantReviewedScore.OrderByDescending(f => f.Score).Select(d => d.Score).FirstOrDefault();
                                var orderedMin = allApplicantReviewedScore.OrderBy(f => f.Score).Select(d => d.Score).FirstOrDefault();
                                var scoreDifference = orderedMax - orderedMin;
                                //this is to ensure that we remove proposal assessment of score diference greated than then are not considered by the admin. unless it has gone through reconcillation by the assessors
                                if (scoreDifference <= 10)
                                {

                                    for (int k = 0; k < allApplicantReviewedScore.Count; k++)
                                    {
                                        var uniqueScore = allApplicantReviewedScore[k];
                                        var score = uniqueScore.Score;
                                        totalScore += score;
                                        scoreRanking.CategoryName = uniqueScore.ApplicantThematic.Theme.Category.Name;
                                        scoreRanking.ThematicAreaName = uniqueScore.ApplicantThematic.Theme.Name;
                                        scoreRanking.Title = uniqueScore.ApplicantThematic.Title;
                                    }
                                    average = (totalScore / allApplicantReviewedScore.Count);
                                    scoreRanking.AverageScore = average;
                                    scoreRanking.ReviewerCount = allApplicantReviewedScore.Count;
                                    scoreRanking.AverageApprovedBudget = Math.Round((decimal)(allApplicantReviewedScore.Sum(f => f.ApprovedBudget) / allApplicantReviewedScore.Count), 2);
                                    scoreRanking.ApplicantThematic = uniqueApplicant[i].Key;
                                    
                                    scoreRankingList.Add(scoreRanking);
                                }
                            }

                        }
                        _adminViewModel.ScoreRankings = scoreRankingList.OrderByDescending(f => f.AverageScore).ToList();
                    }
                    else
                    {
                        _adminViewModel.ScoreRankings = new List<ScoreRanking>();
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }

        [HttpGet]
        public IActionResult BudgetApproval()
        {
            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }
                return View(_adminViewModel);
            }
            catch(Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult BudgetApproval(AdminViewModel adminViewModel)
        {
            try
            {
                if (adminViewModel.SessionId > 0 && adminViewModel.CategoryId > 0)
                {
                    var setBudget = _context.Budget
                                                      .Where(b => b.CategoryId == adminViewModel.CategoryId && b.SessionId == adminViewModel.SessionId)
                                                      .FirstOrDefault();
                    if (setBudget == null)
                    {
                        SetMessage("No set Budget. Please set Budget to continue.");
                        return View(_adminViewModel);
                    }
                    adminViewModel.ShowPanel = true;
                    List<ScoreRanking> scoreRankingList = new List<ScoreRanking>();
                    var allProposalAssessment = _context.ProposalAssessment.Where(g => g.ApplicantThematic.SessionId == adminViewModel.SessionId && g.ApplicantThematic.Theme.CategoryId == adminViewModel.CategoryId).ToList();

                    if (allProposalAssessment.Count > 0)
                    {
                        var uniqueApplicant = allProposalAssessment.GroupBy(o => o.ApplicantThematic).ToList();
                        for (int i = 0; i < uniqueApplicant.Count; i++)
                        {
                            ScoreRanking scoreRanking = new ScoreRanking();
                            var totalScore = 0M;
                            var average = 0M;
                            var applicantThematicId = uniqueApplicant[i].Key.Id;

                            var allApplicantReviewedScore = allProposalAssessment.Where(f => f.ApplicantThematicId == applicantThematicId).ToList();
                            if (allApplicantReviewedScore?.Count > 0)
                            {
                                var orderedMax = allApplicantReviewedScore.OrderByDescending(f => f.Score).Select(d => d.Score).FirstOrDefault();
                                var orderedMin = allApplicantReviewedScore.OrderBy(f => f.Score).Select(d => d.Score).FirstOrDefault();
                                var scoreDifference = orderedMax - orderedMin;
                                //this is to ensure that we remove proposal assessment of score diference greated than then are not considered by the admin. unless it has gone through reconcillation by the assessors
                                if (scoreDifference <= 10)
                                {

                                    for (int k = 0; k < allApplicantReviewedScore.Count; k++)
                                    {
                                        var uniqueScore = allApplicantReviewedScore[k];
                                        var score = uniqueScore.Score;
                                        totalScore += score;
                                        scoreRanking.CategoryName = uniqueScore.ApplicantThematic.Theme.Category.Name;
                                        scoreRanking.ThematicAreaName = uniqueScore.ApplicantThematic.Theme.Name;
                                        
                                        var budgetApplicationValidation = _context.FinalProposalAproval.Where(ap => ap.ApplicantThematicId == uniqueScore.ApplicantThematic.Id).FirstOrDefault();
                                        scoreRanking.iSApproved = (budgetApplicationValidation != null ? budgetApplicationValidation.IsApproved : false);
                                    }
                                    average = (totalScore / allApplicantReviewedScore.Count);
                                    scoreRanking.AverageScore = average;
                                    scoreRanking.ReviewerCount = allApplicantReviewedScore.Count;
                                    scoreRanking.AverageApprovedBudget = Math.Round((decimal)(allApplicantReviewedScore.Sum(f => f.ApprovedBudget) / allApplicantReviewedScore.Count), 2);
                                    scoreRanking.ApplicantThematic = uniqueApplicant[i].Key;
                                    scoreRankingList.Add(scoreRanking);
                                }
                            }
                        }
                        adminViewModel.ScoreRankings = scoreRankingList.OrderByDescending(f => f.AverageScore).ToList();
                        var allocatedBudget = _context.Budget
                                                      .Where(b => b.CategoryId == adminViewModel.CategoryId && b.SessionId == adminViewModel.SessionId)
                                                      .Select(b => new { b.Amount, b.AmountDispensed })
                                                      .FirstOrDefault();

                        adminViewModel.TotalAllocatedAmountForThematicCategory = allocatedBudget.Amount;
                        adminViewModel.UsedAmountFromAllocatedAmount = (decimal)(allocatedBudget.AmountDispensed == null ? 0.0m : (allocatedBudget.Amount - allocatedBudget.AmountDispensed));
                    }
                    else
                    {
                        adminViewModel.ScoreRankings = new List<ScoreRanking>();
                    }
                }

            }
            catch (Exception ex) { throw ex; }

            adminViewModel.PopulateAllDropdown(_context);
            return View(adminViewModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult ApproveBudget(int applicantThematicId, int sessionId, int categoryId, decimal amount)
        {
            try
            {
                string message = string.Empty;
                var budget = _context.Budget.Where(b => b.CategoryId == categoryId && b.SessionId == sessionId).FirstOrDefault();
                if (budget != null)
                {
                    if(budget.AmountDispensed == null)
                    {
                        budget.AmountDispensed = 0;
                    }

                    //if the amount that is newly added + the current amount dispensed > allocated budget send a message
                    if ((budget.Amount - (budget.AmountDispensed + amount)) > 0)
                    {
                        using (TransactionScope transactionScope = new TransactionScope())
                        {
                            budget.AmountDispensed += amount;
                            _context.Update(budget);
                            //_context.SaveChanges();

                            var user = GetLoggedInUser(_context);

                            var approvedBudgetProposal = new FinalProposalAproval()
                            {
                                DateCreated = DateTime.Now,
                                ApplicantThematicId = applicantThematicId,
                                UserId = user.Id,
                                ApprovedAmount = amount,
                                Remark = "Proposal approval completed",
                                IsApproved = true
                            };

                            _context.Add(approvedBudgetProposal);
                            _context.SaveChanges();

                            transactionScope.Complete();
                        }

                        var applicantThematic = _context.ApplicantThematic
                                                        .Where(ap => ap.Id == applicantThematicId)
                                                        .Include(ap => ap.Applicant)
                                                        .ThenInclude(ap => ap.Person)
                                                        .FirstOrDefault();

                        var email = applicantThematic.Applicant.Person.Email;
                        var name = applicantThematic.Applicant.Person.Name;
                        
                        _utility.SendEMail(email, name, (int)MessageType.ProposalApproval, "", "","","",applicantThematic.Title);

                        message = string.Format("{0:N} allocated successfully. Avaliable balance: {1:N}", amount, (budget.Amount - budget.AmountDispensed));
                    }
                    else
                    {
                        message = string.Format("Insufficient Funds...Adding {0:N} to the budget exceeds the allocated total of {1:N}", amount, budget.Amount);
                    }
                }

                return Json(message);
            }
            catch(Exception ex) { throw ex; }
        }

        public IActionResult AverageBudget()
        {
            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }
                decimal totalBudget = 0M;
                int uniqueCount = 0;
                var allApprovedProposal = _context.ProposalAssessment.Where(f => f.Approved).ToList();
                if (allApprovedProposal.Count > 0)
                {
                    var eachApplicant = allApprovedProposal.GroupBy(f => f.ApplicantThematicId).ToList();
                    uniqueCount = eachApplicant.Count;
                    for (int i = 0; i < eachApplicant.Count; i++)
                    {
                        var applicantThematicId = eachApplicant[i].Key;
                        var approvedProposal = allApprovedProposal.Where(u => u.ApplicantThematicId == applicantThematicId).ToList();
                        var approvedBudget = approvedProposal.Sum(d => d.ApprovedBudget);
                        totalBudget += (decimal)approvedBudget / approvedProposal.Count;

                    }
                    _adminViewModel.AverageApprovedBudget = Math.Round((totalBudget / eachApplicant.Count), 2);
                }

                _adminViewModel.ApprovedBudget = totalBudget;

                _adminViewModel.ApplicantCount = uniqueCount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }

        public IActionResult ConceptNoteByTheme()
        {
            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        [HttpPost]
        public IActionResult ConceptNoteByTheme(AdminViewModel viewModel)
        {
            try
            {
                ApplicationList applicationList = new ApplicationList();
                if (viewModel.CategoryId > 0)
                {
                    _adminViewModel.ApplicantThematics = _context.ApplicantThematic.Where(t => t.Theme.CategoryId == viewModel.CategoryId).ToList();
                    _adminViewModel.ShowPanel = true;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult ReviewerExpertiseDistribution()
        {
            try
            {
                _adminViewModel.ReviewerThematicStrenght = _context.ReviewerThematicStrenght.OrderBy(f => f.Reviewer.Name).ThenBy(f => f.Theme.Category.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult ApprovedProposal()
        {
            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }
                List<AssessmentReport> list = new List<AssessmentReport>();
                var allProposalAssessment = _context.ProposalAssessment.Where(f => f.Approved).ToList();
                if (allProposalAssessment.Count > 0)
                {
                    var groupedBy = allProposalAssessment.GroupBy(f => f.ApplicantThematicId).ToList();
                    for (int i = 0; i < groupedBy.Count; i++)
                    {
                        AssessmentReport assessmentReport = new AssessmentReport();
                        var applicantThematicId = groupedBy[i].Key;
                        var myAssesssment = allProposalAssessment.Where(r => r.ApplicantThematicId == applicantThematicId).ToList();
                        if (myAssesssment.Count > 0)
                        {
                            assessmentReport.ApplicantThematic = myAssesssment.FirstOrDefault().ApplicantThematic;
                            assessmentReport.AverageBudget = Math.Round((decimal)(myAssesssment.Sum(f => f.ApprovedBudget) / myAssesssment.Count), 2);
                        }
                        list.Add(assessmentReport);

                    }
                    _adminViewModel.AssessmentReports = list.OrderBy(h => h.ApplicantThematic.Theme.Category.Name).ThenBy(f => f.ApplicantThematic.Theme.Name).ToList();
                }
                else
                {
                    _adminViewModel.AssessmentReports = new List<AssessmentReport>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult SessionBudgetAccount()
        {
            try
            {

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        [HttpPost]
        public IActionResult SessionBudgetAccount(int SessionId)
        {
            try
            {
                _adminViewModel.ShowPanel = true;
                var activeCategory = _context.Category.Where(d => d.Active).ToList();
                List<BudgetSum> list = new List<BudgetSum>();
                if (activeCategory.Count > 0)
                {
                    for (int i = 0; i < activeCategory.Count; i++)
                    {
                        BudgetSum budgetSum = new BudgetSum();
                        var catId = activeCategory[i].Id;
                        var budget = _context.Budget.Where(g => g.CategoryId == catId && g.SessionId == SessionId).FirstOrDefault();
                        budgetSum.Category = new Category();
                        budgetSum.Category = activeCategory[i];
                        if (budget != null)
                        {
                            budgetSum.Amount = budget.Amount;
                            budgetSum.UsedAmount = budget.AmountDispensed;
                            budgetSum.Balance = budget.Amount - (decimal)(budget.AmountDispensed != null ? budget.AmountDispensed : 0);
                        }
                        list.Add(budgetSum);


                    }
                }
                _adminViewModel.BudgetSums = list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult ApproveProposalByState()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }
        //public IActionResult PIRegionDistributionChart()
        //{
        //    try
        //    {

        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //    return View(_adminViewModel);
        //}
        //[HttpPost]
        //public IActionResult PIRegionDistributionChartAjax(int sessionId)
        //{
        //    JsonResultModel result = new JsonResultModel();
        //    try
        //    {
        //        if (sessionId > 0)
        //        {
        //            List<string> Zones = new List<string>();
        //            List<int> PICount = new List<int>();
        //            var zones = _context.GeoPoliticalZone.Where(a => a.Active).ToList();
        //            if (zones != null && zones.Count > 0)
        //            {
        //                for (int i = 0; i < zones.Count; i++)
        //                {
        //                    var zoneId = zones[i].Id;
        //                    var count = _context.ApplicantThematic.Where(f => f.SessionId == sessionId && f.Applicant.Person.State.GeoPoliticalZoneId == zoneId).Count();
        //                    Zones.Add(zones[i].Name);
        //                    PICount.Add(count);
        //                }
        //                result.IsError = false;
        //                result.PICount = PICount;
        //                result.Zones = Zones;
        //                return Json(result);

        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //    return View();
        //}

        public IActionResult UnassignedConceptNotes()
        {
            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }
                //get the SessionList
                _adminViewModel.SessionSL = _utility.GetSessionSL();
                return View(_adminViewModel);
            }
            catch (Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult UnassignedConceptNotes(AdminViewModel viewModel)
        {
            try
            {
                if (viewModel.SessionId > 0)
                {
                    //get the entires applicant thematic
                    var allApplicantThematic = _context.ApplicantThematic
                                                       .Where(s => s.Session.Id == viewModel.SessionId)
                                                       .Include(s => s.Theme)
                                                       .Include(s => s.Applicant)
                                                       .Include(s => s.Session)
                                                       .ToList();

                    //get all the approved applicant thematic
                    var approvedApplicantThematic = _context.ApplicantReviewer
                                                            .Where(a => a.TypeId == (int)Types.ConceptNote)
                                                            .Include(a => a.ApplicantThematic)
                                                            .ThenInclude(a => a.Theme)
                                                            .Include(a => a.ApplicantThematic.Applicant)
                                                            .Include(a => a.ApplicantThematic.Session)
                                                            .ToList();
                    if (approvedApplicantThematic?.Count() > 0)
                    {
                        viewModel.ApplicantThematicList = allApplicantThematic;

                        //Compare the Lists for similarities
                        List<ApplicantThematic> filteredApplicantThematic = new List<ApplicantThematic>();
                        foreach (var item in approvedApplicantThematic)
                        {
                            if (allApplicantThematic.Contains(item.ApplicantThematic))
                            {
                                allApplicantThematic.Remove(item.ApplicantThematic);
                            }
                        }
                    }

                    viewModel.ApplicantThematicList = allApplicantThematic;
                }
                else
                {
                    SetMessage("Research cycle is required");
                }

                viewModel.SessionSL = _utility.GetSessionSL();

                return View(viewModel);
            }
            catch (Exception ex) { throw ex; }
        }

        public IActionResult UnassignedProposals()
        {
            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }
                //get the SessionList
                _adminViewModel.SessionSL = _utility.GetSessionSL();
                return View(_adminViewModel);
            }
            catch (Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult UnassignedProposals(AdminViewModel viewModel)
        {
            try
            {
                if (viewModel.SessionId > 0)
                {
                    //get the entires applicant thematic
                    var allProposal = _context.Proposal
                                              .Where(s => s.ApplicantThematic.Session.Id == viewModel.SessionId)
                                              .Include(s => s.ApplicantThematic)
                                              .ThenInclude(s => s.Applicant)
                                              .Include(s => s.ApplicantThematic.Session)
                                              .ToList();

                    //get all the approved applicant thematic
                    var approvedProposal = _context.ApplicantReviewer
                                                   .Where(a => a.TypeId == (int)Types.Proposal)
                                                   .Include(a => a.ApplicantThematic)
                                                   .ThenInclude(a => a.Theme)
                                                   .Include(a => a.ApplicantThematic.Applicant)
                                                   .Include(a => a.ApplicantThematic.Session)
                                                   .ToList();
                    if (approvedProposal?.Count() > 0)
                    {
                        foreach(var item in approvedProposal)
                        {
                            var proposalExists = allProposal.Where(a => a.ApplicantThemeId == item.ApplicantThematicId).FirstOrDefault();
                            if (proposalExists != null)
                            {
                                allProposal.Remove(proposalExists);
                            }
                        }
                    }

                    viewModel.ProposalList = allProposal;
                }

                //get the SessionList
                viewModel.SessionSL = _utility.GetSessionSL();
                return View(viewModel);
            }
            catch (Exception ex) { throw ex; }
        }

        public IActionResult ViewAllPI()
        {
            try
            {
               _adminViewModel.PrincipalInvestigators= _utility.AllPI();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult ViewPIProfile(long id)
        {
            try
            {
                if (id > 0)
                {
                   var applicant= _context.Applicant.Where(f => f.Id == id && f.RoleId == (int)RoleType.Applicant).FirstOrDefault();
                    if (applicant?.Id > 0)
                    {
                        _adminViewModel.Applicant = applicant;
                        _adminViewModel.ApplicantThematicList = _context.ApplicantThematic.Where(d => d.ApplicantId == applicant.Id).ToList();
                        if (_adminViewModel.ApplicantThematicList?.Count > 0)
                        {
                            var firstThematic = _adminViewModel.ApplicantThematicList.FirstOrDefault();
                            _adminViewModel.TeamMembers = _context.Team.Where(f => f.ApplicantThematicId == firstThematic.Id && f.Active).ToList();
                        }
                        
                        
                    }
                }
                
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(_adminViewModel);
        }
    }
}