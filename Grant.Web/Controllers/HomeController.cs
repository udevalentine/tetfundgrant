﻿using Grant.Web.Infrastructure;
using Grant.Web.Models;
using Grant.Web.Models.ViewModels;
using ICSharpCode.SharpZipLib.Zip;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Grant.Web.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IHostingEnvironment _IhostingEnvironment;
        private readonly DataBaseContext _context;
        private readonly Utility _utility;
        RegistrationViewModel _registrationViewModel;
        private const string Value = "Id";
        private const string Text = "Name";

        public HomeController(DataBaseContext context, Utility utility, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _utility = utility;
            _registrationViewModel = new RegistrationViewModel();
            _registrationViewModel.PopulateAllDropdown(_context);
            _IhostingEnvironment = hostingEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult Register()
        {
            SetMessage("Sorry, registration has ended, thanks");
            return RedirectToAction("Index");
            //_registrationViewModel.StateId = 38;
         
            //return View(_registrationViewModel);
        }
        [AllowAnonymous]
        [HttpPost]
        public IActionResult Register(RegistrationViewModel registrationViewModel)
        {
            if (registrationViewModel != null)
            {
                _registrationViewModel.ReturnInstitutionId = registrationViewModel.InstitutionId;
                SetMessage("Sorry, registration has ended, thanks");
                return View(_registrationViewModel);
            }
            _registrationViewModel.StateId = 38;

            if (registrationViewModel.Applicant == null || registrationViewModel.Person == null || registrationViewModel.Surname == null || registrationViewModel.FirstName == null || registrationViewModel.InstitutionId == 0)
            {
                _registrationViewModel.ReturnInstitutionId = registrationViewModel.InstitutionId;
                SetMessage("Please complete all fields");
                return View(_registrationViewModel);
            }
            if (registrationViewModel.Applicant.Password.Trim() != registrationViewModel.Applicant.ConfirmPassword.Trim())
            {
                _registrationViewModel.ReturnInstitutionId = registrationViewModel.InstitutionId;
                SetMessage("Your password confirmation does not match");
                return View(_registrationViewModel);
            }
            if (registrationViewModel.Person.Email.Trim() != registrationViewModel.Person.ConfirmEmail.Trim())
            {
                _registrationViewModel.ReturnInstitutionId = registrationViewModel.InstitutionId;
                SetMessage("Your email address confirmation does not match");
                return View(_registrationViewModel);
            }
            //if (registrationViewModel.Applicant.ApplicationNo.Contains("/"))
            //{
            //    SetMessage("Special Characters are not Allowed");
            //    return View(registrationViewModel);
            //}
            var existingPerson = _context.Applicant.Where(d => d.ApplicationNo == registrationViewModel.Person.Email && d.RoleId == (int)Roles.Applicant).FirstOrDefault();
            if (existingPerson != null)
            {
                _registrationViewModel.ReturnInstitutionId = registrationViewModel.InstitutionId;
                SetMessage("Email already exists");
                return View(_registrationViewModel);
            }
            var existingPhoneAndDOB = _context.Person.Where(d => d.PhoneNo == registrationViewModel.Person.PhoneNo && d.DOB == registrationViewModel.Person.DOB).FirstOrDefault();
            if (existingPhoneAndDOB != null)
            {
                _registrationViewModel.ReturnInstitutionId = registrationViewModel.InstitutionId;
                SetMessage("Phone No. already exists");
                return View(_registrationViewModel);
            }
            _utility.RegisterApplicant(registrationViewModel.Person, registrationViewModel.Applicant, registrationViewModel.InstitutionId, registrationViewModel.QualificationId, registrationViewModel.TitleId, registrationViewModel.OtherQualification, registrationViewModel.StateId, registrationViewModel.SexId, registrationViewModel);
            if (CheckForInternetConnection())
            {
                _utility.SendEMail(registrationViewModel.Person.Email, registrationViewModel.Person.Name, (int)MessageType.signUp, "", "", "", "","");
                
            }

            TempData["Email"] = registrationViewModel.Person.Email;
            //SetMessage("Your Account as a Principal Investigator has been Successfully Created; Log into" + " "+ registrationViewModel.Person.Email + " " + "to confirm your registration and continue.");
            return RedirectToAction("RegistrationWelcomPage");

        }
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login(string registrationno, string password, bool isReseacher)
        {
            if (String.IsNullOrEmpty(registrationno) || String.IsNullOrEmpty(password))
            {
                SetMessage("Login detail is incorrect!");
                return RedirectToAction("Index");
            }

            var loginHash = Guid.NewGuid().ToString();
            var reviewerDetail = _utility.VerifyReviewerLoginDetail(registrationno, password);
            var userDetail = _utility.VerifyApplicantLoginDetail(registrationno, password);
            var coResearwcherUserDetail = _utility.VerifyCoReseacherLoginDetail(registrationno, password);
            if (reviewerDetail != null)
            {
                //if (reviewerDetail.IsLoggedIn)
                //{
                //    SetMessage("Your account is logged in on another device. Please log out on the other device to continue here");
                //    return RedirectToAction("logout");
                //}
                var reviewer = _context.Reviewer.Where(p => p.Id == reviewerDetail.ReviewerId).FirstOrDefault();
                var identity = new ClaimsIdentity(new[] {
                         new Claim(ClaimTypes.Name, reviewer.FullName),
                         new Claim(ClaimTypes.Email, reviewer.Email),
                         new Claim(ClaimTypes.Role, reviewer.Role.Name),
                         new Claim(ClaimTypes.Hash, loginHash)
                        }, CookieAuthenticationDefaults.AuthenticationScheme);

                var principal = new ClaimsPrincipal(identity);
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                reviewerDetail.IsLoggedIn = true;
                reviewerDetail.Token = loginHash;
                reviewerDetail.LastLogin = DateTime.Now;
                _context.Update(reviewerDetail);
                _context.SaveChanges();
                return RedirectToAction("DashBoard", "ReviewerHome", new { area = "" });
            }
            else if (userDetail != null)
            {

                //check for Researcher login
                //if (userDetail.IsLoggedIn)
                //{
                //    SetMessage("Your account is logged in on another device. Please log out on the other device to continue here");
                //    return RedirectToAction("logout");
                //}
                var applicant = await _context.Applicant.Where(f => f.ApplicationNo == registrationno && f.Active).ToListAsync();
                if ((applicant.Count > 1) || applicant.FirstOrDefault().RoleId == (int)Roles.CoApplicant)
                {
                    var encryptedApplicantId = Utility.Encrypt(applicant.FirstOrDefault().Id.ToString());
                    return RedirectToAction("RedirectToResearcherLogin", new { id = encryptedApplicantId });
                }


                if (!userDetail.IsVerified)
                {
                    SetMessage("This account has not been verified. Log into your email to verify your account");
                    return RedirectToAction("Index");
                }
                var person = _context.Person.Where(p => p.Id == userDetail.PersonId).FirstOrDefault();
                if (person.PassportUrl == null)
                {
                    person.PassportUrl = "~/theme/dist/img/avatar.png";
                }
                
                var identity = new ClaimsIdentity(new[] {
                         new Claim(ClaimTypes.Name, person.ApplicantFullName),
                         new Claim(ClaimTypes.Email,userDetail.ApplicationNo),
                         new Claim(ClaimTypes.StreetAddress, person.PassportUrl),
                         new Claim(ClaimTypes.Role, userDetail.Role.Name),
                         new Claim(ClaimTypes.Hash, loginHash)
                        }, CookieAuthenticationDefaults.AuthenticationScheme);

                var principal = new ClaimsPrincipal(identity);
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                userDetail.LastLogin = DateTime.Now;
                userDetail.IsLoggedIn = true;
                userDetail.DeviceMacAddress = loginHash;
                _context.Update(userDetail);
                _context.SaveChanges();
                return RedirectToAction("Dashboard", "UserHome", new { area = "" });
            }
            else if (coResearwcherUserDetail != null)
            {
                var applicant = await _context.Applicant.Where(f => f.ApplicationNo == registrationno && f.Active).ToListAsync();
                var encryptedApplicantId = Utility.Encrypt(applicant.FirstOrDefault().Id.ToString());
                return RedirectToAction("RedirectToResearcherLogin", new { id = encryptedApplicantId });
            }
            else
            {
                SetMessage("Login detail is incorrect!");
                return RedirectToAction("Index");
            }


        }

        public async Task<IActionResult> Logout()
        {
            //string userEmail = User.FindFirstValue(ClaimTypes.Email);
            UpdateLogout(_context);
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction(nameof(Index));


        }
        public async Task<IActionResult> ExpirationLogout()
        {
            //string userEmail = User.FindFirstValue(ClaimTypes.Email);
            UpdateLogout(_context);
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return View();


        }
        public async Task<IActionResult> AdminLogout()
        {
            //string userEmail = User.FindFirstValue(ClaimTypes.Email);
            UpdateLogout(_context);
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            //return RedirectToAction(nameof(Login2));
            return RedirectToAction("Edmslogin", "Admin");


        }
        [HttpGet]
        public IActionResult Login2()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Login2(string username, string password)
        {
            try
            {
                if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password))
                {
                    SetMessage("Login detail is incorrect!");
                    return RedirectToAction(nameof(Login2));
                }
                var adminDetail = _utility.VerifyAdminLoginDetail(username, password);
                if (adminDetail != null)
                {
                    //check for Admin login elsewhere
                    //if (adminDetail.IsLoggedIn)
                    //{
                    //    SetMessage("Your account is logged in on another device. Please log out on the other device to continue here");
                    //    return RedirectToAction("logout");
                    //}
                    var loginHash = Guid.NewGuid().ToString();
                    var reviewer = _context.Reviewer.Where(p => p.Id == adminDetail.ReviewerId).FirstOrDefault();
                    var identity = new ClaimsIdentity(new[] {
                         new Claim(ClaimTypes.Name, reviewer.FullName),
                         new Claim(ClaimTypes.Email, reviewer.Email),
                         new Claim(ClaimTypes.Role, reviewer.Role.Name),
                         new Claim(ClaimTypes.Hash, loginHash)
                        }, CookieAuthenticationDefaults.AuthenticationScheme);

                    var principal = new ClaimsPrincipal(identity);
                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                    adminDetail.Token = loginHash;
                    adminDetail.IsLoggedIn = true;
                    adminDetail.LastLogin = DateTime.Now;
                    _context.Update(adminDetail);
                    _context.SaveChanges();
                    return RedirectToAction("Index", "AdminHome", new { area = "" });
                }
                else
                {
                    SetMessage("Login detail is incorrect!");
                    return RedirectToAction("Login2");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IActionResult RegistrationWelcomPage()
        {
            ViewBag.Email = TempData["Email"] as string;
            return View();
        }
        public IActionResult VerifyAccount(string encryptData)
        {
            try
            {
                if (!String.IsNullOrEmpty(encryptData))
                {
                    var decryptData = Utility.Decrypt(encryptData);
                    var applicant = _context.Applicant.Where(f => f.ApplicationNo == decryptData).FirstOrDefault();
                    if (applicant != null)
                    {
                        applicant.IsVerified = true;
                        applicant.DateVerified = DateTime.Now;
                        _context.Update(applicant);
                        _context.SaveChanges();

                        SetMessage("Your account verification was successful. Enter login credentials to continue.");
                        return RedirectToAction("Index");
                    }
                }


            }
            catch (Exception ex)
            {

                throw ex;
            }
            SetMessage("Your account verification was not successful.");
            return RedirectToAction("Index");
        }
        //None Action Methods
        public JsonResult ExistingUsername(string username)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                var exist = _context.Applicant.Where(g => g.ApplicationNo == username && g.RoleId==(int)Roles.Applicant).FirstOrDefault();
                if (exist != null)
                {
                    result.IsError = false;
                    result.Message = "Username already exists!";
                    return Json(result);
                }
                else
                {
                    var existReviewer = _context.Reviewer.Where(g => g.Email == username).FirstOrDefault();
                    if (existReviewer != null)
                    {
                        result.IsError = false;
                        result.Message = "Username already exists!";
                        return Json(result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }
            return Json(result);
        }
        public JsonResult ExistingEmail(string email)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                var exist = _context.Applicant.Where(g => g.ApplicationNo == email && g.RoleId==(int)Roles.Applicant).FirstOrDefault();
                if (exist != null)
                {
                    result.IsError = false;
                    result.Message = "Email address already exists!";
                    return Json(result);
                }
                else
                {
                    var existReviewer = _context.Reviewer.Where(g => g.Email == email).FirstOrDefault();
                    if (existReviewer != null)
                    {
                        result.IsError = false;
                        result.Message = "Email address already exists!";
                        return Json(result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }
            return Json(result);
        }
        public IActionResult GetInstitution(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }
            int categoryId = Convert.ToInt32(value);
            var institutionList = _context.Institution.Where(c => c.Active && c.InstitutionCategoryId == categoryId && c.IsPublic).OrderBy(g => g.Name).ToList();

            return Json(new SelectList(institutionList, Value, Text));
        }
        public IActionResult CheckMax()
        {
            List<int> list = new List<int>()
            {
                3,
                4,
                3,
                4,
                8,
                8,
                5

            };
            var max = list.Max();
            return Ok(max);

        }

        public IActionResult ForgotPassword()
        {
            try
            {
                return View();
            }
            catch (Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult ForgotPassword(RegistrationViewModel viewModel)
        {
            try
            {
                if (!string.IsNullOrEmpty(viewModel.Email))
                {
                    var userExists = _context.User.Where(u => u.Email == viewModel.Email).FirstOrDefault();
                    if (userExists != null)
                    {
                        string oneTimeValidator = Convert.ToString(Guid.NewGuid());

                        _utility.SendEMail(userExists.Email, "", (int)MessageType.ForgotPassword, null, "", "", oneTimeValidator,"");
                        userExists.OneTimeValidator = oneTimeValidator;

                        _context.Update(userExists);
                        _context.SaveChanges();

                        SetMessage(string.Format("Check your email({0}) to validate your account", viewModel.Email));
                    }
                    else
                    {
                        var applicantExists = _context.Applicant.Where(a => a.ApplicationNo == viewModel.Email && a.RoleId==(int)Roles.Applicant).FirstOrDefault();
                        if (applicantExists != null)
                        {
                            string oneTimeValidator = Convert.ToString(Guid.NewGuid());

                            _utility.SendEMail(applicantExists.ApplicationNo, "", (int)MessageType.ForgotPassword, null, "", "", oneTimeValidator,"");
                            applicantExists.OneTimeValidator = oneTimeValidator;

                            _context.Update(applicantExists);
                            _context.SaveChanges();

                            SetMessage(string.Format("Check your email ({0})  to reset your account", viewModel.Email));
                        }
                        else
                        {
                            SetMessage("Your email could not be validated");
                        }
                    }
                }
                else
                {
                    SetMessage("Email address is required");
                }

                return View(viewModel);
            }
            catch (Exception ex) { throw ex; }
        }

        public IActionResult ValidateUser(string guid)
        {
            try
            {
                RegistrationViewModel viewModel = new RegistrationViewModel();
                if (!string.IsNullOrEmpty(guid))
                {
                    var userToBeValidated = _context.User.Where(u => u.OneTimeValidator == guid).FirstOrDefault();
                    if (userToBeValidated != null)
                    {
                        viewModel.OneTimeValidator = userToBeValidated.OneTimeValidator;
                    }
                    else
                    {
                        var applicantToBeValidated = _context.Applicant.Where(u => u.OneTimeValidator == guid).FirstOrDefault();
                        if (applicantToBeValidated != null)
                        {
                            viewModel.OneTimeValidator = applicantToBeValidated.OneTimeValidator;
                        }
                        else
                        {
                            SetMessage("This account has already been verified, enter login credentials to continue");
                            return RedirectToAction("Index");
                        }
                    }
                }

                return View(viewModel);
            }
            catch (Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult ValidateUser(RegistrationViewModel viewModel)
        {
            try
            {
                if (!(string.IsNullOrEmpty(viewModel.ConfirmNewPassword) && string.IsNullOrEmpty(viewModel.OneTimeValidator)))
                {
                    var user = _context.User.Where(u => u.OneTimeValidator == viewModel.OneTimeValidator && u.OneTimeValidator != null).FirstOrDefault();
                    if (user != null)
                    {
                        user.Password = viewModel.NewPassword;
                        user.OneTimeValidator = null;

                        _context.Update(user);
                        _context.SaveChanges();

                        SetMessage("Password was changed successfully");
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        var applicant = _context.Applicant.Where(a => a.OneTimeValidator == viewModel.OneTimeValidator && a.OneTimeValidator != null).FirstOrDefault();
                        if (applicant != null)
                        {
                            applicant.Password = viewModel.NewPassword;
                            applicant.OneTimeValidator = null;
                            applicant.IsVerified = true;

                            _context.Update(applicant);
                            _context.SaveChanges();

                            SetMessage("Password was changed successfully");
                            return RedirectToAction("Index");
                        }
                    }
                }
                return View(viewModel);
            }
            catch (Exception ex) { throw ex; }
        }

        //Create Method To output model for countdown
        //Concept Note Start
        [HttpGet]
        public JsonResult GetCountDownForConceptNoteSubmission()
        {
            try
            {
                TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("W. Central Africa Standard Time");
                //DeactivatePastEvent();
                CountDownViewModel model = new CountDownViewModel();
                DateTime startDate;
                DateTime endDate;
                var researchCycleEvent = _context.ResearchCycleEvent.Where(r => r.EventTypeId == (int)Types.ConceptNote && r.Session.ActiveForApplication && r.Active).LastOrDefault();
                if (researchCycleEvent != null)
                {
                    startDate = researchCycleEvent.StartDate;
                    startDate = TimeZoneInfo.ConvertTimeFromUtc(startDate, cstZone);
                    if (DateTime.UtcNow.AddHours(1) >= startDate)
                    {
                        //Use End date
                        //Account for time zone
                        //researchCycleEvent.EndDate = TimeZoneInfo.ConvertTimeFromUtc(researchCycleEvent.EndDate, cstZone);
                        endDate = researchCycleEvent.EndDate;
                        endDate = TimeZoneInfo.ConvertTimeFromUtc(endDate, cstZone);
                        model.Day = endDate.Day;
                        model.Year = endDate.Year;
                        model.Hour = endDate.Hour;
                        model.Month = endDate.Month;
                        model.Second = endDate.Second;
                        model.Minuite = endDate.Minute;
                        model.Type = "Time Left to End of Concept Note Submission";

                        //Get the Proposal Event and compare the times for the one with the least time
                        //var researchCycleProposalEvent = _context.ResearchCycleEvent.Where(r => r.EventTypeId == (int)Types.Proposal && r.Session.ActiveForApplication && r.Active).LastOrDefault();
                        //if ((researchCycleProposalEvent?.EndDate - DateTime.Now) < (researchCycleEvent.EndDate - DateTime.Now))
                        //{
                        //    model = null;
                        //}
                    }
                    //else
                    //{
                    //    //Use Start date
                    //    model.Day = researchCycleEvent.StartDate.Day;
                    //    model.Year = researchCycleEvent.StartDate.Year;
                    //    model.Hour = researchCycleEvent.StartDate.Hour;
                    //    model.Month = researchCycleEvent.StartDate.Month;
                    //    model.Second = researchCycleEvent.StartDate.Second;
                    //    model.Type = "Count Down To Start of Concept Note Submission";

                    //    //Get the Proposal Event and compare the times for the one with the least time
                    //    var researchCycleProposalEvent = _context.ResearchCycleEvent.Where(r => r.EventTypeId == (int)Types.Proposal && r.Session.ActiveForApplication).LastOrDefault();
                    //    if ((researchCycleProposalEvent?.StartDate - DateTime.Now) < (researchCycleEvent.StartDate - DateTime.Now))
                    //    {
                    //        model = null;
                    //    }
                    //}
                }

                return Json(model);
            }
            catch (Exception ex) { throw ex; }
        }

        //Proposal Start
        [HttpGet]
        public JsonResult GetCountDownForProposalSubmission()
        {
            try
            {
                TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("W. Central Africa Standard Time");
                //DeactivatePastEvent();
                CountDownViewModel model = new CountDownViewModel();
                DateTime startDate;
                DateTime endDate;
                var researchCycleEvent = _context.ResearchCycleEvent.Where(r => r.EventTypeId == (int)Types.Proposal && r.Session.ActiveForApplication && r.Active).FirstOrDefault();
                if (researchCycleEvent != null)
                {
                    
                    //researchCycleEvent.StartDate = TimeZoneInfo.ConvertTimeFromUtc(researchCycleEvent.StartDate, cstZone);
                    startDate = researchCycleEvent.StartDate;
                    startDate = TimeZoneInfo.ConvertTimeFromUtc(startDate, cstZone);

                    if (DateTime.UtcNow.AddHours(1) >= startDate)
                    {
                        //Use End date
                        
                        //researchCycleEvent.EndDate = TimeZoneInfo.ConvertTimeFromUtc(researchCycleEvent.EndDate, cstZone);
                        endDate = researchCycleEvent.EndDate;
                        endDate = TimeZoneInfo.ConvertTimeFromUtc(endDate, cstZone);
                        model.Day = endDate.Day;
                        model.Year = endDate.Year;
                        model.Hour = endDate.Hour;
                        model.Month = endDate.Month;
                        model.Second = endDate.Second;
                        model.Minuite = endDate.Minute;
                        model.Type = "Time Left to End of Proposal Submission";

                        //Get the Proposal Event and compare the times for the one with the least time
                        //var researchCycleConceptnoteEvent = _context.ResearchCycleEvent.Where(r => r.EventTypeId == (int)Types.ConceptNote && r.Session.ActiveForApplication && r.Active).LastOrDefault();
                        //if ((researchCycleConceptnoteEvent?.EndDate - DateTime.Now) < (researchCycleEvent.EndDate - DateTime.Now))
                        //{
                        //    model = null;
                        //}
                    }
                    //else
                    //{
                    //    //Use Start date
                    //    model.Day = researchCycleEvent.StartDate.Day;
                    //    model.Year = researchCycleEvent.StartDate.Year;
                    //    model.Hour = researchCycleEvent.StartDate.Hour;
                    //    model.Month = researchCycleEvent.StartDate.Month;
                    //    model.Second = researchCycleEvent.StartDate.Second;
                    //    model.Type = "Count Down To Start of Proposal Submission";

                    //    //Get the Proposal Event and compare the times for the one with the least time
                    //    var researchCycleConceptnoteEvent = _context.ResearchCycleEvent.Where(r => r.EventTypeId == (int)Types.ConceptNote && r.Session.ActiveForApplication && r.Active).LastOrDefault();
                    //    if ((researchCycleConceptnoteEvent?.StartDate - DateTime.Now) < (researchCycleEvent.StartDate - DateTime.Now))
                    //    {
                    //        model = null;
                    //    }
                    //}
                }

                return Json(model);
            }
            catch (Exception ex) { throw ex; }
        }

        //List of other ongoing events that do no have Start and End dates
        [HttpGet]
        public JsonResult GetEventWithoutCountdown()
        {
            try
            {
                //DeactivatePastEvent();
                var events = _context.ResearchCycleEvent
                                     .Where(r => r.EventTypeId != (int)Types.ConceptNote && r.EventTypeId != (int)Types.Proposal && r.Active && r.Session.ActiveForApplication)
                                     .Include(r => r.EventType)
                                     .Select(r => new EventWithoutStartDateViewModel()
                                     {
                                         EventEndDate = r.EndDate.ToString("dd/MM/yyyy"),
                                         EventStartDate = r.StartDate.ToString("dd/MM/yyyy"),
                                         EventName = r.EventType.Name
                                     })
                                     .FirstOrDefault();

                return Json(new { Event = events != null ? events : null });
            }
            catch (Exception ex) { throw ex; }
        }

        [HttpGet]
        public JsonResult GetConceptNoteAndProposalSubmissionCount()
        {
            try
            {
                SubmissionCount model = new SubmissionCount();

                var activeApplicationSession = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                if (activeApplicationSession?.Id > 0)
                {
                    Category category = new Category();
                    //determine if user is admin or coadmin
                    var response = GetCoAdminCategory();
                    List<ApplicantReviewer> applicationReviewer = new List<ApplicantReviewer>();
                    List<ProposalScoring> proposalScoring = new List<ProposalScoring>();
                    if (response != null && !response.IsAdmin && response.Category != null)
                    {
                        category = response.Category;
                        model.ConceptNote = _context.ConceptNote.Where(f => f.ApplicantThematic.SessionId == activeApplicationSession.Id 
                        && f.Submitted && f.ApplicantThematic.Theme.CategoryId==category.Id).ToList().Count();
                        model.Proposal = _context.Proposal.Where(f => f.ApplicantThematic.SessionId == activeApplicationSession.Id && f.Submitted 
                        && f.ApplicantThematic.Theme.CategoryId == category.Id).ToList().Count();
                        //Reviewer Count
                        model.NoOfAccessor=_context.ReviewerThematicStrenght.Where(f => f.Theme.CategoryId == category.Id).Select(f => f.Reviewer).Distinct().ToList().Count();

                        applicationReviewer = _context.ApplicantReviewer.Where(g => g.ApplicantThematic.SessionId == activeApplicationSession.Id && g.ApplicantThematic.Theme.CategoryId==category.Id).ToList();
                        proposalScoring=_context.ProposalScoring.Where(f => f.ApplicantThematic.Theme.CategoryId == category.Id && f.ApplicantThematic.Session.ActiveForApplication).ToList();
                        model.PICount=_context.ApplicantThematic.Where(f => f.Session.ActiveForApplication && f.Theme.CategoryId == category.Id).ToList().Count();
                    }
                    else if(response!=null && response.IsAdmin)
                    {
                        model.ConceptNote = _context.ConceptNote.Where(f => f.ApplicantThematic.SessionId == activeApplicationSession.Id && f.Submitted).ToList().Count();
                        model.Proposal = _context.Proposal.Where(f => f.ApplicantThematic.SessionId == activeApplicationSession.Id && f.Submitted).ToList().Count();
                        model.NoOfAccessor = _context.ReviewerThematicStrenght.Select(f => f.Reviewer).Distinct().ToList().Count();

                        applicationReviewer = _context.ApplicantReviewer.Where(g => g.ApplicantThematic.SessionId == activeApplicationSession.Id).ToList();
                        proposalScoring = _context.ProposalScoring.Where(f => f.ApplicantThematic.Session.ActiveForApplication).ToList();
                        model.PICount = _context.ApplicantThematic.Where(f => f.Session.ActiveForApplication).ToList().Count();
                    }
                    

                    if (applicationReviewer?.Count > 0)
                    {
                        var conceptNote = applicationReviewer.Where(f => f.TypeId == (int)Types.ConceptNote).ToList();
                        var proposal = applicationReviewer.Where(f => f.TypeId == (int)Types.Proposal).ToList();
                        var uniqueConceptNoteApplication = conceptNote.GroupBy(g => g.ApplicantThematicId).ToList();
                        if (uniqueConceptNoteApplication?.Count > 0)
                        {
                            foreach (var item in uniqueConceptNoteApplication)
                            {
                                var approvedConceptNote = _context.Approval.Where(f => f.ApplicantThematicId == item.Key && !f.IsProposal && f.Approved).ToList();
                                if (approvedConceptNote?.Count > 0)
                                {
                                    model.ApprovedConceptNote += 1;
                                }
                                var conceptNoteAssessments=proposalScoring.Where(f => f.ApplicantThematicId == item.Key && f.TypeId==(int)Types.ConceptNote).ToList();
                                if (conceptNoteAssessments.Count > 1)
                                {
                                    //ensure all reviewers have assessed and submitted
                                    if (conceptNoteAssessments.Any(f => f.IsSubmitted == false))
                                    {

                                    }
                                    else
                                    {
                                        model.NoOfAssessedConceptNote += 1;
                                    }

                                }
                                
                            }
                        }
                        var uniqueProposalApplication = proposal.GroupBy(g => g.ApplicantThematicId).ToList();
                        if (uniqueProposalApplication?.Count > 0)
                        {
                            foreach (var item in uniqueProposalApplication)
                            {
                                var approvedProposal = _context.Approval.Where(f => f.ApplicantThematicId == item.Key && f.IsProposal && f.Approved).ToList();
                                if (approvedProposal?.Count > 0)
                                {
                                    model.ApprovedProposal += 1;
                                }
                                var conceptNoteAssessments = proposalScoring.Where(f => f.ApplicantThematicId == item.Key && f.TypeId == (int)Types.Proposal).ToList();
                                if (conceptNoteAssessments.Count > 1)
                                {
                                    //ensure all reviewers have assessed and submitted
                                    if (conceptNoteAssessments.Any(f => f.IsSubmitted == false))
                                    {

                                    }
                                    else
                                    {
                                        model.NoOfAssessedProposal += 1;
                                    }

                                }
                            }
                        }


                    }


                }



                return Json(model);
            }
            catch (Exception ex) { throw ex; }
        }

        [HttpGet]
        [Authorize]
        public IActionResult DownloadDocument()
        {
            try
            {
                string fileRootPath = Path.Combine(_IhostingEnvironment.WebRootPath, "UserGuides");
                string fileName = string.Empty;
                var role = User.FindFirst(ClaimTypes.Role).Value;

                if (role == "Admin" || role == "Super-Admin")
                {
                    fileName = "Admin_User_Guide.pdf";
                }
                else if (role == "Reviewer")
                {
                    fileName = "Assessor_User_Guide.pdf";
                }
                else if (role == "Applicant")
                {
                    fileName = "PI_User_Guide.pdf";
                }
                else if (role == "Co-Applicant")
                {
                    fileName = "Co_Reseacher_User_Guide.pdf";
                }
                else if (role == "Co-Admin")
                {
                    fileName = "Co_Admin_User_Guide.pdf";
                }
                else
                {
                    fileName = "No_Guide.pdf";
                }

                string fullPart = Path.Combine(fileRootPath, fileName);
                //var extName = Path.GetExtension(GuideFile.FileName);
                //string saveName = string.Format("{0}{1}", guideCategory, extName);
                //filePath = Path.Combine(uploads, saveName);

                FileInfo file = new FileInfo(fullPart);
                if (!file.Exists)
                {
                    fullPart = Path.Combine(fileRootPath, "No_Guide.pdf");
                }


                //string fullPart = Path.Combine(fileRootPath, fileName);
                //if (!Directory.Exists(fullPart))
                //{
                //    fullPart = Path.Combine(fileRootPath, "No_Guide.pdf");
                //}
                byte[] fileBytes = System.IO.File.ReadAllBytes(fullPart);
                if (!string.IsNullOrEmpty(fileName))
                {
                    return File(fileBytes, "application/force-download", fileName);
                }

                return null;
            }
            catch (Exception ex) { throw ex; }
        }

        public void DeactivatePastEvent()
        {
            try
            {
                var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                if (activeSessionForApplication?.Id > 0)
                {
                    DateTime currentDateTime = DateTime.UtcNow.AddHours(-8);

                    var activeEvents = _context.ResearchCycleEvent.Where(r => r.Active && r.SessionId == activeSessionForApplication.Id).ToList();
                    if (activeEvents?.Count > 0)
                    {

                        foreach (var item in activeEvents)
                        {
                            //Deactivate events that have gotten to the end time
                            if (item.EndDate < currentDateTime)
                            {
                                item.Active = false;
                                _context.Update(item);
                                _context.SaveChanges();
                            }
                        }
                    }
                    var events = _context.ResearchCycleEvent.Where(r => ((r.StartDate == currentDateTime && r.EndDate > currentDateTime)
                    || (r.StartDate <= currentDateTime && r.EndDate > currentDateTime))
                    && r.SessionId == activeSessionForApplication.Id).OrderBy(r => r.StartDate).FirstOrDefault();
                    if (events?.Id > 0)
                    {
                        events.Active = true;
                        _context.Update(events);
                        _context.SaveChanges();
                    }
                }



            }
            catch (Exception)
            {

                throw;
            }
        }
        //Check for default password
        public bool CheckForDefaultPassword(string password)
        {
            bool stillDefault = false;
            if (password != null)
            {
                if (password == "1234567")
                {
                    stillDefault = true;
                    return stillDefault;
                }
            }
            return stillDefault;
        }
        public IActionResult ChangePassword(string guid)
        {
            try
            {
                RegistrationViewModel viewModel = new RegistrationViewModel();
                if (!string.IsNullOrEmpty(guid))
                {
                    var userToBeValidated = _context.User.Where(u => u.OneTimeValidator == guid).FirstOrDefault();
                    if (userToBeValidated != null)
                    {
                        viewModel.OneTimeValidator = userToBeValidated.OneTimeValidator;
                    }
                    else
                    {
                        var applicantToBeValidated = _context.Applicant.Where(u => u.OneTimeValidator == guid).FirstOrDefault();
                        if (applicantToBeValidated != null)
                        {
                            viewModel.OneTimeValidator = applicantToBeValidated.OneTimeValidator;
                        }
                        else
                        {
                            SetMessage("This account has already been verified, enter login credentials to continue");
                            return RedirectToAction("Index");
                        }
                    }
                }

                return View(viewModel);
            }
            catch (Exception ex) { throw ex; }
        }
        public IActionResult RedirectToResearcherLogin(string id)
        {
            if (!String.IsNullOrEmpty(id))
            {
                var stringId = Utility.Decrypt(id);
                var applicantId = Convert.ToInt32(stringId);
                var applicant = _context.Applicant.Where(f => f.Id == applicantId).FirstOrDefault();
                if (applicant?.Id > 0)
                {
                    _registrationViewModel.ResearcherLogins = _context.Applicant.Where(f => f.ApplicationNo == applicant.ApplicationNo)
                        .Select(f => new ResearcherLogin
                        {
                            ApplicantId = f.Id,
                            RoleId = f.RoleId,

                        })
                        .OrderBy(f=>f.RoleId).ToList();
                }

            }

            return View(_registrationViewModel);
        }
        [HttpPost]
        public async Task<IActionResult> ResearcherContinueingLogin(RegistrationViewModel model)
        {
            List<CoReseacherLoginDetail> coReseacherLoginDetailList = new List<CoReseacherLoginDetail>();
            if (model?.ResearcherLogins?.Count > 0)
            {
                Applicant applicant = new Applicant();
                foreach (var item in model.ResearcherLogins)
                {
                    if (item.RoleId == (int)Roles.Applicant && !String.IsNullOrEmpty(item.Password) && item.ApplicantId > 0)
                    {
                        var userDetail = _context.Applicant.Where(f => f.Id == item.ApplicantId).FirstOrDefault();
                        //var userDetail = _utility.VerifyApplicantLoginDetail(applicant.ApplicationNo, item.Password);
                        if (userDetail != null)
                        {
                            //if (userDetail.IsLoggedIn)
                            //{
                            //    SetMessage("Your account is logged in on another device. Please log out on the other device to continue here");
                            //    return RedirectToAction("logout");
                            //}
                            //if (!userDetail.IsVerified)
                            //{
                            //    SetMessage("This account has not been verified. Log into your email to verify your account");
                            //    return RedirectToAction("Index");
                            //}
                            var person = _context.Person.Where(p => p.Id == userDetail.PersonId).FirstOrDefault();
                            if (person.PassportUrl == null)
                            {
                                person.PassportUrl = "~/theme/dist/img/avatar.png";
                            }
                            var loginHash = Guid.NewGuid().ToString();
                            var identity = new ClaimsIdentity(new[] {
                         new Claim(ClaimTypes.Name, person.ApplicantFullName),
                         new Claim(ClaimTypes.Email,userDetail.ApplicationNo),
                         new Claim(ClaimTypes.StreetAddress, person.PassportUrl),
                         new Claim(ClaimTypes.Role, userDetail.Role.Name),
                         new Claim(ClaimTypes.Hash, loginHash),

                         //new Claim(ClaimTypes.GroupSid, )
                        }, CookieAuthenticationDefaults.AuthenticationScheme);

                            var principal = new ClaimsPrincipal(identity);
                            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                            userDetail.DeviceMacAddress = loginHash;
                            userDetail.IsLoggedIn = true;
                            userDetail.LastLogin = DateTime.Now;
                            _context.Update(userDetail);
                            _context.SaveChanges();
                            return RedirectToAction("Dashboard", "UserHome", new { area = "" });
                        }
                    }
                    else if (item.RoleId == (int)Roles.CoApplicant && !String.IsNullOrEmpty(item.Password) && item.ApplicantId > 0)
                    {
                        applicant = _context.Applicant.Where(f => f.Id == item.ApplicantId).FirstOrDefault();
                        var allTeamRecord = await _context.Team.Where(f => f.PersonId == applicant.PersonId).ToListAsync();
                        if (allTeamRecord?.Count > 0)
                        {
                            foreach (var items in allTeamRecord)
                            {
                                CoReseacherLoginDetail coReseacherLoginDetail = new CoReseacherLoginDetail();
                                var applicantThematic = await _context.ApplicantThematic.Where(f => f.Id == items.ApplicantThematicId).FirstOrDefaultAsync();
                                if (applicantThematic?.Id > 0)
                                {
                                    coReseacherLoginDetail.ApplicantId = applicant.Id;
                                    coReseacherLoginDetail.ApplicantThematicId = items.ApplicantThematicId;
                                    coReseacherLoginDetail.Title = items.ApplicantThematic.Title;
                                    coReseacherLoginDetailList.Add(coReseacherLoginDetail);
                                }
                            }
                        }

                        _registrationViewModel.CoReseacherLoginDetails = coReseacherLoginDetailList;
                        return View(_registrationViewModel);
                    }

                }

            }
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> CoResearcherContinueingLogin(int applicantThematicId, int applicantId)
        {
            if (applicantThematicId > 0 && applicantId > 0)
            {
                var applicant = _context.Applicant.Where(f => f.Id == applicantId).FirstOrDefault();
                if (applicant == null)
                {
                    SetMessage("Applicant detail is invalid");
                    return RedirectToAction("Index");
                }
                var applicantThematic=_context.Team.Where(f => f.ApplicantThematicId == applicantThematicId && f.PersonId == applicant.PersonId).FirstOrDefault();
                if ( applicant?.Id > 0 && applicantThematic?.Id > 0)
                {
                    
                    if (applicant != null)
                    {
                        //if (applicant.IsLoggedIn)
                        //{
                        //    SetMessage("Your account is logged in on another device. Please log out on the other device to continue here");
                        //    return RedirectToAction("logout");
                        //}
                        //if (!userDetail.IsVerified)
                        //{
                        //    SetMessage("This account has not been verified. Log into your email to verify your account");
                        //    return RedirectToAction("Index");
                        //}
                        var person = _context.Person.Where(p => p.Id == applicant.PersonId).FirstOrDefault();
                        if (person.PassportUrl == null)
                        {
                            person.PassportUrl = "~/theme/dist/img/avatar.png";
                        }
                        var loginHash = Guid.NewGuid().ToString();
                        var identity = new ClaimsIdentity(new[] {
                         new Claim(ClaimTypes.Name, person.ApplicantFullName),
                         new Claim(ClaimTypes.Email,applicant.ApplicationNo),
                         new Claim(ClaimTypes.StreetAddress, person.PassportUrl),
                         new Claim(ClaimTypes.Role, applicant.Role.Name),
                         new Claim(ClaimTypes.GroupSid,applicantThematicId.ToString()),
                         new Claim(ClaimTypes.Hash, loginHash),
                        }, CookieAuthenticationDefaults.AuthenticationScheme);

                        var principal = new ClaimsPrincipal(identity);
                        await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                        applicant.LastLogin = DateTime.Now;
                        applicant.DeviceMacAddress = loginHash;
                        applicant.IsLoggedIn = true;
                        _context.Update(applicant);
                        _context.SaveChanges();
                        var redirectUrl = Url.Action("Dashboard", "UserHome", new { area = "" });
                        return Json(new { redirectUrl });
 
                    }

                }

                else
                {
                    SetMessage("Research Team does not exist!");
                    return RedirectToAction("Index");
                }
            }
            SetMessage("Something Went wrong!");
            return RedirectToAction("Index");
        }
        public IActionResult PrivacyPolicy()
        {
            return View();
        }
        public IActionResult CompleteRegistration(string guid)
        {
            try
            {
                if (!string.IsNullOrEmpty(guid))
                {
                    _registrationViewModel.User = new User();
                    var userToBeValidated = _context.User.Where(u => u.OneTimeValidator == guid).FirstOrDefault();
                    if (userToBeValidated != null)
                    {
                        _registrationViewModel.User = userToBeValidated;
                        _registrationViewModel.OneTimeValidator = userToBeValidated.OneTimeValidator;
                    }
                    else
                    {
                       
                            SetMessage("This token is no longer valid");
                            return RedirectToAction("Index");
                        
                    }
                }

                return View(_registrationViewModel);
            }
            catch (Exception ex) { throw ex; }
        }
        [HttpPost]
        public IActionResult CompleteRegistration(RegistrationViewModel model)
        {
            if (!String.IsNullOrEmpty(model.OneTimeValidator))
            {
                var user=_context.User.Where(f => f.OneTimeValidator == model.OneTimeValidator).FirstOrDefault();
                if (user?.Id > 0)
                {
                    user.Active = true;
                    user.Password = model.Password;
                    user.OneTimeValidator = null;
                    _context.Update(user);
                    user.Reviewer.Active = true;
                    user.Reviewer.Name = model.User.Reviewer.Name;
                    user.Reviewer.InstitutionId = model.User.Reviewer.InstitutionId;
                    user.Reviewer.PhoneNo = model.User.Reviewer.PhoneNo;
                    user.Reviewer.TitleId = model.User.Reviewer.TitleId;
                    _context.Update(user.Reviewer);
                    _context.SaveChanges();
                    SetMessage("Registration was successful, you can now proceed to login.");
                    return RedirectToAction(nameof(Index));
                }
                SetMessage("Registration was successful, you can now proceed to login.");
                return RedirectToAction("CompleteRegistration",new { guid =model.OneTimeValidator});
            }
            SetMessage("Registration was not successful, contact System the administrator");
            return RedirectToAction(nameof(Index));

        }
        public FileResult WritePdf()
        {
            List<string> validFileExtension = new List<string>();
            List<string> pathToFile = new List<string>();
            validFileExtension.Add("New text");
            validFileExtension.Add("Second New text");
            if (validFileExtension.Count > 0)
            {
                for (int i = 0; i < validFileExtension.Count; i++)
                {
                    using (MemoryStream stream = new System.IO.MemoryStream())
                    {
                        //This code is responsible for initialize the PDF document object.
                        using (Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 10f))
                        {
                            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                            pdfDoc.Open();
                            var uploads = Path.Combine(_IhostingEnvironment.WebRootPath, "ApplicationDownload");
                            string filename = "newPDF_" + i+".pdf";
                            string filePath = Path.Combine(uploads, filename);
                            //This code is responsible for to add the Image file to the PDF document object.
                            //iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(files.InputStream);
                            pdfDoc.Add(new Paragraph(validFileExtension[i]));
                            pdfDoc.Close();
                            pathToFile.Add(filePath);

                            FileInfo file = new FileInfo(filename);
                            if (file.Exists)
                            {
                                file.Delete();
                            }

                            byte[] content = stream.ToArray();

                            // Write out PDF from memory stream.
                            using (FileStream fs = System.IO.File.Create(filePath))
                            {
                                fs.Write(content, 0, (int)content.Length);
                            }
                            
                            //This code is responsible for download the PDF file.
                            //return File(stream.ToArray(), "application/pdf", "sample.pdf");
                        }
                    }
                }
                var fileName = string.Format("{0}_DownloadSubmission.zip", DateTime.Today.Date.ToString("dd-MM-yyyy") + "_1");
                var newPath = Path.Combine(_IhostingEnvironment.WebRootPath, "ZipDownload");
                var tempOutPutPath = Path.Combine(newPath, fileName);

                using (ZipOutputStream s = new ZipOutputStream(System.IO.File.Create(tempOutPutPath)))
                {
                    s.SetLevel(9); // 0-9, 9 being the highest compression  

                    byte[] buffer = new byte[4096];
                    //download zip
                    for (int i = 0; i < pathToFile.Count; i++)
                {
                    ZipEntry entry = new ZipEntry(Path.GetFileName(pathToFile[i]));
                    entry.DateTime = DateTime.Now;
                    entry.IsUnicodeText = true;
                    s.PutNextEntry(entry);

                    using (FileStream fs = System.IO.File.OpenRead(pathToFile[i]))
                    {
                        int sourceBytes;
                        do
                        {
                            sourceBytes = fs.Read(buffer, 0, buffer.Length);
                            s.Write(buffer, 0, sourceBytes);
                        } while (sourceBytes > 0);
                    }
                }
                s.Finish();
                s.Flush();
                s.Close();


            }
                byte[] finalResult = System.IO.File.ReadAllBytes(tempOutPutPath);
                if (System.IO.File.Exists(tempOutPutPath))
                    System.IO.File.Delete(tempOutPutPath);

                if (finalResult == null || !finalResult.Any())
                    throw new Exception(String.Format("No Files found with Image"));

                return File(finalResult, "application/zip", fileName);

            }
            return File("", "application/pdf", "sample.pdf");

        }
        public AssigedCategory GetCoAdminCategory()
        {
            AssigedCategory assigedCategory = new AssigedCategory();
            string adminEmail = User.FindFirstValue(ClaimTypes.Email);
            var admin = _context.User.Where(d => d.Email == adminEmail).FirstOrDefault();
            if (admin?.Id > 0)
            {
                if (admin.Role.Name != "Admin" && admin.Role.Name != "Super-Admin")
                {

                    var coAdminCategory = _context.CategoryAdministrator.Where(F => F.UserId == admin.Id && F.Active).Select(f => f.Category).FirstOrDefault();
                    assigedCategory.Category = coAdminCategory;
                    return assigedCategory;

                }
                else
                {
                    assigedCategory.IsAdmin = true;
                    return assigedCategory;
                }

            }
            return assigedCategory;
        }
        [HttpGet]
        public JsonResult PopulateReviewer()
        {
            //ReviewerCount model = new ReviewerCount();
            ReviewDashboardModel model = new ReviewDashboardModel();
            string reviewerEmail = User.FindFirstValue(ClaimTypes.Email);
            var reviewer = _context.User.Where(d => d.Email == reviewerEmail).FirstOrDefault();
            if (reviewer?.Id > 0)
            {
                var assignment=_context.ApplicantReviewer.Where(f => f.ApplicantThematic.Session.ActiveForApplication && f.ReviewerId == reviewer.Id).ToList();
                var assessment=_context.ProposalScoring.Where(f => f.ApplicantThematic.Session.ActiveForApplication && f.ReviewerId == reviewer.Id).ToList();
                model.ReviewerConceptNoteCount.ConceptNote = assignment.Where(f => f.TypeId == (int)Types.ConceptNote).ToList().Count();
                model.ReviewerProposalCount.Proposal = assignment.Where(f => f.TypeId == (int)Types.Proposal).ToList().Count();
                model.ReviewerConceptNoteCount.NoOfAssessedConceptNote = assessment.Where(f => f.TypeId == (int)Types.ConceptNote && f.IsSubmitted).ToList().Count();
                model.ReviewerProposalCount.NoOfAssessedProposal = assessment.Where(f => f.TypeId == (int)Types.Proposal && f.IsSubmitted).ToList().Count();
                model.ReviewerConceptNoteCount.SavedConceptNoteAssessment = assessment.Where(f => f.TypeId == (int)Types.ConceptNote && f.IsSubmitted==false).ToList().Count();
                model.ReviewerProposalCount.SavedProposalAssessment = assessment.Where(f => f.TypeId == (int)Types.Proposal && f.IsSubmitted==false).ToList().Count();
                var applicantReviewer = _context.ApplicantReviewer.Where(d => d.ReviewerId == reviewer.Id && d.TypeId == (int)Types.ConceptNote).ToList();
                if (applicantReviewer?.Count > 0)
                {
                    foreach(var item in applicantReviewer)
                    {
                        var conceptNotes = _context.ApplicantReviewer.Where(f => f.ApplicantThematicId == item.ApplicantThematicId && f.ApplicantThematic.Session.ActiveForApplication && f.TypeId == (int)Types.ConceptNote).ToList();
                        var reviewerProposalAssessment = _context.ProposalAssessment.Where(f => f.ApplicantThematicId==item.ApplicantThematicId && f.ApplicantThematic.Session.ActiveForApplication).ToList();
                        if (conceptNotes.Count > 1)
                        {
                            if ((conceptNotes.Max(f=>f.ConceptNoteMark) - conceptNotes.Min(f => f.ConceptNoteMark)) > 10)
                            {
                                model.ReviewerConceptNoteCount.NoOfDiscodanceConceptnote = +1;
                            }
                        }
                        if (reviewerProposalAssessment.Count > 1)
                        {
                            if ((reviewerProposalAssessment.Max(f => f.Score) - reviewerProposalAssessment.Min(f => f.Score)) > 10)
                            {
                                model.ReviewerProposalCount.NoOfDiscodanceProposal = +1;
                            }
                        }
                        var approvedSubmission=_context.Approval.Where(f => f.ApplicantThematicId == item.ApplicantThematicId && f.Approved).ToList();
                        var conceptNoteApproved=approvedSubmission.Where(f => f.IsProposal).FirstOrDefault();
                        if (conceptNoteApproved?.Id > 0)
                            model.ReviewerConceptNoteCount.ApprovedConceptNote += 1;
                        var proposalApproved=approvedSubmission.Where(f => !f.IsProposal).FirstOrDefault();
                        if (proposalApproved?.Id > 0)
                            model.ReviewerProposalCount.ApprovedProposal += 1;
                       
                    }
                }
            }
            return Json(model);

        }

        //[HttpGet]
        //public async Task<IActionResult> DownloadDocument()
        //{
        //    var path = Path.Combine(_IhostingEnvironment.WebRootPath, "UserGuides");
        //    var memory = new MemoryStream();
        //    string fileName = string.Empty;
        //    string fullPath = string.Empty;

        //    if (ClaimTypes.Role.ToString() == "Admin")
        //    {   
        //        fileName = "Admin_Guide.pdf";
        //        fullPath = Path.Combine(path, fileName);
        //    }
        //    if (ClaimTypes.Role.ToString() == "Reviewer")
        //    {
        //        fileName = "Reviewer_Guide.pdf";
        //        fullPath = Path.Combine(path, fileName);
        //    }
        //    if (ClaimTypes.Role.ToString() == "Applicant" || ClaimTypes.Role.ToString() == "Co-Applicant")
        //    {
        //        fileName = "User_Guide.pdf";
        //        fullPath = Path.Combine(path, fileName);
        //    }

        //    using (var stream = new FileStream(path, FileMode.Open))
        //    {
        //        await stream.CopyToAsync(memory);
        //    }
        //    memory.Position = 0;
        //    var ext = Path.GetExtension(path).ToLowerInvariant();
        //    return File(memory, GetMimeTypes()[ext], Path.GetFileName(path));
        //}

        //private Dictionary<string, string> GetMimeTypes()
        //{
        //    return new Dictionary<string, string>
        //    {
        //        {".txt", "text/plain"},
        //        {".pdf", "application/pdf"},
        //        {".doc", "application/vnd.ms-word"},
        //        {".docx", "application/vnd.ms-word"},
        //        {".png", "image/png"},
        //        {".jpg", "image/jpeg"}
        //    };
        //}
    }
}
