﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Grant.Web.Infrastructure;
using Grant.Web.Models;
using Grant.Web.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;

namespace Grant.Web.Controllers
{
    [Authorize(Policy = "MustBeAdmin")]
    public class ManageAssessorController : BaseController
    {
        private readonly DataBaseContext _context;
        AdminViewModel _adminViewModel;
        Utility _utility;
        private const string Value = "Id";
        private const string Text = "Name";
        private const string Id = "Reviewer.Name";
        private const string Name = "ReviewerId";
        private const string ReviewerName = "Name";
        private const string ReviewerId = "ReviewerId";
        public const string Select = "-- Select --";
        private IHostingEnvironment _hostingEnvironment;
        public ManageAssessorController(DataBaseContext context, IHostingEnvironment hostingEnvironment, Utility utility)
        {
            _context = context;
            _adminViewModel = new AdminViewModel();
            _adminViewModel.PopulateAllDropdown(_context);
            _hostingEnvironment = hostingEnvironment;
            _utility = utility;


        }

        public IActionResult AddReviewer()
        {
            return View(_adminViewModel);
        }
        [HttpPost]
        public IActionResult AddReviewer(IFormCollection collection)
        {
            try
            {
                Reviewer reviewer = new Reviewer();
                User user = new Models.User();
                reviewer.Email = (collection["Reviewer.Email"]);
                reviewer.Name = (collection["Reviewer.Name"]);
                reviewer.PhoneNo = (collection["Reviewer.PhoneNo"]);
                reviewer.TitleId = Convert.ToInt32(collection["Reviewer.TitleId"]);
                reviewer.InstitutionId = Convert.ToInt32(collection["Reviewer.InstitutionId"]);
                //user.UserName = (collection["User.UserName"]);
                //if (!user.UserName.Contains("/"))
                //{
                //    SetMessage("Username does not follow the pattern. eg: surname/firstname");
                //    return View();
                //}

                string oneTimeValidator = Convert.ToString(Guid.NewGuid());

                //Send Email
                _utility.SendEMail(reviewer.Email, reviewer.Name, (int)MessageType.ReviewerCreation, "", "","", oneTimeValidator, "");

                _utility.AddReviewer(reviewer.Name, reviewer.PhoneNo, reviewer.Email, user.UserName, reviewer.TitleId, reviewer.InstitutionId, oneTimeValidator);
                SetMessage("Accessor added successfully. The accessor can now click the link sent to his email to complete registration.");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Index");
        }
        public IActionResult Index()
        {
            try
            {
                List<ReviewerDetail> reviewerDetail = new List<ReviewerDetail>();
                var users = _context.User.Where(g => g.RoleId == (int)RoleType.Reviewer).ToList();
                if (users.Count > 0)
                {
                    for (int i = 0; i < users.Count; i++)
                    {
                        ReviewerDetail detail = new ReviewerDetail();
                        var reviewerId = users[i].ReviewerId;
                        var reviewer = _context.Reviewer.Where(g => g.Id == reviewerId).FirstOrDefault();
                        //var count = _context.ApplicantReviewer.Where(f => f.ReviewerId == reviewerId).Count();
                        var thematicStrenght=_context.ReviewerThematicStrenght.Where(f => f.ReviewerId == reviewerId).FirstOrDefault();
                        detail.User = users[i];
                        detail.Phone = reviewer.PhoneNo;
                        //detail.ApplicantCount = count;
                        detail.Reviewer = reviewer;
                        detail.Theme = thematicStrenght!=null?thematicStrenght.Theme:new Theme();
                        reviewerDetail.Add(detail);
                    }

                }
                _adminViewModel.ReviewerDetails = reviewerDetail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult EditReviewer(int id)
        {
            try
            {
                if (id > 0)
                {
                    _adminViewModel.Reviewer = _context.Reviewer.Where(d => d.Id == id).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        //public IActionResult UpdateReviewer(IFormCollection collection)
        public IActionResult UpdateReviewer([Bind("Id,Name,Email,Active,PhoneNo")] Reviewer reviewer)
        {
            try
            {
                if (reviewer.Id > 0)
                {
                    var existingReviewer = _context.Reviewer.Where(f => f.Id == reviewer.Id).FirstOrDefault();
                    var existingUser = _context.User.Where(f => f.ReviewerId == reviewer.Id).FirstOrDefault();
                    var exist = _utility.EmailExist(reviewer.Email, reviewer.Id);
                    if (exist)
                    {
                        SetMessage("You cannot use this Email," + reviewer.Email);
                        return RedirectToAction("ViewReviewer", new { id = reviewer.Id });
                    }
                    if (existingReviewer != null && existingUser != null)
                    {
                        using (TransactionScope scope = new TransactionScope())
                        {
                            existingReviewer.Name = reviewer.Name;
                            existingReviewer.PhoneNo = reviewer.PhoneNo;
                            existingReviewer.Email = reviewer.Email;
                            existingReviewer.Active = reviewer.Active;
                            _context.Update(existingReviewer);
                            // update corresponding User
                            existingUser.Email = reviewer.Email;
                            existingUser.Active = reviewer.Active;
                            _context.Update(existingUser);
                            _context.SaveChanges();
                            scope.Complete();
                        }

                        SetMessage("Update was successfull");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("ViewReviewer");
        }
        public IActionResult GetAssessorExpertiseDetail(long id)
        {
            try
            {
                if (id > 0)
                {
                    var reviewer= _context.Reviewer.Where(f => f.Id == id).FirstOrDefault();
                    if (reviewer?.Id > 0)
                    {
                        _adminViewModel.ReviewerThematicStrenght = _context.ReviewerThematicStrenght.Where(d => d.ReviewerId == reviewer.Id)
                            .Include(r=>r.Theme)
                            .ThenInclude(r=>r.Category)
                            .ToList();
                        _adminViewModel.Reviewer = reviewer;
                        //Get existing reviewers Category
                        var existingReviewerStrenght = _utility.GetAssessorsExpertCategory(reviewer.Id);
                        if (existingReviewerStrenght?.Id > 0)
                        {
                            _adminViewModel.PopulateReviewerExpertriateCategory(existingReviewerStrenght.Theme.Category.Id, null);
                        }
                        else
                        {
                            _adminViewModel.PopulateReviewerExpertriateCategory(0, null);
                        }
                    }
                    else
                    {
                        SetMessage("No Assessor, with selected Identity found");
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult AddAssessorExpertise(AdminViewModel viewModel)
        {
            try
            {
 
                if(viewModel!=null && viewModel.Reviewer?.Id > 0)
                {
                    var reviewer = _context.Reviewer.Where(d => d.Id == viewModel.Reviewer.Id).FirstOrDefault();


                    var existingStregth = _context.ReviewerThematicStrenght.Where(g => g.ReviewerId == reviewer.Id && g.ThemeId == viewModel.ThemeId).FirstOrDefault();
                    if (existingStregth != null)
                    {
                        existingStregth.Strenght = viewModel.Strength;
                        _context.Update(existingStregth);
                    }
                    else
                    {
                        ReviewerThematicStrenght reviewerThematicStrenght = new ReviewerThematicStrenght()
                        {
                            ReviewerId = reviewer.Id,
                            Strenght = viewModel.Strength,
                            ThemeId = viewModel.ThemeId
                        };
                        _context.Add(reviewerThematicStrenght);
                    }
                    _context.SaveChanges();
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("GetAssessorExpertiseDetail",new {id=viewModel.Reviewer.Id });
        }
        //Non Action Method
        public IActionResult GetTheme(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }
            int categoryId = Convert.ToInt32(value);
            var themecategoryList = _context.Theme.Where(c => c.Active && c.CategoryId == categoryId).ToList();

            return Json(new SelectList(themecategoryList, Value, Text));
        }
    }
}