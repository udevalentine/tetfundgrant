﻿using Grant.Web.Infrastructure;
using Grant.Web.Models;
using Grant.Web.Models.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace Grant.Web.Controllers
{
    public class UtilityController : BaseController
    {
        private readonly IHostingEnvironment _IhostingEnvironment;
        private readonly DataBaseContext _context;
        private readonly Utility _utility;
        UtilityViewModel _utilityViewModel;
        private const string Value = "Id";
        private const string Text = "Name";
        private string baseUrl;
        private Microsoft.Extensions.Configuration.IConfiguration _configuration;
        private IFluentEmailLogic _fluentEmailLogic;

        public UtilityController(DataBaseContext context, Utility utility, IHostingEnvironment hostingEnvironment, Microsoft.Extensions.Configuration.IConfiguration configuration, IFluentEmailLogic fluentEmailLogic)
        {
            _context = context;
            _utility = utility;
            _utilityViewModel = new UtilityViewModel();
            //_utilityViewModel.PopulateAllDropdown(_context);
            _configuration = configuration;
            _IhostingEnvironment = hostingEnvironment;
            baseUrl = _configuration.GetValue<string>("Url:onlineVerificationRoot");
            _fluentEmailLogic = fluentEmailLogic;
        }
        public IActionResult SendEmail()
        {

            return View();
        }
        public IActionResult Send(string email, string subject, string message)
        {
            JsonResultModel result = new JsonResultModel();
            if (!String.IsNullOrEmpty(email) && !String.IsNullOrEmpty(subject) && !String.IsNullOrEmpty(message))
            {

                MailGunModel mailGunModel = new MailGunModel();
                mailGunModel.Subject = subject;
                mailGunModel.Link = baseUrl;
                mailGunModel.MessageTo = email ?? "support@lloydant.com";
                mailGunModel.Body = message;
                Email emailObj = new Email();
                var template = emailObj.FormatHtmlTemplate(mailGunModel, 1);
                EmailSenderLogic<MailGunModel> receiptEmailSenderLogic = new EmailSenderLogic<MailGunModel>(template, mailGunModel);

                receiptEmailSenderLogic.Send(mailGunModel);

                result.IsError = false;
                result.Message = "Message Sent Successfully";
                return Json(result);
            }
            result.IsError = true;
            result.Message = "Operation not Successful";
            return Json(result);

        }
        public IActionResult SendPIEmail()
        {
            _utilityViewModel.ApplicantThematics = _context.ApplicantThematic.ToList();
            return View(_utilityViewModel);
        }
        public IActionResult SendAccessorEmail()
        {
            string adminEmail = User.FindFirstValue(ClaimTypes.Email);
            var admin = _context.User.Where(d => d.Email == adminEmail).FirstOrDefault();
            if (admin.RoleId != (int)RoleType.Admin && admin.RoleId != (int)RoleType.SuperAdmin)
            {
                var categoryAdmin = _context.CategoryAdministrator.Where(f => f.User.Id == admin.Id && f.Active).FirstOrDefault();
                if (categoryAdmin == null)
                {
                    SetMessage("Category has not been assigned to you");
                    return RedirectToAction("AdminHome", "Index");
                }
                _utilityViewModel.Reviewers=_context.ReviewerThematicStrenght.Where(f =>f.Theme.CategoryId==categoryAdmin.CategoryId).Select(f=>f.Reviewer).Distinct().ToList();
            }
            else
            {
                _utilityViewModel.Reviewers = _context.Reviewer.Where(f => f.RoleId == (int)Roles.Reviewer).ToList();
            }


            return View(_utilityViewModel);
        }
        public IActionResult SendPI(List<string> emailArray, string subject, string message)
        {
            JsonResultModel result = new JsonResultModel();
            if (emailArray.Count > 0 && !String.IsNullOrEmpty(subject) && !String.IsNullOrEmpty(message))
            {
                foreach (var item in emailArray)
                {
                    MailGunModel mailGunModel = new MailGunModel();
                    mailGunModel.Subject = subject;
                    mailGunModel.Link = baseUrl;
                    mailGunModel.MessageTo = item ?? "support@lloydant.com";
                    mailGunModel.Body = message;
                    mailGunModel.Name = "Researcher";
                    //_fluentEmailLogic.EmailFormatter(mailGunModel, 14);
                    Email emailObj = new Email();
                    var template = emailObj.FormatHtmlTemplate(mailGunModel, (int)MessageType.SendRegularMail);
                    EmailSenderLogic<MailGunModel> receiptEmailSenderLogic = new EmailSenderLogic<MailGunModel>(template, mailGunModel);

                    receiptEmailSenderLogic.Send(mailGunModel);

                }


                result.IsError = false;
                result.Message = "Message Sent Successfully";
                return Json(result);
            }
            result.IsError = true;
            result.Message = "Operation not Successful";
            return Json(result);

        }
        public IActionResult SendAccessor(List<string> emailArray, string subject, string message)
        {
            JsonResultModel result = new JsonResultModel();
            if (emailArray.Count > 0 && !String.IsNullOrEmpty(subject) && !String.IsNullOrEmpty(message))
            {
                foreach (var item in emailArray)
                {
                    MailGunModel mailGunModel = new MailGunModel();
                    mailGunModel.Subject = subject;
                    mailGunModel.Link = baseUrl;
                    mailGunModel.MessageTo = item ?? "support@lloydant.com";
                    mailGunModel.Body = message;
                    mailGunModel.Name = "Accessor";
                    Email emailObj = new Email();
                    var template = emailObj.FormatHtmlTemplate(mailGunModel, (int)MessageType.SendRegularMail);
                    EmailSenderLogic<MailGunModel> receiptEmailSenderLogic = new EmailSenderLogic<MailGunModel>(template, mailGunModel);
                    if (CheckForInternetConnection())
                        receiptEmailSenderLogic.Send(mailGunModel);
                }


                result.IsError = false;
                result.Message = "Message Sent Successfully";
                return Json(result);
            }
            result.IsError = true;
            result.Message = "Operation not Successful";
            return Json(result);

        }
        public IActionResult AllPIAndCoresearcher()
        {
            
           _utilityViewModel.Applicants= _context.Applicant.Where(f=>f.IsVerified==false).ToList();
            return View(_utilityViewModel);
        }
        public IActionResult VerifyReseacher(List<long> idArray)
        {
            JsonResultModel result = new JsonResultModel();
            List<Applicant> applicants = new List<Applicant>();
            if (idArray.Count > 0 )
            {
                foreach (var item in idArray)
                {
                    var applicant=_context.Applicant.Where(f => f.Id == item).FirstOrDefault();
                    if (applicant?.Id > 0)
                    {
                        applicant.IsVerified = true;
                        _context.Update(applicant);
                        applicants.Add(applicant);

                    }
                }
                _context.SaveChanges();
                if (applicants?.Count>0)
                {
                    foreach(var item in applicants)
                    {
                        MailGunModel mailGunModel = new MailGunModel();
                        if (item.RoleId == (int)RoleType.Co_Applicant)
                        {
                            
                            mailGunModel.Body = "Your account has verified you can now proceed to login. Your password is 1234567 ";
                        }
                        else
                        {
                            mailGunModel.Body = "Your account has verified you can now proceed to login";
                        }

                        mailGunModel.Subject = "Account Verified";
                        mailGunModel.Link = baseUrl;
                        mailGunModel.MessageTo = item.ApplicationNo ?? "support@lloydant.com";
                        
                        mailGunModel.Name = "Researcher";

                        Email emailObj = new Email();
                        var template = emailObj.FormatHtmlTemplate(mailGunModel, (int)MessageType.SendRegularMail);
                        EmailSenderLogic<MailGunModel> receiptEmailSenderLogic = new EmailSenderLogic<MailGunModel>(template, mailGunModel);

                        receiptEmailSenderLogic.Send(mailGunModel);
                    }
                }


                result.IsError = false;
                result.Message = "Message Sent Successfully";
                return Json(result);
            }
            result.IsError = true;
            result.Message = "Operation not Successful";
            return Json(result);

        }
        public IActionResult AllResearcher()
        {

            _utilityViewModel.Applicants = _context.Applicant.ToList();
            return View(_utilityViewModel);
        }
        public IActionResult SendPIYetToSubmitConceptNoteEmail()
        {
            var submittedId= _context.ConceptNote.Where(f => f.Submitted && f.ApplicantThematic.Session.ActiveForApplication ).Select(f=>f.ApplicantThematic.ApplicantId).ToList();
           _utilityViewModel.Applicants= _context.Applicant.Where(f => !submittedId.Contains(f.Id) && f.RoleId == (int)RoleType.Applicant).ToList();
            //_utilityViewModel.ApplicantThematics = _context.ApplicantThematic.Where(f=>!submittedId.Contains(f.Id) && f.Session.ActiveForApplication).ToList();
            return View(_utilityViewModel);
        }
    }
}