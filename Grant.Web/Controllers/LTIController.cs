﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Grant.Web.Infrastructure;
using Grant.Web.Models;
using LtiLibrary.AspNetCore.Extensions;
using LtiLibrary.NetCore.Common;
using LtiLibrary.NetCore.Lti.v1;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace Grant.Web.Controllers
{
    public class LTIController : BaseController
    {
        private readonly DataBaseContext _context;
        Utility _utility;
        private readonly string url = "https://api.ithenticate.com/rpc";
        private readonly string password = "1@password";
        private readonly string userName = "ugonwabunike@gmail.com";
        private string token = string.Empty;
        IHostingEnvironment _hostingEnvironment;
        private int folderId = 1794101;


        public LTIController(DataBaseContext context, Utility utility, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _utility = utility;
            _hostingEnvironment = hostingEnvironment;
        }
        public async Task<IActionResult> Index()
        {
            //This method allows other LMS to get send you data or show your UI in them.
            //Blackboard/Moodle can talk to us and after the validations below, you can redirect or return the default page
            if (Request != null && Request.Form != null)
            {
                // Parse and validate the request
                var ltiRequest = await Request.ParseLtiRequestAsync();

                // Make sure this is an LtiRequest
                try
                {
                    ltiRequest.CheckForRequiredLtiParameters();
                }
                catch (Exception ex)
                {
                    return BadRequest(ex);
                }


                if (!ltiRequest.ConsumerKey.Equals("ONE"))
                {
                    return Unauthorized();
                }

                var oauthSignature = ltiRequest.GenerateSignature("TWO");
                if (!oauthSignature.Equals(ltiRequest.Signature))
                {
                    return Unauthorized();
                }
                // The request is legit, so display the tool
                // You can load the default properties here like the user profile/ registered courses

                return View();

            }


            return View();
        }

        public string AddUnderscore(string rawString)
        {
            try
            {
                rawString = rawString.Replace(".", "");
                return rawString.Replace(" ", "_");
            }
            catch(Exception ex) { throw ex; }
        }

        public void DeleteFileIfExists(string filePath)
        {
            try
            {
                string path = Path.GetDirectoryName(filePath);
                string[] filePatches = filePath.Split("\\");
                string fileName = filePatches[filePatches.Length - 1];

                if (System.IO.File.Exists(Path.Combine(path, fileName)))
                {
                    System.IO.File.Delete(Path.Combine(path, fileName));
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public async Task<IActionResult> Launch()
        {
            //This allows us to send data and consume other services like turnitin
            var request = new LtiRequest(LtiConstants.BasicLaunchLtiMessageType)
            {
                ConsumerKey = "12345",
                LisPersonNameFull = "d'Artagnan",
                Nonce = "f68bdf5dbed34d7aa6ff18456bf3185e",
                ResourceLinkId = "link",
                Timestamp = 1517630398,
                Url = Request.GetUri(),
                CustomParameters = "The Proposal/material that you want to be validated",
            };
            await Response.WriteLtiRequest(request, "secret");

            return View();
        }
        //public IActionResult CheckForPlagiarism(long ApplicantThematicId)
        //{
        //    string fullProposalResult = "";
        //    string submitResponse =string.Empty;
        //    string logInResponse = string.Empty;
        //    try
        //    {
        //        var applicantThematic=_context.ApplicantThematic.Where(g => g.Id == ApplicantThematicId).FirstOrDefault();
        //        if (applicantThematic?.Id > 0)
        //        {
        //            fullProposalResult = _utility.ConcatonateProposalSubmission(ApplicantThematicId);
        //            string filePath = "";
        //            var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "TurnitinTextFiles");
        //            string fileName = AddUnderscore(applicantThematic.Applicant.Person.ApplicantFullName) + ".txt";
        //            filePath = Path.Combine(uploads, fileName);

        //            //Check if file exists and delete duplicates
        //            DeleteFileIfExists(filePath);

        //            SaveResults(filePath, fullProposalResult);
        //            var dirfilePath = "/TurnitinTextFiles/" + fileName;
        //            var payLoad = loginNode();
        //            using (WebClient client = new WebClient())
        //            {
        //                logInResponse = client.UploadString(url, "POST", payLoad);
        //            }

        //            token = GetToken(logInResponse);
        //            token = token.Substring(3);
        //            //var folderPayload=createFolder();
        //            //using (WebClient client = new WebClient())
        //            //{
        //            //    client.UploadString(url, "POST", folderPayload);
        //            //}
        //            var splittedName=applicantThematic.Applicant.Person.Name.Split(' ', 2);

        //            var submitPayLoad = submitDocument(splittedName[0], splittedName[1], applicantThematic.Title, dirfilePath);
        //            using (WebClient client = new WebClient())
        //            {
        //                submitResponse=client.UploadString(url, "POST", submitPayLoad);
        //            }


        //        }

        //    }
        //    catch(Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return Ok(submitResponse);
        //}

        [HttpPost]
        public IActionResult CheckForPlagiarism(List<long> idList)
        {
            string fullProposalResult = "";
            string submitResponse = string.Empty;
            string logInResponse = string.Empty;
            try
            {
                foreach (var item in idList)
                {
                    var applicantThematic = _context.ApplicantThematic.Where(g => g.Id == item).FirstOrDefault();
                    if (applicantThematic?.Id > 0)
                    {
                        fullProposalResult = _utility.ConcatonateProposalSubmission(item);
                        string filePath = "";
                        var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "TurnitinTextFiles");
                        string fileName = AddUnderscore(applicantThematic.Applicant.Person.ApplicantFullName) + ".txt";
                        filePath = Path.Combine(uploads, fileName);

                        //Check if file exists and delete duplicates
                        DeleteFileIfExists(filePath);

                        SaveResults(filePath, fullProposalResult);
                        var dirfilePath = "/TurnitinTextFiles/" + fileName;
                        var payLoad = loginNode();
                        using (WebClient client = new WebClient())
                        {
                            logInResponse = client.UploadString(url, "POST", payLoad);
                        }

                        token = GetToken(logInResponse);
                        token = token.Substring(3);
                        //var folderPayload=createFolder();
                        //using (WebClient client = new WebClient())
                        //{
                        //    client.UploadString(url, "POST", folderPayload);
                        //}
                        var splittedName = applicantThematic.Applicant.Person.Name.Split(' ', 2);

                        var submitPayLoad = submitDocument(splittedName[0], splittedName[1], applicantThematic.Title, dirfilePath);
                        using (WebClient client = new WebClient())
                        {
                            submitResponse = client.UploadString(url, "POST", submitPayLoad);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Ok(submitResponse);
        }
        public string GetToken(string xmlDocument)
        {
            try
            {
                string token = string.Empty;
                XDocument xmlDoc = XDocument.Parse(xmlDocument);
                IEnumerable<XElement> nodeList = xmlDoc.Descendants("member");

                //XElement outerNode = nodeList.ElementAt(1);
                //token = outerNode.Value;

                foreach(var item in nodeList)
                {
                    if (item.Value.Contains("sid"))
                    {
                        token = item.Value;
                        break;
                    }
                }

                return token;
            }
            catch(Exception ex) { throw ex; }
        }


        public string loginNode()
        {
            XDocument xmlElement = null;
            StringBuilder sb = new StringBuilder();
            try
            {
                XElement methodCall = new XElement("methodCall");
                XElement methodName = new XElement("methodName", "login");
                XElement paramsList = new XElement("params");
                XElement param = new XElement("param");
                XElement value = new XElement("value");
                XElement structNode = new XElement("struct");
                XElement member1 = new XElement("member");
                XElement name1 = new XElement("name", "password");
                member1.Add(name1);
                XElement value1 = new XElement("value");
                member1.Add(value1);
                value1.Add(new XElement("string", password));

                XElement member2 = new XElement("member");
                XElement name2 = new XElement("name", "username");
                member2.Add(name2);
                XElement value2 = new XElement("value");
                member2.Add(value2);
                value2.Add(new XElement("string", userName));


                structNode.Add(member1);
                structNode.Add(member2);
                value.Add(structNode);
                param.Add(value);
                paramsList.Add(param);

                methodCall.Add(methodName);
                methodCall.Add(paramsList);
                
                xmlElement = new XDocument(methodCall);

                
                XmlWriterSettings xws = new XmlWriterSettings();
                xws.OmitXmlDeclaration = true;
                xws.Encoding = Encoding.UTF8;
                xws.Indent = true;

                using (XmlWriter xw = XmlWriter.Create(sb, xws))
                {
                    //var doc = loginNode("ugonwabunike@gmail.com", "1@password");

                    xmlElement.Save(xw);
                }
            }
            catch (Exception ex) { throw ex; }

            return sb.ToString();
        }
        public string createFolder()
        {
            try
            {
                XDocument xmlElement = null;
                StringBuilder sb = new StringBuilder();

                XElement methodCall = new XElement("methodCall");
                XElement methodName = new XElement("methodName", "folder.add");
                XElement paramList = new XElement("params");
                XElement singleParam = new XElement("param");
                XElement valueEl = new XElement("value");
                XElement structEl = new XElement("struct");

                XElement member1 = new XElement("member");
                XElement name1 = new XElement("name", "sid");
                XElement value1 = new XElement("value");
                XElement string1 = new XElement("string", token);

                XElement member2 = new XElement("member");
                XElement name2 = new XElement("name", "folder_group");
                XElement value2 = new XElement("value");
                XElement string2 = new XElement("int", 123);

                XElement member3 = new XElement("member");
                XElement name3 = new XElement("name", "name");
                XElement value3 = new XElement("value");
                XElement string3 = new XElement("string", "6016_testfolder_773");

                XElement member4 = new XElement("member");
                XElement name4 = new XElement("name", "description");
                XElement value4 = new XElement("value");
                XElement string4 = new XElement("string", "API client test folder");

                XElement member5 = new XElement("member");
                XElement name5 = new XElement("name", "exclude_quotes");
                XElement value5 = new XElement("value");
                XElement string5 = new XElement("boolean", 1);

                value1.Add(string1);
                member1.Add(name1);
                member1.Add(value1);

                value2.Add(string2);
                member2.Add(name2);
                member2.Add(value2);

                value3.Add(string3);
                member3.Add(name3);
                member3.Add(value3);

                value4.Add(string4);
                member4.Add(name4);
                member4.Add(value4);

                value5.Add(string5);
                member5.Add(name5);
                member5.Add(value5);

                structEl.Add(member1);
                structEl.Add(member2);
                structEl.Add(member3);
                structEl.Add(member4);
                structEl.Add(member5);
                valueEl.Add(structEl);
                singleParam.Add(valueEl);
                paramList.Add(singleParam);
                methodCall.Add(methodName);
                methodCall.Add(paramList);

                xmlElement = new XDocument(methodCall);

                XmlWriterSettings xws = new XmlWriterSettings();
                xws.OmitXmlDeclaration = true;
                xws.Encoding = Encoding.UTF8;
                xws.Indent = true;

                using (XmlWriter xw = XmlWriter.Create(sb, xws))
                {
                    xmlElement.Save(xw);
                }

                return sb.ToString();
            }
            catch (Exception ex) { throw ex; }
        }

        public string submitDocument(string firstName, string lastName, string title, string filePath)
        {
            try
            {   
                //string text = System.IO.File.ReadAllText(filePath);
                XDocument xmlElement = null;
                StringBuilder sb = new StringBuilder();

                XElement methodCall = new XElement("methodCall");
                XElement methodName = new XElement("methodName", "document.add");
                XElement paramsList = new XElement("params");
                XElement param = new XElement("param");
                XElement valueEl = new XElement("value");
                XElement structEl = new XElement("struct");

                XElement member1 = new XElement("member");
                XElement name1 = new XElement("name", "sid");
                XElement value1 = new XElement("value");
                value1.Add(new XElement("string",token));

                XElement member2 = new XElement("member");
                XElement name2 = new XElement("name", "uploads");
                XElement value2 = new XElement("value");
                XElement arrayEl = new XElement("array");
                XElement dataEl = new XElement("data");
                XElement innerValue = new XElement("value");
                XElement innerStruct = new XElement("struct");

                XElement innerMember1 = new XElement("member");
                innerMember1.Add(new XElement("name", "filename"));
                XElement innerValue1 = new XElement("value");
                innerValue1.Add(new XElement("string", filePath));
                innerMember1.Add(innerValue1);

                XElement innerMember2 = new XElement("member");
                innerMember2.Add(new XElement("name", "author_last"));
                XElement innerValue2 = new XElement("value");
                innerValue2.Add(new XElement("string", lastName));
                innerMember2.Add(innerValue2);

                XElement innerMember3 = new XElement("member");
                innerMember3.Add(new XElement("name", "upload"));
                XElement innerValue3 = new XElement("value");
                innerValue3.Add(new XElement("base64", "Zm9vIGJhciBiaXo="));
                innerMember3.Add(innerValue3);

                XElement innerMember4 = new XElement("member");
                innerMember4.Add(new XElement("name", "title"));
                XElement innerValue4 = new XElement("value");
                innerValue4.Add(new XElement("string", title));
                innerMember4.Add(innerValue4);

                XElement innerMember5 = new XElement("member");
                innerMember5.Add(new XElement("name", "author_first"));
                XElement innerValue5 = new XElement("value");
                innerValue5.Add(new XElement("string", firstName));
                innerMember5.Add(innerValue5);

                innerStruct.Add(innerMember1);
                innerStruct.Add(innerMember2);
                innerStruct.Add(innerMember3);
                innerStruct.Add(innerMember4);
                innerStruct.Add(innerMember5);
                innerValue.Add(innerStruct);
                dataEl.Add(innerValue);
                arrayEl.Add(dataEl);

                XElement member3 = new XElement("member");
                XElement name3 = new XElement("name", "submit_to");
                XElement value3 = new XElement("value");
                value3.Add(new XElement("int", 1));

                XElement member4 = new XElement("member");
                XElement name4 = new XElement("name", "folder");
                XElement value4 = new XElement("value");
                value4.Add(new XElement("int", folderId));


                member1.Add(name1);
                member1.Add(value1);

                member2.Add(name2);
                value2.Add(arrayEl);
                member2.Add(value2);

                member3.Add(name3);
                member3.Add(value3);

                member4.Add(name4);
                member4.Add(value4);

                param.Add(valueEl);
                paramsList.Add(param);
                structEl.Add(member1);
                structEl.Add(member2);
                structEl.Add(member3);
                structEl.Add(member4);

                valueEl.Add(structEl);
                methodCall.Add(methodName);
                methodCall.Add(paramsList);
                xmlElement = new XDocument(methodCall);

                XmlWriterSettings xws = new XmlWriterSettings();
                xws.OmitXmlDeclaration = true;
                xws.Encoding = Encoding.UTF8;
                xws.Indent = true;

                using (XmlWriter xw = XmlWriter.Create(sb, xws))
                {
                    xmlElement.Save(xw);
                }

                return sb.ToString();
            }
            catch (Exception ex) { throw ex; }
        }

        public void SaveResults(string saveToPath, string text)
        {
            
            using (StreamWriter file =
                new StreamWriter(string.Format(saveToPath), true))
            {
                file.WriteLine(text);
                
            }

        }
    }
}