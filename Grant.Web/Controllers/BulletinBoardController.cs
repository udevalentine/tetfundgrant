﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Grant.Web.Infrastructure;
using Grant.Web.Models;
using Grant.Web.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Grant.Web.Controllers
{
    [Authorize(Policy = "MustBeApplicant")]
    public class BulletinBoardController : BaseController
    {
        private readonly DataBaseContext _context;
        private readonly Utility _utility;
        BulletinViewModel _viewModel;
        IHostingEnvironment _hostingEnvironment;

        public BulletinBoardController(DataBaseContext context, Utility utility, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            _context = context;
            _utility = utility;
            _viewModel = new BulletinViewModel();
            _viewModel.SetViewModelProperties(_context);
        }
        public IActionResult CreateBulletinTopic()
        {
            string personEmail = User.FindFirstValue(ClaimTypes.Email);
            string roleName = User.FindFirstValue(ClaimTypes.Role);
            if (personEmail != null)
            {

                var applicantThematic = new ApplicantThematic();
                var person = _context.Person.Where(f => f.Email == personEmail).FirstOrDefault();
                if (roleName == "Co-Applicant")
                {
                    string teamThematic = User.FindFirstValue(ClaimTypes.GroupSid);
                    var applicantThematiceId = Convert.ToInt32(teamThematic);
                    applicantThematic = _context.ApplicantThematic.Where(f => f.Id == applicantThematiceId).FirstOrDefault();

                }
                else
                {
                    applicantThematic = _context.ApplicantThematic.Where(f => f.Applicant.PersonId == person.Id && f.Applicant.RoleId == (int)Roles.Applicant && f.Session.ActiveForApplication).FirstOrDefault();
                   
                }
                if (applicantThematic == null)
                {
                    SetMessage("Please add a Research Topic to continue");
                    return RedirectToAction("Index","userhome");
                }


            }
            else
            {
                return RedirectToAction("Logout", "Home");
            }
            return View(_viewModel);
        }

        [HttpPost]
        public IActionResult CreateBulletinTopic(BulletinViewModel bulletinViewModel)
        {
            if (bulletinViewModel.BulletinTopic.Topic != null)
            {
                string personEmail = User.FindFirstValue(ClaimTypes.Email);
                string roleName = User.FindFirstValue(ClaimTypes.Role);
                if (personEmail != null)
                {
                    var applicantThematic = new ApplicantThematic();
                    var person=_context.Person.Where(f => f.Email == personEmail).FirstOrDefault();
                    if (roleName == "Co-Applicant")
                    {
                        string teamThematic = User.FindFirstValue(ClaimTypes.GroupSid);
                        var applicantThematiceId = Convert.ToInt32(teamThematic);
                        applicantThematic=_context.ApplicantThematic.Where(f => f.Id == applicantThematiceId).FirstOrDefault();

                    }
                    else
                    {
                        applicantThematic = _context.ApplicantThematic.Where(f => f.Applicant.PersonId==person.Id && f.Applicant.RoleId==(int)Roles.Applicant && f.Session.ActiveForApplication).FirstOrDefault();
                    }
                    
                    var topicCreated=_utility.CreateBulletinTopic(bulletinViewModel.BulletinTopic, person, applicantThematic);
                    if (topicCreated?.Id > 0)
                    {
                        return RedirectToAction("ViewAllBulletinActiveTopic");
                    }
                   
                }
                else
                {
                    return RedirectToAction("Logout", "Home");
                }
                
            }
            else
            {
                SetMessage("Kindly enter a Topic to continue");
            }
            return View(bulletinViewModel);
        }

        public IActionResult ViewAllBulletinActiveTopic()
        {
            try
            {
                string personEmail = User.FindFirstValue(ClaimTypes.Email);
                string roleName = User.FindFirstValue(ClaimTypes.Role);
                if (personEmail != null)
                {

                    var applicantThematic = new ApplicantThematic();
                    var person = _context.Person.Where(f => f.Email == personEmail).FirstOrDefault();
                    if (roleName == "Co-Applicant")
                    {
                        string teamThematic = User.FindFirstValue(ClaimTypes.GroupSid);
                        var applicantThematiceId = Convert.ToInt32(teamThematic);
                        applicantThematic = _context.ApplicantThematic.Where(f => f.Id == applicantThematiceId).FirstOrDefault();
                        if (applicantThematic == null)
                        {
                            SetMessage("Please add a Research Topic to continue");
                            return RedirectToAction("Index", "userhome");
                        }

                    }
                    else
                    {
                        applicantThematic = _context.ApplicantThematic.Where(f => f.Applicant.PersonId == person.Id && f.Applicant.RoleId == (int)Roles.Applicant && f.Session.ActiveForApplication).FirstOrDefault();
                        if (applicantThematic == null)
                        {
                            SetMessage("Please add a Research Topic to continue");
                            return RedirectToAction("Index", "userhome");
                        }
                    }

                        _viewModel.SetBulletinTopics(applicantThematic);

                }
                else
                {
                    return RedirectToAction("Logout", "Home");
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(_viewModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult SendMessageWithoutAttachment(IFormCollection form)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {   
                string message = (form["Message"]);
                var bulletinTopicId = (form["bulletinId"])[0];
                string fileName = "";

                if (!(string.IsNullOrEmpty(message) && string.IsNullOrEmpty(bulletinTopicId)))
                {
                    string personEmail = User.FindFirstValue(ClaimTypes.Email);

                    long bulletinId = Convert.ToInt64(bulletinTopicId);

                    var attachementPath = "";
                    var person = _context.Person.Where(f => f.Email == personEmail).FirstOrDefault();
                    _utility.SaveChatResponse(bulletinId, person, message, attachementPath,fileName);

                    result.IsError = false;
                }
            }
            catch(Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }

            return Json(result);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> SendMessage(IFormCollection form, IFormFile AttachmentFile)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {   
                string message = (form["Message"]);
                var bulletinTopicId = (form["bulletinId"])[0];
                string filePath = "";
                string fileName = "";

                if ((!(string.IsNullOrEmpty(message) && string.IsNullOrEmpty(bulletinTopicId)))|| !(AttachmentFile.Length>0 && !String.IsNullOrEmpty(bulletinTopicId)))
                {
                    string personEmail = User.FindFirstValue(ClaimTypes.Email);
                    long bulletinId = Convert.ToInt64(bulletinTopicId);

                    var person = _context.Person.Where(f => f.Email == personEmail).FirstOrDefault();

                    if (AttachmentFile.Length > 0)
                    {
                        var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "attachments");
                        var extName = Path.GetExtension(AttachmentFile.FileName);
                        fileName = Path.GetFileName(AttachmentFile.FileName);
                        string saveName = string.Format("Attachment_{0}_{1}{2}", DateTime.Now.Second, DateTime.Now.Millisecond, extName);
                        filePath = Path.Combine(uploads, saveName);

                        FileInfo file = new FileInfo(saveName);
                        if (file.Exists)
                        {
                            file.Delete();
                        }

                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            await AttachmentFile.CopyToAsync(stream);
                        }
                        //filePath = string.Format("~/Grant.Web/attachments/{0}", saveName);
                        filePath = "\\attachments\\" + saveName;

                    }
                    _utility.SaveChatResponse(bulletinId, person, message, filePath, fileName);
                    result.IsError = false;
                    result.Message = filePath;
                }
            }
            catch(Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }

            return Json(result);
        }

        public IActionResult JoinBulletinBoard(long bulletinTopicId)
        {
            try
            {
                string personEmail = User.FindFirstValue(ClaimTypes.Email);
                if (bulletinTopicId > 0 && personEmail!=null)
                {
                    _viewModel.Person = _context.Person.Where(f => f.Email == personEmail).FirstOrDefault();
                    _viewModel.BulletinTopic= _context.BulletinTopic.Where(f => f.Id == bulletinTopicId).FirstOrDefault();
                    _viewModel.ChatTopicId = bulletinTopicId;
                    
                }
                else
                {
                    return RedirectToAction("Logout", "Home");
                }



            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(_viewModel);
        }
        public IActionResult AjaxLoadChatBoard (long bulletinTopicId)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                string personEmail = User.FindFirstValue(ClaimTypes.Email);
                if (personEmail != null && bulletinTopicId > 0)
                {
                    var person = _context.Person.Where(f => f.Email == personEmail).FirstOrDefault();
                    result.BulletinChatResponses=_utility.LoadBulletinChatBoard(person, bulletinTopicId);
                    result.IsError = false;
                    
                }
                else
                {
                    return RedirectToAction("Logout", "Home");
                }
                
            }
            catch (Exception ex)
            {

                result.IsError = true;
                result.Message = ex.Message;
            }
            return Json(result);
        }
       
        public IActionResult SaveChatResponse(long bulletinTopicId, string response)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                string fileName = "";
                string personEmail = User.FindFirstValue(ClaimTypes.Email);
                if (personEmail != null && bulletinTopicId > 0 && response!=null)
                {
                    var attachementPath = "dbjfke";
                    var person = _context.Person.Where(f => f.Email == personEmail).FirstOrDefault();
                    _utility.SaveChatResponse(bulletinTopicId, person, response, attachementPath, fileName);
                    result.IsError = false;

                }
                else
                {
                    return RedirectToAction("Logout", "Home");
                }
            }
            catch (Exception ex)
            {

                result.IsError = true;
                result.Message = ex.Message;
            }
            return Json(result);
        }

    }
}