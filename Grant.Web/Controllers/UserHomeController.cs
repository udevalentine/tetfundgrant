﻿using Grant.Web.Infrastructure;
using Grant.Web.Models;
using Grant.Web.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Grant.Web.Controllers
{
    [Authorize(Policy = "MustBeApplicant")]
    public class UserHomeController : BaseController
    {
        private readonly DataBaseContext _context;
        UserViewModel _userViewModel;
        Utility _utility;
        private const string Value = "Id";
        private const string Text = "Name";
        private IHostingEnvironment _hostingEnvironment;
        private IConfiguration _configuration;
        private string rootUrl;
        public UserHomeController(DataBaseContext context, IHostingEnvironment hostingEnvironment, Utility utility, IConfiguration configuration)
        {
            _context = context;
            _userViewModel = new UserViewModel();
            _userViewModel.PopulateAllDropdown(_context);
            _hostingEnvironment = hostingEnvironment;
            _utility = utility;
            _configuration = configuration;
            rootUrl = string.IsNullOrEmpty(_configuration.GetValue<string>("Url:root")) ? "" : "//" + _configuration.GetValue<string>("Url:root");


        }
        public IActionResult Index()
        {
            string personEmail = User.FindFirstValue(ClaimTypes.Email);
            string roleName = User.FindFirstValue(ClaimTypes.Role);
            Applicant applicant = new Applicant();
            if (roleName == "Co-Applicant")
                applicant = _context.Applicant.Where(g => g.ApplicationNo == personEmail && g.RoleId == (int)Roles.CoApplicant).FirstOrDefault();
            applicant = _context.Applicant.Where(g => g.ApplicationNo == personEmail && g.RoleId == (int)Roles.Applicant).FirstOrDefault();
            if (applicant?.Id > 0)
            {
                _userViewModel.Person = applicant.Person;
            }

            return View(_userViewModel);
        }
        public IActionResult Dashboard()
        {
            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }
                string personEmail = User.FindFirstValue(ClaimTypes.Email);
                string roleName = User.FindFirstValue(ClaimTypes.Role);
                int applicantThematiceId = 0;
                Applicant applicant = new Applicant();
                if (roleName == "Co-Applicant")
                {
                    applicant = _context.Applicant.Where(g => g.ApplicationNo == personEmail && g.RoleId == (int)Roles.CoApplicant).FirstOrDefault();
                    string teamThematic = User.FindFirstValue(ClaimTypes.GroupSid);
                    applicantThematiceId = Convert.ToInt32(teamThematic);
                }
                else
                {
                    applicant = _context.Applicant.Where(g => g.ApplicationNo == personEmail && g.RoleId == (int)Roles.Applicant).FirstOrDefault();
                }

                

                if (applicant?.Id > 0)
                {
                    _userViewModel.Person = applicant.Person;
                    if (applicantThematiceId > 0)
                    {
                        var applicantThematic = _context.ApplicantThematic.Where(f => f.Id == applicantThematiceId).FirstOrDefault();
                        if (applicantThematic?.Id > 0)
                        {
                            applicant = applicantThematic.Applicant;
                        }
                    }

                    //_userViewModel.PICronologicalOrder = _utility.PIChronologicalOrder(applicant, applicantThematiceId);
                    _userViewModel.PITimeLine=_utility.PITimeLineStatus(applicant);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(_userViewModel);
        }
        public IActionResult Profile()
        {
            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }

                string personEmail = User.FindFirstValue(ClaimTypes.Email);
                string roleName = User.FindFirstValue(ClaimTypes.Role);
                List<ReviewerAssignedScore> reviewerList = new List<ReviewerAssignedScore>();
                List<Person> personTeamList = new List<Person>();
                Applicant applicant = new Applicant();

                if (roleName == "Co-Applicant")
                {
                    string thematicId = User.FindFirstValue(ClaimTypes.GroupSid);
                    var teamThematicId = Convert.ToInt32(thematicId);
                    _userViewModel.ApplicantThematic = _context.ApplicantThematic.Where(o => o.Id == teamThematicId).FirstOrDefault();
                    //applicant = _userViewModel.ApplicantThematic.Applicant;
                    applicant = _context.Applicant.Where(f => f.ApplicationNo == personEmail && f.RoleId == (int)Roles.CoApplicant).FirstOrDefault();

                }
                else
                {
                    var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                    if (activeSessionForApplication == null)
                    {
                        SetMessage("No Active Research cycle at the moment.");
                        return RedirectToAction("Index", "UserHome");
                    }
                    applicant = _context.Applicant.Where(g => g.ApplicationNo == personEmail && g.RoleId == (int)Roles.Applicant).FirstOrDefault();
                    _userViewModel.ApplicantThematic = _context.ApplicantThematic.Where(d => d.ApplicantId == applicant.Id && d.SessionId==activeSessionForApplication.Id ).FirstOrDefault();
                }
                if (applicant != null)
                {

                    _userViewModel.Person = applicant.Person;
                    _userViewModel.Applicant = applicant;


                    //if (person.InstitutionId > 0)
                    //{
                    //    _userViewModel.Institution= _context.Institution.Where(g => g.Id == person.InstitutionId).FirstOrDefault();
                    //}
                    //if (person.QualificationId > 0)
                    //{
                    //    _userViewModel.Qualification = _context.Qualification.Where(g => g.Id == person.QualificationId).FirstOrDefault();
                    //}
                    _userViewModel.TeamMember = new List<Person>();
                    _userViewModel.TeamMember = personTeamList.Count > 0 ? personTeamList : null;
                    if (roleName == "Applicant")
                    {
                        if (_userViewModel.ApplicantThematic?.Id > 0)
                        {
                            var teamList = _context.Team.Where(f => f.ApplicantThematicId == _userViewModel.ApplicantThematic.Id && f.Active).ToList();
                            if (teamList.Count > 0)
                            {
                                for (int j = 0; j < teamList.Count; j++)
                                {
                                    var teamPersonId = teamList[j].PersonId;
                                    var singleTeamPerson = _context.Person.Where(c => c.Id == teamPersonId).FirstOrDefault();
                                    personTeamList.Add(singleTeamPerson);
                                }
                            }

                            //_userViewModel.Theme = _context.Theme.Where(f => f.Id == _userViewModel.ApplicantThematic.ThemeId).FirstOrDefault();

                            //_userViewModel.Category = _context.Category.Where(f => f.Id == _userViewModel.Theme.CategoryId).FirstOrDefault();
                            //_userViewModel.ConceptNote = _context.ConceptNote.Where(f => f.ApplicantThemeId == _userViewModel.ApplicantThematic.Id).FirstOrDefault();

                            //_userViewModel.ApplicantReviewers = _context.ApplicantReviewer.Where(f => f.ApplicantThematicId == _userViewModel.ApplicantThematic.Id).ToList();
                            //if (_userViewModel.ApplicantReviewers.Count > 0)
                            //{
                            //    for (int i = 0; i < _userViewModel.ApplicantReviewers.Count; i++)
                            //    {
                            //        int reviewerId = _userViewModel.ApplicantReviewers[i].ReviewerId;
                            //        int? scoreId = _userViewModel.ApplicantReviewers[i].ScoreId;
                            //        var singleReviewer = _context.Reviewer.Where(f => f.Id == reviewerId).FirstOrDefault();
                            //        ReviewerAssignedScore reviewerAssignedScore = new ReviewerAssignedScore();
                            //        reviewerAssignedScore.Reviewer = new Reviewer();
                            //        reviewerAssignedScore.Reviewer.Email = singleReviewer.Email;
                            //        reviewerAssignedScore.Reviewer.Name = singleReviewer.Name;
                            //        reviewerAssignedScore.Score = scoreId == null ? "Not Reviewed Yet" : _context.Score.Where(f => f.Id == scoreId).Select(g => g.Name).FirstOrDefault();

                            //        reviewerList.Add(reviewerAssignedScore);
                            //    }
                            //}
                            //var allProposalAssessment = _context.ProposalAssessment.Where(f => f.ApplicantThematicId == _userViewModel.ApplicantThematic.Id).ToList();
                            //if (allProposalAssessment.Count > 0)
                            //{
                            //    var notApproved = allProposalAssessment.Where(f => f.Approved == false).FirstOrDefault();
                            //    if (notApproved == null)
                            //    {
                            //        _userViewModel.ProposalIsApproved = true;
                            //    }
                            //}
                            //_userViewModel.Approval = _context.Approval.Where(f => f.ApplicantThematicId == _userViewModel.ApplicantThematic.Id && f.IsProposal == false).FirstOrDefault();
                            //_userViewModel.Proposal = _context.Proposal.Where(f => f.ApplicantThemeId == _userViewModel.ApplicantThematic.Id).FirstOrDefault();
                            //_userViewModel.ReviewerAssignedScoreList = reviewerList;
                            _userViewModel.TeamMember = personTeamList;
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_userViewModel);
        }
        [HttpPost]
        public IActionResult AddTeam([Bind("Name,Email,PhoneNo,TitleId,SexId,RankId")] Person person, int InstitutionId, int QualificationId, string OtherQualification,string OtherRank)
        {
            try
            {
                bool noCoResearcherAccount = false;
                var passportUrl = TempData["saveimage"] as string;
                string personEmail = User.FindFirstValue(ClaimTypes.Email);
                var applicant = _context.Applicant.Where(u => u.ApplicationNo == personEmail && u.RoleId == (int)Roles.Applicant).FirstOrDefault();
                var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();

                //Team member must use existing Email
                var existingEmail = _context.Applicant.Where(e => e.ApplicationNo == person.Email).FirstOrDefault();
                //if (existingEmail != null)
                //{
                //    SetMessage("This email already exists!");
                //    return View(_userViewModel);
                //}
                if (applicant?.Id > 0 && activeSessionForApplication?.Id > 0)
                {
                    string oneTimeValidator = Convert.ToString(Guid.NewGuid());
                    var applicantThematic = _context.ApplicantThematic.Where(f => f.ApplicantId == applicant.Id && f.SessionId == activeSessionForApplication.Id).FirstOrDefault();
                    if (applicantThematic == null)
                    {
                        SetMessage("You have not applied for a research grant in the current Research Cycle and thus you cannot add a Co-Researcher.");
                        return RedirectToAction("Index", "UserHome");
                    }
                    if (existingEmail?.Id > 0)
                    {
                        var existAsTeam=_context.Team.Where(f => f.ApplicantThematicId == applicantThematic.Id && f.PersonId == existingEmail.PersonId).FirstOrDefault();
                        if (existAsTeam?.Id > 0)
                        {
                            SetMessage("Co-Researcher already exists as your research team member. ");
                            return RedirectToAction("Index", "UserHome");
                        }
                        Team team = new Team()
                        {
                            Active = true,
                            ApplicantThematic = applicantThematic,
                            Person = existingEmail.Person
                        };
                        _context.Add(team);
                        if (existingEmail.RoleId == (int)Roles.Applicant)
                        {
                            var existAsCoApplicant = _context.Applicant.Where(f => f.ApplicationNo == existingEmail.ApplicationNo && f.RoleId == (int)Roles.CoApplicant).FirstOrDefault();
                            if (existAsCoApplicant == null)
                            {
                                var existAsApplicant = _context.Applicant.Where(f => f.ApplicationNo == existingEmail.ApplicationNo && f.RoleId == (int)Roles.Applicant).FirstOrDefault();
                                var newTeamApplicant = new Applicant()
                                {
                                    Active = true,
                                    ApplicationNo = person.Email,
                                    DateVerified = DateTime.Now,
                                    IsVerified = false,
                                    Password = existAsApplicant?.Id>0?existAsApplicant.Password: "1234567",
                                    Person = existingEmail.Person,
                                    RoleId = (int)Roles.CoApplicant,
                                    OneTimeValidator = oneTimeValidator
                                };
                                _context.Add(newTeamApplicant);
                                noCoResearcherAccount = true;

                            }
                        }
                        _context.SaveChanges();
                        if (noCoResearcherAccount)
                        {
                            //Co-researcher has no co-Applicant Account, now been created and sending the login detail
                            _utility.SendEMail(person.Email, person.Name, (int)MessageType.AddTeamMember, "", "", applicant.Person.Name, oneTimeValidator, applicantThematic.Title);
                        }
                        else
                        {
                            //already has Co-applicant Account
                            _utility.SendEMail(person.Email, person.Name, (int)MessageType.AddExistingCoResearcher, "", "", applicant.Person.Name, "", applicantThematic.Title);
                        }
                        SetMessage("Operation Successful!");
                        return RedirectToAction("ViewTeamMembers");
                    }

                    _utility.AddTeam(person, applicant.Id, InstitutionId, QualificationId, passportUrl, OtherQualification, oneTimeValidator, applicantThematic,OtherRank);
                    _utility.SendEMail(person.Email, person.Name, (int)MessageType.AddTeamMember, "", "", applicant.Person.Name, oneTimeValidator, applicantThematic.Title);


                }
                SetMessage("Operation Successful!");

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return RedirectToAction("ViewTeamMembers");
        }
        public IActionResult ViewProposal(int id)
        {
            try
            {
                if (id > 0)
                {
                    _userViewModel.Proposal = _context.Proposal.Where(o => o.ApplicantThemeId == id).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_userViewModel);
        }
        public IActionResult MyProposal(int id)
        {
            try
            {
                if (id > 0)
                {
                    _userViewModel.ApplicantSubmission = _context.ApplicantSubmission.Where(f => f.ApplicantThematicId == id).FirstOrDefault();
                    var allAssessment = _context.ProposalScoring.Where(f => f.ApplicantThematicId == id).ToList();
                    _userViewModel.Proposal = _context.Proposal.Where(g => g.ApplicantThemeId == id).FirstOrDefault();
                    var settings = _context.AdminSettings.FirstOrDefault();
                    _userViewModel.ViewReview = settings.ViewReview;
                    if (settings.ViewReview)
                    {
                        if (allAssessment.Count > 0)
                        {
                            var totalReviewers = allAssessment.Count;
                            _userViewModel.ProposalScoring = new ProposalScoring()
                            {
                                ActivityIndicatorScore = ((allAssessment.Sum(f => f.ActivityIndicatorScore)) / totalReviewers),
                                AdditionalSourcesScore = ((allAssessment.Sum(f => f.AdditionalSourcesScore)) / totalReviewers),
                                AimsScore = ((allAssessment.Sum(f => f.AimsScore)) / totalReviewers),
                                BudgetJustificationScore = ((allAssessment.Sum(f => f.BudgetJustificationScore)) / totalReviewers),
                                ConceptualFrameworkScore = ((allAssessment.Sum(f => f.ConceptualFrameworkScore)) / totalReviewers),
                                DataManagementScore = ((allAssessment.Sum(f => f.DataManagementScore)) / totalReviewers),
                                EthicalScore = ((allAssessment.Sum(f => f.EthicalScore)) / totalReviewers),
                                DisseminationStrategiesScore = ((allAssessment.Sum(f => f.DisseminationStrategiesScore)) / totalReviewers),
                                ExecutiveSummaryScore = ((allAssessment.Sum(f => f.ExecutiveSummaryScore)) / totalReviewers),
                                GroupResearchScore = ((allAssessment.Sum(f => f.GroupResearchScore)) / totalReviewers),
                                IntroductionBackgroundScore = ((allAssessment.Sum(f => f.IntroductionBackgroundScore)) / totalReviewers),
                                LiteratureReviewScore = ((allAssessment.Sum(f => f.LiteratureReviewScore)) / totalReviewers),
                                MonitoringScore = ((allAssessment.Sum(f => f.MonitoringScore)) / totalReviewers),
                                PreviousGrantScore = ((allAssessment.Sum(f => f.PreviousGrantScore)) / totalReviewers),
                                ProjectActivities = ((allAssessment.Sum(f => f.ProjectActivities)) / totalReviewers),
                                ProjectGoalScore = ((allAssessment.Sum(f => f.ProjectGoalScore)) / totalReviewers),
                                ProjectImpactScore = ((allAssessment.Sum(f => f.ProjectImpactScore)) / totalReviewers),
                                ResearchMethodolyScore = ((allAssessment.Sum(f => f.ResearchMethodolyScore)) / totalReviewers),
                                ResearchWorkToDateScore = ((allAssessment.Sum(f => f.ResearchWorkToDateScore)) / totalReviewers),
                                StatementOfProblemScore = ((allAssessment.Sum(f => f.StatementOfProblemScore)) / totalReviewers),
                                StudyLocationScore = ((allAssessment.Sum(f => f.StudyLocationScore)) / totalReviewers),
                                TimeFrameScore = ((allAssessment.Sum(f => f.TimeFrameScore)) / totalReviewers),
                                TitleScore = ((allAssessment.Sum(f => f.TitleScore)) / totalReviewers)

                            };
                        }
                        else
                        {
                            _userViewModel.ProposalScoring = new ProposalScoring();
                        }
                    }



                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_userViewModel);
        }
        public IActionResult AddTeam()
        {
            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }

                _userViewModel.PassportUrl = TempData["image"] as string;
                if (_userViewModel.PassportUrl != null)
                {
                    TempData["saveimage"] = _userViewModel.PassportUrl;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_userViewModel);
        }
        [Authorize(Policy = "MustBePI")]
        public IActionResult ViewTeamMembers()
        {
            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }

                string personEmail = User.FindFirstValue(ClaimTypes.Email);

                _userViewModel.Teams = _context.Team.Where(g => g.ApplicantThematic.Applicant.ApplicationNo == personEmail && g.ApplicantThematic.Session.ActiveForApplication && g.Active).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_userViewModel);
        }
        public async Task<IActionResult> UploadTeamImage(IFormFile UploadFile)
        {
            //string storePath = "/Passport/";
            if (UploadFile == null || UploadFile.Length == 0)
                return RedirectToAction("AddTeam");

            string ext = UploadFile.ContentType;

            var fileSize = UploadFile.Length;
            if (fileSize > 1048576)
            {
                SetMessage("Allowed image size is 1MB");
                return RedirectToAction("AddTeam");
            }

            if (!(ext.ToLower().EndsWith("jpg") || ext.ToLower().EndsWith("jpeg") || ext.ToLower().EndsWith("png")))
            {
                SetMessage("Only jpg, jpeg and png Files are allowed!");
                return RedirectToAction("AddTeam");
            }



            var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "Passport");

            string extName = Path.GetExtension(UploadFile.FileName);
            var fileName = UploadFile.FileName;
            var saveName = "teamMemberPassport" + "_" + +DateTime.Now.Millisecond + DateTime.Now.Ticks + extName;
            var filePath = Path.Combine(uploads, saveName);

            FileInfo file = new FileInfo(saveName);
            if (file.Exists)
            {
                file.Delete();
            }

            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await UploadFile.CopyToAsync(stream);
            }
            ViewBag.PassportUrl = rootUrl + "\\Passport\\" + saveName;
            //ViewBag.PassportUrl = "\\tetfundgrant\\Passport\\" + saveName;
            //TempData["image"] = "\\tetfundgrant\\Passport\\" + saveName;
            TempData["image"] = rootUrl + "\\Passport\\" + saveName;
            return RedirectToAction("AddTeam");

        }

        //[Authorize(Policy = "MustBePI")]
        public IActionResult UpdateDetail()
        {
            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }

                string personEmail = User.FindFirstValue(ClaimTypes.Email);
                string roleName = User.FindFirstValue(ClaimTypes.Role);
                var person = _context.Person.Where(g => g.Email == personEmail).FirstOrDefault();
                if (person != null)
                {
                    _userViewModel.Person = person;

                    if (person.InstitutionId > 0)
                    {
                        _userViewModel.Institution = _context.Institution.Where(f => f.Id == person.InstitutionId).FirstOrDefault();
                    }
                    if (person.QualificationId > 0)
                    {
                        _userViewModel.Qualification = _context.Qualification.Where(f => f.Id == person.QualificationId).FirstOrDefault();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_userViewModel);
        }
        [HttpPost]
        public IActionResult UpdateDetail(UserViewModel viewModel)
        {
            try
            {
                string personEmail = User.FindFirstValue(ClaimTypes.Email);
                var existingPerson = _context.Person.Where(g => g.Email == personEmail).FirstOrDefault();
                if (existingPerson != null)
                {
                    if (viewModel.QualificationId > 0)
                    {
                        existingPerson.QualificationId = viewModel.QualificationId;
                    }
                    if (viewModel.InstitutionId > 0)
                    {
                        existingPerson.InstitutionId = viewModel.InstitutionId;
                    }
                    //existingPerson.Name = viewModel.Person.Name;
                    //existingPerson.PhoneNo = viewModel.Person.PhoneNo;
                    _context.Update(existingPerson);
                    _context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Profile");
        }
        //non action method
        public IActionResult GetInstitution(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }
            int categoryId = Convert.ToInt32(value);
            var institutionList = _context.Institution.Where(c => c.Active && c.InstitutionCategoryId == categoryId).OrderBy(d => d.Name).ToList();

            return Json(new SelectList(institutionList, Value, Text));
        }
        public IActionResult GetPublicInstitution(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }
            int categoryId = Convert.ToInt32(value);
            var institutionList = _context.Institution.Where(c => c.Active && c.InstitutionCategoryId == categoryId && c.IsPublic).ToList();

            return Json(new SelectList(institutionList, Value, Text));
        }

        [HttpPost]
        public async Task<IActionResult> UploadImage(IFormFile UploadFile)
        {
            if (UploadFile == null || UploadFile.Length == 0)
                return RedirectToAction("UpdateDetail");

            string ext = UploadFile.ContentType;

            var fileSize = UploadFile.Length;
            if (fileSize > 1048576)
            {
                SetMessage("Allowed image size is 1MB");
                return RedirectToAction("UpdateDetail");
            }

            if (!(ext.ToLower().EndsWith("jpg") || ext.ToLower().EndsWith("jpeg") || ext.ToLower().EndsWith("png")))
            {
                SetMessage("Only jpg, jpeg and png Files are allowed!");
                return RedirectToAction("UpdateDetail");
            }



            var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "Passport");
            //Get the user record

            string personEmail = User.FindFirstValue(ClaimTypes.Email);

            if (personEmail != null)
            {
                var existingPerson = _context.Person.Where(g => g.Email == personEmail).FirstOrDefault();
                string extName = Path.GetExtension(UploadFile.FileName);
                var fileName = UploadFile.FileName;
                var saveName = "applicantPassport" + "_" + personEmail + extName;
                var filePath = Path.Combine(uploads, saveName);

                FileInfo file = new FileInfo(saveName);
                if (file.Exists)
                {
                    file.Delete();
                }

                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await UploadFile.CopyToAsync(stream);
                }

                //existingPerson.PassportUrl = "\\tetfundgrant\\Passport\\" + saveName;
                existingPerson.PassportUrl = rootUrl + "\\Passport\\" + saveName;
                _context.Update(existingPerson);
                _context.SaveChanges();
            }

            return RedirectToAction("UpdateDetail");

        }

        [HttpGet]
        public IActionResult ManageTeam()
        {
            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }
                var loggedInUser = GetLoggedInApplicant(_context);
                if (loggedInUser != null)
                {
                    //get the applicant Number
                    var applicant = _context.Applicant.Where(ap => ap.ApplicationNo == loggedInUser.ApplicationNo && ap.RoleId == (int)Roles.Applicant).FirstOrDefault();
                    if (applicant != null)
                    {
                        _userViewModel.TeamMemberList = _context.Team.Where(ap => ap.ApplicantThematic.ApplicantId == applicant.Id).ToList();
                    }
                }
                return View(_userViewModel);
            }
            catch (Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult DeactivateTeamMember(int id)
        {
            try
            {
                var teamMember = _context.Team.Where(m => m.Id == id).FirstOrDefault();

                if (teamMember != null)
                {
                    bool active = teamMember.Active;

                    teamMember.Active = !teamMember.Active;
                    _context.Update(teamMember);
                    _context.SaveChanges();

                    string msg = string.Format("'{0}' was {1} successfully", teamMember.Person.Name, (active == true ? "Deactivated" : "Activated"));
                    return Json(msg);
                }

                return RedirectToAction("Profile");
            }
            catch (Exception ex) { throw ex; }
        }
        public IActionResult ChangePassword()
        {
            return View(_userViewModel);
        }
        [HttpPost]
        public IActionResult ChangePassword(IFormCollection collection)
        {
            try
            {
                _userViewModel.CurrentPassword = collection["CurrentPassword"];
                _userViewModel.NewPassword = collection["NewPassword"];
                _userViewModel.ConfirmPassword = collection["ConfirmPassword"];
                if (String.IsNullOrEmpty(_userViewModel.NewPassword) || String.IsNullOrEmpty(_userViewModel.CurrentPassword) || String.IsNullOrEmpty(_userViewModel.ConfirmPassword))
                {
                    SetMessage("Please, provide all required fields.");
                    return View(_userViewModel);
                }
                string email = User.FindFirstValue(ClaimTypes.Email);
                string role = User.FindFirstValue(ClaimTypes.Role);
                var user = _context.Applicant.Where(d => d.ApplicationNo == email && d.Role.Name==role).FirstOrDefault();
                if (user != null)
                {
                    if (user.Password != _userViewModel.CurrentPassword)
                    {
                        SetMessage("Your current password dont match, log out and try again.");
                        return View(_userViewModel);
                    }
                    if (_userViewModel.ConfirmPassword != _userViewModel.NewPassword)
                    {
                        SetMessage("Your confirm password do not match, try again.");
                        return View(_userViewModel);
                    }
                    _utility.ChangePassword(_userViewModel.CurrentPassword, _userViewModel.NewPassword, user.ApplicationNo, role);
                    SetMessage("Password changed successfully");
                    return RedirectToAction("Logout", "Home");

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_userViewModel);
        }

    }
}