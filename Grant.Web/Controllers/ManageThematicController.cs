﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grant.Web.Infrastructure;
using Grant.Web.Models;
using Grant.Web.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Grant.Web.Controllers
{
    [Authorize(Policy = "MustBeAdmin")]
    public class ManageThematicController : BaseController
    {
        private ManageThematicViewModel _viewModel;
        private DataBaseContext _context;
        private Utility _utility;

        public ManageThematicController(DataBaseContext context, Utility utility)
        {
            _context = context;
            _utility = utility;
            _viewModel = new ManageThematicViewModel();
        }

        public IActionResult Index()
        {
            var themeList = _context.Theme
                                    .Where(t => t.Active)
                                    .OrderBy(t => t.Name)
                                    .ToList();

            _viewModel.Themes = themeList;

            return View(_viewModel);
        }

        public IActionResult AddThematicTheme()
        {
            try
            {
                _viewModel.CategorySL = _utility.GetCategorySL();
                return View(_viewModel);
            }
            catch (Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult AddThematicTheme(ManageThematicViewModel viewModel)
        {
            try
            {
                if (!string.IsNullOrEmpty(viewModel.Name))
                {
                    var theme = new Theme()
                    {
                        Name = viewModel.Name,
                        Description = string.IsNullOrEmpty(viewModel.Description) ? viewModel.Name : viewModel.Description,
                        Active = true,
                        CategoryId = viewModel.CategoryId,
                        Code = viewModel.Code
                    };

                    _context.Add(theme);
                    _context.SaveChanges();

                    SetMessage("Theme was added successfully");

                    return RedirectToAction("AddThematicTheme");
                }
                return View(viewModel);
            }
            catch (Exception ex) { throw ex; }
        }
        
        public IActionResult EditThematicTheme(int Id)
        {
            try
            {
                if (Id < 1)
                {
                    return RedirectToAction("Index");
                }

                var theme = _context.Theme.Where(t => t.Id == Id).FirstOrDefault();
                if (theme != null)
                {
                    _viewModel.Name = theme.Name;
                    _viewModel.Description = theme.Description;
                    _viewModel.CategoryId = theme.Id;
                    _viewModel.Code = theme.Code;
                    _viewModel.CategoryId = theme.CategoryId;
                }

                _viewModel.ThematicId = Id;
                _viewModel.CategorySL = _utility.GetCategorySL();

                return View(_viewModel);
            }
            catch (Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult EditThematicTheme(ManageThematicViewModel viewModel)
        {
            try
            {
                if (!string.IsNullOrEmpty(viewModel.Name) && viewModel.CategoryId > 0)
                {
                    var theme = _context.Theme.Where(t => t.Id == viewModel.CategoryId).FirstOrDefault();
                    if (theme != null)
                    {
                        theme.Name = viewModel.Name;
                        theme.Description = viewModel.Description;
                        theme.CategoryId = viewModel.CategoryId;
                        theme.Code = viewModel.Code;

                        _context.Update(theme);
                        _context.SaveChanges();

                        SetMessage("Theme was updated successfully");

                        return RedirectToAction("EditThematicTheme", new { Id = theme.Id });
                    }
                }
                return View(viewModel);
            }
            catch (Exception ex) { throw ex; }
        }

        public IActionResult ViewAllCategories()
        {
            try
            {
                _viewModel.Categories = _context.Category.ToList();
                return View(_viewModel);
            }
            catch(Exception ex) { throw ex; }
        }

        public IActionResult AddCategory()
        {
            try
            {
                return View();
            }
            catch (Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult AddCategory(ManageThematicViewModel viewModel)
        {
            try
            {
                if (!string.IsNullOrEmpty(viewModel.CategoryName))
                {
                    var categoryExists = _context.Category.Where(c => c.Name == viewModel.CategoryName).FirstOrDefault();
                    if (categoryExists == null)
                    {
                        var category = new Category()
                        {
                            Name = viewModel.CategoryName,
                            Description = viewModel.CategoryDescription,
                            Code = viewModel.Code,
                            Active = true
                        };

                        _context.Add(category);
                        _context.SaveChanges();

                        SetMessage("Category created successfully");
                    }
                    else
                    {
                        SetMessage("Category already exists");
                    }
                }
                else
                {
                    SetMessage("Please fill in all required fields");
                }

                return RedirectToAction("AddCategory");
            }
            catch (Exception ex) { throw ex; }
        }

        public IActionResult EditCategory(int Id)
        {
            try
            {
                if (Id < 1)
                {
                    return RedirectToAction("ViewAllCategories");
                }

                var category = _context.Category.Where(c => c.Id == Id).FirstOrDefault();
                if (category != null)
                {   
                    _viewModel.CategoryName = category.Name;
                    _viewModel.CategoryDescription = category.Description;
                    _viewModel.Code = category.Code;
                }
                _viewModel.CategoryId = Id;

                return View(_viewModel);
            }
            catch(Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult EditCategory(ManageThematicViewModel viewModel)
        {
            try
            {
                if (viewModel.CategoryId < 1)
                {
                    return RedirectToAction("ViewAllCategories");
                }

               if(!string.IsNullOrEmpty(viewModel.CategoryName))
               {
                    var category = _context.Category.Where(c => c.Id == viewModel.CategoryId).FirstOrDefault();
                    if (category != null)
                    {
                        category.Name = viewModel.CategoryName;
                        category.Description = viewModel.CategoryDescription;
                        category.Code = viewModel.Code;

                        _context.Update(category);
                        _context.SaveChanges();

                        SetMessage("Category updated successfully");
                    }
                }
                else
                {
                    SetMessage("Please fill in all required fields");
                }

                return View(viewModel);
            }
            catch (Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult DeactivateCategory(int id)
        {
            try
            {
                var category = _context.Category.Where(m => m.Id == id).FirstOrDefault();

                if (category != null)
                {
                    bool active = category.Active;

                    category.Active = !category.Active;
                    _context.Update(category);
                    _context.SaveChanges();

                    string msg = string.Format("'{0}' was {1} successfully", category.Name, (active == true ? "Deactivated" : "Activated"));
                    return Json(msg);
                }

                return RedirectToAction("ViewAllCategories");
            }
            catch (Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult DeactivateTheme(int id)
        {
            try
            {
                var theme = _context.Theme.Where(m => m.Id == id).FirstOrDefault();

                if (theme != null)
                {
                    bool active = theme.Active;

                    theme.Active = !theme.Active;
                    _context.Update(theme);
                    _context.SaveChanges();

                    string msg = string.Format("'{0}' was {1} successfully", theme.Name, (active == true ? "Deactivated" : "Activated"));
                    return Json(msg);
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex) { throw ex; }
        }
    }
}