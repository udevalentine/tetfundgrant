﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Grant.Web.Models.ViewModels;
using Newtonsoft.Json;
using System.Net;
using Grant.Web.Models;
using System.Security.Claims;
using Grant.Web.Infrastructure;

namespace Grant.Web.Controllers
{
    public class BaseController : Controller
    {
        protected void SetMessage(string message, Message.Category messageType)
        {
            Message msg = new Message(message, (int)messageType);
            TempData["Message"] = msg;
           // TempData["Message"] = JsonConvert.SerializeObject(msg);

        }
        protected void SetMessage(string message)
        {
            Message msg = new Message(message);
            //ViewBag.Message = msg;
            TempData["Message"] = message;

        }
        protected static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead("http://google.com/generate_204"))
                    return true;
            }
            catch
            {
                return false;
            }
        }

        public Applicant GetLoggedInApplicant(DataBaseContext _context)
        {
            try
            {
                string email = User.FindFirstValue(ClaimTypes.Email);
                return _context.Applicant.Where(d => d.ApplicationNo == email).FirstOrDefault();
            }
            catch (Exception ex) { throw ex; }
        }

        public User GetLoggedInUser(DataBaseContext _context)
        {
            try
            {
                string email = User.FindFirstValue(ClaimTypes.Email);
                return _context.User.Where(d => d.Email == email).FirstOrDefault();
            }
            catch (Exception ex) { throw ex; }
        }
        public string ValidateUserLogin(DataBaseContext _context)
        {
            string personEmail = User.FindFirstValue(ClaimTypes.Email);
            string role = User.FindFirstValue(ClaimTypes.Role);
            string hashCookies = User.FindFirstValue(ClaimTypes.Hash);
            
            string returnMessage = "Your account is logged in on another device. Please log out on the other device to continue here";
            string storedToken = string.Empty;
            if(personEmail!=null && role!=null && hashCookies != null)
            {
                switch (role)
                {
                    case "Applicant":
                       storedToken= _context.Applicant.Where(f => f.ApplicationNo == personEmail && f.RoleId == (int)Roles.Applicant).Select(f=>f.DeviceMacAddress).FirstOrDefault();
                        
                        break;
                    case "Co-Applicant":
                        storedToken = _context.Applicant.Where(f => f.ApplicationNo == personEmail && f.RoleId == (int)Roles.CoApplicant).Select(f => f.DeviceMacAddress).FirstOrDefault();
                        break;
                    case "Reviewer":
                        storedToken=_context.User.Where(f => f.UserName == personEmail && f.RoleId == (int)Roles.Reviewer).Select(f => f.Token).FirstOrDefault();
                        break;
                    case "Admin":
                        storedToken = _context.User.Where(f => f.UserName == personEmail && f.RoleId == (int)Roles.Admin).Select(f => f.Token).FirstOrDefault();
                        break;
                    case "Co-Admin":
                        storedToken = _context.User.Where(f => f.UserName == personEmail && f.RoleId == (int)Roles.CoAdmin).Select(f => f.Token).FirstOrDefault();
                        break;
                    case "Super-Admin":
                        storedToken = _context.User.Where(f => f.UserName == personEmail && f.RoleId == (int)Roles.SuperAdmin).Select(f => f.Token).FirstOrDefault();
                        break;
                }
                if (storedToken == hashCookies)
                {
                    returnMessage = string.Empty;
                }
            }
            return returnMessage;
        }
        public void UpdateLogout(DataBaseContext _context)
        {
            string personEmail = User.FindFirstValue(ClaimTypes.Email);
            string role = User.FindFirstValue(ClaimTypes.Role);
            User user = new User();
            Applicant applicant = new Applicant();
            if (personEmail != null && role != null)
            {
                switch (role)
                {
                    case "Applicant":
                        applicant = _context.Applicant.Where(f => f.ApplicationNo == personEmail && f.RoleId == (int)Roles.Applicant).FirstOrDefault();
                        applicant.DeviceMacAddress = null;
                        applicant.IsLoggedIn = false;
                        _context.Update(applicant);
                        _context.SaveChanges();

                        break;
                    case "Co-Applicant":
                        applicant = _context.Applicant.Where(f => f.ApplicationNo == personEmail && f.RoleId == (int)Roles.CoApplicant).FirstOrDefault();
                        applicant.DeviceMacAddress = null;
                        applicant.IsLoggedIn = false;
                        _context.Update(applicant);
                        _context.SaveChanges();
                        break;
                    case "Reviewer":
                        user=_context.User.Where(f => f.UserName == personEmail && f.RoleId == (int)Roles.Reviewer).FirstOrDefault();
                        user.IsLoggedIn = false;
                        user.Token = null;
                        _context.Update(user);
                        _context.SaveChanges();
                        break;
                    case "Admin":
                        user = _context.User.Where(f => f.UserName == personEmail && f.RoleId == (int)Roles.Admin).FirstOrDefault();
                        user.IsLoggedIn = false;
                        user.Token = null;
                        _context.Update(user);
                        _context.SaveChanges();
                        break;
                    case "Super-Admin":
                        user = _context.User.Where(f => f.UserName == personEmail && f.RoleId == (int)Roles.SuperAdmin).FirstOrDefault();
                        user.IsLoggedIn = false;
                        user.Token = null;
                        _context.Update(user);
                        _context.SaveChanges();
                        break;
                    case "Co-Admin":
                        user = _context.User.Where(f => f.UserName == personEmail && f.RoleId == (int)Roles.CoAdmin).FirstOrDefault();
                        user.IsLoggedIn = false;
                        user.Token = null;
                        _context.Update(user);
                        _context.SaveChanges();
                        break;
                }
            }
        }
    }
}