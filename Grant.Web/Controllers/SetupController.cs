﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Transactions;
using Grant.Web.Infrastructure;
using Grant.Web.Models;
using Grant.Web.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Grant.Web.Controllers
{
    [Authorize(Policy = "MustBeAdmin")]
    public class SetupController : BaseController
    {
        private DataBaseContext _context;
        private Utility _utility;
        private SetupViewModel _viewModel;
        private const string Value = "Id";
        private const string Text = "Name";

        public SetupController(DataBaseContext context, Utility utility)
        {
            _context = context;
            _utility = utility;
            _viewModel = new SetupViewModel();
            _viewModel.PopulateAllDropdown(_context);
        }

        public IActionResult ViewAllInstitutions()
        {
            try
            {
                _viewModel.Institutions = _utility.GetInstitutions();
                return View(_viewModel);
            }
            catch(Exception ex) { throw ex; }
        }

        public IActionResult AddInstitution()
        {
            try
            {
                _viewModel.InstitutionCategorySL = _utility.GetInstitutionCategorySL();
                _viewModel.StateSL = _utility.GetStateSL();
                return View(_viewModel);
            }
            catch (Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult AddInstitution(SetupViewModel viewModel)
        {
            try
            {
                if (viewModel.StateId > 0 && viewModel.InstitutionCategoryId > 0 && !string.IsNullOrEmpty(viewModel.InstitutionName))
                {
                    var institutionExists = _utility.GetInstitutionByNameAndState(viewModel.InstitutionName, viewModel.StateId);
                    if (institutionExists == null)
                    {
                        var newInstitution = new Institution()
                        {
                            StateId = viewModel.StateId,
                            Active = true,
                            InstitutionCategoryId = viewModel.InstitutionCategoryId,
                            IsPublic = viewModel.IsPublic,
                            Name = viewModel.InstitutionName
                        };

                        _context.Add(newInstitution);
                        _context.SaveChanges();

                        SetMessage("The institution was added successfully");

                        return RedirectToAction("AddInstitution");
                    }
                    else
                    {
                        SetMessage("This institution already exists in the system");
                    }
                }
                else
                {
                    SetMessage("Please fill in all required fields");
                }

                viewModel.InstitutionCategorySL = _utility.GetInstitutionCategorySL();
                viewModel.StateSL = _utility.GetStateSL();

                return View(viewModel);
            }
            catch(Exception ex) { throw ex; }
        }

        public IActionResult EditInstitution(int Id)
        {
            try
            {
                if (Id < 1)
                {
                    return RedirectToAction("ViewAllInstitutions");
                }

                var institution = _utility.GetInstitutionById(Id);

                if (institution != null)
                {
                    _viewModel.InstitutionName = institution.Name;
                    _viewModel.StateId = institution.StateId;
                    _viewModel.InstitutionCategoryId = institution.InstitutionCategoryId;
                    _viewModel.IsPublic = institution.IsPublic;
                    _viewModel.InstitutionId = institution.Id;
                }
                else
                {
                    return RedirectToAction("ViewAllInstitutions");
                }

                _viewModel.InstitutionCategorySL = _utility.GetInstitutionCategorySL();
                _viewModel.StateSL = _utility.GetStateSL();

                return View(_viewModel);
            }
            catch(Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult EditInstitution(SetupViewModel viewModel)
        {
            try
            {
                if (viewModel.StateId > 0 && viewModel.InstitutionCategoryId > 0 && viewModel.InstitutionId > 0 && !string.IsNullOrEmpty(viewModel.InstitutionName))
                {
                    var editedInstitution = _utility.GetInstitutionById(viewModel.InstitutionId);

                    if (editedInstitution != null)
                    {
                        editedInstitution.InstitutionCategoryId = viewModel.InstitutionCategoryId;
                        editedInstitution.IsPublic = viewModel.IsPublic;
                        editedInstitution.Name = viewModel.InstitutionName;
                        editedInstitution.StateId = viewModel.StateId;

                        _context.Update(editedInstitution);
                        _context.SaveChanges();

                        SetMessage("Instution was updated successfully");

                        return RedirectToAction("EditInstitution", new { Id = viewModel.InstitutionId });
                    }
                }
                else
                {
                    SetMessage("Required fields were not set");
                }

                viewModel.InstitutionCategorySL = _utility.GetInstitutionCategorySL();
                viewModel.StateSL = _utility.GetStateSL();

                return View(viewModel);
            }
            catch(Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult DeactivateInsitution(int id)
        {
            try
            {
                var institution = _context.Institution.Where(m => m.Id == id).FirstOrDefault();

                if (institution != null)
                {
                    bool active = institution.Active;

                    institution.Active = !institution.Active;
                    _context.Update(institution);
                    _context.SaveChanges();

                    string msg = string.Format("'{0}' was {1} successfully", institution.Name, (active == true ? "Deactivated" : "Activated"));
                    return Json(msg);
                }

                return RedirectToAction("ViewAllInstitutions");
            }
            catch (Exception ex) { throw ex; }
        }

        public IActionResult ViewAllAdminUser()
        {
            try
            {
                _viewModel.AdminUsers = _utility.GetAdminUsers();
                return View(_viewModel);
            }
            catch(Exception ex) { throw ex; }
        }

        public IActionResult AddAdminUser()
        {
            try
            {
                //_viewModel.RoleId = (int)RoleType.Admin;
                string roleName = User.FindFirstValue(ClaimTypes.Role);
                //remove superAdmin role from the list
                if (roleName != "Super-Admin")
                {
                    _viewModel.RoleSL = _utility.GetAdminRoleSL(true);

                }
                else
                {
                    _viewModel.RoleSL = _utility.GetAdminRoleSL(false);
                }
                
                _viewModel.TitleSL = _utility.GetTitleSL();
                return View(_viewModel);
            }
            catch(Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult AddAdminUser(SetupViewModel viewModel)
        {
            try
            {
                string roleName = User.FindFirstValue(ClaimTypes.Role);
                if ((string.IsNullOrEmpty(viewModel.Email) 
                    && string.IsNullOrEmpty(viewModel.PhoneNumber) 
                    && string.IsNullOrEmpty(viewModel.FullName)) == false
                    && viewModel.TitleId > 0)
                {
                    var userExists = _utility.GetUserByUsername(viewModel.Email);
                    if (userExists == null)
                    {
                        using (TransactionScope transaction = new TransactionScope())
                        {
                            var reviewer = new Reviewer()
                            {
                                Email = viewModel.Email,
                                Name = viewModel.FullName,
                                PhoneNo = viewModel.PhoneNumber,
                                Active = true,
                                //RoleId = (int)RoleType.Admin,
                                RoleId=viewModel.RoleId,
                                TitleId = viewModel.TitleId,
                                //Assign Default institution
                                InstitutionId=1
                            };

                            _context.Add(reviewer);
                            _context.SaveChanges();

                            var user = new User()
                            {
                                UserName = viewModel.Email,
                                Email = viewModel.Email,
                                Password = "1234567",
                                Active = true,
                                //RoleId = (int)RoleType.Admin,
                                RoleId = viewModel.RoleId,
                                LastLogin = DateTime.Now,
                                ReviewerId = reviewer.Id
                            };

                            _context.Add(user);
                            _context.SaveChanges();

                            transaction.Complete();
                        }

                        SetMessage("User was created successfully");

                        return RedirectToAction("AddAdminUser");
                    }
                    else
                    {
                        SetMessage("A user with a similar username already exists");
                    }
                }
                else
                {
                    SetMessage("Required fields were not set");
                }

                //viewModel.RoleId = (int)RoleType.Admin;
                if (roleName != "Super-Admin")
                {
                    viewModel.RoleSL = _utility.GetAdminRoleSL(true);

                }
                else
                {
                    viewModel.RoleSL = _utility.GetAdminRoleSL(false);
                }
                viewModel.TitleSL = _utility.GetTitleSL();
                return View(viewModel);
            }
            catch(Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult DeactivateAdminUser(int id)
        {
            try
            {
                var user = _context.User.Where(m => m.Id == id).FirstOrDefault();

                if (user != null)
                {
                    bool active = user.Active;

                    user.Active = !user.Active;
                    _context.Update(user);
                    _context.SaveChanges();

                    string msg = string.Format("'{0}' was {1} successfully", user.UserName, (active == true ? "Deactivated" : "Activated"));
                    return Json(msg);
                }

                return RedirectToAction("ViewAllAdminUser");
            }
            catch (Exception ex) { throw ex; }
        }
        public IActionResult AllThematicCategoryAdministrator()
        {
            try
            {
                _viewModel.CategoryAdministrators= _context.CategoryAdministrator.Where(f=>f.Active).ToList();
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            return View(_viewModel);
        }
        public IActionResult AssignThematicCategoryAdministrator(SetupViewModel viewModel)
        {
            try
            {
                if(viewModel.CategoryId>0 && viewModel.CoAdminId > 0)
                {
                   var existingCategoryAdmin= _context.CategoryAdministrator.Where(f => f.CategoryId == viewModel.CategoryId && f.Active).ToList();
                    if (existingCategoryAdmin?.Count >= 2)
                    {
                        SetMessage("You can nolonger assign this Thematic Category to a Co-Administrator. It has" + existingCategoryAdmin.Count() + " " + "Administrator already Assigned");
                        return RedirectToAction("AllThematicCategoryAdministrator");
                    }
                    else
                    {
                        var existingCoAdmin = _context.CategoryAdministrator.Where(f => f.CategoryId == viewModel.CategoryId && f.Active && f.User.ReviewerId==viewModel.CoAdminId).ToList();
                        if (existingCoAdmin?.Count > 0)
                        {
                            SetMessage("The Selected Co-Administrator already handling the selected Thematic Category");
                            return RedirectToAction("AllThematicCategoryAdministrator");
                        }
                        else
                        {
                            var CoAdmin = _context.CategoryAdministrator.Where(f => f.Active && f.User.ReviewerId == viewModel.CoAdminId).ToList();
                            if (CoAdmin?.Count > 0)
                            {
                                SetMessage("A Co-Administrator cannot handle More than 1 Thematic Category");
                                return RedirectToAction("AllThematicCategoryAdministrator");
                            }
                            else
                            {
                                CategoryAdministrator categoryAdministrator = new CategoryAdministrator()
                                {
                                    Active = true,
                                    CategoryId = viewModel.CategoryId,
                                    DateTime = DateTime.Now,
                                    User = _context.User.Where(f => f.ReviewerId == viewModel.CoAdminId).FirstOrDefault(),

                                };
                                _context.Add(categoryAdministrator);
                                _context.SaveChanges();
                                SetMessage("You have Successfully Assigned Thematic Category");
                                return RedirectToAction("AllThematicCategoryAdministrator");
                            }
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(_viewModel);
        }
        [HttpPost]
        public IActionResult DeactivateAdministratorCategory(int id)
        {
            try
            {
                var categoryAdministrator = _context.CategoryAdministrator.Where(m => m.Id == id).FirstOrDefault();

                if (categoryAdministrator != null)
                {
                    bool active = categoryAdministrator.Active;

                    categoryAdministrator.Active = !categoryAdministrator.Active;
                    _context.Update(categoryAdministrator);
                    _context.SaveChanges();

                    string msg = string.Format("'{0}' was {1} successfully", categoryAdministrator.Category.Name, (active == true ? "Deactivated" : "Activated"));
                    return Json(msg);
                }

                return RedirectToAction("AllThematicCategoryAdministrator");
            }
            catch (Exception ex) { throw ex; }
        }
        
        //Non Action Method
        public IActionResult GetCategory(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }
            var categoryList = new List<Category>();
            int reviewerId = Convert.ToInt32(value);
            var existingCategoryAdministrator = _context.CategoryAdministrator.Where(c => c.Active && c.User.ReviewerId == reviewerId).ToList();
            categoryList=_context.Category.Where(d=>d.Active).ToList();
            if (existingCategoryAdministrator?.Count > 0)
            {
                HashSet<int> categoryIds = new HashSet<int>(existingCategoryAdministrator.Select(x => x.CategoryId));

                categoryList.RemoveAll(x => categoryIds.Contains(x.Id));
            }

            return Json(new SelectList(categoryList, Value, Text));
        }


    }
}