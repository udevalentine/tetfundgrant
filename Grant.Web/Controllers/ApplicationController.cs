﻿using Grant.Web.Infrastructure;
using Grant.Web.Models;
using Grant.Web.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Transactions;

namespace Grant.Web.Controllers
{
    [Authorize(Policy = "MustBeApplicant")]
    public class ApplicationController : BaseController
    {
        private readonly DataBaseContext _context;
        UserViewModel _userViewModel;
        private IHostingEnvironment _hostingEnvironment;
        Utility _utility;
        private const string Value = "Id";
        private const string Text = "Name";
        private IConfiguration _configuration;
        private string rootUrl;


        public ApplicationController(DataBaseContext context, IHostingEnvironment hostingEnvironment, Utility utility, IConfiguration configuration)
        {
            _context = context;
            _userViewModel = new UserViewModel();
            _userViewModel.PopulateAllDropdown(_context);
            _hostingEnvironment = hostingEnvironment;
            _utility = utility;
            _configuration = configuration;
            rootUrl = string.IsNullOrEmpty(_configuration.GetValue<string>("Url:root")) ? "" : "//" + _configuration.GetValue<string>("Url:root");


        }
        [Authorize(Policy = "MustBePI")]
        public IActionResult SubmitConceptNote()
        {
            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }
                var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                if (activeSessionForApplication == null)
                {
                    SetMessage("No active Research cycle at the moment.");
                    return RedirectToAction("Index", "UserHome");
                }
                if (!_utility.CheckResearchCycleEvent((int)EventTypes.ConceptNote))
                {
                    SetMessage("Concept Note cannot be submitted at this time");
                    return RedirectToAction("Index", "UserHome");
                }

                string userEmail = User.FindFirstValue(ClaimTypes.Email);

                var applicant = _context.Applicant.Where(f => f.ApplicationNo == userEmail && f.RoleId == (int)Roles.Applicant).FirstOrDefault();
                if (applicant != null)
                {
                    _userViewModel.ApplicantThematic = _context.ApplicantThematic.Where(f => f.ApplicantId == applicant.Id && f.SessionId == activeSessionForApplication.Id).FirstOrDefault();

                    if (_userViewModel.ApplicantThematic?.Id > 0)
                    {
                        var hasSubmitted = _utility.HasSubmitted((int)Types.ConceptNote, _userViewModel.ApplicantThematic.Id);
                        if (hasSubmitted)
                        {
                            _userViewModel.ConceptNote = _context.ConceptNote.Where(f => f.ApplicantThemeId == _userViewModel.ApplicantThematic.Id && f.ApplicantThematic.SessionId == activeSessionForApplication.Id).FirstOrDefault();
                            SetMessage("You can no longer modify your Concept Note");
                            _userViewModel.HideButton = true;
                            _userViewModel.ThemeId = _userViewModel.ApplicantThematic.Theme.Id;
                            return RedirectToAction("ConceptNotePreview");
                        }
                        else
                        {
                            _userViewModel.ThemeId = _userViewModel.ApplicantThematic.Theme.Id;
                            _userViewModel.ConceptNote = _context.ConceptNote.Where(f => f.ApplicantThemeId == _userViewModel.ApplicantThematic.Id && f.ApplicantThematic.SessionId == activeSessionForApplication.Id).FirstOrDefault();
                        }

                    }
                    else
                    {

                        _userViewModel.ConceptNote = new ConceptNote();
                    }

                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            return View(_userViewModel);
        }
        [HttpPost]
        public IActionResult SubmitConceptNote(string Title, int CategoryId, int ThemeId, IFormFile UploadFile, UserViewModel viewModel)
        {
            if (!_utility.CheckResearchCycleEvent((int)EventTypes.ConceptNote))
            {
                SetMessage("Concept Notes Cannot be submitted at this time");
                return RedirectToAction("Index", "UserHome");
            }

            string userEmail = User.FindFirstValue(ClaimTypes.Email);
            var person = _context.Person.Where(f => f.Email == userEmail).FirstOrDefault();
            if (person != null)
            {
                var applicant = _context.Applicant.Where(k => k.PersonId == person.Id && k.RoleId == (int)Roles.Applicant).FirstOrDefault();
                var exist = _context.ApplicantThematic.Where(f => f.ApplicantId == applicant.Id).FirstOrDefault();


                if (exist != null)
                {
                    exist.Title = viewModel.ApplicantThematic.Title;
                    exist.ThemeId = ThemeId;
                    _context.Update(exist);
                    _context.SaveChanges();
                    //SetMessage("Cannot Submit more than one Content Note!");
                    //return RedirectToAction("Profile", "UserHome");
                }
                //if (UploadFile == null || UploadFile.Length == 0)
                //{
                //    SetMessage("file not selected!");
                //    return View(_userViewModel);
                //}

                //var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "ConceptNote");
                //string ext = Path.GetExtension(UploadFile.FileName);
                //if (ext.ToLower() != ".pdf")
                //{
                //    SetMessage("Only Pdf File is allowed!");
                //    return View(_userViewModel);
                //}

                //var fileName = UploadFile.FileName;
                //var saveName = person.Email + "_" + "ConceptNote" + ".pdf";
                //var filePath = Path.Combine(uploads, saveName);

                //FileInfo file = new FileInfo(filePath);
                //if (file.Exists)
                //{
                //    file.Delete();
                //}


                //using (var stream = new FileStream(filePath, FileMode.Create))
                //{
                //    UploadFile.CopyTo(stream);
                //}
                //var savePath = "\\tetfundgrant\\ConceptNote\\" + saveName;
                //var savePath = "\\ConceptNote\\" + saveName;
                //_utility.AddConceptNote(applicant, ThemeId, Title, savePath, ConceptNoteSummary);
                //_utility.AddConceptNote(applicant, ThemeId, viewModel.ApplicantThematic.Title, viewModel.ConceptNote.ConceptNoteSummary);
                _utility.AddConceptNote(applicant, ThemeId, viewModel.ApplicantThematic.Title, viewModel.ConceptNote.ConceptNoteSubmittedBudget);
                //SetMessage("Successfully submitted Concept Note!");
                return RedirectToAction("ConceptNoteApplication");
            }
            SetMessage("Something went wrong!");

            return RedirectToAction("Index", "UserHome");
        }
        [Authorize(Policy = "MustBePI")]
        public IActionResult ConceptNoteApplication()
        {
            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }
                var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                if (activeSessionForApplication == null)
                {
                    SetMessage("No active research cycle at the moment.");
                    return RedirectToAction("Index", "UserHome");
                }
                if (!_utility.CheckResearchCycleEvent((int)EventTypes.ConceptNote))
                {
                    SetMessage("Concept Note cannot be submitted at this time");
                    return RedirectToAction("Index", "UserHome");
                }

                string userEmail = User.FindFirstValue(ClaimTypes.Email);
                var applicant = _context.Applicant.Where(f => f.ApplicationNo == userEmail && f.RoleId == (int)Roles.Applicant).FirstOrDefault();
                if (applicant != null)
                {
                    _userViewModel.ApplicantThematic = _context.ApplicantThematic.Where(f => f.ApplicantId == applicant.Id && f.SessionId == activeSessionForApplication.Id).FirstOrDefault();

                    if (_userViewModel.ApplicantThematic != null)
                    {
                        var hasSubmitted = _utility.HasSubmitted((int)Types.ConceptNote, _userViewModel.ApplicantThematic.Id);
                        _userViewModel.ApplicantThematicId = _userViewModel.ApplicantThematic.Id;
                        if (hasSubmitted)
                        {
                            _userViewModel.ConceptNote = _context.ConceptNote.Where(f => f.ApplicantThemeId == _userViewModel.ApplicantThematic.Id && f.ApplicantThematic.SessionId == activeSessionForApplication.Id).FirstOrDefault();
                            SetMessage("Your Concept Note has been submitted, you can no longer make changes.");
                            _userViewModel.HideButton = true;
                            _userViewModel.ThemeId = _userViewModel.ApplicantThematic.Theme.Id;
                            return RedirectToAction("ConceptNotePreview");
                        }
                        _userViewModel.HideButton = true;
                        _userViewModel.ConceptNote = _context.ConceptNote.Where(f => f.ApplicantThemeId == _userViewModel.ApplicantThematic.Id).FirstOrDefault();
                        _userViewModel.ThemeId = _userViewModel.ApplicantThematic.Theme.Id;
                        var existingConceptNot = _context.ApplicantSubmission.Where(g => g.ApplicantThematicId == _userViewModel.ApplicantThematic.Id && g.TypeId == (int)Types.ConceptNote).FirstOrDefault();
                        if (existingConceptNot != null)
                        {
                            _userViewModel.ApplicantSubmission = existingConceptNot;
                        }
                        else
                        {
                            _userViewModel.ApplicantSubmission = existingConceptNot;
                        }

                    }
                    else
                    {
                        _userViewModel.ApplicantSubmission = new ApplicantSubmission();
                        _userViewModel.ConceptNote = new ConceptNote();
                    }


                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_userViewModel);
        }
        [HttpPost]
        public IActionResult ConceptNoteApplication(UserViewModel viewmodel)
        {
            try
            {
                if (!_utility.CheckResearchCycleEvent((int)EventTypes.ConceptNote))
                {
                    SetMessage("Concept Note cannot be submitted at this time");
                    return RedirectToAction("Index", "UserHome");
                }

                if (viewmodel.ApplicantSubmission != null)
                {
                    var savePath = "";
                    string userEmail = User.FindFirstValue(ClaimTypes.Email);
                    var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                    var applicant = _context.Applicant.Where(k => k.ApplicationNo == userEmail && k.RoleId == (int)Roles.Applicant).FirstOrDefault();
                    var exist = _context.ApplicantThematic.Where(f => f.ApplicantId == applicant.Id && f.SessionId == activeSessionForApplication.Id).FirstOrDefault();

                    //_userViewModel.BudgetDetails = _context.BudgetDetail.Where(f => f.ApplicantThematicId == exist.Id && f.TypeId==(int)Types.ConceptNote).ToList();

                    //if (_userViewModel.BudgetDetails.Count == 0)
                    //{
                    //    SetMessage("Please Add Concept Budget to Continue");
                    //    return RedirectToAction("AddConceptNoteBudget");
                    //}
                    if (exist != null)
                    {
                        var conceptNote = _context.ConceptNote.Where(f => f.ApplicantThemeId == exist.Id).FirstOrDefault();
                        _utility.SaveApplicationSubmission(viewmodel.ApplicantSubmission, exist.Id, (int)Types.ConceptNote, savePath, conceptNote);
                        //Send email to applicant for Concept Note Submission
                        if (CheckForInternetConnection())
                        {
                            //_utility.SendEMail(applicant.ApplicationNo, applicant.Person.Name, (int)MessageType.ConceptNoteSubmission, "", "", "", "",exist.Title);
                        }

                        SetMessage("Successfully submitted Concept Note");

                        //return RedirectToAction("Index", "UserHome");


                        return RedirectToAction("Index", "UserHome");
                        //return RedirectToAction("ConceptNoteApplication");
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }
        [Authorize(Policy = "MustBePI")]
        public IActionResult SubmitProposal()
        {
            try
            {
                string userEmail = User.FindFirstValue(ClaimTypes.Email);
                var person = _context.Person.Where(f => f.Email == userEmail).FirstOrDefault();
                var applicant = _context.Applicant.Where(f => f.PersonId == person.Id).FirstOrDefault();
                if (applicant != null)
                {
                    var applicantThematic = _context.ApplicantThematic.Where(f => f.ApplicantId == applicant.Id).FirstOrDefault();
                    if (applicantThematic != null)
                    {
                        var approval = _context.Approval.Where(g => g.ApplicantThematicId == applicantThematic.Id && g.Approved && g.IsProposal == false).FirstOrDefault();
                        if (approval == null)
                        {
                            SetMessage("You cant submit Proposal until your Concept Note is Approved");
                            return RedirectToAction("Index", "UserHome");
                        }
                        else
                        {
                            _userViewModel.ApplicantThematic = applicantThematic;
                            _userViewModel.Theme = _context.Theme.Where(f => f.Id == applicantThematic.ThemeId).FirstOrDefault();
                            _userViewModel.Category = _context.Category.Where(f => f.Id == _userViewModel.Theme.CategoryId).FirstOrDefault();
                            _userViewModel.Proposal = _context.Proposal.Where(f => f.ApplicantThemeId == applicantThematic.Id).FirstOrDefault();
                            //TempData["viewModel"] = _userViewModel;
                            return View(_userViewModel);
                        }
                    }
                    else
                    {
                        SetMessage("You can not submit without submitting Concept Note!");
                        return RedirectToAction("Index", "UserHome");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View();
        }
        [HttpPost]
        public IActionResult SubmitProposal(IFormCollection collection, IFormFile UploadFile)
        {
            try
            {
                //_userViewModel = (UserViewModel)TempData["viewModel"];
                ApplicantThematic applicantThematic = new ApplicantThematic();
                applicantThematic.Id = Convert.ToInt32(collection["ApplicantThematic.Id"]);
                var proposedBudget = Convert.ToDecimal(collection["Proposal.SubmittedBudget"]);
                string userEmail = User.FindFirstValue(ClaimTypes.Email);
                var person = _context.Person.Where(f => f.Email == userEmail).FirstOrDefault();
                if (person != null)
                {
                    var applicant = _context.Applicant.Where(k => k.PersonId == person.Id).FirstOrDefault();
                    var exist = _context.Proposal.Where(f => f.ApplicantThemeId == applicantThematic.Id).FirstOrDefault();
                    if (exist != null)
                    {
                        SetMessage("Cannot Submit more than one Proposal!");
                        return RedirectToAction("Profile", "UserHome");
                    }

                    if (UploadFile == null || UploadFile.Length == 0)
                    {
                        SetMessage("file not selected!");
                        return View(_userViewModel);
                    }

                    var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "ConceptNote");
                    string ext = Path.GetExtension(UploadFile.FileName);
                    if (ext.ToLower() != ".pdf")
                    {
                        SetMessage("Only Pdf File is allowed!");
                        return RedirectToAction("SubmitProposal");
                    }

                    var fileName = UploadFile.FileName;
                    var saveName = person.Email + "_" + "Proposal" + ".pdf";
                    var filePath = Path.Combine(uploads, saveName);

                    FileInfo file = new FileInfo(filePath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }


                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        UploadFile.CopyTo(stream);
                    }
                    // var savePath = "\\tetfundgrant\\ConceptNote\\" + saveName;
                    var savePath = rootUrl + "\\ConceptNote\\" + saveName;
                    _utility.AddProposal(applicantThematic.Id, savePath, proposedBudget);
                    SetMessage("Proposal Submitted Successfully!");
                    return RedirectToAction("Index", "UserHome");
                }
                SetMessage("Something went wrong!");

                return RedirectToAction("Index", "UserHome");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IActionResult GetTheme(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }
            int categoryId = Convert.ToInt32(value);
            var themecategoryList = _context.Theme.Where(c => c.Active && c.CategoryId == categoryId).ToList();

            return Json(new SelectList(themecategoryList, Value, Text));
        }
        //public JsonResult PreloadSelectedThematicArea()
        [Authorize(Policy = "MustBePI")]
        public IActionResult ProposalApplication()
        {
            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }
                var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                if (activeSessionForApplication == null)
                {
                    SetMessage("No Active Research cycle at the moment.");
                    return RedirectToAction("Index", "UserHome");
                }
                if (!_utility.CheckResearchCycleEvent((int)EventTypes.Proposal))
                {
                    SetMessage("Proposal cannot be accepted at this time");
                    return RedirectToAction("Index", "UserHome");
                }

                string userEmail = User.FindFirstValue(ClaimTypes.Email);

                var applicant = _context.Applicant.Where(k => k.ApplicationNo == userEmail && k.RoleId == (int)Roles.Applicant).FirstOrDefault();
                var exist = _context.ApplicantThematic.Where(f => f.ApplicantId == applicant.Id && f.SessionId == activeSessionForApplication.Id).FirstOrDefault();
                _userViewModel.Person = applicant.Person;

                if (exist != null)
                {
                    var hasSubmitted = _utility.HasSubmitted((int)Types.Proposal, exist.Id);
                    if (hasSubmitted)
                    {
                        SetMessage("You can no longer modify your Full Proposal");
                        return RedirectToAction("PreviewProposalBeforeSubmittion", new { id = exist.Id });
                    }

                    var approval = _context.Approval.Where(g => g.ApplicantThematicId == exist.Id && g.Approved && g.IsProposal == false).FirstOrDefault();
                    if (approval == null)
                    {
                        SetMessage("You cannot submit your Full Proposal until your Concept Note is approved");
                        return RedirectToAction("Index", "UserHome");
                    }

                    var existingProposal = _context.ApplicantSubmission.Where(g => g.ApplicantThematicId == exist.Id && g.TypeId == (int)Types.Proposal).FirstOrDefault();
                    if (existingProposal != null)
                    {
                        _userViewModel.ApplicantSubmission = existingProposal;
                        _userViewModel.ApplicantThematic = exist;
                        _userViewModel.BudgetDetails = _context.BudgetDetail.Where(f => f.ApplicantThematicId == exist.Id && f.TypeId == (int)Types.Proposal).ToList();
                        if (_userViewModel.BudgetDetails.Count > 0)
                        {
                            _userViewModel.BudgetDetailsSectionOne = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.PersonnelAllowances).ToList();


                            _userViewModel.BudgetDetailsSectionFour = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.DataCollectionandAnalysis).ToList();
                            _userViewModel.BudgetDetailsSectionThree = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.SuppliesConsumables).ToList();
                            _userViewModel.BudgetDetailsSectionTwo = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Equipment).ToList();
                            _userViewModel.BudgetDetailsSectionFive = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Travel).ToList();
                            _userViewModel.BudgetDetailsSectionSix = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Dissemination).ToList();
                            _userViewModel.BudgetDetailsSectionSeven = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Other).ToList();
                            _userViewModel.SectionPercentage1 = Math.Round(((_userViewModel.BudgetDetailsSectionOne.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage2 = Math.Round(((_userViewModel.BudgetDetailsSectionTwo.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage3 = Math.Round(((_userViewModel.BudgetDetailsSectionThree.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage4 = Math.Round(((_userViewModel.BudgetDetailsSectionFour.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage5 = Math.Round(((_userViewModel.BudgetDetailsSectionFive.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage6 = Math.Round(((_userViewModel.BudgetDetailsSectionSix.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage7 = Math.Round(((_userViewModel.BudgetDetailsSectionSeven.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        }
                        else
                        {
                            _userViewModel.BudgetDetailsSectionOne = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFour = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionThree = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionTwo = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFive = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSix = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSeven = new List<BudgetDetail>();
                        }

                    }
                    else if (exist != null)
                    {
                        _userViewModel.ApplicantSubmission = new ApplicantSubmission();
                        _userViewModel.ApplicantSubmission.Title = exist.Title;
                        _userViewModel.ApplicantThematic = exist;
                        _userViewModel.BudgetDetails = _context.BudgetDetail.Where(f => f.ApplicantThematicId == exist.Id && f.TypeId == (int)Types.Proposal).ToList();
                        if (_userViewModel.BudgetDetails.Count > 0)
                        {
                            _userViewModel.BudgetDetailsSectionOne = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.PersonnelAllowances).ToList();
                            _userViewModel.BudgetDetailsSectionFour = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.DataCollectionandAnalysis).ToList();
                            _userViewModel.BudgetDetailsSectionThree = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.SuppliesConsumables).ToList();
                            _userViewModel.BudgetDetailsSectionTwo = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Equipment).ToList();
                            _userViewModel.BudgetDetailsSectionFive = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Travel).ToList();
                            _userViewModel.BudgetDetailsSectionSix = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Dissemination).ToList();
                            _userViewModel.BudgetDetailsSectionSeven = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Other).ToList();
                            _userViewModel.SectionPercentage1 = Math.Round(((_userViewModel.BudgetDetailsSectionOne.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage2 = Math.Round(((_userViewModel.BudgetDetailsSectionTwo.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage3 = Math.Round(((_userViewModel.BudgetDetailsSectionThree.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage4 = Math.Round(((_userViewModel.BudgetDetailsSectionFour.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage5 = Math.Round(((_userViewModel.BudgetDetailsSectionFive.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage6 = Math.Round(((_userViewModel.BudgetDetailsSectionSix.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage7 = Math.Round(((_userViewModel.BudgetDetailsSectionSeven.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        }
                        else
                        {
                            _userViewModel.BudgetDetailsSectionOne = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFour = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionThree = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionTwo = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFive = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSix = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSeven = new List<BudgetDetail>();
                        }
                    }

                }
                else
                {
                    SetMessage("Please, you are to submit Concept Note before proceeding to Proposal");
                    return RedirectToAction("SubmitConceptNote");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_userViewModel);
        }
        [HttpPost]
        public IActionResult ProposalApplication(UserViewModel viewmodel, IFormFile UploadFile)
        {
            try
            {
                if (!_utility.CheckResearchCycleEvent((int)EventTypes.Proposal))
                {
                    SetMessage("Proposal cannot be accepted at this time");
                    return RedirectToAction("Index", "UserHome");
                }
                var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                if (viewmodel.ApplicantSubmission != null)
                {
                    var savePath = "";
                    string userEmail = User.FindFirstValue(ClaimTypes.Email);

                    var applicant = _context.Applicant.Where(k => k.ApplicationNo == userEmail && k.RoleId == (int)Roles.Applicant).FirstOrDefault();
                    var exist = _context.ApplicantThematic.Where(f => f.ApplicantId == applicant.Id && f.SessionId == activeSessionForApplication.Id).FirstOrDefault();

                    _userViewModel.BudgetDetails = _context.BudgetDetail.Where(f => f.ApplicantThematicId == exist.Id && f.TypeId == (int)Types.Proposal).ToList();
                    if (_userViewModel.BudgetDetails.Count == 0)
                    {
                        SetMessage("Please add Budget to Continue");
                        return RedirectToAction("AddBudget");
                    }
                    //Process the uploaded file
                    if (UploadFile == null || UploadFile.Length == 0)
                    {
                        //SetMessage("file not selected!");
                        //return View(viewmodel);
                    }
                    else
                    {
                        var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "ConceptNote");
                        string ext = Path.GetExtension(UploadFile.FileName);
                        if (ext.ToLower() != ".pdf")
                        {
                            SetMessage("Only Pdf file is allowed!");
                            return View(viewmodel);
                        }

                        var fileName = UploadFile.FileName;
                        var saveName = userEmail + "_" + "Proposal" + ".pdf";
                        var filePath = Path.Combine(uploads, saveName);

                        FileInfo file = new FileInfo(filePath);
                        if (file.Exists)
                        {
                            file.Delete();
                        }


                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            UploadFile.CopyTo(stream);
                        }
                        //savePath = "\\tetfundgrant\\ConceptNote\\" + saveName;
                        savePath = rootUrl + "\\ConceptNote\\" + saveName;
                    }


                    //var savePath = "\\ConceptNote\\" + saveName;
                    //
                    if (exist != null)
                    {

                        _utility.SaveApplicationSubmission(viewmodel.ApplicantSubmission, exist.Id, 2, savePath, viewmodel.ConceptNote);
                        SetMessage("Successfully submitted Full Proposal!");
                        //Send email to applicant for Full Proposal Submission
                        _utility.SendEMail(applicant.ApplicationNo, applicant.Person.Name, (int)MessageType.ProposalSubmission, "", "", "", "",exist.Title);



                        return RedirectToAction("ProposalPreview");
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }
        public IActionResult ProposalPreview(int? id)
        {
            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }
                var exist = new ApplicantThematic();
                if (id > 0)
                {
                    exist = _context.ApplicantThematic.Where(f => f.Id == id).FirstOrDefault();
                }
                else
                {
                    string userEmail = User.FindFirstValue(ClaimTypes.Email);
                    string roleName = User.FindFirstValue(ClaimTypes.Role);

                    var applicant = _context.Applicant.Where(k => k.ApplicationNo == userEmail && k.RoleId == (int)Roles.Applicant).FirstOrDefault();
                    var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                    if (applicant?.Id > 0)
                        exist = _context.ApplicantThematic.Where(f => f.ApplicantId == applicant.Id && f.SessionId == activeSessionForApplication.Id).FirstOrDefault();
                    //Determine if its principal investigator of Team member that is logged in
                    if (roleName == "Co-Applicant")
                    {
                        string thematicId = User.FindFirstValue(ClaimTypes.GroupSid);
                        var teamThematicId = Convert.ToInt32(thematicId);
                        exist = _context.ApplicantThematic.Where(f => f.Id == teamThematicId).FirstOrDefault();
                    }
                }

                if (exist == null)
                {
                    SetMessage("No Full Proposal submitted yet.");
                    return RedirectToAction("Index", "UserHome");
                }
                if (exist?.Id > 0)
                {

                    var existingProposal = _context.ApplicantSubmission.Where(g => g.ApplicantThematicId == exist.Id && g.TypeId == (int)Types.Proposal).FirstOrDefault();
                    if (existingProposal == null)
                    {
                        SetMessage("No Full Proposal submitted yet.");
                        return RedirectToAction("Index", "UserHome");
                    }
                    _userViewModel.Proposal = _context.Proposal.Where(f => f.ApplicantThemeId == exist.Id).FirstOrDefault();
                    if (existingProposal != null)
                    {
                        _userViewModel.ApplicantSubmission = existingProposal;
                        _userViewModel.ApplicantThematic = exist;
                        _userViewModel.BudgetDetails = _context.BudgetDetail.Where(f => f.ApplicantThematicId == exist.Id && f.TypeId == (int)Types.Proposal).ToList();
                        if (_userViewModel.BudgetDetails.Count > 0)
                        {

                            _userViewModel.BudgetDetailsSectionOne = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.PersonnelAllowances).ToList();


                            _userViewModel.BudgetDetailsSectionFour = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.DataCollectionandAnalysis).ToList();
                            _userViewModel.BudgetDetailsSectionThree = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.SuppliesConsumables).ToList();
                            _userViewModel.BudgetDetailsSectionTwo = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Equipment).ToList();
                            _userViewModel.BudgetDetailsSectionFive = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Travel).ToList();
                            _userViewModel.BudgetDetailsSectionSix = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Dissemination).ToList();
                            _userViewModel.BudgetDetailsSectionSeven = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Other).ToList();
                            _userViewModel.SectionPercentage1 = Math.Round(((_userViewModel.BudgetDetailsSectionOne.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage2 = Math.Round(((_userViewModel.BudgetDetailsSectionTwo.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage3 = Math.Round(((_userViewModel.BudgetDetailsSectionThree.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage4 = Math.Round(((_userViewModel.BudgetDetailsSectionFour.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage5 = Math.Round(((_userViewModel.BudgetDetailsSectionFive.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage6 = Math.Round(((_userViewModel.BudgetDetailsSectionSix.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage7 = Math.Round(((_userViewModel.BudgetDetailsSectionSeven.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        }
                        else
                        {

                            _userViewModel.BudgetDetailsSectionOne = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFour = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionThree = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionTwo = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFive = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSix = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSeven = new List<BudgetDetail>();
                        }

                    }
                    else if (exist != null)
                    {
                        _userViewModel.ApplicantSubmission = new ApplicantSubmission();
                        _userViewModel.ApplicantSubmission.Title = exist.Title;
                        _userViewModel.ApplicantThematic = exist;
                        _userViewModel.BudgetDetails = _context.BudgetDetail.Where(f => f.ApplicantThematicId == exist.Id).ToList();
                        if (_userViewModel.BudgetDetails.Count > 0)
                        {
                            _userViewModel.BudgetDetailsSectionOne = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.PersonnelAllowances).ToList();
                            _userViewModel.BudgetDetailsSectionFour = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.DataCollectionandAnalysis).ToList();
                            _userViewModel.BudgetDetailsSectionThree = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.SuppliesConsumables).ToList();
                            _userViewModel.BudgetDetailsSectionTwo = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Equipment).ToList();
                            _userViewModel.BudgetDetailsSectionFive = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Travel).ToList();
                            _userViewModel.BudgetDetailsSectionSix = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Dissemination).ToList();
                            _userViewModel.BudgetDetailsSectionSeven = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Other).ToList();
                            _userViewModel.SectionPercentage1 = Math.Round(((_userViewModel.BudgetDetailsSectionOne.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage2 = Math.Round(((_userViewModel.BudgetDetailsSectionTwo.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage3 = Math.Round(((_userViewModel.BudgetDetailsSectionThree.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage4 = Math.Round(((_userViewModel.BudgetDetailsSectionFour.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage5 = Math.Round(((_userViewModel.BudgetDetailsSectionFive.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage6 = Math.Round(((_userViewModel.BudgetDetailsSectionSix.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage7 = Math.Round(((_userViewModel.BudgetDetailsSectionSeven.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        }
                        else
                        {
                            _userViewModel.BudgetDetailsSectionOne = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFour = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionThree = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionTwo = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFive = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSix = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSeven = new List<BudgetDetail>();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_userViewModel);
        }
        public IActionResult ConceptNotePreview(int? id)
        {
            try
            {
                //if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                //{
                //    SetMessage(ValidateUserLogin(_context));
                //    return RedirectToAction("Logout", "Home");
                //}
                var exist = new ApplicantThematic();
                if (id > 0)
                {
                    exist = _context.ApplicantThematic.Where(f => f.Id == id).FirstOrDefault();
                }
                else
                {
                    string userEmail = User.FindFirstValue(ClaimTypes.Email);
                    string roleName = User.FindFirstValue(ClaimTypes.Role);

                    var applicant = _context.Applicant.Where(k => k.ApplicationNo == userEmail && k.RoleId == (int)Roles.Applicant).FirstOrDefault();
                    var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                    if (applicant?.Id > 0)
                        exist = _context.ApplicantThematic.Where(f => f.ApplicantId == applicant.Id && f.SessionId == activeSessionForApplication.Id).FirstOrDefault();
                    if (exist == null)
                    {
                        SetMessage("No Concept Note submitted yet.");
                        return RedirectToAction("Index", "UserHome");
                    }

                    //Determine if its principal investigator of Team member that is logged in
                    if (roleName == "Co-Applicant")
                    {
                        string thematicId = User.FindFirstValue(ClaimTypes.GroupSid);
                        var teamThematicId = Convert.ToInt32(thematicId);
                        exist = _context.ApplicantThematic.Where(f => f.Id == teamThematicId).FirstOrDefault();

                    }
                }


                if (id > 0)
                {
                    _userViewModel.HideButton = true;
                }
                if (exist != null)
                {
                    _userViewModel.ConceptNote = _context.ConceptNote.Where(g => g.ApplicantThemeId == exist.Id).FirstOrDefault();
                    var existingConceptNote = _context.ApplicantSubmission.Where(g => g.ApplicantThematicId == exist.Id && g.TypeId == (int)Types.ConceptNote).FirstOrDefault();
                    if (existingConceptNote != null)
                    {

                        _userViewModel.ApplicantSubmission = existingConceptNote;
                        _userViewModel.ApplicantThematic = exist;
                        _userViewModel.BudgetDetails = _context.BudgetDetail.Where(f => f.ApplicantThematicId == exist.Id && f.TypeId == (int)Types.ConceptNote).ToList();
                        if (_userViewModel.BudgetDetails.Count > 0)
                        {

                            _userViewModel.BudgetDetailsSectionOne = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.PersonnelAllowances).ToList();


                            _userViewModel.BudgetDetailsSectionFour = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.DataCollectionandAnalysis).ToList();
                            _userViewModel.BudgetDetailsSectionThree = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.SuppliesConsumables).ToList();
                            _userViewModel.BudgetDetailsSectionTwo = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Equipment).ToList();
                            _userViewModel.BudgetDetailsSectionFive = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Travel).ToList();
                            _userViewModel.BudgetDetailsSectionSix = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Dissemination).ToList();
                            _userViewModel.BudgetDetailsSectionSeven = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Other).ToList();
                            _userViewModel.SectionPercentage1 = Math.Round(((_userViewModel.BudgetDetailsSectionOne.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage2 = Math.Round(((_userViewModel.BudgetDetailsSectionTwo.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage3 = Math.Round(((_userViewModel.BudgetDetailsSectionThree.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage4 = Math.Round(((_userViewModel.BudgetDetailsSectionFour.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage5 = Math.Round(((_userViewModel.BudgetDetailsSectionFive.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage6 = Math.Round(((_userViewModel.BudgetDetailsSectionSix.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage7 = Math.Round(((_userViewModel.BudgetDetailsSectionSeven.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        }
                        else
                        {

                            _userViewModel.BudgetDetailsSectionOne = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFour = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionThree = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionTwo = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFive = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSix = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSeven = new List<BudgetDetail>();
                        }

                    }
                    else if (exist != null)
                    {
                        _userViewModel.ApplicantSubmission = new ApplicantSubmission();
                        _userViewModel.ApplicantSubmission.Title = exist.Title;
                        _userViewModel.ApplicantThematic = exist;
                        _userViewModel.BudgetDetails = _context.BudgetDetail.Where(f => f.ApplicantThematicId == exist.Id && f.TypeId == (int)Types.ConceptNote).ToList();
                        if (_userViewModel.BudgetDetails.Count > 0)
                        {
                            _userViewModel.BudgetDetailsSectionOne = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.PersonnelAllowances).ToList();
                            _userViewModel.BudgetDetailsSectionFour = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.DataCollectionandAnalysis).ToList();
                            _userViewModel.BudgetDetailsSectionThree = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.SuppliesConsumables).ToList();
                            _userViewModel.BudgetDetailsSectionTwo = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Equipment).ToList();
                            _userViewModel.BudgetDetailsSectionFive = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Travel).ToList();
                            _userViewModel.BudgetDetailsSectionSix = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Dissemination).ToList();
                            _userViewModel.BudgetDetailsSectionSeven = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Other).ToList();
                            _userViewModel.SectionPercentage1 = Math.Round(((_userViewModel.BudgetDetailsSectionOne.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage2 = Math.Round(((_userViewModel.BudgetDetailsSectionTwo.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage3 = Math.Round(((_userViewModel.BudgetDetailsSectionThree.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage4 = Math.Round(((_userViewModel.BudgetDetailsSectionFour.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage5 = Math.Round(((_userViewModel.BudgetDetailsSectionFive.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage6 = Math.Round(((_userViewModel.BudgetDetailsSectionSix.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage7 = Math.Round(((_userViewModel.BudgetDetailsSectionSeven.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        }
                        else
                        {
                            _userViewModel.BudgetDetailsSectionOne = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFour = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionThree = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionTwo = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFive = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSix = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSeven = new List<BudgetDetail>();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_userViewModel);
        }

        public JsonResult SavePersonnelCost(List<decimal> tetfundingArray, List<decimal> institutionFundingArray, List<decimal> otherFundingArray, List<string> descriptionArray, int applicantThematicId, int typeId)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                if (descriptionArray.Count > 0)
                {
                    using (TransactionScope scope = new TransactionScope())
                    {

                        //Create tetfunding
                        for (int i = 0; i < descriptionArray.Count; i++)
                        {
                            if (descriptionArray[i] != null)
                            {
                                if (typeId == 2)
                                {
                                    BudgetDetail budgetDetail = new BudgetDetail();
                                    budgetDetail.TetFundAmount = tetfundingArray[i];
                                    budgetDetail.InstitutionFundingAmount = institutionFundingArray[i];
                                    budgetDetail.OtherFunding = otherFundingArray[i];
                                    budgetDetail.BudgetItemId = (int)BudgetItems.PersonnelAllowances;
                                    budgetDetail.Description = descriptionArray[i];
                                    budgetDetail.TypeId = (int)Types.Proposal;
                                    budgetDetail.ApplicantThematicId = applicantThematicId;
                                    budgetDetail.Total = (tetfundingArray[i] + institutionFundingArray[i] + otherFundingArray[i]);
                                    _context.Add(budgetDetail);

                                    _context.SaveChanges();
                                }
                                else if (typeId == 1)
                                {
                                    BudgetDetail budgetDetail = new BudgetDetail();
                                    budgetDetail.TetFundAmount = tetfundingArray[i];
                                    budgetDetail.InstitutionFundingAmount = institutionFundingArray[i];
                                    budgetDetail.OtherFunding = otherFundingArray[i];
                                    budgetDetail.BudgetItemId = (int)BudgetItems.PersonnelAllowances;
                                    budgetDetail.Description = descriptionArray[i];
                                    budgetDetail.TypeId = (int)Types.ConceptNote;
                                    budgetDetail.ApplicantThematicId = applicantThematicId;
                                    budgetDetail.Total = (tetfundingArray[i] + institutionFundingArray[i] + otherFundingArray[i]);
                                    _context.Add(budgetDetail);

                                    _context.SaveChanges();
                                }

                            }



                        }
                        scope.Complete();

                    }
                    //result.PersonnelBudgetDetails = _context.BudgetDetail.Where(f => f.BudgetItemId == (int)BudgetItems.PersonnelAllowances && f.ApplicantThematicId==applicantThematicId).ToList();

                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }
            return Json(result);
        }
        public JsonResult SaveEquipmentCost(List<decimal> tetfundingArrayEquip, List<decimal> institutionFundingArrayEquip, List<decimal> otherFundingArrayEquip, List<string> descriptionArrayEquip, int applicantThematicId, int typeId)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                if (descriptionArrayEquip.Count > 0)
                {
                    using (TransactionScope scope = new TransactionScope())
                    {

                        //Create tetfunding
                        for (int i = 0; i < descriptionArrayEquip.Count; i++)
                        {

                            if (typeId == 2)
                            {
                                BudgetDetail budgetDetail = new BudgetDetail();
                                budgetDetail.TetFundAmount = tetfundingArrayEquip[i];
                                budgetDetail.InstitutionFundingAmount = institutionFundingArrayEquip[i];
                                budgetDetail.OtherFunding = otherFundingArrayEquip[i];
                                budgetDetail.BudgetItemId = (int)BudgetItems.Equipment;
                                budgetDetail.Description = descriptionArrayEquip[i];
                                budgetDetail.TypeId = (int)Types.Proposal;
                                budgetDetail.ApplicantThematicId = applicantThematicId;
                                budgetDetail.Total = (tetfundingArrayEquip[i] + institutionFundingArrayEquip[i] + otherFundingArrayEquip[i]);
                                _context.Add(budgetDetail);

                                _context.SaveChanges();
                            }
                            else if (typeId == 1)
                            {
                                BudgetDetail budgetDetail = new BudgetDetail();
                                budgetDetail.TetFundAmount = tetfundingArrayEquip[i];
                                budgetDetail.InstitutionFundingAmount = institutionFundingArrayEquip[i];
                                budgetDetail.OtherFunding = otherFundingArrayEquip[i];
                                budgetDetail.BudgetItemId = (int)BudgetItems.Equipment;
                                budgetDetail.Description = descriptionArrayEquip[i];
                                budgetDetail.TypeId = (int)Types.ConceptNote;
                                budgetDetail.ApplicantThematicId = applicantThematicId;
                                budgetDetail.Total = (tetfundingArrayEquip[i] + institutionFundingArrayEquip[i] + otherFundingArrayEquip[i]);
                                _context.Add(budgetDetail);

                                _context.SaveChanges();
                            }


                        }
                        scope.Complete();

                    }
                    //result.PersonnelBudgetDetails = _context.BudgetDetail.Where(f => f.BudgetItemId == (int)BudgetItems.Equipment && f.ApplicantThematicId == applicantThematicId).ToList();

                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }
            return Json(result);
        }
        public JsonResult SaveConsumableCost(List<decimal> tetfundingArraySupply, List<decimal> institutionFundingArraySupply, List<decimal> otherFundingArraySupply, List<string> descriptionArraySupply, int applicantThematicId, int typeId)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                if (descriptionArraySupply.Count > 0)
                {
                    using (TransactionScope scope = new TransactionScope())
                    {

                        //Create tetfunding
                        for (int i = 0; i < descriptionArraySupply.Count; i++)
                        {
                            if (typeId == 2)
                            {
                                BudgetDetail budgetDetail = new BudgetDetail();
                                budgetDetail.TetFundAmount = tetfundingArraySupply[i];
                                budgetDetail.InstitutionFundingAmount = institutionFundingArraySupply[i];
                                budgetDetail.OtherFunding = otherFundingArraySupply[i];
                                budgetDetail.BudgetItemId = (int)BudgetItems.SuppliesConsumables;
                                budgetDetail.Description = descriptionArraySupply[i];
                                budgetDetail.TypeId = (int)Types.Proposal;
                                budgetDetail.ApplicantThematicId = applicantThematicId;
                                budgetDetail.Total = (tetfundingArraySupply[i] + institutionFundingArraySupply[i] + otherFundingArraySupply[i]);
                                _context.Add(budgetDetail);

                                _context.SaveChanges();
                            }
                            else if (typeId == 1)
                            {
                                BudgetDetail budgetDetail = new BudgetDetail();
                                budgetDetail.TetFundAmount = tetfundingArraySupply[i];
                                budgetDetail.InstitutionFundingAmount = institutionFundingArraySupply[i];
                                budgetDetail.OtherFunding = otherFundingArraySupply[i];
                                budgetDetail.BudgetItemId = (int)BudgetItems.SuppliesConsumables;
                                budgetDetail.Description = descriptionArraySupply[i];
                                budgetDetail.TypeId = (int)Types.ConceptNote;
                                budgetDetail.ApplicantThematicId = applicantThematicId;
                                budgetDetail.Total = (tetfundingArraySupply[i] + institutionFundingArraySupply[i] + otherFundingArraySupply[i]);
                                _context.Add(budgetDetail);

                                _context.SaveChanges();
                            }



                        }
                        scope.Complete();

                    }
                    //result.PersonnelBudgetDetails = _context.BudgetDetail.Where(f => f.BudgetItemId == (int)BudgetItems.SuppliesConsumables && f.ApplicantThematicId == applicantThematicId).ToList();

                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }
            return Json(result);
        }
        public JsonResult SaveDataCollectionCost(List<decimal> tetfundingArrayData, List<decimal> institutionFundingArrayData, List<decimal> otherFundingArrayData, List<string> descriptionArrayData, int applicantThematicId, int typeId)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                if (descriptionArrayData.Count > 0)
                {
                    using (TransactionScope scope = new TransactionScope())
                    {

                        //Create tetfunding
                        for (int i = 0; i < descriptionArrayData.Count; i++)
                        {

                            if (typeId == 2)
                            {
                                BudgetDetail budgetDetail = new BudgetDetail();
                                budgetDetail.TetFundAmount = tetfundingArrayData[i];
                                budgetDetail.InstitutionFundingAmount = institutionFundingArrayData[i];
                                budgetDetail.OtherFunding = otherFundingArrayData[i];
                                budgetDetail.BudgetItemId = (int)BudgetItems.DataCollectionandAnalysis;
                                budgetDetail.Description = descriptionArrayData[i];
                                budgetDetail.TypeId = (int)Types.Proposal;
                                budgetDetail.ApplicantThematicId = applicantThematicId;
                                budgetDetail.Total = (tetfundingArrayData[i] + institutionFundingArrayData[i] + otherFundingArrayData[i]);
                                _context.Add(budgetDetail);

                                _context.SaveChanges();
                            }
                            else if (typeId == 1)
                            {
                                BudgetDetail budgetDetail = new BudgetDetail();
                                budgetDetail.TetFundAmount = tetfundingArrayData[i];
                                budgetDetail.InstitutionFundingAmount = institutionFundingArrayData[i];
                                budgetDetail.OtherFunding = otherFundingArrayData[i];
                                budgetDetail.BudgetItemId = (int)BudgetItems.DataCollectionandAnalysis;
                                budgetDetail.Description = descriptionArrayData[i];
                                budgetDetail.TypeId = (int)Types.ConceptNote;
                                budgetDetail.ApplicantThematicId = applicantThematicId;
                                budgetDetail.Total = (tetfundingArrayData[i] + institutionFundingArrayData[i] + otherFundingArrayData[i]);
                                _context.Add(budgetDetail);

                                _context.SaveChanges();
                            }


                        }
                        scope.Complete();

                    }
                    //result.PersonnelBudgetDetails = _context.BudgetDetail.Where(f => f.BudgetItemId == (int)BudgetItems.DataCollectionandAnalysis && f.ApplicantThematicId == applicantThematicId).ToList();

                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }
            return Json(result);
        }
        public JsonResult SaveTravelCost(List<decimal> tetfundingArrayTravel, List<decimal> institutionFundingArrayTravel, List<decimal> otherFundingArrayTravel, List<string> descriptionArrayTravel, int applicantThematicId, int typeId)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                if (descriptionArrayTravel.Count > 0)
                {
                    using (TransactionScope scope = new TransactionScope())
                    {

                        //Create tetfunding
                        for (int i = 0; i < descriptionArrayTravel.Count; i++)
                        {
                            if (typeId == 2)
                            {
                                BudgetDetail budgetDetail = new BudgetDetail();
                                budgetDetail.TetFundAmount = tetfundingArrayTravel[i];
                                budgetDetail.InstitutionFundingAmount = institutionFundingArrayTravel[i];
                                budgetDetail.OtherFunding = otherFundingArrayTravel[i];
                                budgetDetail.BudgetItemId = (int)BudgetItems.Travel;
                                budgetDetail.Description = descriptionArrayTravel[i];
                                budgetDetail.TypeId = (int)Types.Proposal;
                                budgetDetail.ApplicantThematicId = applicantThematicId;
                                budgetDetail.Total = (tetfundingArrayTravel[i] + institutionFundingArrayTravel[i] + otherFundingArrayTravel[i]);
                                _context.Add(budgetDetail);

                                _context.SaveChanges();
                            }
                            else if (typeId == 1)
                            {
                                BudgetDetail budgetDetail = new BudgetDetail();
                                budgetDetail.TetFundAmount = tetfundingArrayTravel[i];
                                budgetDetail.InstitutionFundingAmount = institutionFundingArrayTravel[i];
                                budgetDetail.OtherFunding = otherFundingArrayTravel[i];
                                budgetDetail.BudgetItemId = (int)BudgetItems.Travel;
                                budgetDetail.Description = descriptionArrayTravel[i];
                                budgetDetail.TypeId = (int)Types.Proposal;
                                budgetDetail.ApplicantThematicId = applicantThematicId;
                                budgetDetail.Total = (tetfundingArrayTravel[i] + institutionFundingArrayTravel[i] + otherFundingArrayTravel[i]);
                                _context.Add(budgetDetail);

                                _context.SaveChanges();
                            }



                        }
                        scope.Complete();

                    }
                    //result.PersonnelBudgetDetails = _context.BudgetDetail.Where(f => f.BudgetItemId == (int)BudgetItems.Travel && f.ApplicantThematicId == applicantThematicId).ToList();

                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }
            return Json(result);
        }
        public JsonResult SaveDisseminationCost(List<decimal> tetfundingArrayDissemination, List<decimal> institutionFundingArrayDissemination, List<decimal> otherFundingArrayDissemination, List<string> descriptionArrayDissemination, int applicantThematicId, int typeId)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                if (descriptionArrayDissemination.Count > 0)
                {
                    using (TransactionScope scope = new TransactionScope())
                    {

                        //Create tetfunding
                        for (int i = 0; i < descriptionArrayDissemination.Count; i++)
                        {
                            if (typeId == 2)
                            {

                                BudgetDetail budgetDetail = new BudgetDetail();
                                budgetDetail.TetFundAmount = tetfundingArrayDissemination[i];
                                budgetDetail.InstitutionFundingAmount = institutionFundingArrayDissemination[i];
                                budgetDetail.OtherFunding = otherFundingArrayDissemination[i];
                                budgetDetail.BudgetItemId = (int)BudgetItems.Dissemination;
                                budgetDetail.Description = descriptionArrayDissemination[i];
                                budgetDetail.TypeId = (int)Types.Proposal;
                                budgetDetail.ApplicantThematicId = applicantThematicId;
                                budgetDetail.Total = (tetfundingArrayDissemination[i] + institutionFundingArrayDissemination[i] + otherFundingArrayDissemination[i]);
                                _context.Add(budgetDetail);

                                _context.SaveChanges();
                            }
                            else if (typeId == 1)
                            {

                                BudgetDetail budgetDetail = new BudgetDetail();
                                budgetDetail.TetFundAmount = tetfundingArrayDissemination[i];
                                budgetDetail.InstitutionFundingAmount = institutionFundingArrayDissemination[i];
                                budgetDetail.OtherFunding = otherFundingArrayDissemination[i];
                                budgetDetail.BudgetItemId = (int)BudgetItems.Dissemination;
                                budgetDetail.Description = descriptionArrayDissemination[i];
                                budgetDetail.TypeId = (int)Types.Proposal;
                                budgetDetail.ApplicantThematicId = applicantThematicId;
                                budgetDetail.Total = (tetfundingArrayDissemination[i] + institutionFundingArrayDissemination[i] + otherFundingArrayDissemination[i]);
                                _context.Add(budgetDetail);

                                _context.SaveChanges();
                            }


                        }
                        scope.Complete();

                    }
                    //result.PersonnelBudgetDetails = _context.BudgetDetail.Where(f => f.BudgetItemId == (int)BudgetItems.Dissemination && f.ApplicantThematicId == applicantThematicId).ToList();

                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }
            return Json(result);
        }
        public JsonResult SaveOtherCost(List<decimal> tetfundingArrayOther, List<decimal> institutionFundingArrayOther, List<decimal> otherFundingArrayOther, List<string> descriptionArrayOther, int applicantThematicId, int typeId)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                if (descriptionArrayOther.Count > 0)
                {
                    using (TransactionScope scope = new TransactionScope())
                    {

                        //Create tetfunding
                        for (int i = 0; i < descriptionArrayOther.Count; i++)
                        {

                            if (typeId == 2)
                            {
                                BudgetDetail budgetDetail = new BudgetDetail();
                                budgetDetail.TetFundAmount = tetfundingArrayOther[i];
                                budgetDetail.InstitutionFundingAmount = institutionFundingArrayOther[i];
                                budgetDetail.OtherFunding = otherFundingArrayOther[i];
                                budgetDetail.BudgetItemId = (int)BudgetItems.Other;
                                budgetDetail.Description = descriptionArrayOther[i];
                                budgetDetail.TypeId = (int)Types.Proposal;
                                budgetDetail.ApplicantThematicId = applicantThematicId;
                                budgetDetail.Total = (tetfundingArrayOther[i] + institutionFundingArrayOther[i] + otherFundingArrayOther[i]);
                                _context.Add(budgetDetail);

                                _context.SaveChanges();
                            }
                            else if (typeId == 1)
                            {
                                BudgetDetail budgetDetail = new BudgetDetail();
                                budgetDetail.TetFundAmount = tetfundingArrayOther[i];
                                budgetDetail.InstitutionFundingAmount = institutionFundingArrayOther[i];
                                budgetDetail.OtherFunding = otherFundingArrayOther[i];
                                budgetDetail.BudgetItemId = (int)BudgetItems.Other;
                                budgetDetail.Description = descriptionArrayOther[i];
                                budgetDetail.TypeId = (int)Types.Proposal;
                                budgetDetail.ApplicantThematicId = applicantThematicId;
                                budgetDetail.Total = (tetfundingArrayOther[i] + institutionFundingArrayOther[i] + otherFundingArrayOther[i]);
                                _context.Add(budgetDetail);

                                _context.SaveChanges();
                            }


                        }
                        scope.Complete();

                    }
                    //result.PersonnelBudgetDetails = _context.BudgetDetail.Where(f => f.BudgetItemId == (int)BudgetItems.Other && f.ApplicantThematicId == applicantThematicId).ToList();

                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }
            return Json(result);
        }
        public JsonResult AutoSaveProposalDetail(string stringifyProposalDetail, int applicantThematicId, int typeId, decimal? budget)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                if (!string.IsNullOrEmpty(stringifyProposalDetail))
                {
                    //JavaScriptSerializer proposalDetailModelserializer = new JavaScriptSerializer();
                    ApplicationSubmission proposalDetail = JsonConvert.DeserializeObject<ApplicationSubmission>(stringifyProposalDetail);
                    if (typeId == 2)
                    {
                        _utility.AutoSaveApplicationSubmission(proposalDetail, applicantThematicId, (int)Types.Proposal, budget);
                    }
                    else if (typeId == 1)
                    {
                        _utility.AutoSaveApplicationSubmission(proposalDetail, applicantThematicId, (int)Types.ConceptNote, budget);
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(result);
        }
        public IActionResult PreviewProposalBeforeSubmittion(int id)
        {
            try
            {
                //if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                //{
                //    SetMessage(ValidateUserLogin(_context));
                //    return RedirectToAction("Logout", "Home");
                //}
                List<Person> personTeamList = new List<Person>();

                if (id > 0)
                {

                    var existingProposal = _context.ApplicantSubmission.Where(g => g.ApplicantThematicId == id && g.TypeId == (int)Types.Proposal).FirstOrDefault();
                    var applicantThematic = _context.ApplicantThematic.Where(f => f.Id == id).FirstOrDefault();
                    _userViewModel.Proposal = _context.Proposal.Where(f => f.ApplicantThemeId == id).FirstOrDefault();
                    if (applicantThematic != null)
                    {
                        var teamList = _context.Team.Where(f => f.ApplicantThematicId == applicantThematic.Id).ToList();
                        if (teamList.Count > 0)
                        {
                            for (int j = 0; j < teamList.Count; j++)
                            {
                                var teamPersonId = teamList[j].PersonId;
                                var singleTeamPerson = _context.Person.Where(c => c.Id == teamPersonId).FirstOrDefault();
                                personTeamList.Add(singleTeamPerson);
                            }
                        }
                    }
                    _userViewModel.Person = applicantThematic != null ? applicantThematic.Applicant.Person : new Person();
                    _userViewModel.TeamMember = new List<Person>();
                    _userViewModel.TeamMember = personTeamList.Count > 0 ? personTeamList : null;
                    if (existingProposal != null)
                    {


                        _userViewModel.ApplicantSubmission = existingProposal;
                        _userViewModel.ApplicantThematic = _context.ApplicantThematic.Where(f => f.Id == id).FirstOrDefault();
                        _userViewModel.BudgetDetails = _context.BudgetDetail.Where(f => f.ApplicantThematicId == id && f.TypeId == (int)Types.Proposal).ToList();
                        if (_userViewModel.BudgetDetails.Count > 0)
                        {
                            _userViewModel.BudgetDetails.OrderBy(g => g.BudgetItemId).ToList();
                            _userViewModel.BudgetDetailsSectionOne = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.PersonnelAllowances).ToList();


                            _userViewModel.BudgetDetailsSectionFour = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.DataCollectionandAnalysis).ToList();
                            _userViewModel.BudgetDetailsSectionThree = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.SuppliesConsumables).ToList();
                            _userViewModel.BudgetDetailsSectionTwo = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Equipment).ToList();
                            _userViewModel.BudgetDetailsSectionFive = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Travel).ToList();
                            _userViewModel.BudgetDetailsSectionSix = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Dissemination).ToList();
                            _userViewModel.BudgetDetailsSectionSeven = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Other).ToList();
                            _userViewModel.SectionPercentage1 = Math.Round(((_userViewModel.BudgetDetailsSectionOne.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage2 = Math.Round(((_userViewModel.BudgetDetailsSectionTwo.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage3 = Math.Round(((_userViewModel.BudgetDetailsSectionThree.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage4 = Math.Round(((_userViewModel.BudgetDetailsSectionFour.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage5 = Math.Round(((_userViewModel.BudgetDetailsSectionFive.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage6 = Math.Round(((_userViewModel.BudgetDetailsSectionSix.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage7 = Math.Round(((_userViewModel.BudgetDetailsSectionSeven.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();

                        }
                        else
                        {
                            _userViewModel.BudgetDetailsSectionOne = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFour = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionThree = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionTwo = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFive = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSix = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSeven = new List<BudgetDetail>();
                        }



                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_userViewModel);
        }
        public IActionResult PreviewConceptNoteBeforeSubmittion(int id)
        {
            try
            {

                if (id > 0)
                {
                    _userViewModel.ApplicantThematic = _context.ApplicantThematic.Where(f => f.Id == id).FirstOrDefault();
                    _userViewModel.ConceptNote = _context.ConceptNote.Where(g => g.ApplicantThemeId == id).FirstOrDefault();
                    var existingConceptNote = _context.ApplicantSubmission.Where(g => g.ApplicantThematicId == id && g.TypeId == (int)Types.ConceptNote).FirstOrDefault();
                    if (existingConceptNote != null)
                    {
                        _userViewModel.ApplicantSubmission = existingConceptNote;
                        _userViewModel.ApplicantThematic = _context.ApplicantThematic.Where(f => f.Id == id).FirstOrDefault();
                        _userViewModel.BudgetDetails = _context.BudgetDetail.Where(f => f.ApplicantThematicId == id && f.TypeId == (int)Types.ConceptNote).ToList();
                        if (_userViewModel.BudgetDetails.Count > 0)
                        {
                            _userViewModel.BudgetDetails.OrderBy(g => g.BudgetItemId).ToList();
                            _userViewModel.BudgetDetailsSectionOne = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.PersonnelAllowances).ToList();


                            _userViewModel.BudgetDetailsSectionFour = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.DataCollectionandAnalysis).ToList();
                            _userViewModel.BudgetDetailsSectionThree = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.SuppliesConsumables).ToList();
                            _userViewModel.BudgetDetailsSectionTwo = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Equipment).ToList();
                            _userViewModel.BudgetDetailsSectionFive = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Travel).ToList();
                            _userViewModel.BudgetDetailsSectionSix = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Dissemination).ToList();
                            _userViewModel.BudgetDetailsSectionSeven = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Other).ToList();
                            _userViewModel.SectionPercentage1 = Math.Round(((_userViewModel.BudgetDetailsSectionOne.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage2 = Math.Round(((_userViewModel.BudgetDetailsSectionTwo.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage3 = Math.Round(((_userViewModel.BudgetDetailsSectionThree.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage4 = Math.Round(((_userViewModel.BudgetDetailsSectionFour.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage5 = Math.Round(((_userViewModel.BudgetDetailsSectionFive.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage6 = Math.Round(((_userViewModel.BudgetDetailsSectionSix.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage7 = Math.Round(((_userViewModel.BudgetDetailsSectionSeven.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();

                        }
                        else
                        {
                            _userViewModel.BudgetDetailsSectionOne = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFour = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionThree = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionTwo = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFive = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSix = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSeven = new List<BudgetDetail>();
                        }



                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_userViewModel);
        }

        public JsonResult CheckForProposalUpload(int applicantThematicId)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                if (applicantThematicId > 0)
                {
                    // check that proposal has been uploaded
                    var proposal = _context.Proposal.Where(f => f.ApplicantThemeId == applicantThematicId).FirstOrDefault();
                    if (proposal.ProposalUrl == null)
                    {
                        result.IsError = false;
                        result.Message = "You have not uploaded Full Proposal PDf Document. Do you wish to continue, with the upload?";
                    }
                    else
                    {
                        result.IsError = true;
                    }
                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }
            return Json(result);
        }
        [Authorize(Policy = "MustBePI")]
        public IActionResult AddBudget()
        {

            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }
                string userEmail = User.FindFirstValue(ClaimTypes.Email);

                var applicant = _context.Applicant.Where(k => k.ApplicationNo == userEmail && k.RoleId == (int)Roles.Applicant).FirstOrDefault();
                var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                if (activeSessionForApplication == null)
                {
                    SetMessage("No Active Research cycle at the Moment.");
                    return RedirectToAction("Index", "UserHome");
                }
                var exist = _context.ApplicantThematic.Where(f => f.ApplicantId == applicant.Id && f.SessionId == activeSessionForApplication.Id).FirstOrDefault();

                if (exist != null)
                {
                    var approval = _context.Approval.Where(g => g.ApplicantThematicId == exist.Id && g.Approved && g.IsProposal == false).FirstOrDefault();
                    if (approval == null)
                    {
                        SetMessage("You cannot submit Full Proposal budget until your Concept Note is approved");
                        return RedirectToAction("Index", "UserHome");
                    }
                    var assessed = _context.ProposalAssessment.Where(r => r.ApplicantThematicId == exist.Id).FirstOrDefault();
                    var submitedProposal = _context.Proposal.Where(f => f.ApplicantThemeId == exist.Id).FirstOrDefault();
                    if (assessed != null || (submitedProposal != null && submitedProposal.Submitted))
                    {
                        SetMessage("You can no longer modify your Full Proposal");
                        return RedirectToAction("PreviewProposalBeforeSubmittion", new { id = exist.Id });
                    }
                    var existingProposal = _context.ApplicantSubmission.Where(g => g.ApplicantThematicId == exist.Id && g.TypeId == (int)Types.Proposal).FirstOrDefault();
                    if (existingProposal != null)
                    {
                        _userViewModel.ApplicantSubmission = existingProposal;
                        _userViewModel.ApplicantThematic = exist;
                        _userViewModel.BudgetDetails = _context.BudgetDetail.Where(f => f.ApplicantThematicId == exist.Id && f.TypeId == (int)Types.Proposal).ToList();
                        if (_userViewModel.BudgetDetails.Count > 0)
                        {
                            _userViewModel.BudgetDetailsSectionOne = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.PersonnelAllowances).ToList();


                            _userViewModel.BudgetDetailsSectionFour = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.DataCollectionandAnalysis).ToList();
                            _userViewModel.BudgetDetailsSectionThree = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.SuppliesConsumables).ToList();
                            _userViewModel.BudgetDetailsSectionTwo = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Equipment).ToList();
                            _userViewModel.BudgetDetailsSectionFive = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Travel).ToList();
                            _userViewModel.BudgetDetailsSectionSix = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Dissemination).ToList();
                            _userViewModel.BudgetDetailsSectionSeven = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Other).ToList();
                            _userViewModel.SectionPercentage1 = Math.Round(((_userViewModel.BudgetDetailsSectionOne.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage2 = Math.Round(((_userViewModel.BudgetDetailsSectionTwo.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage3 = Math.Round(((_userViewModel.BudgetDetailsSectionThree.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage4 = Math.Round(((_userViewModel.BudgetDetailsSectionFour.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage5 = Math.Round(((_userViewModel.BudgetDetailsSectionFive.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage6 = Math.Round(((_userViewModel.BudgetDetailsSectionSix.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage7 = Math.Round(((_userViewModel.BudgetDetailsSectionSeven.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        }
                        else
                        {
                            _userViewModel.BudgetDetailsSectionOne = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFour = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionThree = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionTwo = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFive = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSix = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSeven = new List<BudgetDetail>();
                        }

                    }
                    else if (exist != null)
                    {
                        _userViewModel.ApplicantSubmission = new ApplicantSubmission();
                        _userViewModel.ApplicantSubmission.Title = exist.Title;
                        _userViewModel.ApplicantThematic = exist;
                        _userViewModel.BudgetDetails = _context.BudgetDetail.Where(f => f.ApplicantThematicId == exist.Id).ToList();
                        if (_userViewModel.BudgetDetails.Count > 0)
                        {
                            _userViewModel.BudgetDetailsSectionOne = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.PersonnelAllowances).ToList();
                            _userViewModel.BudgetDetailsSectionFour = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.DataCollectionandAnalysis).ToList();
                            _userViewModel.BudgetDetailsSectionThree = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.SuppliesConsumables).ToList();
                            _userViewModel.BudgetDetailsSectionTwo = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Equipment).ToList();
                            _userViewModel.BudgetDetailsSectionFive = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Travel).ToList();
                            _userViewModel.BudgetDetailsSectionSix = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Dissemination).ToList();
                            _userViewModel.BudgetDetailsSectionSeven = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Other).ToList();
                            _userViewModel.SectionPercentage1 = Math.Round(((_userViewModel.BudgetDetailsSectionOne.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage2 = Math.Round(((_userViewModel.BudgetDetailsSectionTwo.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage3 = Math.Round(((_userViewModel.BudgetDetailsSectionThree.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage4 = Math.Round(((_userViewModel.BudgetDetailsSectionFour.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage5 = Math.Round(((_userViewModel.BudgetDetailsSectionFive.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage6 = Math.Round(((_userViewModel.BudgetDetailsSectionSix.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage7 = Math.Round(((_userViewModel.BudgetDetailsSectionSeven.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        }
                        else
                        {
                            _userViewModel.BudgetDetailsSectionOne = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFour = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionThree = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionTwo = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFive = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSix = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSeven = new List<BudgetDetail>();
                        }
                    }
                }
                else
                {
                    SetMessage("Please, You are to submit Concept Note before proceeding to Proposal");
                    return RedirectToAction("SubmitConceptNote");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_userViewModel);
        }
        [Authorize(Policy = "MustBePI")]
        public IActionResult AddConceptNoteBudget()
        {

            try
            {
                string userEmail = User.FindFirstValue(ClaimTypes.Email);

                var applicant = _context.Applicant.Where(k => k.ApplicationNo == userEmail && k.RoleId == (int)Roles.Applicant).FirstOrDefault();
                var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                if (activeSessionForApplication == null)
                {
                    SetMessage("No Active Research cycle at the Moment.");
                    return RedirectToAction("Index", "UserHome");
                }
                var exist = _context.ApplicantThematic.Where(f => f.ApplicantId == applicant.Id && f.SessionId == activeSessionForApplication.Id).FirstOrDefault();

                if (exist != null)
                {

                    var existingConceptNote = _context.ApplicantSubmission.Where(g => g.ApplicantThematicId == exist.Id && g.TypeId == (int)Types.ConceptNote).FirstOrDefault();
                    if (existingConceptNote != null)
                    {
                        _userViewModel.ApplicantSubmission = existingConceptNote;
                        _userViewModel.ApplicantThematic = exist;
                        _userViewModel.BudgetDetails = _context.BudgetDetail.Where(f => f.ApplicantThematicId == exist.Id && f.TypeId == (int)Types.ConceptNote).ToList();
                        if (_userViewModel.BudgetDetails.Count > 0)
                        {
                            _userViewModel.BudgetDetailsSectionOne = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.PersonnelAllowances).ToList();


                            _userViewModel.BudgetDetailsSectionFour = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.DataCollectionandAnalysis).ToList();
                            _userViewModel.BudgetDetailsSectionThree = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.SuppliesConsumables).ToList();
                            _userViewModel.BudgetDetailsSectionTwo = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Equipment).ToList();
                            _userViewModel.BudgetDetailsSectionFive = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Travel).ToList();
                            _userViewModel.BudgetDetailsSectionSix = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Dissemination).ToList();
                            _userViewModel.BudgetDetailsSectionSeven = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Other).ToList();
                            _userViewModel.SectionPercentage1 = Math.Round(((_userViewModel.BudgetDetailsSectionOne.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage2 = Math.Round(((_userViewModel.BudgetDetailsSectionTwo.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage3 = Math.Round(((_userViewModel.BudgetDetailsSectionThree.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage4 = Math.Round(((_userViewModel.BudgetDetailsSectionFour.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage5 = Math.Round(((_userViewModel.BudgetDetailsSectionFive.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage6 = Math.Round(((_userViewModel.BudgetDetailsSectionSix.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage7 = Math.Round(((_userViewModel.BudgetDetailsSectionSeven.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        }
                        else
                        {
                            _userViewModel.BudgetDetailsSectionOne = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFour = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionThree = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionTwo = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFive = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSix = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSeven = new List<BudgetDetail>();
                        }

                    }
                    else if (exist != null)
                    {
                        _userViewModel.ApplicantSubmission = new ApplicantSubmission();
                        _userViewModel.ApplicantSubmission.Title = exist.Title;
                        _userViewModel.ApplicantThematic = exist;
                        _userViewModel.BudgetDetails = _context.BudgetDetail.Where(f => f.ApplicantThematicId == exist.Id && f.TypeId == (int)Types.ConceptNote).ToList();
                        if (_userViewModel.BudgetDetails.Count > 0)
                        {
                            _userViewModel.BudgetDetailsSectionOne = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.PersonnelAllowances).ToList();
                            _userViewModel.BudgetDetailsSectionFour = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.DataCollectionandAnalysis).ToList();
                            _userViewModel.BudgetDetailsSectionThree = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.SuppliesConsumables).ToList();
                            _userViewModel.BudgetDetailsSectionTwo = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Equipment).ToList();
                            _userViewModel.BudgetDetailsSectionFive = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Travel).ToList();
                            _userViewModel.BudgetDetailsSectionSix = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Dissemination).ToList();
                            _userViewModel.BudgetDetailsSectionSeven = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Other).ToList();
                            _userViewModel.SectionPercentage1 = Math.Round(((_userViewModel.BudgetDetailsSectionOne.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage2 = Math.Round(((_userViewModel.BudgetDetailsSectionTwo.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage3 = Math.Round(((_userViewModel.BudgetDetailsSectionThree.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage4 = Math.Round(((_userViewModel.BudgetDetailsSectionFour.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage5 = Math.Round(((_userViewModel.BudgetDetailsSectionFive.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage6 = Math.Round(((_userViewModel.BudgetDetailsSectionSix.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage7 = Math.Round(((_userViewModel.BudgetDetailsSectionSeven.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        }
                        else
                        {
                            _userViewModel.BudgetDetailsSectionOne = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFour = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionThree = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionTwo = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFive = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSix = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSeven = new List<BudgetDetail>();
                        }
                    }
                }
                else
                {
                    SetMessage("Please, You are to submit Concept Note before proceeding to Concept Note Proposed Budget");
                    return RedirectToAction("ConceptNoteApplication");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_userViewModel);
        }
        public JsonResult CreateApplicantThematic(int ThemeId, string title)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                if (activeSessionForApplication == null)
                {
                    result.IsError = true;
                    result.Message = "No Active Research cycle at the Moment.";
                    return Json(result);

                }
                if (ThemeId > 0 && !String.IsNullOrEmpty(title))
                {
                    string userEmail = User.FindFirstValue(ClaimTypes.Email);
                    var applicant = _context.Applicant.Where(f => f.ApplicationNo == userEmail && f.RoleId == (int)Roles.Applicant).FirstOrDefault();
                    if (applicant != null)
                    {
                        var applicantThematic = _context.ApplicantThematic.Where(f => f.ApplicantId == applicant.Id && f.SessionId == activeSessionForApplication.Id).FirstOrDefault();
                        if (applicantThematic != null)
                        {
                            applicantThematic.ThemeId = ThemeId;
                            _context.Update(applicantThematic);
                            _context.SaveChanges();
                            result.IsError = false;
                            result.ApplicantThematicId = applicantThematic.Id;
                            return Json(result);
                        }
                        else
                        {
                            ApplicantThematic newApplicantThematic = new ApplicantThematic()
                            {
                                ApplicantId = applicant.Id,
                                DateApplied = DateTime.Now,
                                ThemeId = ThemeId,
                                Title = title
                            };
                            _context.ApplicantThematic.Add(newApplicantThematic);
                            _context.SaveChanges();
                            result.IsError = false;
                            result.ApplicantThematicId = newApplicantThematic.Id;
                            return Json(result);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }
            return Json(result);
        }
        [Authorize(Policy = "MustBePI")]
        public IActionResult EditProposalBudget()
        {
            try
            {
                string userEmail = User.FindFirstValue(ClaimTypes.Email);
                var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                if (activeSessionForApplication == null)
                {
                    SetMessage("No active research cycle at the moment.");
                    return RedirectToAction("Index", "UserHome");
                }
                var applicant = _context.Applicant.Where(k => k.ApplicationNo == userEmail && k.RoleId == (int)Roles.Applicant).FirstOrDefault();
                var exist = _context.ApplicantThematic.Where(f => f.ApplicantId == applicant.Id && f.SessionId == activeSessionForApplication.Id).FirstOrDefault();
                if (exist != null)
                {
                    _userViewModel.BudgetDetailsSectionOne = new List<BudgetDetail>();
                    _userViewModel.BudgetDetailsSectionFour = new List<BudgetDetail>();
                    _userViewModel.BudgetDetailsSectionThree = new List<BudgetDetail>();
                    _userViewModel.BudgetDetailsSectionTwo = new List<BudgetDetail>();
                    _userViewModel.BudgetDetailsSectionFive = new List<BudgetDetail>();
                    _userViewModel.BudgetDetailsSectionSix = new List<BudgetDetail>();
                    _userViewModel.BudgetDetailsSectionSeven = new List<BudgetDetail>();
                    var proposal = _context.Proposal.Where(f => f.ApplicantThemeId == exist.Id).FirstOrDefault();
                    if (proposal != null && proposal.Submitted)
                    {

                        SetMessage("Your Full Proposal has been submitted, you can no longer make changes to your budget.");
                        return RedirectToAction("Index", "UserHome");
                    }
                    _userViewModel.BudgetDetails = _context.BudgetDetail.Where(f => f.ApplicantThematicId == exist.Id && f.TypeId == (int)Types.Proposal).ToList();
                    if (_userViewModel.BudgetDetails.Count > 0)
                    {
                        _userViewModel.BudgetDetailsSectionOne = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.PersonnelAllowances).ToList();
                        _userViewModel.BudgetDetailsSectionFour = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.DataCollectionandAnalysis).ToList();
                        _userViewModel.BudgetDetailsSectionThree = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.SuppliesConsumables).ToList();
                        _userViewModel.BudgetDetailsSectionTwo = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Equipment).ToList();
                        _userViewModel.BudgetDetailsSectionFive = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Travel).ToList();
                        _userViewModel.BudgetDetailsSectionSix = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Dissemination).ToList();
                        _userViewModel.BudgetDetailsSectionSeven = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Other).ToList();
                        _userViewModel.SectionPercentage1 = Math.Round(((_userViewModel.BudgetDetailsSectionOne.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        _userViewModel.SectionPercentage2 = Math.Round(((_userViewModel.BudgetDetailsSectionTwo.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        _userViewModel.SectionPercentage3 = Math.Round(((_userViewModel.BudgetDetailsSectionThree.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        _userViewModel.SectionPercentage4 = Math.Round(((_userViewModel.BudgetDetailsSectionFour.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        _userViewModel.SectionPercentage5 = Math.Round(((_userViewModel.BudgetDetailsSectionFive.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        _userViewModel.SectionPercentage6 = Math.Round(((_userViewModel.BudgetDetailsSectionSix.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        _userViewModel.SectionPercentage7 = Math.Round(((_userViewModel.BudgetDetailsSectionSeven.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                        return View(_userViewModel);
                    }
                    else
                    {
                        SetMessage("No Budget for modification");
                        return RedirectToAction("Index", "UserHome");
                    }

                }
                else
                {
                    SetMessage("No Budget for modification");
                    return RedirectToAction("Index", "UserHome");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //return RedirectToAction("Index", "UserHome");
        }

        public JsonResult DeleteBudgetItem(int itemId)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                if (itemId > 0)
                {
                    var budgetDetail = _context.BudgetDetail.Where(o => o.Id == itemId).FirstOrDefault();
                    if (budgetDetail != null)
                    {
                        _context.Remove(budgetDetail);
                        _context.SaveChanges();
                        result.IsError = false;
                        return Json(result);
                    }

                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }
            return Json(result);
        }
        public JsonResult FetchBudgetItem(int itemId)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                if (itemId > 0)
                {
                    var budgetDetail = _context.BudgetDetail.Where(o => o.Id == itemId).FirstOrDefault();
                    if (budgetDetail != null)
                    {

                        result.IsError = false;
                        result.Tetfund = (decimal)budgetDetail.TetFundAmount;
                        result.Institution = (decimal)budgetDetail.InstitutionFundingAmount;
                        result.Others = (decimal)budgetDetail.OtherFunding;
                        return Json(result);
                    }

                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }
            return Json(result);
        }
        public JsonResult SaveBudgetItem(int editItemId, decimal tetfund, decimal inst, decimal other)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                if (editItemId > 0)
                {
                    var budgetDetail = _context.BudgetDetail.Where(o => o.Id == editItemId).FirstOrDefault();
                    if (budgetDetail != null)
                    {
                        budgetDetail.TetFundAmount = tetfund;
                        budgetDetail.InstitutionFundingAmount = inst;
                        budgetDetail.OtherFunding = other;
                        budgetDetail.Total = (tetfund + inst + other);
                        _context.Update(budgetDetail);
                        _context.SaveChanges();
                        result.IsError = false;
                        return Json(result);
                    }

                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }
            return Json(result);
        }


        // Status checking
        public JsonResult Status()
        {
            JsonResultModel result = new JsonResultModel();
            try
            {

                string userEmail = User.FindFirstValue(ClaimTypes.Email);
                var applicant = _context.Applicant.Where(f => f.ApplicationNo == userEmail && f.RoleId == (int)Roles.Applicant).FirstOrDefault();
                if (applicant != null)
                {
                    var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                    if (activeSessionForApplication == null)
                    {
                        result.IsError = true;
                        result.Message = "No Active Research Cycle at the Moment.";
                        return Json(result);

                    }
                    var applicantThematic = _context.ApplicantThematic.Where(g => g.ApplicantId == applicant.Id && g.SessionId == activeSessionForApplication.Id).FirstOrDefault();

                    if (applicantThematic != null)
                    {
                        var proposal = _context.Proposal.Where(f => f.ApplicantThemeId == applicantThematic.Id).FirstOrDefault();
                        var proposalSubmission = _context.ApplicantSubmission.Where(f => f.ApplicantThematicId == applicantThematic.Id && f.TypeId == (int)Types.Proposal).FirstOrDefault();
                        var approval = _context.Approval.Where(y => y.ApplicantThematicId == applicantThematic.Id && y.IsProposal == false).FirstOrDefault();
                        var proposalAssessment = _context.ProposalAssessment.Where(f => f.ApplicantThematicId == applicantThematic.Id).ToList();
                        if (approval != null && approval.Approved)
                        {
                            result.IsError = false;
                            result.ConceptNoteApprove = "Yes";
                        }
                        else if (applicantThematic != null && approval == null)
                        {
                            result.IsError = false;
                            result.ConceptNoteApprove = "No";
                        }

                        if (proposalAssessment.Count > 0 && proposalAssessment.Any(d => d.Approved))
                        {
                            result.IsError = false;
                            result.ProposalApprove = "Yes";
                        }
                        else if (proposal != null && proposal.Submitted)
                        {
                            result.IsError = false;
                            result.ProposalApprove = "No";

                        }

                    }
                }


            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }
            return Json(result);
        }
        public IActionResult PrevieConceptNoteByTeamMember()
        {
            try
            {
                string personEmail = User.FindFirstValue(ClaimTypes.Email);
                string roleName = User.FindFirstValue(ClaimTypes.Role);
                var applicantThematic = new ApplicantThematic();
                var applicant = _context.Applicant.Where(g => g.ApplicationNo == personEmail && g.RoleId == (int)Roles.Applicant).FirstOrDefault();
                var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();

                if (activeSessionForApplication == null)
                {
                    SetMessage("No Active Research Cycle at the Moment.");
                    return RedirectToAction("Index", "UserHome");
                }
                if (applicant?.Id > 0)
                    applicantThematic = _context.ApplicantThematic.Where(o => o.ApplicantId == applicant.Id && o.SessionId == activeSessionForApplication.Id).FirstOrDefault();
                //Determine if its principal investigator of Team member that is logged in
                if (roleName == "Co-Applicant")
                {
                    string thematicId = User.FindFirstValue(ClaimTypes.GroupSid);
                    var teamThematicId = Convert.ToInt32(thematicId);
                    applicantThematic = _context.ApplicantThematic.Where(f => f.Id == teamThematicId).FirstOrDefault();

                }

                if (applicantThematic?.Id > 0)
                {
                    _userViewModel.ApplicantThematic = _context.ApplicantThematic.Where(f => f.Id == applicantThematic.Id).FirstOrDefault();
                    _userViewModel.ConceptNote = _context.ConceptNote.Where(g => g.ApplicantThemeId == applicantThematic.Id).FirstOrDefault();
                    var existingConceptNote = _context.ApplicantSubmission.Where(g => g.ApplicantThematicId == applicantThematic.Id && g.TypeId == (int)Types.ConceptNote).FirstOrDefault();
                    if (existingConceptNote != null)
                    {
                        _userViewModel.ApplicantSubmission = existingConceptNote;
                        _userViewModel.ApplicantSubmission.Title = applicantThematic.Title;
                        _userViewModel.ApplicantThematic = _context.ApplicantThematic.Where(f => f.Id == applicantThematic.Id).FirstOrDefault();
                        _userViewModel.BudgetDetails = _context.BudgetDetail.Where(f => f.ApplicantThematicId == applicantThematic.Id && f.TypeId == (int)Types.ConceptNote).ToList();
                        if (_userViewModel.BudgetDetails.Count > 0)
                        {
                            _userViewModel.BudgetDetails.OrderBy(g => g.BudgetItemId).ToList();
                            _userViewModel.BudgetDetailsSectionOne = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.PersonnelAllowances).ToList();


                            _userViewModel.BudgetDetailsSectionFour = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.DataCollectionandAnalysis).ToList();
                            _userViewModel.BudgetDetailsSectionThree = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.SuppliesConsumables).ToList();
                            _userViewModel.BudgetDetailsSectionTwo = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Equipment).ToList();
                            _userViewModel.BudgetDetailsSectionFive = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Travel).ToList();
                            _userViewModel.BudgetDetailsSectionSix = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Dissemination).ToList();
                            _userViewModel.BudgetDetailsSectionSeven = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Other).ToList();
                            _userViewModel.SectionPercentage1 = Math.Round(((_userViewModel.BudgetDetailsSectionOne.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage2 = Math.Round(((_userViewModel.BudgetDetailsSectionTwo.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage3 = Math.Round(((_userViewModel.BudgetDetailsSectionThree.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage4 = Math.Round(((_userViewModel.BudgetDetailsSectionFour.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage5 = Math.Round(((_userViewModel.BudgetDetailsSectionFive.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage6 = Math.Round(((_userViewModel.BudgetDetailsSectionSix.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage7 = Math.Round(((_userViewModel.BudgetDetailsSectionSeven.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();

                        }
                        else
                        {
                            _userViewModel.BudgetDetailsSectionOne = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFour = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionThree = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionTwo = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFive = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSix = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSeven = new List<BudgetDetail>();
                        }



                    }
                    else
                    {
                        SetMessage("You are yet to Submit a Concept Note");
                        return RedirectToAction("Index", "UserHome");
                    }
                }
                else
                {
                    SetMessage("You are yet to Submit a Concept Note");
                    return RedirectToAction("Index", "UserHome");
                }


            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(_userViewModel);
        }
        public IActionResult PreviewProposalByTeamMember()
        {
            List<Person> personTeamList = new List<Person>();
            try
            {
                string personEmail = User.FindFirstValue(ClaimTypes.Email);
                string roleName = User.FindFirstValue(ClaimTypes.Role);
                var applicantThematicPreview = new ApplicantThematic();
                var applicant = _context.Applicant.Where(g => g.ApplicationNo == personEmail).FirstOrDefault();
                var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                if (activeSessionForApplication == null)
                {
                    SetMessage("No Active Research Cycle at the Moment.");
                    return RedirectToAction("Index", "UserHome");
                }
                if (applicant?.Id > 0)
                    applicantThematicPreview = _context.ApplicantThematic.Where(o => o.ApplicantId == applicant.Id && o.SessionId == activeSessionForApplication.Id).FirstOrDefault();

                //Determine if its principal investigator of Team member that is logged in
                if (roleName == "Co-Applicant")
                {
                    string thematicId = User.FindFirstValue(ClaimTypes.GroupSid);
                    var teamThematicId = Convert.ToInt32(thematicId);
                    applicantThematicPreview = _context.ApplicantThematic.Where(o => o.Id == teamThematicId && o.SessionId == activeSessionForApplication.Id).FirstOrDefault();
                }

                if (applicantThematicPreview?.Id > 0)
                {
                    var existingProposal = _context.ApplicantSubmission.Where(g => g.ApplicantThematicId == applicantThematicPreview.Id && g.TypeId == (int)Types.Proposal).FirstOrDefault();
                    var applicantThematic = _context.ApplicantThematic.Where(f => f.Id == applicantThematicPreview.Id).FirstOrDefault();
                    _userViewModel.Proposal = _context.Proposal.Where(f => f.ApplicantThemeId == applicantThematicPreview.Id).FirstOrDefault();
                    if (applicantThematic != null)
                    {
                        var teamList = _context.Team.Where(f => f.ApplicantThematicId == applicantThematic.Id).ToList();
                        if (teamList.Count > 0)
                        {
                            for (int j = 0; j < teamList.Count; j++)
                            {
                                var teamPersonId = teamList[j].PersonId;
                                var singleTeamPerson = _context.Person.Where(c => c.Id == teamPersonId).FirstOrDefault();
                                personTeamList.Add(singleTeamPerson);
                            }
                        }
                    }
                    _userViewModel.Person = applicantThematic != null ? applicantThematic.Applicant.Person : new Person();
                    _userViewModel.TeamMember = new List<Person>();
                    _userViewModel.TeamMember = personTeamList.Count > 0 ? personTeamList : null;
                    if (existingProposal != null)
                    {


                        _userViewModel.ApplicantSubmission = existingProposal;
                        _userViewModel.ApplicantThematic = _context.ApplicantThematic.Where(f => f.Id == applicantThematicPreview.Id).FirstOrDefault();
                        _userViewModel.BudgetDetails = _context.BudgetDetail.Where(f => f.ApplicantThematicId == applicantThematicPreview.Id && f.TypeId == (int)Types.Proposal).ToList();
                        if (_userViewModel.BudgetDetails.Count > 0)
                        {
                            _userViewModel.BudgetDetails.OrderBy(g => g.BudgetItemId).ToList();
                            _userViewModel.BudgetDetailsSectionOne = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.PersonnelAllowances).ToList();


                            _userViewModel.BudgetDetailsSectionFour = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.DataCollectionandAnalysis).ToList();
                            _userViewModel.BudgetDetailsSectionThree = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.SuppliesConsumables).ToList();
                            _userViewModel.BudgetDetailsSectionTwo = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Equipment).ToList();
                            _userViewModel.BudgetDetailsSectionFive = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Travel).ToList();
                            _userViewModel.BudgetDetailsSectionSix = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Dissemination).ToList();
                            _userViewModel.BudgetDetailsSectionSeven = _userViewModel.BudgetDetails.Where(f => f.BudgetItemId == (int)BudgetItems.Other).ToList();
                            _userViewModel.SectionPercentage1 = Math.Round(((_userViewModel.BudgetDetailsSectionOne.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage2 = Math.Round(((_userViewModel.BudgetDetailsSectionTwo.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage3 = Math.Round(((_userViewModel.BudgetDetailsSectionThree.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage4 = Math.Round(((_userViewModel.BudgetDetailsSectionFour.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage5 = Math.Round(((_userViewModel.BudgetDetailsSectionFive.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage6 = Math.Round(((_userViewModel.BudgetDetailsSectionSix.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();
                            _userViewModel.SectionPercentage7 = Math.Round(((_userViewModel.BudgetDetailsSectionSeven.Sum(d => d.Total) / _userViewModel.BudgetDetails.Sum(d => d.Total)) * 100), 2).ToString();

                        }
                        else
                        {
                            _userViewModel.BudgetDetailsSectionOne = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFour = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionThree = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionTwo = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionFive = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSix = new List<BudgetDetail>();
                            _userViewModel.BudgetDetailsSectionSeven = new List<BudgetDetail>();
                        }
                    }
                    else
                    {
                        SetMessage("You are yet to Submit a Full Proposal");
                        return RedirectToAction("Index", "UserHome");
                    }
                }
                else
                {
                    SetMessage("You are yet to Submit a Full Proposal");
                    return RedirectToAction("Index", "UserHome");
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(_userViewModel);
        }
        public async Task<IActionResult> MySubmissions()
        {
            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }
                string userEmail = User.FindFirstValue(ClaimTypes.Email);
                var logginUserRole = User.FindFirstValue(ClaimTypes.Role);
                if (logginUserRole != null && logginUserRole == "Applicant" && userEmail != null)
                {
                    var applicant = _context.Applicant.Where(f => f.ApplicationNo == userEmail && f.RoleId == (int)Roles.Applicant).FirstOrDefault();
                    if (applicant?.Id > 0)
                    {
                        _userViewModel.ApplicantSubmissions = await _context.ApplicantSubmission.Where(f => f.ApplicantThematic.ApplicantId == applicant.Id)
                            .Include(f => f.ApplicantThematic)
                            .ThenInclude(f => f.Applicant)
                            .Include(f => f.ApplicantThematic.Theme)
                            .ToListAsync();
                    }

                }
                else
                {
                    SetMessage("This module is for Principal Investigator only");
                    return RedirectToAction("Index", "UserHome");
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(_userViewModel);
        }
        public IActionResult CreateConceptNote(int themeId, string title, decimal budget, int applicantThematicId)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                string userEmail = User.FindFirstValue(ClaimTypes.Email);
                var person = _context.Person.Where(f => f.Email == userEmail).FirstOrDefault();
                ApplicantThematic applicantThematic = new ApplicantThematic();
                if (person != null)
                {


                    var applicant = _context.Applicant.Where(k => k.PersonId == person.Id && k.RoleId == (int)Roles.Applicant).FirstOrDefault();
                    if (applicantThematicId > 0)
                    {
                        applicantThematic = _context.ApplicantThematic.Where(f => f.Id == applicantThematicId).FirstOrDefault();
                    }
                    else
                    {
                        applicantThematic = _context.ApplicantThematic.Where(f => f.ApplicantId == applicant.Id && f.Session.ActiveForApplication).FirstOrDefault();
                    }

                    if (applicantThematic?.Id > 0)
                    {
                        result.IsError = false;
                        result.ApplicantThematicId = applicantThematic.Id;
                        applicantThematic.Title = title;
                        applicantThematic.ThemeId = themeId;
                        _context.Update(applicantThematic);
                        _context.SaveChanges();
                        var conceptNote=_context.ConceptNote.Where(f => f.ApplicantThemeId == applicantThematicId).FirstOrDefault();
                        if (conceptNote?.Id > 0)
                        {
                            conceptNote.ConceptNoteSubmittedBudget = budget;
                            _context.Update(conceptNote);
                            _context.SaveChanges();
                            
                        }
                        return Json(result);

                    }
                    else
                    {
                        _utility.AddConceptNote(applicant, themeId, title, budget);
                        return Json(result);
                    }

                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }
            return Json(result);



        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> UploadConceptNoteAttachement(AttachmentUpload attachmentUpload)
        {
            JsonResultModel result = new JsonResultModel();
            if (attachmentUpload.AttachmentFile.Length > 0)
            {
                var url=await UploadAttachment(attachmentUpload.AttachmentFile, attachmentUpload.applicantThematicId, attachmentUpload.attachmentNo, (int)Types.ConceptNote);
                var conceptNote=await _context.ConceptNote.Where(f => f.ApplicantThemeId == attachmentUpload.applicantThematicId).FirstOrDefaultAsync();
                if (conceptNote?.Id > 0 && url!="" && url!=null)
                {
                    switch (attachmentUpload.attachmentNo)
                    {
                        case "one":
                            conceptNote.AttachmentOne = url;
                            conceptNote.NameOne = attachmentUpload.AttachmentFile.FileName;
                            _context.Update(conceptNote);
                            await _context.SaveChangesAsync();
                            result.IsError = false;
                            result.Message = "Upload was successful";
                            result.FileName= attachmentUpload.AttachmentFile.FileName;
                            break;
                        case "two":
                            conceptNote.AttachmentTwo = url;
                            conceptNote.NameTwo = attachmentUpload.AttachmentFile.FileName;
                            _context.Update(conceptNote);
                            await _context.SaveChangesAsync();
                            result.IsError = false;
                            result.Message = "Upload was successful";
                            result.FileName = attachmentUpload.AttachmentFile.FileName;
                            break;
                        case "three":
                            conceptNote.AttachmentThree = url;
                            conceptNote.NameThree = attachmentUpload.AttachmentFile.FileName;
                            _context.Update(conceptNote);
                            await _context.SaveChangesAsync();
                            result.IsError = false;
                            result.Message = "Upload was successful";
                            result.FileName = attachmentUpload.AttachmentFile.FileName;
                            break;
                        case "four":
                            conceptNote.AttachmentFour = url;
                            conceptNote.NameFour = attachmentUpload.AttachmentFile.FileName;
                            _context.Update(conceptNote);
                            await _context.SaveChangesAsync();
                            result.IsError = false;
                            result.Message = "Upload was successful";
                            result.FileName = attachmentUpload.AttachmentFile.FileName;
                            break;
                        case "five":
                            conceptNote.AttachmentFive = url;
                            conceptNote.NameFive = attachmentUpload.AttachmentFile.FileName;
                            _context.Update(conceptNote);
                            await _context.SaveChangesAsync();
                            result.IsError = false;
                            result.Message = "Upload was successful";
                            result.FileName = attachmentUpload.AttachmentFile.FileName;
                            break;
                        default:
                            result.IsError = true;
                            result.Message = "Something went wrong";
                            break;

                    }
                }

            }
            return Json(result);
        }
        public IActionResult DeleteAttachment(string attachmentNo,int applicantThematicId)
        {
            JsonResultModel result = new JsonResultModel();
            var conceptNote=_context.ConceptNote.Where(f => f.ApplicantThemeId == applicantThematicId).FirstOrDefault();
            string fileName = string.Empty;
            if (conceptNote?.Id > 0)
            {
                switch (attachmentNo)
                {
                    case "one":
                        
                        fileName=conceptNote.AttachmentOne;
                        
                        break;
                    case "two":
                        fileName = conceptNote.AttachmentTwo;
                        break;
                    case "three":
                        fileName = conceptNote.AttachmentThree;
                        break;
                    case "four":
                        fileName = conceptNote.AttachmentFour;
                        break;
                    case "five":
                        fileName = conceptNote.AttachmentFive;
                        break;
                    default:
                        result.IsError = true;
                        result.Message = "Something went wrong";
                        break;

                }
                if (!String.IsNullOrEmpty(fileName))
                {
                    var split = fileName.Split('\\');
                    var name=split[2];
                    DeleteDirectory(name,attachmentNo, conceptNote.Id);
                    result.IsError = false;
                    result.Message = "Deleted successfully";
                }
            }
            return Json(result);
        }
        public async Task<string> UploadAttachment(IFormFile AttachmentFile, int applicantThematicId,string attachmentNo,int type)
        {
            string filePath = string.Empty;
            List<string> validFileExtension = new List<string>();
            validFileExtension.Add(".pdf");
            validFileExtension.Add(".doc");
            validFileExtension.Add(".docx");
            validFileExtension.Add(".xls");
            validFileExtension.Add(".xlsx");
            validFileExtension.Add(".docx");
            validFileExtension.Add(".ppt");
            validFileExtension.Add(".jpeg");
            validFileExtension.Add(".jpg");
            validFileExtension.Add(".png");

            if (AttachmentFile.Length > 0)
            {
                var extType = Path.GetExtension(AttachmentFile.FileName);


                if (validFileExtension.Contains(extType))
                {
                    var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "attachments");
                    var extName = Path.GetExtension(AttachmentFile.FileName);
                    string fileName = Path.GetFileName(AttachmentFile.FileName);
                    string isProposal = type == (int)Types.Proposal ? "Proposal" : "ConceptNote";
                    string saveName = string.Format("Attachment_{0}_{1}_{2}{3}", isProposal,attachmentNo, DateTime.Now.Millisecond, extName);
                    filePath = Path.Combine(uploads, saveName);

                    var fullPath = Path.Combine(filePath, fileName);

                    FileInfo file = new FileInfo(saveName);
                    if (file.Exists)
                    {
                        file.Delete();
                    }

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await AttachmentFile.CopyToAsync(stream);
                    }
                    filePath = "\\attachments\\" + saveName;
                }
            }
            return filePath;
        }
        public async void DeleteDirectory(string fileName,string attachmentNo, int ConceptNoteId)
        {
            string fileRootPath = Path.Combine(_hostingEnvironment.WebRootPath, "attachments");
            string fullPart = Path.Combine(fileRootPath, fileName);
            FileInfo file = new FileInfo(fullPart);
            if (file.Exists)
            {
                file.Delete();
            }
           var conceptNote= _context.ConceptNote.Where(f => f.Id == ConceptNoteId).FirstOrDefault();
            if (conceptNote?.Id > 0)
            {
                switch (attachmentNo)
                {
                    case "one":
                        conceptNote.AttachmentOne = null;
                        conceptNote.NameOne = null;
                        _context.Update(conceptNote);
                        await _context.SaveChangesAsync();
                       
                        break;
                    case "two":
                        conceptNote.AttachmentTwo = null;
                        conceptNote.NameTwo = null;
                        _context.Update(conceptNote);
                        await _context.SaveChangesAsync();
                        break;
                    case "three":
                        conceptNote.AttachmentThree = null;
                        conceptNote.NameThree = null;
                        _context.Update(conceptNote);
                        await _context.SaveChangesAsync();
                        break;
                    case "four":
                        conceptNote.AttachmentFour = null;
                        conceptNote.NameFour = null;
                        _context.Update(conceptNote);
                        await _context.SaveChangesAsync();
                        break;
                    case "five":
                        conceptNote.AttachmentFive = null;
                        conceptNote.NameFive = null;
                        _context.Update(conceptNote);
                        await _context.SaveChangesAsync();
                        break;
                    default:
                        
                        break;

                }
            }
        }



    }
}