﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Grant.Web.Models;
using Grant.Web.Models.ViewModels;
using Microsoft.AspNetCore.Hosting;

namespace Grant.Web.Controllers
{
    public class DefaultController : Controller
    {
        private readonly DataBaseContext _context;
        UserViewModel _userViewModel;
        private IHostingEnvironment _hostingEnvironment;
        private const string ID = "Id";
        private const string NAME = "Name";


        public DefaultController(DataBaseContext context, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _userViewModel = new UserViewModel();
            _userViewModel.PopulateAllDropdown(_context);
            _hostingEnvironment = hostingEnvironment;


        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult SubmitConceptNote()
        {
            return View(_userViewModel);
        }
    }
}