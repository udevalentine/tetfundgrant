﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grant.Web.Models;
using Grant.Web.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Grant.Web.Controllers
{
    [Authorize(Policy = "MustBeAdmin")]
    public class MenuController : BaseController
    {
        private const string Value = "Id";
        private const string Text = "Name";
        public const string Select = "-- Select --";
        private MenuViewModel _viewModel;
        private readonly DataBaseContext _context;
        public MenuController(DataBaseContext context)
        {
            _viewModel = new MenuViewModel();
            _context = context;
            _viewModel.PopulateAllDropdown(_context);
        }
        public IActionResult AddMenu()
        {
            if (TempData["ViewModel"] != null)
            {
                _viewModel = (MenuViewModel)TempData["ViewModel"];
                _viewModel.ShowList = true;
            }
            return View(_viewModel);
        }
        [HttpPost]
        public IActionResult AddMenu(MenuViewModel menuViewModel)
        {
            try
            {
                if (!string.IsNullOrEmpty(menuViewModel.ActionName) && !string.IsNullOrEmpty(menuViewModel.ControllerName) && !string.IsNullOrEmpty(menuViewModel.DisplayName) && menuViewModel?.MenuGroupId > 0)
                {
                    var menuOption = _context.Menu
                                             .Where(m =>
                                             m.ActionName.ToLower().Trim() == menuViewModel.ActionName.ToLower().Trim() &&
                                             m.Controller.ToLower().Trim() == menuViewModel.ControllerName.ToLower().Trim() &&
                                             m.Menu_GroupId == menuViewModel.MenuGroupId
                                             )
                                             .FirstOrDefault();
                    if (menuOption != null)
                    {
                        SetMessage("This Menu already exists");
                    }
                    else
                    {
                        var newMenu = new Menu()
                        {
                            ActionName = menuViewModel.ActionName,
                            Controller = menuViewModel.ControllerName,
                            DisplayName = menuViewModel.DisplayName,
                            Menu_GroupId = menuViewModel.MenuGroupId,
                            Active=true
                            
                        };

                        _context.Add(newMenu);
                        _context.SaveChanges();

                        SetMessage("Menu added successfully");
                        ModelState.Clear();
                        _viewModel.ShowList = true;
                        _viewModel.SetMenuList();
                        return View(_viewModel);
                    }
                    
                }
                if (string.IsNullOrEmpty(menuViewModel.ActionName))
                {
                    SetMessage("Action Name is required");
                }
                if (string.IsNullOrEmpty(menuViewModel.ControllerName))
                {
                    SetMessage("Controller Name is required");
                }
                if (string.IsNullOrEmpty(menuViewModel.DisplayName))
                {
                    SetMessage("Display Name is required");
                }
                if (menuViewModel.MenuGroupId <= 0)
                {
                    SetMessage("Menu Group is required");
                }
                 menuViewModel.MenuGroupSL= _viewModel.MenuGroupSL;
                return View(menuViewModel);
            }
            catch (Exception ex) { throw ex; }
        }

        public IActionResult ViewAllMenu()
        {
            try
            {
                _viewModel.SetMenuList();
                return View(_viewModel);
            }
            catch(Exception ex) { throw ex; }
        }

        public IActionResult ViewMenu()
        {

            _viewModel.SetMenuList();
            _viewModel.ShowList = true;
            TempData["ViewModel"] = _viewModel;
            TempData.Keep();
            return RedirectToAction("AddMenu");
        }

        public IActionResult EditMenu(int Id)
        {
            var menu = _context.Menu.Where(m => m.Id == Id).FirstOrDefault();
            if (menu != null)
            {
                _viewModel.MenuId = menu.Id;
                _viewModel.MenuName = menu.DisplayName;
                _viewModel.ControllerName = menu.Controller;
                _viewModel.ActionName = menu.ActionName;
                _viewModel.DisplayName = menu.DisplayName;
                _viewModel.MenuGroupId = menu.Menu_GroupId;
            }
            return View(_viewModel);
        }

        [HttpPost]
        public IActionResult EditMenu(MenuViewModel viewModel)
        {
            try
            {
                if (!string.IsNullOrEmpty(viewModel.DisplayName) 
                    && !string.IsNullOrEmpty(viewModel.ControllerName)
                    && !string.IsNullOrEmpty(viewModel.ActionName)
                    && viewModel.MenuGroupId > 0)
                {
                    var menu = _context.Menu.Where(m => m.Id == viewModel.MenuId).FirstOrDefault();

                    if(menu != null)
                    {
                        menu.Controller = viewModel.ControllerName;
                        menu.DisplayName = viewModel.DisplayName;
                        menu.ActionName = viewModel.ActionName;
                        menu.Menu_GroupId = viewModel.MenuGroupId;

                        _context.Update(menu);
                        _context.SaveChanges();

                        SetMessage("Update was successful");
                        return RedirectToAction("ViewAllMenu");
                    }
                }

                return View(viewModel);
            }
            catch(Exception ex) { throw ex; }
        }

        public IActionResult AssignMenuToRole()
        {
            try
            {
                return View(_viewModel);
            }
            catch(Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult AssignMenuToRole(MenuViewModel viewModel)
        {
            try
            {
                if (viewModel.MenuId > 0 && viewModel.RoleId > 0)
                {
                    var menuInRoleExists = _context.MenuInRole
                                                   .Where(m => m.MenuId == viewModel.MenuId && m.RoleId == viewModel.RoleId)
                                                   .FirstOrDefault();

                    if (menuInRoleExists == null)
                    {
                        var menuInRole = new Menu_In_Role()
                        {
                            RoleId = viewModel.RoleId,
                            MenuId = viewModel.MenuId
                        };

                        _context.Add(menuInRole);
                        _context.SaveChanges();

                        SetMessage("Menu in role added successfuly");

                        return RedirectToAction("AssignMenuToRole");
                    }
                    else
                    {
                        SetMessage("Selected Menu In Role already exists");
                    }
                }

                return View(viewModel);
            }
            catch (Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult DeactivateMenu(int id)
        {
            try
            {
                var menu = _context.Menu.Where(m => m.Id == id).FirstOrDefault();

                if (menu != null)
                {
                    bool active = menu.Active;

                    menu.Active = !menu.Active;
                    _context.Update(menu);
                    _context.SaveChanges();

                    string msg = string.Format("'{0}' was {1} successfully", string.Format("{0}/{1}", menu.Controller, menu.ActionName), (active == true ? "Deactivated" : "Activated"));
                    return Json(msg);
                }

                return RedirectToAction("ViewAllMenu");
            }
            catch (Exception ex) { throw ex; }
        }
        public IActionResult ManageMenuInRole()
        {
            _viewModel.MenuInRoles = _context.MenuInRole.ToList();
            return View(_viewModel);
        }
        public IActionResult RemoveMenuInRole(int id)
        {
            if (id > 0)
            {
                var menuInRole=_context.MenuInRole.Where(f => f.Id == id).FirstOrDefault();
                _context.Remove(menuInRole);
                _context.SaveChanges();
                return RedirectToAction("ManageMenuInRole");
            }
            else
            {
                SetMessage("Operation not Successful");
                return RedirectToAction("ManageMenuInRole");
            }
        }
    }
}