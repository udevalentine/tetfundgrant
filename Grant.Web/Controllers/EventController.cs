﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grant.Web.Infrastructure;
using Grant.Web.Models;
using Grant.Web.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Grant.Web.Controllers
{
    [Authorize(Policy = "MustBeAdmin")]
    public class EventController : BaseController
    {
        private readonly DataBaseContext _context;
        private Utility _utility;
        private EventViewModel _viewModel;

        public EventController(Utility utility, DataBaseContext context)
        {
            _context = context;
            _utility = utility;
            _viewModel = new EventViewModel();
            _viewModel.PopulateDropdown(_context);
        }

        public IActionResult Index()
        {
            _viewModel.ResearchCycleEventList = _viewModel.SetResearchCycleEventList();
            return View(_viewModel);
        }

        public IActionResult AddResearchCycleEvent()
        {
            try
            {
                _viewModel.SessionList = _utility.GetSessionSL();
                _viewModel.EventTypeList = _utility.GetEventTypeSL();

                return View(_viewModel);
            }
            catch (Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult AddResearchCycleEvent(EventViewModel viewModel)
        {
            try
            {
                if (viewModel.SessionId > 0 && viewModel.EventTypeId > 0)
                {
                    if (viewModel.EventStartDate != null && viewModel.EventEndDate != null)
                    {
                        DateTime currentDate = DateTime.UtcNow.AddHours(-8);
                        //convert to utc time before storing
                        viewModel.EventStartDate = viewModel.EventStartDate.ToUniversalTime().AddHours(-8);
                        viewModel.EventEndDate = viewModel.EventEndDate.ToUniversalTime().AddHours(-8);
                        if (viewModel.EventStartDate >= currentDate)
                        {
                            if (viewModel.EventEndDate > currentDate && viewModel.EventEndDate > viewModel.EventStartDate)
                            {
                               var eventClash = _context.ResearchCycleEvent.Where(r => ((r.StartDate <= viewModel.EventEndDate && r.StartDate >= viewModel.EventStartDate && r.Active) || (r.EndDate <= viewModel.EventEndDate && r.EndDate >= viewModel.EventStartDate && r.Active) || (r.StartDate >= viewModel.EventStartDate && r.StartDate <= viewModel.EventEndDate && r.Active)) && r.SessionId==viewModel.SessionId).ToList();
                                if (eventClash?.Count > 0)
                                {
                                    SetMessage("More than one event cannot run at the same time.");

                                    return RedirectToAction("AddResearchCycleEvent");
                                }
                                //Make Sure that a similar record does not exist
                                var eventRecord = _context.ResearchCycleEvent
                                                          .Where(r => r.EventTypeId == viewModel.EventTypeId && r.SessionId == viewModel.SessionId)
                                                          .FirstOrDefault();
                                if (eventRecord == null)
                                {
                                    //Create the new Event
                                    var researchCycleEvent = new Models.ResearchCycleEvent()
                                    {
                                        //Session = new Session() { Id = viewModel.SessionId },
                                        //EventType = new EventType() { Id = viewModel.EventTypeId },
                                        SessionId = viewModel.SessionId,
                                        EventTypeId = viewModel.EventTypeId,
                                        StartDate = viewModel.EventStartDate,
                                        EndDate = viewModel.EventEndDate
                                    };

                                    _context.Add(researchCycleEvent);
                                    _context.SaveChanges();

                                    SetMessage("Research cycle event was added sucessfully");

                                    return RedirectToAction("AddResearchCycleEvent");
                                }
                                else
                                {
                                    SetMessage("Similar research cycle event already exists");
                                }
                            }
                            else
                            {
                                SetMessage("Event end date must be set to a date in the future");
                            }
                        }
                        else
                        {
                            SetMessage("Event start date must be set to a date in the future");
                        }
                    }
                    else
                    {
                        SetMessage("Event 'start' & 'end' dates are required");
                    }
                }
                else
                {
                    SetMessage("Please select Research cycle & Event Type");
                }

                viewModel.SessionList = _utility.GetSessionSL();
                viewModel.EventTypeList = _utility.GetEventTypeSL();

                return View(viewModel);
            }
            catch(Exception ex) { throw ex; }
        }

        public IActionResult EditResearchCycleEvent(int Id)
        {
            try
            {
                if (Id < 1)
                {
                    return RedirectToAction("Index");
                }

                var existingEvent = _context.ResearchCycleEvent.Where(r => r.Id == Id).FirstOrDefault();
                TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("W. Central Africa Standard Time");
                if (existingEvent != null)
                {
                    
                    _viewModel.ReserchCycleEventId = existingEvent.Id;
                    _viewModel.SessionId = existingEvent.Session.Id;
                    _viewModel.EventTypeId = existingEvent.EventType.Id;
                    _viewModel.EventStartDate = TimeZoneInfo.ConvertTimeFromUtc(existingEvent.StartDate, cstZone);
                    _viewModel.EventEndDate = TimeZoneInfo.ConvertTimeFromUtc(existingEvent.EndDate, cstZone);

                    _viewModel.SessionList = _utility.GetSessionSL();
                    _viewModel.EventTypeList = _utility.GetEventTypeSL();

                }
                else
                {
                    return RedirectToAction("Index");
                }

                return View(_viewModel);
            }
            catch(Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult EditResearchCycleEvent(EventViewModel viewModel)
        {
            try
            {
                if (viewModel.SessionId > 0 && viewModel.EventTypeId > 0)
                {
                    if (viewModel.EventEndDate != null && viewModel.EndTime != null && viewModel.EventStartDate != null && viewModel.StartTime != null)
                    {
                        DateTime currentDate = DateTime.UtcNow.AddHours(-8);
                        viewModel.EventStartDate = viewModel.EventStartDate.Add(viewModel.StartTime);
                        
                        viewModel.EventEndDate = viewModel.EventEndDate.Add(viewModel.EndTime);
                        //convert to utc
                        viewModel.EventStartDate = viewModel.EventStartDate.ToUniversalTime().AddHours(-8);
                        viewModel.EventEndDate = viewModel.EventEndDate.ToUniversalTime().AddHours(-8);
                        if (viewModel.EventStartDate >= currentDate)
                        {
                            if (viewModel.EventEndDate > currentDate && viewModel.EventEndDate > viewModel.EventStartDate)
                            {
                                var eventClash = _context.ResearchCycleEvent.Where(r => ((r.StartDate <= viewModel.EventEndDate && r.StartDate >= viewModel.EventStartDate && r.Active) || (r.EndDate <= viewModel.EventEndDate && r.EndDate >= viewModel.EventStartDate && r.Active) || (r.StartDate >= viewModel.EventStartDate && r.StartDate <= viewModel.EventEndDate && r.Active)) && r.SessionId == viewModel.SessionId && r.Id != viewModel.ReserchCycleEventId).ToList();
                                if (eventClash?.Count > 0)
                                {
                                    SetMessage("More than one event cannot run the same time.");

                                    return RedirectToAction("AddResearchCycleEvent");
                                }
                                //Make Sure that a similar record does not exist
                                var eventRecord = _context.ResearchCycleEvent
                                                          .Where(r => r.Id == viewModel.ReserchCycleEventId)
                                                          .FirstOrDefault();
                                if (eventRecord != null)
                                {
                                    //Update existing Event
                                    eventRecord.SessionId = viewModel.SessionId;
                                    eventRecord.EventTypeId = viewModel.EventTypeId;
                                    eventRecord.StartDate = viewModel.EventStartDate;
                                    eventRecord.EndDate = viewModel.EventEndDate;

                                    _context.Update(eventRecord);
                                    _context.SaveChanges();

                                    SetMessage("Research Cycle Event was updated successfully");

                                    return RedirectToAction("Index");
                                }
                            }
                            else
                            {
                                SetMessage("Event end date must be a date in the future");
                            }
                        }
                        else
                        {
                            SetMessage("Event start date must be set a date in the future");
                        }
                    }
                    else
                    {
                        SetMessage("Event 'start' & 'end' dates are required");
                    }
                }
                else
                {
                    SetMessage("Please select Research cycle & Event Type");
                }

                viewModel.SessionList = _utility.GetSessionSL();
                viewModel.EventTypeList = _utility.GetEventTypeSL();

                return View(viewModel);
            }
            catch(Exception ex) { throw ex; }
        }

        public IActionResult ViewAllEventTypes()
        {
            try
            {
                _viewModel.EventTypes = _context.EventType.ToList();
                return View(_viewModel);
            }
            catch (Exception ex) { throw ex; }
        }


        public IActionResult AddEventType()
        {
            try
            {
                return View();
            }
            catch (Exception ex) { throw ex; }
        }


        [HttpPost]
        public IActionResult AddEventType(EventViewModel viewModel)
        {
            try
            {
                if (!string.IsNullOrEmpty(viewModel.EventTypeName))
                {
                    var eventTypeExists = _context.EventType.Where(c => c.Name == viewModel.EventTypeName).FirstOrDefault();
                    if (eventTypeExists == null)
                    {
                        var eventType = new EventType()
                        {
                            Name = viewModel.EventTypeName,
                            Active = true
                        };

                        _context.Add(eventType);
                        _context.SaveChanges();

                        SetMessage("Event type created sucessfully");
                    }
                    else
                    {
                        SetMessage("Event type already exists");
                    }
                }
                else
                {
                    SetMessage("Please fill in all required fields");
                }

                return RedirectToAction("AddEventType");
            }
            catch (Exception ex) { throw ex; }
        }

        public IActionResult EditEventType(int Id)
        {
            try
            {
                if (Id < 1)
                {
                    return RedirectToAction("ViewAllEventTypes");
                }

                var eventType = _context.EventType.Where(c => c.Id == Id).FirstOrDefault();
                if (eventType != null)
                {
                    _viewModel.EventTypeName = eventType.Name;
                }
                _viewModel.EventTypeId = Id;

                return View(_viewModel);
            }
            catch (Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult EditEventType(EventViewModel viewModel)
        {
            try
            {
                if (viewModel.EventTypeId < 1)
                {
                    return RedirectToAction("ViewAllEventTypes");
                }

                if (!string.IsNullOrEmpty(viewModel.EventTypeName))
                {
                    var category = _context.EventType.Where(c => c.Id == viewModel.EventTypeId).FirstOrDefault();
                    if (category != null)
                    {
                        category.Name = viewModel.EventTypeName;

                        _context.Update(category);
                        _context.SaveChanges();

                        SetMessage("Event type updated sucessfully");
                    }
                }
                else
                {
                    SetMessage("Please fill in all required fields");
                }

                return View(viewModel);
            }
            catch (Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult DeactivateEventType(int id)
        {
            try
            {
                var eventType = _context.EventType.Where(m => m.Id == id).FirstOrDefault();

                if (eventType != null)
                {
                    bool active = eventType.Active;

                    eventType.Active = !eventType.Active;
                    _context.Update(eventType);
                    _context.SaveChanges();

                    string msg = string.Format("'{0}' was {1} successfully", eventType.Name, (active == true ? "Deactivated" : "Activated"));
                    return Json(msg);
                }

                return RedirectToAction("ViewAllEventTypes");
            }
            catch (Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult DeactivateResearchCycleEvent(int id, int sessionId)
        {
            try
            {
                var researchCycleEvent = _context.ResearchCycleEvent.Where(m => m.Id == id).FirstOrDefault();

                if (researchCycleEvent != null)
                {
                    bool active = researchCycleEvent.Active;

                    researchCycleEvent.Active = !researchCycleEvent.Active;
                    if (researchCycleEvent.Active)
                    {
                        //Deactivate all Events for the selected session
                        var allEventsForThisSession = _context.ResearchCycleEvent.Where(r => r.Session.Id == sessionId && r.Id != id).ToList();
                        if (allEventsForThisSession?.Count() > 0)
                        {
                            for (int i = 0; i < allEventsForThisSession.Count(); i++)
                            {
                                allEventsForThisSession[i].Active = false;

                                _context.Update(allEventsForThisSession[i]);

                                _context.SaveChanges();
                            }
                        }
                    }
                    _context.Update(researchCycleEvent);
                    _context.SaveChanges();

                    string msg = string.Format("'{0}' was {1} successfully", researchCycleEvent.EventType.Name, (active == true ? "Deactivated" : "Activated"));
                    return Json(msg);
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex) { throw ex; }
        }
    }
}