﻿using Grant.Web.Infrastructure;
using Grant.Web.Models;
using Grant.Web.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Transactions;

namespace Grant.Web.Controllers
{
    [Authorize(Policy = "MustBeAdmin")]
    public class AdminHomeController : BaseController
    {
        private readonly DataBaseContext _context;
        AdminViewModel _adminViewModel;
        Utility _utility;
        private const string Value = "Id";
        private const string Text = "Name";
        private const string Id = "Reviewer.Name";
        private const string Name = "ReviewerId";
        private const string ReviewerName = "Name";
        private const string ReviewerId = "ReviewerId";
        public const string Select = "-- Select --";
        private IHostingEnvironment _hostingEnvironment;
        public AdminHomeController(DataBaseContext context, IHostingEnvironment hostingEnvironment, Utility utility)
        {
            _context = context;
            _adminViewModel = new AdminViewModel();
            _adminViewModel.PopulateAllDropdown(_context);
            _hostingEnvironment = hostingEnvironment;
            _utility = utility;


        }
        public IActionResult Index()
        {
            if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
            {
                SetMessage(ValidateUserLogin(_context));
                return RedirectToAction("Logout", "Home");
            }

            return View();
        }

        public IActionResult ViewApplication()
        {
            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }
                //string adminEmail = User.FindFirstValue(ClaimTypes.Email);
                //var admin = _context.User.Where(d => d.Email == adminEmail).FirstOrDefault();
                //if (admin?.Id > 0)
                //{
                //    if (admin.Role.Name == "Admin")
                //    {
                //        _adminViewModel.PopulateReviewerExpertriateCategory(0);
                //    }
                //    else
                //    {
                //        var coAdminCategory=_context.CategoryAdministrator.Where(F => F.UserId == admin.Id && F.Active).FirstOrDefault();
                //        if (coAdminCategory?.Id > 0)
                //        {
                //            _adminViewModel.PopulateReviewerExpertriateCategory(coAdminCategory.CategoryId);
                //        }
                //    }
                //}
                SetDropdown();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        [HttpPost]
        public IActionResult ViewApplication(int CategoryId, int ThemeId, int SessionId)
        {
            try
            {
                if (SessionId <= 0)
                {
                    SetMessage("Please you must select atleast research cycle.");
                    return View(_adminViewModel);
                }
                string adminEmail = User.FindFirstValue(ClaimTypes.Email);
                var admin = _context.User.Where(d => d.Email == adminEmail).FirstOrDefault();
                List<ApplicationList> list = new List<ApplicationList>();
                var applications = new List<ApplicantThematic>();
                if (SessionId > 0 && CategoryId == 0 && ThemeId == 0)
                {


                    if (admin.RoleId != (int)RoleType.Admin && admin.RoleId != (int)RoleType.SuperAdmin)
                    {
                        var categoryAdmin = _context.CategoryAdministrator.Where(f => f.User.Id == admin.Id && f.Active).FirstOrDefault();
                        if (categoryAdmin == null)
                        {
                            SetMessage("Category has not been assigned to you");
                            return RedirectToAction("ViewApplication");
                        }
                        applications = _context.ApplicantThematic.Where(f => f.SessionId == SessionId && f.Theme.CategoryId == categoryAdmin.CategoryId).ToList();
                    }
                    else
                    {
                        applications = _context.ApplicantThematic.Where(f => f.SessionId == SessionId).ToList();
                    }

                }
                else if (SessionId > 0 && CategoryId > 0 && ThemeId == 0)
                {
                    applications = _context.ApplicantThematic.Where(f => f.Theme.CategoryId == CategoryId && f.SessionId == SessionId).ToList();
                }
                else if (SessionId > 0 && CategoryId > 0 && ThemeId > 0)
                {
                    applications = _context.ApplicantThematic.Where(f => f.ThemeId == ThemeId && f.SessionId == SessionId).ToList();
                }

                //_adminViewModel.PopulateReviewerExpertriateCategory(CategoryId);
                SetDropdown();

                _adminViewModel.ThemeId = ThemeId;
                _adminViewModel.Theme = _context.Theme.Where(f => f.Id == ThemeId).FirstOrDefault();
                if (applications.Count > 0)
                {

                    for (int i = 0; i < applications.Count; i++)
                    {
                        List<int> reviewerIdList = new List<int>();
                        var thematicapplicant = applications[i];
                        var conceptNote = _context.ConceptNote.Where(g => g.ApplicantThemeId == thematicapplicant.Id).FirstOrDefault();
                        if (conceptNote != null && conceptNote.Submitted)
                        {
                            ApplicationList applicationList = new ApplicationList();
                            var applicantId = applications[i].ApplicantId;

                            var applicant = _context.Applicant.Where(f => f.Id == applicantId).FirstOrDefault();
                            var Person = _context.Person.Where(j => j.Id == applicant.PersonId).FirstOrDefault();
                            applicationList.ApplicantThematic = applications[i];
                            applicationList.Person = Person;
                            var applicantReviewerList = _context.ApplicantReviewer.Where(f => f.ApplicantThematicId == thematicapplicant.Id && f.TypeId == (int)Types.ConceptNote).ToList();
                            if (applicantReviewerList.Count > 0)
                            {
                                List<Reviewer> reviewerList = new List<Reviewer>();
                                for (int j = 0; j < applicantReviewerList.Count; j++)
                                {
                                    var reviewerId = applicantReviewerList[j].ReviewerId;
                                    var reviewer = _context.Reviewer.Where(f => f.Id == reviewerId).FirstOrDefault();
                                    reviewerList.Add(reviewer);
                                }
                                applicationList.Reviewers = reviewerList;
                            }
                            else
                            {
                                List<Reviewer> reviewerList = new List<Reviewer>();
                                applicationList.Reviewers = reviewerList;
                            }
                            applicationList.Category = applications[i].Theme.Category;
                            applicationList.Theme = applications[i].Theme;
                            //applicationList.Category = _context.Category.Where(f => f.Id == CategoryId).FirstOrDefault();
                            //applicationList.Theme = _context.Theme.Where(f => f.Id == ThemeId).FirstOrDefault();
                            //For conceptNote accessors ar selected by category and not be thematicArea
                            var reviewerStrengthList = _context.ReviewerThematicStrenght.Where(d => d.Theme.CategoryId == applicationList.Category.Id
                            && d.Reviewer.InstitutionId != thematicapplicant.Applicant.Person.InstitutionId).ToList();

                            if (reviewerStrengthList?.Count > 0)
                            {
                                List<AssessorDropdown> assessorDropdownList = new List<AssessorDropdown>();
                                foreach (var item in reviewerStrengthList)
                                {
                                    //ensure a reviewer is added to dropdown list once irrespective his numerous strenght in many thematic area of thesame category
                                    if (!reviewerIdList.Contains(item.ReviewerId))
                                    {
                                        reviewerIdList.Add(item.ReviewerId);
                                        var name = "";
                                        var perAssessor = _context.ApplicantReviewer.Where(f => f.ApplicantThematic.Theme.CategoryId == applicationList.Category.Id
                                        && f.TypeId == (int)Types.ConceptNote && f.ReviewerId == item.ReviewerId && f.ApplicantThematic.SessionId == SessionId).ToList();
                                        name = item.Reviewer.Name + "-" + "(" + perAssessor.Count + ")";
                                        AssessorDropdown assessorDropdown = new AssessorDropdown();
                                        assessorDropdown.ReviewerId = item.ReviewerId;
                                        assessorDropdown.Name = name;
                                        assessorDropdownList.Add(assessorDropdown);
                                    }

                                }
                                applicationList.ReviewerHighStrengthNameSL = new SelectList(assessorDropdownList, ReviewerId, ReviewerName);
                            }
                            else
                            {
                                List<AssessorDropdown> assessorDropdownList = new List<AssessorDropdown>();
                                applicationList.ReviewerHighStrengthNameSL = new SelectList(assessorDropdownList, ReviewerId, ReviewerName);
                            }


                            list.Add(applicationList);
                        }

                    }
                }
                _adminViewModel.ApplicationList = list;
                _adminViewModel.ShowPanel = true;
                //var reviewerStrength = _context.ReviewerThematicStrenght.Where(d => d.ThemeId == ThemeId).ToList();

                //_adminViewModel.ReviewerHighStrengthNameSL = new SelectList(reviewerStrength, Name, Id);


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }


        public IActionResult AssignAccessor(int id, int reviewerId)
        {

            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }
                if (!_utility.CheckResearchCycleEvent((int)EventTypes.AssignConceptNoteAssessor))
                {
                    //SetMessage("Assessor(s) for Concept Note cannot be assigned at this time.");
                    return Json(new { success = false, responseText = "Assessor(s) for Concept Note cannot be assigned at this time." });
                    //return RedirectToAction("ViewApplication", "AdminHome");
                }

                if (id > 0 && reviewerId > 0)
                {
                    var existingReviewer = _context.ApplicantReviewer.Where(o => o.ReviewerId == reviewerId && o.ApplicantThematicId == id && o.TypeId == 1).FirstOrDefault();
                    if (existingReviewer == null)
                    {
                        ApplicantReviewer applicantReviewer = new ApplicantReviewer()
                        {
                            ApplicantThematicId = id,
                            ReviewerId = reviewerId,
                            DateAssigned = DateTime.Now,
                            TypeId = 1
                        };
                        _context.Add(applicantReviewer);
                        _context.SaveChanges();

                        //get a reviewer
                        var reviewer = _context.Reviewer.Where(r => r.Id == reviewerId).FirstOrDefault();
                        if (reviewer != null)
                        {
                            _utility.SendEMail(reviewer.Email, reviewer.Name, (int)MessageType.ReviewProposalAssignment, "", "", "", "", "");
                        }

                        //  Send "Success"
                        return Json(new { success = true, responseText = "Assessor assigned successfully!" });


                    }
                    else
                    {
                        return Json(new { success = false, responseText = "This Assessor has already been assigned to this PI!" });
                    }


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View();
        }
        public IActionResult AssignProposalAccessor(int id, int reviewerId)
        {
            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }
                if (!_utility.CheckResearchCycleEvent((int)EventTypes.AssignProposalAssessor))
                {
                    //SetMessage("Assessor(s) for Concept Note cannot be assigned at this time.");
                    //return RedirectToAction("ViewApplication", "AdminHome");
                    return Json(new { success = false, responseText = "Assessor(s) for Full Proposal cannot be assigned at this time." });
                }
                if (id > 0 && reviewerId > 0)
                {
                    var existingReviewer = _context.ApplicantReviewer.Where(o => o.ReviewerId == reviewerId && o.ApplicantThematicId == id && o.TypeId == (int)Types.Proposal).FirstOrDefault();
                    if (existingReviewer == null)
                    {
                        ApplicantReviewer applicantReviewer = new ApplicantReviewer()
                        {
                            ApplicantThematicId = id,
                            ReviewerId = reviewerId,
                            DateAssigned = DateTime.Now,
                            TypeId = 2
                        };
                        _context.Add(applicantReviewer);
                        _context.SaveChanges();

                        //get a reviewer
                        var reviewer = _context.Reviewer.Where(r => r.Id == reviewerId).FirstOrDefault();
                        if (reviewer != null)
                        {
                            _utility.SendEMail(reviewer.Email, reviewer.Name, (int)MessageType.ReviewerAssignment, "", "", "", "", "");
                        }

                        //  Send "Success"
                        return Json(new { success = true, responseText = "Assessor assigned successfully!" });
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "This Assessor has already been assigned to this PI!" });
                    }


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View();
        }
        public IActionResult ViewReviewedConceptNote()
        {
            if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
            {
                SetMessage(ValidateUserLogin(_context));
                return RedirectToAction("Logout", "Home");
            }
            return View(_adminViewModel);
        }
        [HttpPost]
        public IActionResult ViewReviewedConceptNote(AdminViewModel adminViewModel)
        {
            try
            {
                if (adminViewModel.SessionId > 0)
                {
                    _adminViewModel.ShowPanel = true;
                    List<ApplicationList> list = new List<ApplicationList>();
                    var allReviewedConceptNote = _context.ApplicantReviewer.Where(r => r.ConceptNoteMark != null && r.ApplicantThematic.SessionId == adminViewModel.SessionId).GroupBy(t => t.ApplicantThematicId).ToList();

                    if (allReviewedConceptNote.Count > 0)
                    {
                        for (int i = 0; i < allReviewedConceptNote.Count; i++)
                        {
                            ApplicationList applicationList = new ApplicationList();
                            var applicantthematicId = allReviewedConceptNote[i].Key;
                            var applicantThematic = _context.ApplicantThematic.Where(g => g.Id == applicantthematicId).FirstOrDefault();
                            var applicant = _context.Applicant.Where(o => o.Id == applicantThematic.ApplicantId).FirstOrDefault();
                            var person = _context.Person.Where(g => g.Id == applicant.PersonId).FirstOrDefault();
                            var applicationReviewer = _context.ApplicantReviewer.Where(g => g.ApplicantThematicId == applicantthematicId && g.TypeId == (int)Types.ConceptNote).ToList();
                            if (applicationReviewer.Count > 0)
                            {
                                decimal mark = 0m;
                                int count = 0;



                                List<ReviewerScore> scoreList = new List<ReviewerScore>();

                                //list of Scores by Assessors 
                                var orderedScore = applicationReviewer.Where(g => g.ConceptNoteMark != null).Select(f => f.ConceptNoteMark);
                                var maxMark = orderedScore.LastOrDefault().Value;
                                var minMark = orderedScore.FirstOrDefault().Value;
                                //identify discripancies in score
                                if (maxMark - minMark <= 10 || applicationReviewer.FirstOrDefault().IsReconciled)
                                {
                                    for (int u = 0; u < applicationReviewer.Count; u++)
                                    {
                                        ReviewerScore reviewerScore = new ReviewerScore();
                                        count += 1;
                                        //int? scoreId = applicationReviewer[u].ScoreId;
                                        int reviewerId = applicationReviewer[u].ReviewerId;
                                        reviewerScore.Reviewer = _context.Reviewer.Where(f => f.Id == reviewerId).FirstOrDefault();
                                        reviewerScore.Mark = applicationReviewer[u].ConceptNoteMark != null ? applicationReviewer[u].ConceptNoteMark.ToString() : "NoScore";
                                        mark += applicationReviewer[u].ConceptNoteMark != null ? (decimal)applicationReviewer[u].ConceptNoteMark : 0;
                                        //reviewerScore.Reviewer = _context.Reviewer.Where(f => f.Id == reviewerId).FirstOrDefault();




                                        //reviewerScore.Score = scoreId == null ? "Not Reviewed Yet" : _context.Score.Where(f => f.Id == scoreId).Select(g => g.Name).FirstOrDefault();
                                        scoreList.Add(reviewerScore);
                                    }
                                    //applicationList.Reviewers = reviewerList;
                                    applicationList.Approval = _context.Approval.Where(g => g.ApplicantThematicId == applicantthematicId && g.IsProposal == false).FirstOrDefault();
                                    applicationList.Theme = _context.Theme.Where(f => f.Id == applicantThematic.ThemeId).FirstOrDefault();
                                    applicationList.Category = _context.Category.Where(f => f.Id == applicationList.Theme.CategoryId).FirstOrDefault();
                                    applicationList.ReviewerScores = scoreList;
                                    applicationList.Person = person;
                                    applicationList.Average = (mark / count);
                                    applicationList.ApplicantThematic = applicantThematic;
                                    list.Add(applicationList);
                                }

                            }
                            else
                            {
                                List<Reviewer> reviewerList = new List<Reviewer>();
                                applicationList.Reviewers = reviewerList;
                            }
                            _adminViewModel.ApplicationList = list;

                        }
                    }
                    else
                    {
                        _adminViewModel.ApplicationList = new List<ApplicationList>();
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult ApproveConceptNote(string id, string score)
        {
            try
            {
                if (!_utility.CheckResearchCycleEvent((int)EventTypes.AcceptConceptnote))
                {
                    SetMessage("Concept Note cannot be approved at this time.");
                    return RedirectToAction("ViewReviewedConceptNote", "AdminHome");
                }

                string adminEmail = User.FindFirstValue(ClaimTypes.Email);
                var admin = _context.User.Where(d => d.Email == adminEmail).FirstOrDefault();
                if (!String.IsNullOrEmpty(id) && !String.IsNullOrEmpty(score))
                {
                    int applicantthematicId = Convert.ToInt32(Utility.Decrypt(id));
                    decimal averageScore = Convert.ToDecimal(Utility.Decrypt(score));
                    var applicantThematic = _context.ApplicantThematic.Where(g => g.Id == applicantthematicId).FirstOrDefault();
                    var existApproved = _context.Approval.Where(f => f.ApplicantThematicId == applicantthematicId && f.IsProposal == false).FirstOrDefault();
                    if (existApproved == null)
                    {
                        Approval approval = new Approval()
                        {
                            IsProposal = false,
                            ApplicantThematicId = applicantthematicId,
                            Approved = true,
                            AverageScore = averageScore,
                            ReviewerId = admin.Id

                        };
                        _context.Add(approval);
                        _context.SaveChanges();

                    }
                    else
                    {
                        existApproved.Approved = true;
                        existApproved.AverageScore = averageScore;
                        existApproved.ReviewerId = admin.Id;
                        _context.Update(existApproved);
                        _context.SaveChanges();
                    }
                    if (CheckForInternetConnection())
                    {
                        _utility.SendEMail(applicantThematic.Applicant.Person.Email, applicantThematic.Applicant.Person.Name, (int)MessageType.ConceptNoteApproval, "", "", "", "", applicantThematic.Title);
                    }

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("ViewReviewedConceptNote");
        }
        public IActionResult ApproveProposal(int applicantthematicId, decimal averageScore)
        {
            try
            {
                if (!_utility.CheckResearchCycleEvent((int)EventTypes.AcceptProposal))
                {
                    SetMessage("Full Proposal cannot be approved at this time.");
                    return RedirectToAction("ViewReviewedProposal", "AdminHome");
                }
                string adminEmail = User.FindFirstValue(ClaimTypes.Email);
                var admin = _context.User.Where(d => d.Email == adminEmail).FirstOrDefault();
                if (applicantthematicId > 0)
                {
                    var existApproved = _context.Approval.Where(f => f.ApplicantThematicId == applicantthematicId && f.IsProposal == true).FirstOrDefault();
                    if (existApproved == null)
                    {
                        Approval approval = new Approval()
                        {
                            IsProposal = true,
                            ApplicantThematicId = applicantthematicId,
                            Approved = true,
                            AverageScore = averageScore,
                            ReviewerId = admin.Id

                        };
                        _context.Add(approval);
                        _context.SaveChanges();
                    }
                    else
                    {
                        existApproved.Approved = true;
                        existApproved.AverageScore = averageScore;
                        existApproved.ReviewerId = admin.Id;
                        existApproved.IsProposal = true;
                        _context.Update(existApproved);
                        _context.SaveChanges();
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("ViewReviewedConceptNote");
        }
        public IActionResult GetAllApprovedConceptNote()
        {
            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }
                List<ApplicationList> list = new List<ApplicationList>();
                var allReviewedConceptNote = _context.Approval.Where(d => d.IsProposal == false && d.Approved).ToList();
                List<ReviewerScore> scoreList = new List<ReviewerScore>();
                if (allReviewedConceptNote.Count > 0)
                {
                    for (int i = 0; i < allReviewedConceptNote.Count; i++)
                    {
                        ApplicationList applicationList = new ApplicationList();
                        var applicantthematicId = allReviewedConceptNote[i].ApplicantThematicId;
                        var applicantThematic = _context.ApplicantThematic.Where(g => g.Id == applicantthematicId).FirstOrDefault();
                        var applicant = _context.Applicant.Where(o => o.Id == applicantThematic.ApplicantId).FirstOrDefault();
                        var person = _context.Person.Where(g => g.Id == applicant.PersonId).FirstOrDefault();
                        var applicationReviewer = _context.ApplicantReviewer.Where(g => g.ApplicantThematicId == applicantThematic.Id).ToList();
                        if (applicationReviewer.Count > 0)
                        {
                            decimal mark = 0m;
                            int count = 0;
                            for (int u = 0; u < applicationReviewer.Count; u++)
                            {
                                count += 1;
                                int? scoreId = applicationReviewer[u].ScoreId;
                                int reviewerId = applicationReviewer[u].ReviewerId;
                                ReviewerScore reviewerScore = new ReviewerScore();
                                mark += applicationReviewer[u].ConceptNoteMark != null ? (decimal)applicationReviewer[u].ConceptNoteMark : 0;
                                reviewerScore.Reviewer = _context.Reviewer.Where(f => f.Id == reviewerId).FirstOrDefault();

                                reviewerScore.Mark = applicationReviewer[u].ConceptNoteMark != null ? applicationReviewer[u].ConceptNoteMark.ToString() : "NoScore";

                                reviewerScore.Score = scoreId == null ? "Not Reviewed Yet" : _context.Score.Where(f => f.Id == scoreId).Select(g => g.Name).FirstOrDefault();
                                scoreList.Add(reviewerScore);
                            }
                            applicationList.Approval = _context.Approval.Where(g => g.ApplicantThematicId == applicantthematicId && g.IsProposal == false).FirstOrDefault();
                            applicationList.Theme = _context.Theme.Where(f => f.Id == applicantThematic.ThemeId).FirstOrDefault();
                            applicationList.Category = _context.Category.Where(f => f.Id == applicationList.Theme.CategoryId).FirstOrDefault();
                            applicationList.ReviewerScores = scoreList;
                            applicationList.Person = person;
                            applicationList.Average = (mark / count);
                            applicationList.ApplicantThematic = applicantThematic;
                            list.Add(applicationList);
                        }
                        _adminViewModel.ApplicationList = list;

                    }
                }
                else
                {
                    _adminViewModel.ApplicationList = new List<ApplicationList>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult ApplicantProfile(int id)
        {
            try
            {
                List<ReviewerAssignedScore> reviewerList = new List<ReviewerAssignedScore>();
                List<Person> personTeamList = new List<Person>();
                var person = _context.Person.Where(g => g.Id == id).FirstOrDefault();
                if (person != null)
                {
                    var applicant = _context.Applicant.Where(f => f.PersonId == person.Id).FirstOrDefault();
                    if (applicant != null)
                    {
                        _adminViewModel.ApplicantThematic = _context.ApplicantThematic.Where(d => d.ApplicantId == applicant.Id).FirstOrDefault();

                        _adminViewModel.Person = person;
                        if (_adminViewModel.ApplicantThematic?.Id > 0)
                        {
                            var teamList = _context.Team.Where(f => f.ApplicantThematicId == _adminViewModel.ApplicantThematic.Id).ToList();
                            if (teamList.Count > 0)
                            {
                                for (int j = 0; j < teamList.Count; j++)
                                {
                                    var teamPersonId = teamList[j].PersonId;
                                    var singleTeamPerson = _context.Person.Where(c => c.Id == teamPersonId).FirstOrDefault();
                                    personTeamList.Add(singleTeamPerson);
                                }
                            }
                        }

                        _adminViewModel.TeamMember = personTeamList;
                        if (_adminViewModel.ApplicantThematic != null)
                        {
                            _adminViewModel.Theme = _context.Theme.Where(f => f.Id == _adminViewModel.ApplicantThematic.ThemeId).FirstOrDefault();
                            _adminViewModel.Category = _context.Category.Where(f => f.Id == _adminViewModel.Theme.CategoryId).FirstOrDefault();
                            _adminViewModel.ConceptNote = _context.ConceptNote.Where(f => f.ApplicantThemeId == _adminViewModel.ApplicantThematic.Id).FirstOrDefault();

                            _adminViewModel.ApplicantReviewers = _context.ApplicantReviewer.Where(f => f.ApplicantThematicId == _adminViewModel.ApplicantThematic.Id).ToList();
                            if (_adminViewModel.ApplicantReviewers.Count > 0)
                            {
                                for (int i = 0; i < _adminViewModel.ApplicantReviewers.Count; i++)
                                {
                                    int reviewerId = _adminViewModel.ApplicantReviewers[i].ReviewerId;
                                    int? scoreId = _adminViewModel.ApplicantReviewers[i].ScoreId;
                                    var singleReviewer = _context.Reviewer.Where(f => f.Id == reviewerId).FirstOrDefault();
                                    ReviewerAssignedScore reviewerAssignedScore = new ReviewerAssignedScore();
                                    reviewerAssignedScore.Reviewer = new Reviewer();
                                    reviewerAssignedScore.Reviewer.Email = singleReviewer.Email;
                                    reviewerAssignedScore.Reviewer.Name = singleReviewer.Name;
                                    reviewerAssignedScore.Score = scoreId == null ? "Not assessed Yet" : _context.Score.Where(f => f.Id == scoreId).Select(g => g.Name).FirstOrDefault();

                                    reviewerList.Add(reviewerAssignedScore);
                                }
                            }
                            _adminViewModel.Approval = _context.Approval.Where(f => f.ApplicantThematicId == _adminViewModel.ApplicantThematic.Id && f.IsProposal == false).FirstOrDefault();
                        }
                        _adminViewModel.Proposal = _context.Proposal.Where(f => f.ApplicantThemeId == _adminViewModel.ApplicantThematic.Id).FirstOrDefault();
                        _adminViewModel.ReviewerAssignedScoreList = reviewerList;

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult ViewApplicantProposal(int id)
        {
            try
            {
                if (id > 0)
                {
                    _adminViewModel.Proposal = _context.Proposal.Where(o => o.ApplicantThemeId == id).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult AddReviewer()
        {
            if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
            {
                SetMessage(ValidateUserLogin(_context));
                return RedirectToAction("Logout", "Home");
            }
            return View(_adminViewModel);
        }
        [HttpPost]
        public IActionResult AddReviewer(IFormCollection collection)
        {
            try
            {
                Reviewer reviewer = new Reviewer();
                User user = new Models.User();
                reviewer.Email = (collection["Reviewer.Email"]);
                reviewer.Name = (collection["Reviewer.Name"]);
                reviewer.PhoneNo = (collection["Reviewer.PhoneNo"]);
                reviewer.TitleId = Convert.ToInt32(collection["Reviewer.TitleId"]);
                reviewer.InstitutionId = Convert.ToInt32(collection["Reviewer.InstitutionId"]);
                //user.UserName = (collection["User.UserName"]);
                //if (!user.UserName.Contains("/"))
                //{
                //    SetMessage("Username does not follow the pattern. eg: surname/firstname");
                //    return View();
                //}


                string oneTimeValidator = Convert.ToString(Guid.NewGuid());
                _utility.AddReviewer(reviewer.Name, reviewer.PhoneNo, reviewer.Email, user.UserName, reviewer.TitleId, reviewer.InstitutionId, oneTimeValidator);
                //Send Email
                _utility.SendEMail(reviewer.Email, reviewer.Name, (int)MessageType.ReviewerCreation, "", "", "", oneTimeValidator, "");
                SetMessage("Accessor added successfully. The accessor can now click the link sent to his email to complete registration.");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Index");
        }
        public IActionResult ViewReviewer()
        {
            try
            {
                List<ReviewerDetail> reviewerDetail = new List<ReviewerDetail>();
                string userEmail = User.FindFirstValue(ClaimTypes.Email);
                var loggedInAdmin = _context.User.Where(f => f.Email == userEmail).FirstOrDefault();
                var users = new List<User>();
                _adminViewModel.ReviewerDetails = new List<ReviewerDetail>();
                if (loggedInAdmin?.Id > 0 && loggedInAdmin.RoleId == (int)RoleType.CoAdmin)
                {

                    var categoryAdmin = _context.CategoryAdministrator.Where(f => f.UserId == loggedInAdmin.Id && f.Active).FirstOrDefault();
                    if (categoryAdmin == null)
                    {
                        SetMessage("No category has been assigned to you, as such you can not view accessors");
                        return View(_adminViewModel);
                    }
                    var allCategoryReviewers = _context.ReviewerThematicStrenght.Where(f => f.Theme.CategoryId == categoryAdmin.CategoryId).Select(f => f.ReviewerId).Distinct().ToList();
                    if (allCategoryReviewers?.Count > 0)
                    {
                        users = _context.User.Where(f => allCategoryReviewers.Contains(f.ReviewerId) && f.RoleId == (int)RoleType.Reviewer && f.Active).ToList();
                    }

                }
                else if (loggedInAdmin?.Id > 0)
                {
                    users = _context.User.Where(g => g.RoleId == (int)RoleType.Reviewer).ToList();
                }

                if (users.Count > 0)
                {
                    for (int i = 0; i < users.Count; i++)
                    {
                        ReviewerDetail detail = new ReviewerDetail();
                        var reviewerId = users[i].ReviewerId;
                        var reviewer = _context.Reviewer.Where(g => g.Id == reviewerId).FirstOrDefault();
                        var count = _context.ApplicantReviewer.Where(f => f.ReviewerId == reviewerId && f.TypeId == (int)Types.ConceptNote && f.ApplicantThematic.Session.ActiveForApplication).Count();
                        detail.User = users[i];
                        detail.Phone = reviewer.PhoneNo;
                        detail.ApplicantCount = count;
                        detail.Reviewer = reviewer;
                        detail.ProposalCount = _context.ApplicantReviewer.Where(f => f.ReviewerId == reviewerId && f.TypeId == (int)Types.Proposal && f.ApplicantThematic.Session.ActiveForApplication).Count();
                        reviewerDetail.Add(detail);
                    }

                }
                _adminViewModel.ReviewerDetails = reviewerDetail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult EditReviewer(int id)
        {
            try
            {
                if (id > 0)
                {
                    _adminViewModel.Reviewer = _context.Reviewer.Where(d => d.Id == id).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        //public IActionResult UpdateReviewer(IFormCollection collection)
        public IActionResult UpdateReviewer([Bind("Id,Name,Email,Active,PhoneNo")] Reviewer reviewer)
        {
            try
            {

                //Reviewer reviewer = new Reviewer();
                //reviewer.Email = (collection["Reviewer.Email"]);
                //reviewer.Name = (collection["Reviewer.Name"]);
                //reviewer.PhoneNo = (collection["Reviewer.PhoneNo"]);
                //reviewer.Id =Convert.ToInt32(collection["Reviewer.Id"]);
                //bool strArray = Convert.ToBoolean(collection["Reviewer.Active"]);
                //char[] delimiterChar = { ',' };
                //var d = strArray;
                if (reviewer.Id > 0)
                {
                    var existingReviewer = _context.Reviewer.Where(f => f.Id == reviewer.Id).FirstOrDefault();
                    var existingUser = _context.User.Where(f => f.ReviewerId == reviewer.Id).FirstOrDefault();
                    var exist = _utility.EmailExist(reviewer.Email, reviewer.Id);
                    if (exist)
                    {
                        SetMessage("You cannot use this Email," + reviewer.Email);
                        return RedirectToAction("ViewReviewer", new { id = reviewer.Id });
                    }
                    if (existingReviewer != null && existingUser != null)
                    {
                        using (TransactionScope scope = new TransactionScope())
                        {
                            existingReviewer.Name = reviewer.Name;
                            existingReviewer.PhoneNo = reviewer.PhoneNo;
                            existingReviewer.Email = reviewer.Email;
                            existingReviewer.Active = reviewer.Active;
                            _context.Update(existingReviewer);
                            // update corresponding User
                            existingUser.Email = reviewer.Email;
                            existingUser.Active = reviewer.Active;
                            _context.Update(existingUser);
                            _context.SaveChanges();
                            scope.Complete();
                        }

                        SetMessage("Update was successfull");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("ViewReviewer");
        }
        public IActionResult SetBudget()
        {
            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }
                if (!_utility.CheckResearchCycleEvent((int)EventTypes.SetBudgetParameter))
                {
                    SetMessage("Budget cannot be set at this time.");
                    return RedirectToAction("Index", "AdminHome");
                }
                var activeCategory = _context.Category.Where(d => d.Active).ToList();
                List<BudgetSum> list = new List<BudgetSum>();
                if (activeCategory.Count > 0)
                {
                    for (int i = 0; i < activeCategory.Count; i++)
                    {
                        BudgetSum budgetSum = new BudgetSum();
                        //var catId = activeCategory[i].Id;
                        //var budget = _context.Budget.Where(g => g.CategoryId == catId).FirstOrDefault();
                        budgetSum.Category = new Category();
                        budgetSum.Category = activeCategory[i];
                        //if (budget != null)
                        //{
                        //    budgetSum.Amount = budget.Amount;
                        //}
                        list.Add(budgetSum);
                    }
                }
                _adminViewModel.BudgetSums = list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        [HttpPost]
        public IActionResult SetBudget(IFormCollection form)
        {
            try
            {
                var Amount = form["item.Amount"];
                var categoryId = form["item.Category.Id"];
                var Id = Convert.ToInt32(form["Person.Id"]);
                var sessionId = Convert.ToInt32(form["SessionId"]);
                string userEmail = User.FindFirstValue(ClaimTypes.Email);
                var loggedInAdmin = _context.User.Where(f => f.Email == userEmail).FirstOrDefault();
                List<BudgetSum> list = new List<BudgetSum>();

                if (categoryId.Count > 0)
                {
                    for (int i = 0; i < categoryId.Count; i++)
                    {
                        Budget budget = new Budget();
                        var catId = Convert.ToInt32(categoryId[i]);
                        var existbudget = _context.Budget.Where(g => g.CategoryId == catId && g.SessionId == sessionId).FirstOrDefault();
                        if (existbudget != null)
                        {
                            SetMessage("Budget for this research cycle already exists");
                        }
                        else
                        {
                            budget.Amount = Convert.ToDecimal(Amount[i]);
                            budget.CategoryId = catId;
                            budget.SessionId = sessionId;
                            budget.User = loggedInAdmin;
                            _context.Add(budget);
                            _context.SaveChanges();

                            SetMessage("Budget set successfully");

                            return RedirectToAction("Index");
                        }
                    }
                }
            }
            catch (Exception ex) { throw ex; }

            return RedirectToAction("SetBudget");
        }

        public IActionResult ViewResearchCycles()
        {
            try
            {
                return View(_adminViewModel);
            }
            catch (Exception ex) { throw ex; }
        }

        public IActionResult EditBudget(int Id)
        {
            try
            {
                var researchCycleCategories = _context.Budget.Where(s => s.SessionId == Id).ToList();

                List<BudgetSum> list = new List<BudgetSum>();
                if (researchCycleCategories?.Count() > 0)
                {
                    _adminViewModel.TotalAllocatedAmountForThematicCategory = researchCycleCategories.Sum(s => s.Amount);

                    for (int i = 0; i < researchCycleCategories.Count(); i++)
                    {
                        BudgetSum budgetSum = new BudgetSum();
                        budgetSum.UsedAmount = researchCycleCategories[i].AmountDispensed;
                        budgetSum.Amount = researchCycleCategories[i].Amount;
                        budgetSum.Category = new Category();
                        budgetSum.Category = researchCycleCategories[i].Category;
                        //budgetSum.Name = researchCycleCategories[i].

                        list.Add(budgetSum);
                    }

                    _adminViewModel.SessionId = Id;
                    _adminViewModel.BudgetSums = list;
                }
                return View(_adminViewModel);
            }
            catch (Exception ex) { throw ex; }
        }

        [HttpPost]
        public IActionResult EditBudget(IFormCollection form)
        {
            try
            {
                var Amount = form["item.Amount"];
                var categoryId = form["item.Category.Id"];
                var Id = Convert.ToInt32(form["Person.Id"]);
                var sessionId = Convert.ToInt32(form["SessionId"]);
                string userEmail = User.FindFirstValue(ClaimTypes.Email);
                var loggedInAdmin = _context.User.Where(f => f.Email == userEmail).FirstOrDefault();
                List<BudgetSum> list = new List<BudgetSum>();
                if (categoryId.Count > 0)
                {
                    for (int i = 0; i < categoryId.Count; i++)
                    {
                        Budget budget = new Budget();
                        var catId = Convert.ToInt32(categoryId[i]);
                        var existbudget = _context.Budget.Where(g => g.CategoryId == catId && g.SessionId == sessionId).FirstOrDefault();
                        if (existbudget != null)
                        {
                            existbudget.Amount = Convert.ToDecimal(Amount[i]);
                            existbudget.SessionId = sessionId;
                            _context.Update(existbudget);
                            _context.SaveChanges();
                        }
                        else
                        {
                            SetMessage("No such research cycle exists");
                        }

                    }
                    SetMessage("Budget updated successfully");
                    return RedirectToAction("ViewResearchCycles");
                }

                return RedirectToAction(" EditBudget", new { Id = sessionId });
            }
            catch (Exception ex) { throw ex; }
            //return View(_adminViewModel);
        }

        public IActionResult AssessedProposal()
        {
            try
            {
                List<ApplicationList> list = new List<ApplicationList>();
                var allAssessedProposal = _context.ProposalAssessment.GroupBy(t => t.ApplicantThematicId).ToList();
                List<ReviewerScore> scoreList = new List<ReviewerScore>();

                if (allAssessedProposal.Count > 0)
                {

                    for (int i = 0; i < allAssessedProposal.Count; i++)
                    {
                        ApplicationList applicationList = new ApplicationList();

                        var applicantThematicId = allAssessedProposal[i].Key;
                        var individualReviewer = _context.ProposalAssessment.Where(f => f.ApplicantThematicId == applicantThematicId).GroupBy(f => f.ReviewerId).ToList();
                        for (int j = 0; j < individualReviewer.Count; j++)
                        {
                            ReviewerScore reviewerScore = new ReviewerScore();
                            var reviewerId = individualReviewer[j].Key;
                            var myProposalAssessment = _context.ProposalAssessment.Where(f => f.ApplicantThematicId == applicantThematicId && f.ReviewerId == reviewerId).ToList();
                            reviewerScore.proposalScore = myProposalAssessment.Sum(f => f.Score);
                            var firstRecord = myProposalAssessment.FirstOrDefault();
                            reviewerScore.ProposedBudget = firstRecord != null ? (decimal)firstRecord.ApprovedBudget : 0;
                            var applicantThematic = _context.ApplicantThematic.Where(f => f.Id == firstRecord.ApplicantThematicId).FirstOrDefault();
                            applicationList.ApplicantThematic = applicantThematic;
                            var applicant = _context.Applicant.Where(g => g.Id == applicantThematic.ApplicantId).FirstOrDefault();
                            var reviewer = _context.Reviewer.Where(r => r.Id == firstRecord.ReviewerId).FirstOrDefault();
                            reviewerScore.Reviewer = reviewer;
                            var person = _context.Person.Where(t => t.Id == applicant.PersonId).FirstOrDefault();
                            scoreList.Add(reviewerScore);
                            applicationList.Theme = _context.Theme.Where(f => f.Id == applicantThematic.ThemeId).FirstOrDefault();
                            applicationList.Category = _context.Category.Where(f => f.Id == applicationList.Theme.CategoryId).FirstOrDefault();
                            applicationList.Person = person;
                            applicationList.ApplicantThematic = applicantThematic;
                            applicationList.ReviewerScores = scoreList;

                        }

                        list.Add(applicationList);
                    }


                }

                _adminViewModel.ApplicationList = list; ;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        //public IActionResult ViewProposalbycategory(int CategoryId)
        //{
        //    try
        //    {
        //        if (CategoryId <= 0)
        //        {
        //            SetMessage("Please, select Category to Continue");
        //            return View(_adminViewModel);
        //        }
        //        List<Reviewer> reviewerList = new List<Reviewer>();
        //        List<ApplicationList> list = new List<ApplicationList>();
        //        var themes = _context.Theme.Where(h => h.CategoryId == CategoryId).ToList();
        //        var uniqueApplicant=_context.ApplicantThematic.GroupBy(g => g.ApplicantId).ToList();

        //        if (themes.Count > 0)
        //        {
        //            for (int t = 0; t < themes.Count; t++)
        //            {
        //                var ThemeId = themes[t].Id;
        //                var applicantThematic = _context.ApplicantThematic.Where(f =>f.ThemeId == ThemeId).ToList();
        //            }
        //        }
        //        var applications = _context.ApplicantThematic.Where(f => f.c == ThemeId).ToList();
        //        if (applications.Count > 0)
        //        {
        //            for (int i = 0; i < applications.Count; i++)
        //            {
        //                ApplicationList applicationList = new ApplicationList();
        //                var applicantId = applications[i].ApplicantId;
        //                var thematicapplicant = applications[i];
        //                var applicant = _context.Applicant.Where(f => f.Id == applicantId).FirstOrDefault();
        //                var Person = _context.Person.Where(j => j.Id == applicant.PersonId).FirstOrDefault();
        //                applicationList.ApplicantThematic = applications[i];
        //                applicationList.Person = Person;
        //                var applicantReviewerList = _context.ApplicantReviewer.Where(f => f.ApplicantThemeId == thematicapplicant.Id).ToList();
        //                if (applicantReviewerList.Count > 0)
        //                {
        //                    for (int j = 0; j < applicantReviewerList.Count; j++)
        //                    {
        //                        var reviewerId = applicantReviewerList[j].ReviewerId;
        //                        var reviewer = _context.Reviewer.Where(f => f.Id == reviewerId).FirstOrDefault();
        //                        reviewerList.Add(reviewer);
        //                    }
        //                }
        //                applicationList.Reviewers = reviewerList;
        //                applicationList.Category = _context.Category.Where(f => f.Id == CategoryId).FirstOrDefault();
        //                applicationList.Theme = _context.Theme.Where(f => f.Id == ThemeId).FirstOrDefault();
        //                list.Add(applicationList);
        //            }
        //        }
        //        _adminViewModel.ApplicationList = list;
        //        _adminViewModel.ShowPanel = true;
        //    }
        //    catch(Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return View();
        //}

        //None Action Methods
        public IActionResult GetTheme(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }
            int categoryId = Convert.ToInt32(value);
            var themecategoryList = _context.Theme.Where(c => c.Active && c.CategoryId == categoryId).ToList();

            return Json(new SelectList(themecategoryList, Value, Text));
        }
        public IActionResult ViewSubmittedProposal()
        {
            try
            {
                //_adminViewModel.PopulatePopulateThemeDropDownListByCategory(0);
                string adminEmail = User.FindFirstValue(ClaimTypes.Email);
                var admin = _context.User.Where(d => d.Email == adminEmail).FirstOrDefault();
                if (admin?.Id > 0)
                {
                    if (admin.Role.Name == "Admin" || admin.Role.Name == "Super-Admin")
                    {
                        _adminViewModel.PopulateReviewerExpertriateCategory(0);
                    }
                    else
                    {
                        var coAdminCategory = _context.CategoryAdministrator.Where(F => F.UserId == admin.Id && F.Active).FirstOrDefault();
                        if (coAdminCategory?.Id > 0)
                        {
                            _adminViewModel.PopulateReviewerExpertriateCategory(coAdminCategory.CategoryId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        [HttpPost]
        public IActionResult ViewSubmittedProposal(int ThemeId, int SessionId)
        {
            try
            {
                if (ThemeId <= 0)
                {
                    SetMessage("Please select Thematic Area to continue");
                    return View(_adminViewModel);
                }

                List<ApplicationList> list = new List<ApplicationList>();
                var Theme = _context.Theme.Where(f => f.Id == ThemeId).FirstOrDefault();
                _adminViewModel.PopulateReviewerExpertriateCategory(Theme.CategoryId);
                //_adminViewModel.PopulatePopulateThemeDropDownListByCategory(Theme.CategoryId);
                //var applications = _context.ApplicantThematic.Where(f => f.ThemeId == ThemeId).ToList();
                var submittedProposal = _context.ApplicantSubmission.Where(t => t.ApplicantThematic.ThemeId == ThemeId && t.TypeId == (int)Types.Proposal && t.ApplicantThematic.SessionId == SessionId).ToList();
                if (submittedProposal.Count > 0)
                {

                    for (int i = 0; i < submittedProposal.Count; i++)
                    {
                        ApplicationList applicationList = new ApplicationList();
                        var applicantId = submittedProposal[i].ApplicantThematicId;
                        var thematicapplicant = submittedProposal[i].ApplicantThematic;
                        var submitedProposal = _context.Proposal.Where(r => r.ApplicantThemeId == applicantId).FirstOrDefault();
                        if (submitedProposal != null && submitedProposal.Submitted)
                        {
                            var applicantReviewerList = _context.ApplicantReviewer.Where(f => f.ApplicantThematicId == applicantId && f.TypeId == (int)Types.Proposal).ToList();
                            if (applicantReviewerList.Count > 0)
                            {
                                List<Reviewer> reviewerList = new List<Reviewer>();
                                for (int j = 0; j < applicantReviewerList.Count; j++)
                                {
                                    var reviewerId = applicantReviewerList[j].ReviewerId;
                                    var reviewer = _context.Reviewer.Where(f => f.Id == reviewerId).FirstOrDefault();
                                    reviewerList.Add(reviewer);
                                }
                                applicationList.Reviewers = reviewerList;
                            }
                            else
                            {
                                List<Reviewer> reviewerList = new List<Reviewer>();
                                applicationList.Reviewers = reviewerList;
                            }
                            applicationList.ApplicantSubmission = submittedProposal[i];


                            /////start
                            var reviewerStrengthList = _context.ReviewerThematicStrenght.Where(d => d.ThemeId == ThemeId && d.Reviewer.InstitutionId != thematicapplicant.Applicant.Person.InstitutionId).ToList();

                            if (reviewerStrengthList?.Count > 0)
                            {
                                List<AssessorDropdown> assessorDropdownList = new List<AssessorDropdown>();
                                foreach (var item in reviewerStrengthList)
                                {
                                    var name = "";
                                    var perAssessor = _context.ApplicantReviewer.Where(f => f.ApplicantThematic.ThemeId == ThemeId && f.TypeId == (int)Types.Proposal && f.ReviewerId == item.ReviewerId && f.ApplicantThematic.SessionId == SessionId).ToList();
                                    name = item.Reviewer.Name + "-" + "(" + perAssessor.Count + ")";
                                    AssessorDropdown assessorDropdown = new AssessorDropdown();
                                    assessorDropdown.ReviewerId = item.ReviewerId;
                                    assessorDropdown.Name = name;
                                    assessorDropdownList.Add(assessorDropdown);
                                }
                                applicationList.ReviewerHighStrengthNameSL = new SelectList(assessorDropdownList, ReviewerId, ReviewerName);
                            }
                            else
                            {
                                //applicationList.ReviewerHighStrengthNameSL = new SelectList(null, null, null);
                                List<AssessorDropdown> assessorDropdownList = new List<AssessorDropdown>();
                                applicationList.ReviewerHighStrengthNameSL = new SelectList(assessorDropdownList, ReviewerId, ReviewerName);
                            }
                            /////end


                            list.Add(applicationList);
                        }

                    }
                }
                _adminViewModel.ApplicationList = list;
                _adminViewModel.ShowPanel = true;
                var reviewerStrength = _context.ReviewerThematicStrenght.Where(d => d.ThemeId == ThemeId).ToList();
                _adminViewModel.Category = Theme.Category;
                _adminViewModel.CategoryId = Theme.Category.Id;
                _adminViewModel.Theme = Theme;
                _adminViewModel.ReviewerHighStrengthNameSL = new SelectList(reviewerStrength, Name, Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View(_adminViewModel);

        }
        public IActionResult ViewReviewedProposal()
        {
            try
            {

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }

        [HttpPost]
        public IActionResult ViewReviewedProposal(int SessionId, int CategoryId)
        {
            try
            {
                List<ScoreRanking> scoreRankingList = new List<ScoreRanking>();
                _adminViewModel.ShowPanel = true;
                var allProposalAssessment = _context.ProposalAssessment.Where(f => f.Approved && f.ApplicantThematic.SessionId == SessionId && f.ApplicantThematic.Theme.CategoryId == CategoryId).ToList();
                if (allProposalAssessment.Count > 0)
                {
                    var uniqueApplicant = allProposalAssessment.GroupBy(o => o.ApplicantThematicId).ToList();
                    for (int i = 0; i < uniqueApplicant.Count; i++)
                    {
                        ScoreRanking scoreRanking = new ScoreRanking();
                        var totalScore = 0M;
                        var average = 0M;
                        var applicantThematicId = uniqueApplicant[i].Key;
                        var allApplicantReviewedScore = allProposalAssessment.Where(f => f.ApplicantThematicId == applicantThematicId).ToList();
                        if (allApplicantReviewedScore.Count > 0)
                        {
                            for (int k = 0; k < allApplicantReviewedScore.Count; k++)
                            {
                                var uniqueScore = allApplicantReviewedScore[k];
                                var score = uniqueScore.Score;
                                totalScore += score;
                                scoreRanking.CategoryName = uniqueScore.ApplicantThematic.Theme.Category.Name;
                                scoreRanking.ThematicAreaName = uniqueScore.ApplicantThematic.Theme.Name;
                            }
                            average = (totalScore / allApplicantReviewedScore.Count);

                        }
                        var finalProposalApproval = _context.FinalProposalAproval.Where(f => f.ApplicantThematicId == applicantThematicId).FirstOrDefault();
                        scoreRanking.AverageScore = average;
                        scoreRanking.iSApproved = finalProposalApproval != null ? finalProposalApproval.IsApproved : false;
                        scoreRanking.ReviewerCount = allApplicantReviewedScore.Count;
                        scoreRanking.AverageApprovedBudget = Math.Round((decimal)(allApplicantReviewedScore.Sum(f => f.ApprovedBudget) / allApplicantReviewedScore.Count), 2);
                        scoreRanking.ApplicantThematic = allApplicantReviewedScore.FirstOrDefault().ApplicantThematic;
                        // this is to ensure that atleast more than one accessor has reviewed the proposal, before the final approval
                        if (allApplicantReviewedScore != null && allApplicantReviewedScore.Count > 1)
                        {
                            scoreRankingList.Add(scoreRanking);
                        }

                    }
                    _adminViewModel.ScoreRankings = scoreRankingList.OrderByDescending(f => f.AverageScore).ToList();
                }
                else
                {
                    _adminViewModel.ScoreRankings = new List<ScoreRanking>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult RejectProposal(bool isDefault, int applicantThematicId, string remark)
        {
            try
            {
                string message = "";
                if (applicantThematicId > 0)
                {

                    var applicantThematic = _context.ApplicantThematic.Where(f => f.Id == applicantThematicId).FirstOrDefault();
                    if (applicantThematic != null)
                    {
                        string userEmail = User.FindFirstValue(ClaimTypes.Email);
                        var loggedInAdmin = _context.User.Where(f => f.Email == userEmail).FirstOrDefault();

                        var finalPropoalApproval = _context.FinalProposalAproval.Where(f => f.ApplicantThematicId == applicantThematic.Id).FirstOrDefault();
                        if (finalPropoalApproval == null)
                        {
                            FinalProposalAproval finalProposalAproval = new FinalProposalAproval()
                            {
                                ApplicantThematicId = applicantThematic.Id,
                                IsApproved = false,
                                Remark = isDefault ? _context.DefaultMessage.Where(f => f.Active && f.isProposal && f.ProposalApprove == false).Select(f => f.Message).LastOrDefault() : remark,
                                User = loggedInAdmin
                            };
                            _context.Add(finalProposalAproval);
                            _context.SaveChanges();
                            message = isDefault ? _context.DefaultMessage.Where(f => f.Active && f.isProposal && f.ProposalApprove == false).Select(f => f.Message).LastOrDefault() : remark;
                            //_utility.SendEMail(applicantThematic.Applicant.Person.Email, applicantThematic.Applicant.Person.ApplicantFullName,(int)MessageType.ProposalRejection, message, "Proposal Rejected");
                            //  Send "Success"
                            return Json(new { success = true, responseText = "Operation Successful" });
                        }
                    }
                    //return Json(new { success = false, responseText = "This Reviewer has Already been assigned to this Applicant!" });


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View();
        }

        public IActionResult ProposalFinalApproval(decimal Amount, int applicantThematicId)
        {
            try
            {
                string message = "";
                if (applicantThematicId > 0)
                {

                    var applicantThematic = _context.ApplicantThematic.Where(f => f.Id == applicantThematicId).FirstOrDefault();
                    if (applicantThematic != null)
                    {
                        string userEmail = User.FindFirstValue(ClaimTypes.Email);
                        var loggedInAdmin = _context.User.Where(f => f.Email == userEmail).FirstOrDefault();

                        var finalPropoalApproval = _context.FinalProposalAproval.Where(f => f.ApplicantThematicId == applicantThematic.Id).FirstOrDefault();
                        if (finalPropoalApproval == null)
                        {

                            message = _context.DefaultMessage.Where(f => f.Active && f.isProposal && f.ProposalApprove == true).Select(f => f.Message).LastOrDefault();
                            var sessionBudget = _context.Budget.Where(f => f.SessionId == applicantThematic.SessionId && f.CategoryId == applicantThematic.Theme.CategoryId).FirstOrDefault();
                            if (sessionBudget == null)
                            {
                                return Json(new { success = false, responseText = "Budget for this Research Cycle has not been set" });
                            }
                            using (TransactionScope scope = new TransactionScope())
                            {
                                FinalProposalAproval finalProposalAproval = new FinalProposalAproval()
                                {
                                    ApplicantThematicId = applicantThematic.Id,
                                    IsApproved = true,
                                    ApprovedAmount = Amount,
                                    User = loggedInAdmin,

                                };
                                _context.Add(finalProposalAproval);

                                //Update the General Budget

                                var balance = sessionBudget.Amount - Amount;
                                var currentDispense = sessionBudget.AmountDispensed == null ? (0 + Amount) : (decimal)sessionBudget.AmountDispensed + Amount;
                                sessionBudget.Amount = balance;
                                sessionBudget.AmountDispensed = currentDispense;
                                _context.Update(sessionBudget);

                                _context.SaveChanges();
                                scope.Complete();
                            }
                            // _utility.SendEMail(applicantThematic.Applicant.Person.Email, applicantThematic.Applicant.Person.ApplicantFullName, (int)MessageType.ProposalRejection, message, "Proposal Approved");
                            //  Send "Success"
                            return Json(new { success = true, responseText = "Operation Successful" });
                        }
                    }
                    //return Json(new { success = false, responseText = "This Reviewer has Already been assigned to this Applicant!" });


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View();
        }
        public IActionResult Setup()
        {
            try
            {
                _adminViewModel.TitleList = _context.Title.ToList();
                _adminViewModel.SessionList = _context.Session.ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(_adminViewModel);
        }
        public JsonResult CreateSession(string sessioname, bool sessionActive, bool applicationActive)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {

                var trimmedName = sessioname.Trim();
                var sessionExist = _context.Session.Where(f => f.Name == trimmedName).FirstOrDefault();
                if (sessionExist != null)
                {
                    result.IsError = true;
                    result.Message = "Research cycle Exists!";
                    return Json(result);
                }
                else
                {
                    if (applicationActive)
                    {
                        var existActiveApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                        if (existActiveApplication != null)
                        {
                            result.IsError = true;
                            result.Message = "Two research cycle should not be activated at the same time!";
                            return Json(result);
                        }
                    }
                    Session session = new Session();
                    session.Name = trimmedName;
                    session.Active = sessionActive;
                    session.ActiveForApplication = applicationActive;
                    _context.Add(session);
                    _context.SaveChanges();
                    result.IsError = false;
                    result.Message = "Operation Successful";
                    return Json(result);
                }

            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }

            return Json(result);
        }
        public JsonResult SaveEditSession(int sessionId, string sessioname, bool sessionActive, bool applicationActive)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {

                var trimmedName = sessioname.Trim();
                var sessionExist = _context.Session.Where(f => f.Name == trimmedName && f.Id != sessionId).FirstOrDefault();
                if (sessionExist != null)
                {
                    result.IsError = true;
                    result.Message = "Research cycle Exists!";
                    return Json(result);
                }
                else
                {
                    if (applicationActive)
                    {
                        var existActiveApplication = _context.Session.Where(f => f.ActiveForApplication && f.Id != sessionId).LastOrDefault();
                        if (existActiveApplication != null)
                        {
                            result.IsError = true;
                            result.Message = "Two research cycle should not be activated at the same time!";
                            return Json(result);
                        }
                    }
                    var existingSession = _context.Session.Where(f => f.Id == sessionId).FirstOrDefault();
                    existingSession.Name = trimmedName;
                    existingSession.Active = sessionActive;
                    existingSession.ActiveForApplication = applicationActive;
                    _context.Update(existingSession);
                    _context.SaveChanges();
                    result.IsError = false;
                    result.Message = "Operation Successful";
                    return Json(result);
                }

            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }

            return Json(result);
        }
        public JsonResult FetchSession(int sessionId)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                if (sessionId > 0)
                {
                    var existingSession = _context.Session.Where(f => f.Id == sessionId).FirstOrDefault();
                    if (existingSession != null)
                    {
                        result.Session = existingSession;
                        result.IsError = false;
                        return Json(result);
                    }
                    else
                    {
                        result.IsError = true;
                        result.Message = "Research cycle does not exist";
                        return Json(result);
                    }
                }

            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }
            return Json(result);
        }
        public JsonResult CreateTitle(string titleName, bool activeTitle)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {

                var trimmedName = titleName.Trim();
                var titleExist = _context.Title.Where(f => f.Name == trimmedName).FirstOrDefault();
                if (titleExist != null)
                {
                    result.IsError = true;
                    result.Message = "Title Exists!";
                    return Json(result);
                }
                else
                {
                    Title title = new Title();
                    title.Name = trimmedName;
                    title.Active = activeTitle;

                    _context.Add(title);
                    _context.SaveChanges();
                    result.IsError = false;
                    result.Message = "Operation Successful";
                    return Json(result);
                }

            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }
            return Json(result);
        }
        public IActionResult AddInstitution()
        {

            try
            {
                if (!String.IsNullOrEmpty(ValidateUserLogin(_context)))
                {
                    SetMessage(ValidateUserLogin(_context));
                    return RedirectToAction("Logout", "Home");
                }
                _adminViewModel.InstitutionList = _context.Institution.ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult SaveInstitution(AdminViewModel adminViewModel)
        {
            try
            {
                if (adminViewModel.Institution != null && adminViewModel.State != null && adminViewModel.State.Id > 0 && adminViewModel.InstitutionCategory != null && adminViewModel.InstitutionCategory.Id > 0)
                {


                    Institution institution = new Institution()
                    {
                        Active = true,
                        StateId = adminViewModel.State.Id,
                        InstitutionCategoryId = adminViewModel.InstitutionCategory.Id,
                        IsPublic = adminViewModel.isPublic,
                        Name = adminViewModel.Institution.Name
                    };
                    _context.Add(institution);
                    _context.SaveChanges();
                    SetMessage("Operation Successful");
                    return RedirectToAction("AddInstitution");

                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult EditInstitution(int institutionId)
        {
            try
            {
                if (institutionId > 0)
                {
                    var existInstitutiton = _context.Institution.Where(f => f.Id == institutionId).FirstOrDefault();
                    if (existInstitutiton != null)
                    {
                        _adminViewModel.Institution = existInstitutiton;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(_adminViewModel);
        }

        public IActionResult GetInstitution(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }
            int categoryId = Convert.ToInt32(value);
            var institutionList = _context.Institution.Where(c => c.Active && c.InstitutionCategoryId == categoryId).OrderBy(d => d.Name).ToList();

            return Json(new SelectList(institutionList, Value, Text));
        }

        public IActionResult AddUserGuide()
        {
            try
            {

                _adminViewModel.UserGuideList.Add(new SelectListItem() { Text = "-- Select User Category --", Value = "" });
                _adminViewModel.UserGuideList.Add(new SelectListItem() { Value = "Assessor_User_Guide", Text = "Assessor User Guide" });
                _adminViewModel.UserGuideList.Add(new SelectListItem() { Value = "PI_User_Guide", Text = "PI User Guide" });
                _adminViewModel.UserGuideList.Add(new SelectListItem() { Value = "Co_Reseacher_User_Guide", Text = "Co_Reseacher_User_Guide" });
                _adminViewModel.UserGuideList.Add(new SelectListItem() { Value = "Admin_User_Guide", Text = "Admin User Guide" });
                _adminViewModel.UserGuideList.Add(new SelectListItem() { Value = "Co_Admin_User_Guide", Text = "Co_Admin_User_Guide" });

                return View(_adminViewModel);
            }
            catch (Exception ex) { throw ex; }
        }

        [HttpPost]
        public async Task<IActionResult> AddUserGuide(IFormCollection Form, IFormFile GuideFile)
        {
            try
            {
                string guideCategory = (Form["GuideCategory"]);
                if (guideCategory.Contains(","))
                {
                    guideCategory = guideCategory.Split(",")[0];
                }
                string filePath = string.Empty;

                if (!string.IsNullOrEmpty(guideCategory))
                {
                    if (GuideFile.Length > 0)
                    {
                        var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "UserGuides");
                        var extName = Path.GetExtension(GuideFile.FileName);
                        string saveName = string.Format("{0}{1}", guideCategory, extName);
                        filePath = Path.Combine(uploads, saveName);

                        FileInfo file = new FileInfo(saveName);
                        if (file.Exists)
                        {
                            file.Delete();
                        }

                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            await GuideFile.CopyToAsync(stream);
                        }

                        return Json("Guide uploaded successfully");
                    }
                    else return Json("PDF file is required");
                }

                return RedirectToAction("AddUserGuide");
            }
            catch (Exception ex) { throw ex; }
        }
        public IActionResult ViewSubmittedConceptNoteByResearchCycle()
        {
            try
            {

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        [HttpPost]
        public IActionResult ViewSubmittedConceptNoteByResearchCycle(AdminViewModel adminViewModel)
        {
            try
            {
                if (adminViewModel?.SessionId <= 0)
                {
                    SetMessage("Please select Research Cycle to continue.");
                    return View(_adminViewModel);
                }

                List<ApplicationList> list = new List<ApplicationList>();
                var applications = _context.ApplicantThematic.Where(f => f.SessionId == adminViewModel.SessionId).ToList();
                if (applications.Count > 0)
                {

                    for (int i = 0; i < applications.Count; i++)
                    {
                        var thematicapplicant = applications[i];
                        var conceptNote = _context.ConceptNote.Where(g => g.ApplicantThemeId == thematicapplicant.Id).FirstOrDefault();
                        if (conceptNote != null && conceptNote.Submitted)
                        {
                            ApplicationList applicationList = new ApplicationList();
                            var applicantId = applications[i].ApplicantId;

                            var applicant = _context.Applicant.Where(f => f.Id == applicantId).FirstOrDefault();
                            var Person = _context.Person.Where(j => j.Id == applicant.PersonId).FirstOrDefault();
                            applicationList.ApplicantThematic = applications[i];
                            applicationList.Person = Person;
                            var applicantReviewerList = _context.ApplicantReviewer.Where(f => f.ApplicantThematicId == thematicapplicant.Id && f.TypeId == (int)Types.ConceptNote).ToList();
                            if (applicantReviewerList.Count > 0)
                            {
                                List<Reviewer> reviewerList = new List<Reviewer>();
                                for (int j = 0; j < applicantReviewerList.Count; j++)
                                {
                                    var reviewerId = applicantReviewerList[j].ReviewerId;
                                    var reviewer = _context.Reviewer.Where(f => f.Id == reviewerId).FirstOrDefault();
                                    reviewerList.Add(reviewer);
                                }
                                applicationList.Reviewers = reviewerList;
                            }
                            else
                            {
                                List<Reviewer> reviewerList = new List<Reviewer>();
                                applicationList.Reviewers = reviewerList;
                            }
                            list.Add(applicationList);
                        }

                    }
                }
                _adminViewModel.ApplicationList = list;
                _adminViewModel.ShowPanel = true;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult ViewSubmittedProposalByResearchCycle()
        {
            try
            {

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        [HttpPost]
        public IActionResult ViewSubmittedProposalByResearchCycle(AdminViewModel adminViewModel)
        {
            try
            {
                if (adminViewModel.SessionId <= 0)
                {
                    SetMessage("Please, select Research cycle to Continue");
                    return View(_adminViewModel);
                }

                List<ApplicationList> list = new List<ApplicationList>();
                var submittedProposal = _context.ApplicantSubmission.Where(t => t.TypeId == (int)Types.Proposal && t.ApplicantThematic.SessionId == adminViewModel.SessionId).ToList();
                if (submittedProposal.Count > 0)
                {

                    for (int i = 0; i < submittedProposal.Count; i++)
                    {
                        ApplicationList applicationList = new ApplicationList();
                        var applicantId = submittedProposal[i].ApplicantThematicId;
                        var thematicapplicant = submittedProposal[i].ApplicantThematic;
                        var submitedProposal = _context.Proposal.Where(r => r.ApplicantThemeId == applicantId).FirstOrDefault();
                        if (submitedProposal != null && submitedProposal.Submitted)
                        {
                            var applicantReviewerList = _context.ApplicantReviewer.Where(f => f.ApplicantThematicId == applicantId && f.TypeId == (int)Types.Proposal).ToList();
                            if (applicantReviewerList.Count > 0)
                            {
                                List<Reviewer> reviewerList = new List<Reviewer>();
                                for (int j = 0; j < applicantReviewerList.Count; j++)
                                {
                                    var reviewerId = applicantReviewerList[j].ReviewerId;
                                    var reviewer = _context.Reviewer.Where(f => f.Id == reviewerId).FirstOrDefault();
                                    reviewerList.Add(reviewer);
                                }
                                applicationList.Reviewers = reviewerList;
                            }
                            else
                            {
                                List<Reviewer> reviewerList = new List<Reviewer>();
                                applicationList.Reviewers = reviewerList;
                            }
                            applicationList.ApplicantSubmission = submittedProposal[i];

                            list.Add(applicationList);
                        }

                    }
                }
                _adminViewModel.ApplicationList = list;
                _adminViewModel.ShowPanel = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View(_adminViewModel);
        }
        public JsonResult ExistingEmail(string email)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                var exist = _context.Reviewer.Where(g => g.Email == email).FirstOrDefault();
                if (exist != null)
                {
                    result.IsError = false;
                    result.Message = "Email address already exists!";
                    return Json(result);
                }
                else
                {
                    var existPI = _context.Applicant.Where(g => g.ApplicationNo == email).FirstOrDefault();
                    if (existPI != null)
                    {
                        result.IsError = false;
                        result.Message = "Email address already exists for PI!";
                        return Json(result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }
            return Json(result);
        }
        public IActionResult ConceptNoteAutoAccessorAssignment()
        {
            try
            {

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        [HttpPost]
        public IActionResult ConceptNoteAutoAccessorAssignment(AdminViewModel adminViewModel)
        {
            try
            {
                if (adminViewModel?.SessionId <= 0)
                {
                    SetMessage("Please select Research Cycle to continue.");
                    return View(_adminViewModel);
                }

                List<ApplicationList> list = new List<ApplicationList>();
                var applications = _context.ApplicantThematic.Where(f => f.SessionId == adminViewModel.SessionId).ToList();
                if (applications.Count > 0)
                {

                    for (int i = 0; i < applications.Count; i++)
                    {
                        var thematicapplicant = applications[i];
                        var conceptNote = _context.ConceptNote.Where(g => g.ApplicantThemeId == thematicapplicant.Id).FirstOrDefault();
                        if (conceptNote != null && conceptNote.Submitted)
                        {
                            ApplicationList applicationList = new ApplicationList();
                            var applicantId = applications[i].ApplicantId;

                            var applicant = _context.Applicant.Where(f => f.Id == applicantId).FirstOrDefault();
                            var Person = _context.Person.Where(j => j.Id == applicant.PersonId).FirstOrDefault();
                            applicationList.ApplicantThematic = applications[i];
                            applicationList.Person = Person;
                            var applicantReviewerList = _context.ApplicantReviewer.Where(f => f.ApplicantThematicId == thematicapplicant.Id && f.TypeId == (int)Types.ConceptNote).ToList();
                            if (applicantReviewerList.Count > 0)
                            {
                                List<Reviewer> reviewerList = new List<Reviewer>();
                                for (int j = 0; j < applicantReviewerList.Count; j++)
                                {
                                    var reviewerId = applicantReviewerList[j].ReviewerId;
                                    var reviewer = _context.Reviewer.Where(f => f.Id == reviewerId).FirstOrDefault();
                                    reviewerList.Add(reviewer);
                                }
                                applicationList.Reviewers = reviewerList;
                            }
                            else
                            {
                                List<Reviewer> reviewerList = new List<Reviewer>();
                                applicationList.Reviewers = reviewerList;
                            }
                            list.Add(applicationList);
                        }

                    }
                }
                _adminViewModel.ApplicationList = list;
                _adminViewModel.ShowPanel = true;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }

        public async Task<IActionResult> ConceptNoteAccessorAutoDistribution(List<int> applicantThematicIdArray)
        {
            JsonResultModel result = new JsonResultModel();
            if (applicantThematicIdArray.Count > 0)
            {
                foreach (var item in applicantThematicIdArray)
                {
                    var applicantThematic = _context.ApplicantThematic.Where(f => f.Id == item).FirstOrDefault();
                    if (applicantThematic?.Id > 0)
                    {
                        var qualifiedAccessors = await GetReviewerBy(applicantThematic);
                        if (qualifiedAccessors?.Count > 0)
                        {
                            var reviewerId = qualifiedAccessors.FirstOrDefault();
                            ApplicantReviewer applicantReviewer = new ApplicantReviewer()
                            {
                                ApplicantThematicId = item,
                                DateAssigned = DateTime.UtcNow,
                                ReviewerId = reviewerId.ReviewerId,
                                TypeId = (int)Types.ConceptNote,

                            };
                            _context.Add(applicantReviewer);
                            await _context.SaveChangesAsync();
                        }
                    }

                }


                result.IsError = false;
                result.Message = "Operation Successfully";
                return Json(result);
            }
            result.IsError = true;
            result.Message = "Operation not Successful";
            return Json(result);

        }
        public async Task<List<AccessorDistributionModel>> GetReviewerBy(ApplicantThematic applicantThematic)
        {
            List<AccessorDistributionModel> list = new List<AccessorDistributionModel>();
            if (applicantThematic?.Id > 0)
            {
                var reviewerstrenght = await _context.ReviewerThematicStrenght.Where(f => f.ThemeId == applicantThematic.ThemeId).OrderBy(f => f.Strenght).ToListAsync();
                if (reviewerstrenght?.Count > 0)
                {
                    foreach (var item in reviewerstrenght)
                    {
                        // _context.ApplicantReviewer.Where(f=>f.ApplicantThematicId==)
                        //Ensure the system do not assign accessor from the same institution to a researcher
                        if (item.Reviewer.InstitutionId != applicantThematic.Applicant.Person.InstitutionId)
                        {
                            AccessorDistributionModel accessorDistributionModel = new AccessorDistributionModel();
                            var applicantReviewer = _context.ApplicantReviewer.Where(f => f.ReviewerId == item.ReviewerId && f.ApplicantThematic.SessionId == applicantThematic.SessionId).ToList();
                            if (applicantReviewer?.Count > 0)
                            {

                            }
                            accessorDistributionModel.ReviewerId = item.ReviewerId;
                            accessorDistributionModel.NoOfAssignment = applicantReviewer.Count;
                            accessorDistributionModel.ReviewerStrenght = item.Strenght;
                            accessorDistributionModel.ReviewerThemeId = item.ThemeId;
                            list.Add(accessorDistributionModel);
                        }

                    }
                }
            }

            return list.OrderBy(f => f.ReviewerStrenght).ThenBy(f => f.NoOfAssignment).ToList();
        }
        public IActionResult UploadAccessors()
        {
            return View(_adminViewModel);
        }
        [HttpPost]
        public IActionResult UploadAccessors(IFormFile AttachmentFile)
        {
            if (AttachmentFile.Length > 0)
            {
                List<string> validFileExtension = new List<string>();
                //validFileExtension.Add(".xls");
                validFileExtension.Add(".xlsx");
                var extType = Path.GetExtension(AttachmentFile.FileName);


                if (validFileExtension.Contains(extType))
                {
                    _adminViewModel.ReadExcelList = new List<ReadExcel>();

                    var filePath = Path.GetTempFileName();
                    using (var stream = System.IO.File.Create(filePath))
                    {
                        AttachmentFile.CopyToAsync(stream);

                        //Initialize the Data from the excel sheet into stream
                        ExcelPackage package = new ExcelPackage(stream);
                        ExcelWorksheet worksheet = package.Workbook.Worksheets.FirstOrDefault();

                        if (worksheet != null)
                        {
                            //Give 1 rows space from the top to allow for Excel Headers
                            int totalRows = worksheet.Dimension.Rows;
                            _adminViewModel.ShowPanel = true;
                            for (int i = 2; i <= totalRows; i++)
                            {
                                ReadExcel readExcel = new ReadExcel();

                                readExcel.SerialNumber = Convert.ToInt32(worksheet.Cells[i, 1].Value);
                                readExcel.FullName = worksheet.Cells[i, 2].Value != null ? worksheet.Cells[i, 2].Value.ToString() : null;
                                readExcel.Email = worksheet.Cells[i, 2].Value != null ? worksheet.Cells[i, 3].Value.ToString() : null;
                                readExcel.PhoneNo = worksheet.Cells[i, 2].Value != null ? worksheet.Cells[i, 4].Value.ToString() : null;
                                if (readExcel.Email != null)
                                {
                                    _adminViewModel.ReadExcelList.Add(readExcel);
                                }
                            }
                            TempDataHelper.Put<List<ReadExcel>>(TempData, "AccessorList", _adminViewModel.ReadExcelList);
                        }

                    }
                }
                else
                {
                    SetMessage("Only excel format is allowed");
                    return RedirectToAction("UploadAccessors");
                }
                return View(_adminViewModel);
            }
            SetMessage("No File selected for upload");
            return RedirectToAction("UploadAccessors");
        }
        public IActionResult SaveAccessorList()
        {
            List<ReadExcel> successfulUpload = new List<ReadExcel>();
            List<ReadExcel> failedUpload = new List<ReadExcel>();
            List<ReadExcel> accessorList = TempDataHelper.Get<List<ReadExcel>>(TempData, "AccessorList");
            if (accessorList?.Count > 0)
            {
                foreach (var item in accessorList)
                {
                    var user = _context.User.Where(f => f.Email == item.Email).FirstOrDefault();
                    if (user == null)
                    {
                        string oneTimeValidator = Convert.ToString(Guid.NewGuid());
                        item.guid = oneTimeValidator;
                        _utility.AddReviewer(item.FullName, item.PhoneNo, item.Email, item.Email, 1, 1, item.guid);
                        successfulUpload.Add(item);
                    }
                    else
                    {
                        failedUpload.Add(item);
                    }

                }
                if (successfulUpload?.Count > 0)
                {
                    foreach (var item in successfulUpload)
                    {

                        //Send Email
                        _utility.SendEMail(item.Email, item.FullName, (int)MessageType.ReviewerCreation, "", "", "", item.guid, "");
                    }
                }
                if (failedUpload?.Count > 0)
                {
                    SetMessage(successfulUpload.Count + " accessor(s)  was(were) uploaded successfully, while the following accessor(s) was(were) not uploaded because it(they) already exist.");
                    _adminViewModel.ReadExcelList = failedUpload;
                    return View(_adminViewModel);
                }

                SetMessage(successfulUpload.Count + " accessor(s) was(were) uploaded successfully.");
                return RedirectToAction("UploadAccessors");
            }
            return View("UploadAccessors");
        }
        public IActionResult ConceptNoteAccessorDistribution()
        {
            _adminViewModel.AccessorDistributionCount = new List<AccessorDistributionCount>();
            var categories = _context.Category.Where(f => f.Active).ToList();

            var response = GetCoAdminCategory();
            if (response != null && !response.IsAdmin && response.Category == null)
            {
                SetMessage("Category has not been assigned to you");
                return RedirectToAction("Index");
            }
            if (response != null && !response.IsAdmin && response.Category != null)
            {
                categories = new List<Category>();
                categories.Add(response.Category);
            }

            if (categories?.Count > 0)
            {
                foreach (var category in categories)
                {

                    var submittedConceptNote = _context.ConceptNote.Where(f => f.ApplicantThematic.Theme.CategoryId == category.Id && f.Submitted).ToList();
                    var assignedConceptNote = _context.ApplicantReviewer.Where(f => f.ApplicantThematic.Theme.CategoryId == category.Id && f.TypeId == (int)Types.ConceptNote)
                        .Select(f => f.ApplicantThematicId).Distinct().ToList();
                    AccessorDistributionCount accessorDistributionCount = new AccessorDistributionCount()
                    {
                        Category = category,
                        TotalAssigned = assignedConceptNote.Count,
                        TotalSubmission = submittedConceptNote.Count,
                        TotalUnassigned = submittedConceptNote.Count - assignedConceptNote.Count,
                    };
                    _adminViewModel.AccessorDistributionCount.Add(accessorDistributionCount);
                }
            }
            return View(_adminViewModel);
        }
        public IActionResult AutoDustribution()
        {
            JsonResultModel result = new JsonResultModel();
            List<Reviewer> reviewers = new List<Reviewer>();
            Category category = new Category();
            //determine if user is admin or coadmin
            var response = GetCoAdminCategory();
            if (response != null && !response.IsAdmin && response.Category != null)
            {
                category = response.Category;
            }

            var activeAccessors = _context.ActiveReviewer.Where(f => f.Session.ActiveForApplication && f.Active && f.Type == (int)Types.ConceptNote).Select(f => f.ReviewerId).ToList();
            if (activeAccessors?.Count <= 0)
            {
                result.IsError = false;
                result.Message = "No accessor is activated for concept note assessement for this research cycle.";
                return Json(result);
            }
            var allSubmittedConceptNoteForActiveSession = _context.ConceptNote.Where(f => f.ApplicantThematic.Session.ActiveForApplication && f.Submitted).ToList();
            if (allSubmittedConceptNoteForActiveSession?.Count > 0)
            {
                var groupByCategoryId = new List<IGrouping<int, ConceptNote>>();
                if (category?.Id > 0)
                {
                    groupByCategoryId = allSubmittedConceptNoteForActiveSession.Where(f => f.ApplicantThematic.Theme.Category.Id == category.Id).GroupBy(f => f.ApplicantThematic.Theme.CategoryId).ToList();
                }
                else
                {
                    groupByCategoryId = allSubmittedConceptNoteForActiveSession.GroupBy(f => f.ApplicantThematic.Theme.CategoryId).ToList();
                }


                foreach (var item in groupByCategoryId)
                {
                    var conceptNoteByCategory = allSubmittedConceptNoteForActiveSession.Where(f => f.ApplicantThematic.Session.ActiveForApplication && f.ApplicantThematic.Theme.Category.Id == item.Key).ToList();
                    if (conceptNoteByCategory?.Count > 0)
                    {
                        foreach (var conceptnote in conceptNoteByCategory)
                        {
                            var applicantReviewer = _context.ApplicantReviewer.Where(f => f.ApplicantThematicId == conceptnote.ApplicantThemeId).ToList();
                            int count = 0;
                            while (applicantReviewer?.Count <= 1)
                            {
                                //this is to ensure the loop did not run more than twice due to inability to find suitable accessor
                                count += 1;

                                var reviewer = GetAccessorForConceptNote(conceptnote.ApplicantThematic, activeAccessors);
                                if (reviewer?.Id > 0)
                                {
                                    CreateApplicantReviewer(reviewer, conceptnote.ApplicantThematic);
                                }
                                applicantReviewer = _context.ApplicantReviewer.Where(f => f.ApplicantThematicId == conceptnote.ApplicantThemeId).ToList();
                                if (applicantReviewer.Count >= 2 || count >= 2)
                                    break;
                            }
                        }
                    }
                }
                result.IsError = false;
                result.Message = "Distribution Completed";
                return Json(result);
            }
            result.IsError = false;
            result.Message = "No submission to distribute";
            return Json(result);
        }
        private Reviewer GetAccessorForConceptNote(ApplicantThematic applicantThematic, List<int> ActivatedAccessors)
        {
            var reviewer = new Reviewer();
            List<AccessorDistributionModel> accessorDistributionModelList = new List<AccessorDistributionModel>();

            var reviewStrenght = _context.ReviewerThematicStrenght.Where(f => f.Theme.Category.Id == applicantThematic.Theme.Category.Id && ActivatedAccessors.Contains(f.ReviewerId)).Select(f => f.Reviewer).Distinct().ToList();
            if (reviewStrenght?.Count > 0)
            {
                foreach (var item in reviewStrenght)
                {
                    var assignedConceptnotes = _context.ApplicantReviewer.Where(f => f.ReviewerId == item.Id
                      && f.ApplicantThematic.Session.ActiveForApplication && f.TypeId == (int)Types.ConceptNote).ToList();
                    AccessorDistributionModel accessorDistributionModel = new AccessorDistributionModel()
                    {
                        NoOfAssignment = assignedConceptnotes.Count,
                        ReviewerId = item.Id,
                        Reviewer = item
                    };
                    accessorDistributionModelList.Add(accessorDistributionModel);
                }
                var orderedaccessorDistributionModelList = accessorDistributionModelList.OrderBy(f => f.NoOfAssignment).ToList();
                if (orderedaccessorDistributionModelList?.Count > 0)
                {
                    foreach (var item in orderedaccessorDistributionModelList)
                    {
                        var alreadyAssigned = _context.ApplicantReviewer.Where(f => f.ApplicantThematicId == applicantThematic.Id && f.ReviewerId == item.ReviewerId).ToList();
                        if (applicantThematic.Applicant.Person.InstitutionId != item.Reviewer.InstitutionId && alreadyAssigned?.Count == 0)
                        {
                            return item.Reviewer;
                        }
                    }
                }
            }
            return reviewer;

        }
        private void CreateApplicantReviewer(Reviewer reviewer, ApplicantThematic applicantThematic)
        {
            ApplicantReviewer applicantReviewer = new ApplicantReviewer()
            {
                ApplicantThematic = applicantThematic,
                DateAssigned = DateTime.UtcNow.AddHours(-8),
                ReviewerId = reviewer.Id,
                TypeId = (int)Types.ConceptNote,

            };
            _context.Add(applicantReviewer);
            _context.SaveChanges();
        }
        public IActionResult ActivatAccessor(string cId)
        {
            try
            {
                if (!String.IsNullOrEmpty(cId))
                {
                    int categoryId = Convert.ToInt32(Utility.Decrypt(cId));
                    _adminViewModel.AccessorActivationList = new List<AccessorActivation>();

                    var reviewers = _context.ReviewerThematicStrenght.Where(f => f.Theme.CategoryId == categoryId).Select(f => f.Reviewer).Distinct().ToList();
                    if (reviewers?.Count > 0)
                    {
                        foreach (var reviewer in reviewers)
                        {
                            var exist = _context.ActiveReviewer.Where(f => f.ReviewerId == reviewer.Id && f.Session.ActiveForApplication && f.Active).FirstOrDefault();
                            AccessorActivation accessorActivation = new AccessorActivation()
                            {
                                Active = exist != null ? true : false,
                                Reviewer = reviewer,
                            };
                            _adminViewModel.AccessorActivationList.Add(accessorActivation);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult SaveActivation(List<ReviewerJsonObj> reviewerArray, int type)
        {
            JsonResultModel result = new JsonResultModel();
            if (reviewerArray?.Count > 0)
            {
                var session = _context.Session.Where(f => f.ActiveForApplication).FirstOrDefault();
                if (session == null)
                {
                    result.IsError = false;
                    result.Message = "No active research cycle for application!";
                    return Json(result);
                }
                foreach (var item in reviewerArray)
                {
                    var activeReviewer = _context.ActiveReviewer.Where(f => f.ReviewerId == item.Id).FirstOrDefault();
                    if (activeReviewer?.Id > 0)
                    {
                        activeReviewer.Active = item.Status;
                        _context.Update(activeReviewer);
                    }
                    else
                    {
                        activeReviewer = new ActiveReviewer()
                        {
                            Active = item.Status,
                            Session = session,
                            ReviewerId = item.Id,
                            Type = type
                        };
                        _context.Add(activeReviewer);
                    }
                }
                _context.SaveChanges();
                result.IsError = false;
                result.Message = "Operation was successful.";
                return Json(result);
            }
            else
            {
                result.IsError = false;
                result.Message = "No selection was made.";
                return Json(result);
            }
        }
        public IActionResult AssignedConceptNoteByCategory(string cId)
        {
            if (!String.IsNullOrEmpty(cId))
            {
                int categoryId = Convert.ToInt32(Utility.Decrypt(cId));
                _adminViewModel.PIAccessorSummary = new List<PIAccessorSummary>();
                var assignedConceptNote = _context.ApplicantReviewer.Where(f => f.ApplicantThematic.Session.ActiveForApplication
                  && f.ApplicantThematic.Theme.CategoryId == categoryId && f.TypeId == (int)Types.ConceptNote).ToList();
                if (assignedConceptNote?.Count > 0)
                {
                    var allreviewers = _context.Reviewer.ToList();
                    var distinctAccessor = assignedConceptNote.Select(f => f.ApplicantThematic).Distinct().ToList();
                    foreach (var item in distinctAccessor)
                    {
                        PIAccessorSummary pIAccessorSummary = new PIAccessorSummary();
                        var reviewerIds = assignedConceptNote.Where(f => f.ApplicantThematicId == item.Id)
                            .Select(f => f.ReviewerId).ToList();
                        pIAccessorSummary.Reviewers = allreviewers.Where(f => reviewerIds.Contains(f.Id)).ToList();
                        pIAccessorSummary.ApplicantThematic = item;
                        _adminViewModel.PIAccessorSummary.Add(pIAccessorSummary);
                    }
                }
            }
            return View(_adminViewModel);

        }
        public IActionResult SubmittedConceptNoteByCategory(string cId)
        {
            if (!String.IsNullOrEmpty(cId))
            {
                int categoryId = Convert.ToInt32(Utility.Decrypt(cId));
                _adminViewModel.PIAccessorSummary = new List<PIAccessorSummary>();
                var submittedConceptNote = _context.ConceptNote.Where(f => f.ApplicantThematic.Session.ActiveForApplication
                  && f.ApplicantThematic.Theme.CategoryId == categoryId && f.Submitted).Select(f => f.ApplicantThematic).ToList();

                var assignedConceptNote = _context.ApplicantReviewer.Where(f => f.ApplicantThematic.Session.ActiveForApplication
                  && f.ApplicantThematic.Theme.CategoryId == categoryId && f.TypeId == (int)Types.ConceptNote).ToList();
                if (submittedConceptNote?.Count > 0)
                {
                    var allreviewers = _context.Reviewer.ToList();
                    foreach (var item in submittedConceptNote)
                    {
                        PIAccessorSummary pIAccessorSummary = new PIAccessorSummary();
                        var reviewerIds = assignedConceptNote.Where(f => f.ApplicantThematicId == item.Id)
                            .Select(f => f.ReviewerId).ToList();
                        pIAccessorSummary.Reviewers = allreviewers.Where(f => reviewerIds.Contains(f.Id)).ToList();
                        pIAccessorSummary.ApplicantThematic = item;
                        _adminViewModel.PIAccessorSummary.Add(pIAccessorSummary);
                    }
                }
            }
            return View(_adminViewModel);
        }
        public void SetDropdown()
        {
            string adminEmail = User.FindFirstValue(ClaimTypes.Email);
            var admin = _context.User.Where(d => d.Email == adminEmail).FirstOrDefault();
            if (admin?.Id > 0)
            {
                if (admin.Role.Name == "Admin" || admin.Role.Name == "Super-Admin")
                {
                    _adminViewModel.PopulateReviewerExpertriateCategory(0);
                }
                else
                {
                    var coAdminCategory = _context.CategoryAdministrator.Where(F => F.UserId == admin.Id && F.Active).FirstOrDefault();
                    if (coAdminCategory?.Id > 0)
                    {
                        _adminViewModel.PopulateReviewerExpertriateCategory(coAdminCategory.CategoryId);
                    }
                }
            }
        }
        public AssigedCategory GetCoAdminCategory()
        {
            AssigedCategory assigedCategory = new AssigedCategory();
            string adminEmail = User.FindFirstValue(ClaimTypes.Email);
            var admin = _context.User.Where(d => d.Email == adminEmail).FirstOrDefault();
            if (admin?.Id > 0)
            {
                if (admin.Role.Name != "Admin" && admin.Role.Name != "Super-Admin")
                {

                    var coAdminCategory = _context.CategoryAdministrator.Where(F => F.UserId == admin.Id && F.Active).Select(f => f.Category).FirstOrDefault();
                    assigedCategory.Category = coAdminCategory;
                    return assigedCategory;


                }
                else
                {
                    assigedCategory.IsAdmin = true;
                    return assigedCategory;
                }

            }
            return assigedCategory;
        }
        public IActionResult DiscordedConceptNote()
        {
            _adminViewModel.ApplicantThematics = new List<ApplicantThematic>();
            try
            {
                List<ApplicantReviewer> reviewedConceptNote = new List<ApplicantReviewer>();
                var response = GetCoAdminCategory();
                if (response != null && !response.IsAdmin && response.Category == null)
                {
                    SetMessage("Category has not been assigned to you");
                    return RedirectToAction("Index");
                }
                if (response != null && !response.IsAdmin && response.Category != null)
                {
                    reviewedConceptNote = _context.ApplicantReviewer.Where(f => f.ApplicantThematic.Session.ActiveForApplication && f.TypeId == (int)Types.ConceptNote
                    && f.ApplicantThematic.Theme.CategoryId == response.Category.Id && !f.IsReconciled && f.ConceptNoteMark!=null).ToList();
                }
                else
                {
                    reviewedConceptNote = _context.ApplicantReviewer.Where(f => f.ApplicantThematic.Session.ActiveForApplication && f.TypeId == (int)Types.ConceptNote && !f.IsReconciled && f.ConceptNoteMark != null).ToList();
                }

                if (reviewedConceptNote?.Count > 0)
                {
                    var distinctApplication = reviewedConceptNote.Select(f => f.ApplicantThematic).Distinct().ToList();
                    foreach (var item in distinctApplication)
                    {
                        var applicationReview = reviewedConceptNote.Where(f => f.ApplicantThematicId == item.Id).ToList();
                        if (applicationReview?.Count > 1)
                        {
                            if ((applicationReview.Max(f => f.ConceptNoteMark) - applicationReview.Min(f => f.ConceptNoteMark) > 10))
                            {
                                _adminViewModel.ApplicantThematics.Add(item);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult ViewReconciliation(string id)
        {
            try
            {
                if (!String.IsNullOrEmpty(id))
                {
                    int applicantThematicId = Convert.ToInt32(Utility.Decrypt(id));
                    _adminViewModel.ApplicantReviewerViewModel = _context.ApplicantReviewer.Where(f => f.ApplicantThematicId == applicantThematicId && f.TypeId == (int)Types.ConceptNote)
                        .Select(f => new ApplicantReviewerViewModel
                        {
                            ApplicantThematic = f.ApplicantThematic,
                            ConceptNoteMark = f.ConceptNoteMark,
                            DateAssigned = f.DateAssigned,
                            DateReviewed = f.DateReviewed,
                            Id = f.Id,
                            Reviewer = _context.Reviewer.Where(x => x.Id == f.ReviewerId).FirstOrDefault(),
                            ScoreId = f.ScoreId,
                            Type = f.Type
                        })
                        .ToList();
                    _adminViewModel.Reconciliation= _context.Reconciliation.Where(f => f.ApplicantThematicId == applicantThematicId).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult Reconcile(string id, string tid)
        {
            try
            {
                string redirectId = id;
                if (!String.IsNullOrEmpty(id))
                {
                    int applicantThematicId = Convert.ToInt32(Utility.Decrypt(id));
                    int typeId = Convert.ToInt32(Utility.Decrypt(tid));
                    if(applicantThematicId>0 && typeId == (int)Types.ConceptNote)
                    {
                        var allAssessment=_context.ApplicantReviewer.Where(f => f.ApplicantThematicId == applicantThematicId).ToList();
                        if (allAssessment?.Count > 0)
                        {
                            Reconciliation reconciliation = new Reconciliation()
                            {
                                ApplicantThematicId = applicantThematicId,
                                DateReconcilled = DateTime.Now,
                                ReconcilliationAverageScore = allAssessment.Average(f => f.ConceptNoteMark),
                                User = GetLoggedInUser(),
                                Type=(int)Types.ConceptNote

                            };
                            _context.Add(reconciliation);
                            foreach(var item in allAssessment)
                            {
                                item.IsReconciled = true;
                                _context.Update(item);
                            }
                            var reconciled=_context.SaveChanges();
                            if (reconciled > 0)
                            {
                                SetMessage("Operation was successful");
                                return RedirectToAction("ViewReconciliation", new { id = redirectId });
                            }
                        }
                    }
                    else if(applicantThematicId > 0 && typeId == (int)Types.Proposal)
                    {

                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            SetMessage("something went wrong. ");
            return RedirectToAction("DiscordedConceptNote");
        }
        public IActionResult AdminRecociledConceptNote()
        {
            _adminViewModel.Reconciliations = new List<Reconciliation>();
            try
            {
                List<ApplicantReviewer> reviewedConceptNote = new List<ApplicantReviewer>();
                var response = GetCoAdminCategory();
                if (response != null && !response.IsAdmin && response.Category == null)
                {
                    SetMessage("Category has not been assigned to you");
                    return RedirectToAction("Index");
                }
                if (response != null && !response.IsAdmin && response.Category != null)
                {
                    _adminViewModel.Reconciliations = _context.Reconciliation.Where(f => f.ApplicantThematic.Session.ActiveForApplication && f.Type == (int)Types.ConceptNote
                    && f.ApplicantThematic.Theme.CategoryId == response.Category.Id ).ToList();
                }
                else
                {
                    _adminViewModel.Reconciliations = _context.Reconciliation.Where(f => f.ApplicantThematic.Session.ActiveForApplication && f.Type == (int)Types.ConceptNote).ToList();
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(_adminViewModel);
        }
        public IActionResult AdminRecociledProposal()
        {
            _adminViewModel.Reconciliations = new List<Reconciliation>();
            try
            {
                List<ApplicantReviewer> reviewedConceptNote = new List<ApplicantReviewer>();
                var response = GetCoAdminCategory();
                if (response != null && !response.IsAdmin && response.Category == null)
                {
                    SetMessage("Category has not been assigned to you");
                    return RedirectToAction("Index");
                }
                if (response != null && !response.IsAdmin && response.Category != null)
                {
                    _adminViewModel.Reconciliations = _context.Reconciliation.Where(f => f.ApplicantThematic.Session.ActiveForApplication && f.Type == (int)Types.Proposal
                    && f.ApplicantThematic.Theme.CategoryId == response.Category.Id).ToList();
                }
                else
                {
                    _adminViewModel.Reconciliations = _context.Reconciliation.Where(f => f.ApplicantThematic.Session.ActiveForApplication && f.Type == (int)Types.Proposal).ToList();
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(_adminViewModel);
        }
        public User GetLoggedInUser()
        {
            string adminEmail = User.FindFirstValue(ClaimTypes.Email);
            return _context.User.Where(d => d.Email == adminEmail).FirstOrDefault();
        }
    }
}