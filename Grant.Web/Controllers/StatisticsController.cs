﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grant.Web.Infrastructure;
using Grant.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Grant.Web.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace Grant.Web.Controllers
{
    [Authorize(Policy = "MustBeAdmin")]
    public class StatisticsController : BaseController
    {
        private readonly DataBaseContext _context;
        Utility _utility;
        StatisticsViewModel _viewModel;
        private const string Value = "Id";
        private const string Text = "Name";

        public StatisticsController(DataBaseContext context, Utility utility)
        {
            _context = context;
            _utility = utility;
            _viewModel = new StatisticsViewModel();
            _viewModel.PopulateAllDropdown(_context);
        }

        public IActionResult Index()
        {
            try
            {
                return View();
            }
            catch(Exception ex) { throw ex; }
        }

        [HttpGet]
        public IActionResult GetApplicantGraphicalData()
        {
            try
            {
                StatisticsViewModel viewModel = _utility.ApplicantGraphicalReport();
                return Json(viewModel);
            }
            catch(Exception ex) { throw ex; }
        }
        public IActionResult ConceptNoteSubmitSummary()
        {
            return View(_viewModel);
        }
        [HttpPost]
        public IActionResult ConceptNoteSubmitSummary(int sessionId)
        {
            if (sessionId <= 0)
            {
                SetMessage("Please you must select research cycle.");
                return View(_viewModel);
            }
            var submittedConceptNote=_context.ConceptNote.Where(f => f.ApplicantThematic.SessionId == sessionId && f.Submitted).ToList();
            _viewModel.SubmissionSummary = new List<SubmissionSummary>();
            if (submittedConceptNote?.Count > 0)
            {
                _viewModel.SessionName = submittedConceptNote.FirstOrDefault().ApplicantThematic.Session.Name;
                var categoryGroup=submittedConceptNote.Select(f => f.ApplicantThematic.Theme.Category).Distinct().ToList();
                foreach(var item in categoryGroup)
                {
                    SubmissionSummary submissionSummary = new SubmissionSummary();
                    submissionSummary.Category = item;
                    var themeGroup=submittedConceptNote.Where(f => f.ApplicantThematic.Theme.CategoryId == item.Id).Select(f => f.ApplicantThematic.Theme).Distinct().ToList();
                    submissionSummary.ThematicArea = new List<ThematicArea>();
                    foreach (var theme in themeGroup)
                    {
                        
                        ThematicArea thematicArea = new ThematicArea();
                        thematicArea.ThemeName = theme.Name;
                        thematicArea.Count=submittedConceptNote.Where(f => f.ApplicantThematic.ThemeId == theme.Id).ToList().Count();
                        submissionSummary.ThematicArea.Add(thematicArea);
                    }
                    submissionSummary.TotalCount = submissionSummary.ThematicArea.Sum(f => f.Count);
                    _viewModel.SubmissionSummary.Add(submissionSummary);
;                }
            }
            return View(_viewModel);

        }
    }
}