﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Grant.Web.Models;
using Grant.Web.Infrastructure;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Grant.Web.Models.Interface;

namespace Grant.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Configure Database Context Services
            services.AddDbContext<DataBaseContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddScoped(typeof(Utility));

            services.AddScoped<IFluentEmailLogic, FluentEmailLogic>();
            //fluent email
            services
           .AddFluentEmail("support@elearn.ng")
           .AddMailGunSender(
               Configuration.GetValue<string>("MailGun:domain"),
               Configuration.GetValue<string>("MailGun:apiKey"),
               FluentEmail.Mailgun.MailGunRegion.EU
               )
           .AddRazorRenderer();

            //services.ConfigureApplicationCookie(options =>
            //{
            //    //options.ExpireTimeSpan = TimeSpan.FromMinutes(1);
            //    //options.LoginPath = "/Home/Index";
            //    ////options.LogoutPath = "/Home/Logout";
            //    //options.SlidingExpiration = true;
            //    //options.ReturnUrlParameter = "/Home/Index";


            //    options.Cookie.HttpOnly = true;
            //    options.ExpireTimeSpan = TimeSpan.FromMinutes(1);
            //    options.LoginPath = "/Home/Index";
            //    options.AccessDeniedPath = "/Identity/Account/AccessDenied";
            //    options.SlidingExpiration = true;
            //    //options.Cookie.Name = GlobalConstants.AuthCookieName;



            //});




            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(options => {
                options.AccessDeniedPath = "/Home/Logout"; //Replace with Error page for Access Denied
                options.LoginPath = "/Home/index"; //Replace with Error Page for Not Logged in
                options.LogoutPath = "/Home/Logout";
            });

            services.ConfigureApplicationCookie(options =>
            {
                options.AccessDeniedPath = "/Home/Index";
                //options.Cookie.Name = "YourAppCookieName";
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(10);
                options.LoginPath = "/Home/Logout";
                // ReturnUrlParameter requires 
                //using Microsoft.AspNetCore.Authentication.Cookies;
                options.ReturnUrlParameter = CookieAuthenticationDefaults.ReturnUrlParameter;
                options.SlidingExpiration = true;
            });
            //adding authorization policy
            services.AddAuthorization(options => {
                options.AddPolicy("MustBeAdmin", p => p.RequireAuthenticatedUser().RequireRole("Admin", "Co-Admin","Super-Admin"));
                options.AddPolicy("MustBeReviewer", p => p.RequireAuthenticatedUser().RequireRole("Reviewer"));
                options.AddPolicy("MustBeApplicant", p => p.RequireAuthenticatedUser().RequireRole("Applicant", "Co-Applicant"));
                //options.AddPolicy("RequireElevatedRights", policy => policy.RequireRole("SuperAdministrator", "ChannelAdministrator"));
                options.AddPolicy("MustBePI", p => p.RequireAuthenticatedUser().RequireRole("Applicant"));
            });
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddMvc(options =>
            options.EnableEndpointRouting = false)
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            //.AddJsonOptions(opt=>opt.SerializerSettings.DateTimeZoneHandling=Newtonsoft.Json.DateTimeZoneHandling.Utc);

            services.AddSingleton<ITempDataProvider, CookieTempDataProvider>();
            //services.Configure<CookieTempDataProviderOptions>(options =>
            //{
            //    options.Cookie.IsEssential = true;
            //});
            //services.AddMemoryCache();
            services.AddSession();
            services.AddTransient<IMenuDisplay, MenuDisplay>();
            services.AddMvc().AddXmlSerializerFormatters();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<DataBaseContext>();
                context.Database.Migrate();
            }


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseDeveloperExceptionPage(); //Temp
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
                //app.UseExceptionHandler("/Home/Error");
                //// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                //app.UseHsts();
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();
            
            app.UseAuthentication();
            //app.UseCookiePolicy();
            app.UseMvc(routes =>
            {
                routes.MapAreaRoute(
                    name: "Admin",
                    areaName:"Admin",
                    template: "Admin/{controller=Home}/{action=Index}/{id?}");

                routes.MapAreaRoute(
                    name: "User",
                    areaName: "User",
                    template: "User/{controller=UserHome}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            app.UseSession();
            app.UseCookiePolicy();

        }
    }
}
