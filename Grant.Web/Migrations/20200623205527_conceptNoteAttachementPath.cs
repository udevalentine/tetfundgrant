﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Grant.Web.Migrations
{
    public partial class conceptNoteAttachementPath : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AttachmentFive",
                table: "ConceptNote",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AttachmentFour",
                table: "ConceptNote",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AttachmentOne",
                table: "ConceptNote",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AttachmentThree",
                table: "ConceptNote",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AttachmentTwo",
                table: "ConceptNote",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastLogin",
                value: new DateTime(2020, 6, 23, 20, 55, 19, 246, DateTimeKind.Utc).AddTicks(7335));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 2,
                column: "LastLogin",
                value: new DateTime(2020, 6, 23, 20, 55, 19, 246, DateTimeKind.Utc).AddTicks(8314));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AttachmentFive",
                table: "ConceptNote");

            migrationBuilder.DropColumn(
                name: "AttachmentFour",
                table: "ConceptNote");

            migrationBuilder.DropColumn(
                name: "AttachmentOne",
                table: "ConceptNote");

            migrationBuilder.DropColumn(
                name: "AttachmentThree",
                table: "ConceptNote");

            migrationBuilder.DropColumn(
                name: "AttachmentTwo",
                table: "ConceptNote");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastLogin",
                value: new DateTime(2020, 6, 21, 6, 33, 27, 279, DateTimeKind.Utc).AddTicks(4537));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 2,
                column: "LastLogin",
                value: new DateTime(2020, 6, 21, 6, 33, 27, 279, DateTimeKind.Utc).AddTicks(5480));
        }
    }
}
