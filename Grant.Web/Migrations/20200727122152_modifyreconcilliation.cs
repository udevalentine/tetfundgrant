﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Grant.Web.Migrations
{
    public partial class modifyreconcilliation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "Reconciliation",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastLogin",
                value: new DateTime(2020, 7, 27, 12, 21, 49, 108, DateTimeKind.Utc).AddTicks(7833));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 2,
                column: "LastLogin",
                value: new DateTime(2020, 7, 27, 12, 21, 49, 108, DateTimeKind.Utc).AddTicks(8803));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Type",
                table: "Reconciliation");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastLogin",
                value: new DateTime(2020, 7, 27, 11, 41, 35, 927, DateTimeKind.Utc).AddTicks(5822));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 2,
                column: "LastLogin",
                value: new DateTime(2020, 7, 27, 11, 41, 35, 927, DateTimeKind.Utc).AddTicks(6722));
        }
    }
}
