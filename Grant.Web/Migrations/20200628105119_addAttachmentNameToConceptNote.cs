﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Grant.Web.Migrations
{
    public partial class addAttachmentNameToConceptNote : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "NameFive",
                table: "ConceptNote",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NameFour",
                table: "ConceptNote",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NameOne",
                table: "ConceptNote",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NameThree",
                table: "ConceptNote",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NameTwo",
                table: "ConceptNote",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastLogin",
                value: new DateTime(2020, 6, 28, 10, 51, 17, 8, DateTimeKind.Utc).AddTicks(4708));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 2,
                column: "LastLogin",
                value: new DateTime(2020, 6, 28, 10, 51, 17, 8, DateTimeKind.Utc).AddTicks(5246));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NameFive",
                table: "ConceptNote");

            migrationBuilder.DropColumn(
                name: "NameFour",
                table: "ConceptNote");

            migrationBuilder.DropColumn(
                name: "NameOne",
                table: "ConceptNote");

            migrationBuilder.DropColumn(
                name: "NameThree",
                table: "ConceptNote");

            migrationBuilder.DropColumn(
                name: "NameTwo",
                table: "ConceptNote");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastLogin",
                value: new DateTime(2020, 6, 23, 20, 55, 19, 246, DateTimeKind.Utc).AddTicks(7335));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 2,
                column: "LastLogin",
                value: new DateTime(2020, 6, 23, 20, 55, 19, 246, DateTimeKind.Utc).AddTicks(8314));
        }
    }
}
