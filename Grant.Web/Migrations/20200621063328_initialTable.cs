﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Grant.Web.Migrations
{
    public partial class initialTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AdminSettings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ViewReview = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdminSettings", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BudgetItem",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BudgetItem", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Category",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DefaultMessage",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Message = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    ProposalApprove = table.Column<bool>(nullable: false),
                    isProposal = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DefaultMessage", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EventType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FundingSource",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FundingSource", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GeoPoliticalZone",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeoPoliticalZone", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "InstitutionCategory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstitutionCategory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MenuGroup",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MenuGroup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Qualification",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Qualification", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Quarter",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Quarter", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Rank",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rank", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Score",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Score", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Session",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    ActiveForApplication = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Session", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sex",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sex", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Title",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Title", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Type",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Type", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Theme",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Theme", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Theme_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "State",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    GeoPoliticalZoneId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_State", x => x.Id);
                    table.ForeignKey(
                        name: "FK_State_GeoPoliticalZone_GeoPoliticalZoneId",
                        column: x => x.GeoPoliticalZoneId,
                        principalTable: "GeoPoliticalZone",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Menu",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Controller = table.Column<string>(nullable: true),
                    ActionName = table.Column<string>(nullable: true),
                    DisplayName = table.Column<string>(nullable: true),
                    Menu_GroupId = table.Column<int>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Menu", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Menu_MenuGroup_Menu_GroupId",
                        column: x => x.Menu_GroupId,
                        principalTable: "MenuGroup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ResearchCycleEvent",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EventTypeId = table.Column<int>(nullable: false),
                    SessionId = table.Column<int>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResearchCycleEvent", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ResearchCycleEvent_EventType_EventTypeId",
                        column: x => x.EventTypeId,
                        principalTable: "EventType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ResearchCycleEvent_Session_SessionId",
                        column: x => x.SessionId,
                        principalTable: "Session",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Institution",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    IsPublic = table.Column<bool>(nullable: false),
                    InstitutionCategoryId = table.Column<int>(nullable: false),
                    StateId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Institution", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Institution_InstitutionCategory_InstitutionCategoryId",
                        column: x => x.InstitutionCategoryId,
                        principalTable: "InstitutionCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Institution_State_StateId",
                        column: x => x.StateId,
                        principalTable: "State",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MenuInRole",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<int>(nullable: false),
                    MenuId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MenuInRole", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MenuInRole_Menu_MenuId",
                        column: x => x.MenuId,
                        principalTable: "Menu",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MenuInRole_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Person",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Email = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    PhoneNo = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    QualificationId = table.Column<int>(nullable: false),
                    InstitutionId = table.Column<int>(nullable: false),
                    PassportUrl = table.Column<string>(nullable: true),
                    TitleId = table.Column<int>(nullable: false),
                    SexId = table.Column<int>(nullable: false),
                    Age = table.Column<int>(nullable: false),
                    DOB = table.Column<DateTime>(nullable: false),
                    StateId = table.Column<int>(nullable: false),
                    RankId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Person", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Person_Institution_InstitutionId",
                        column: x => x.InstitutionId,
                        principalTable: "Institution",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Person_Qualification_QualificationId",
                        column: x => x.QualificationId,
                        principalTable: "Qualification",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Person_Rank_RankId",
                        column: x => x.RankId,
                        principalTable: "Rank",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Person_Sex_SexId",
                        column: x => x.SexId,
                        principalTable: "Sex",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Person_State_StateId",
                        column: x => x.StateId,
                        principalTable: "State",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Person_Title_TitleId",
                        column: x => x.TitleId,
                        principalTable: "Title",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Applicant",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApplicationNo = table.Column<string>(nullable: true),
                    PersonId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false),
                    LastLogin = table.Column<DateTime>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    Password = table.Column<string>(nullable: false),
                    IsVerified = table.Column<bool>(nullable: false),
                    DateVerified = table.Column<DateTime>(nullable: true),
                    OneTimeValidator = table.Column<string>(nullable: true),
                    IsLoggedIn = table.Column<bool>(nullable: false),
                    DeviceName = table.Column<string>(nullable: true),
                    DeviceMacAddress = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Applicant", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Applicant_Person_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Person",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Applicant_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ApplicantThematic",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApplicantId = table.Column<int>(nullable: false),
                    ThemeId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    SessionId = table.Column<int>(nullable: false),
                    DateApplied = table.Column<DateTime>(nullable: false),
                    FileNo = table.Column<string>(nullable: true),
                    ApprovalId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicantThematic", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ApplicantThematic_Applicant_ApplicantId",
                        column: x => x.ApplicantId,
                        principalTable: "Applicant",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ApplicantThematic_Session_SessionId",
                        column: x => x.SessionId,
                        principalTable: "Session",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ApplicantThematic_Theme_ThemeId",
                        column: x => x.ThemeId,
                        principalTable: "Theme",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ApplicantReviewer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ReviewerId = table.Column<int>(nullable: false),
                    ApplicantThematicId = table.Column<int>(nullable: false),
                    DateAssigned = table.Column<DateTime>(nullable: false),
                    ScoreId = table.Column<int>(nullable: true),
                    DateReviewed = table.Column<DateTime>(nullable: true),
                    ConceptNoteMark = table.Column<decimal>(nullable: true),
                    TypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicantReviewer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ApplicantReviewer_ApplicantThematic_ApplicantThematicId",
                        column: x => x.ApplicantThematicId,
                        principalTable: "ApplicantThematic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ApplicantReviewer_Type_TypeId",
                        column: x => x.TypeId,
                        principalTable: "Type",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ApplicantSubmission",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApplicantThematicId = table.Column<int>(nullable: false),
                    TypeId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    ProposalTitle = table.Column<string>(nullable: true),
                    ExecutiveSummary = table.Column<string>(nullable: true),
                    IntroductionBackground = table.Column<string>(nullable: true),
                    Aims = table.Column<string>(nullable: true),
                    StatementOfProblem = table.Column<string>(nullable: true),
                    ConceptualFramework = table.Column<string>(nullable: true),
                    ProjectGoal = table.Column<string>(nullable: true),
                    ProjectImpact = table.Column<string>(nullable: true),
                    LiteratureReview = table.Column<string>(nullable: true),
                    ResearchMethodoly = table.Column<string>(nullable: true),
                    ProjectActivities = table.Column<string>(nullable: true),
                    TimeFrame = table.Column<string>(nullable: true),
                    ActivityIndicator = table.Column<string>(nullable: true),
                    StudyLocation = table.Column<string>(nullable: true),
                    DataManagement = table.Column<string>(nullable: true),
                    Ethical = table.Column<string>(nullable: true),
                    Monitoring = table.Column<string>(nullable: true),
                    DisseminationStrategies = table.Column<string>(nullable: true),
                    ResearchWorkToDate = table.Column<string>(nullable: true),
                    PreviousGrant = table.Column<string>(nullable: true),
                    GroupResearch = table.Column<string>(nullable: true),
                    BudgetJustification = table.Column<string>(nullable: true),
                    AdditionalSources = table.Column<string>(nullable: true),
                    ResearchQuestions = table.Column<string>(nullable: true),
                    ExpectedResult = table.Column<string>(nullable: true),
                    Innovations = table.Column<string>(nullable: true),
                    References = table.Column<string>(nullable: true),
                    TeamMates = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicantSubmission", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ApplicantSubmission_ApplicantThematic_ApplicantThematicId",
                        column: x => x.ApplicantThematicId,
                        principalTable: "ApplicantThematic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BudgetDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApplicantThematicId = table.Column<int>(nullable: false),
                    BudgetItemId = table.Column<int>(nullable: false),
                    TetFundAmount = table.Column<decimal>(nullable: true),
                    InstitutionFundingAmount = table.Column<decimal>(nullable: true),
                    OtherFunding = table.Column<decimal>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    TypeId = table.Column<int>(nullable: false),
                    Total = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BudgetDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BudgetDetail_ApplicantThematic_ApplicantThematicId",
                        column: x => x.ApplicantThematicId,
                        principalTable: "ApplicantThematic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BudgetDetail_BudgetItem_BudgetItemId",
                        column: x => x.BudgetItemId,
                        principalTable: "BudgetItem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BudgetDetail_Type_TypeId",
                        column: x => x.TypeId,
                        principalTable: "Type",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BulletinTopic",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Topic = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    PersonId = table.Column<int>(nullable: false),
                    ApplicantThematicId = table.Column<int>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BulletinTopic", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BulletinTopic_ApplicantThematic_ApplicantThematicId",
                        column: x => x.ApplicantThematicId,
                        principalTable: "ApplicantThematic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BulletinTopic_Person_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Person",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ConceptNote",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NoteUrl = table.Column<string>(nullable: true),
                    ApplicantThemeId = table.Column<int>(nullable: false),
                    ConceptNoteSubmittedBudget = table.Column<decimal>(nullable: false),
                    ConceptNoteSummary = table.Column<string>(nullable: true),
                    Submitted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConceptNote", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ConceptNote_ApplicantThematic_ApplicantThemeId",
                        column: x => x.ApplicantThemeId,
                        principalTable: "ApplicantThematic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProjectActivities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Activity = table.Column<string>(nullable: true),
                    Outcome = table.Column<string>(nullable: true),
                    ApplicantThematicId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectActivities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectActivities_ApplicantThematic_ApplicantThematicId",
                        column: x => x.ApplicantThematicId,
                        principalTable: "ApplicantThematic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Proposal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProposalUrl = table.Column<string>(nullable: true),
                    ApplicantThemeId = table.Column<int>(nullable: false),
                    SubmittedBudget = table.Column<decimal>(nullable: false),
                    Submitted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Proposal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Proposal_ApplicantThematic_ApplicantThemeId",
                        column: x => x.ApplicantThemeId,
                        principalTable: "ApplicantThematic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Team",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PersonId = table.Column<int>(nullable: false),
                    ApplicantThematicId = table.Column<int>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Team", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Team_ApplicantThematic_ApplicantThematicId",
                        column: x => x.ApplicantThematicId,
                        principalTable: "ApplicantThematic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Team_Person_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Person",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TimeFrame",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApplicantThematicId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Year = table.Column<string>(nullable: true),
                    Duration = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TimeFrame", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TimeFrame_ApplicantThematic_ApplicantThematicId",
                        column: x => x.ApplicantThematicId,
                        principalTable: "ApplicantThematic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Reviewer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Email = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    PhoneNo = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    TitleId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false),
                    InstitutionId = table.Column<int>(nullable: false),
                    ApplicantReviewerId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reviewer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Reviewer_ApplicantReviewer_ApplicantReviewerId",
                        column: x => x.ApplicantReviewerId,
                        principalTable: "ApplicantReviewer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Reviewer_Institution_InstitutionId",
                        column: x => x.InstitutionId,
                        principalTable: "Institution",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Reviewer_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Reviewer_Title_TitleId",
                        column: x => x.TitleId,
                        principalTable: "Title",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BulletinResponse",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BulletinTopicId = table.Column<long>(nullable: false),
                    PersonId = table.Column<int>(nullable: false),
                    ResponseMessage = table.Column<string>(nullable: true),
                    Upload = table.Column<string>(nullable: true),
                    FileName = table.Column<string>(nullable: true),
                    ResponseDate = table.Column<DateTime>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BulletinResponse", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BulletinResponse_BulletinTopic_BulletinTopicId",
                        column: x => x.BulletinTopicId,
                        principalTable: "BulletinTopic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BulletinResponse_Person_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Person",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "QuaterData",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    QuarterId = table.Column<int>(nullable: false),
                    Year = table.Column<string>(nullable: true),
                    TimeFrameId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuaterData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuaterData_TimeFrame_TimeFrameId",
                        column: x => x.TimeFrameId,
                        principalTable: "TimeFrame",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Approval",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApplicantThematicId = table.Column<int>(nullable: false),
                    AverageScore = table.Column<decimal>(nullable: false),
                    Approved = table.Column<bool>(nullable: false),
                    ReviewerId = table.Column<int>(nullable: false),
                    IsProposal = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Approval", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Approval_Reviewer_ReviewerId",
                        column: x => x.ReviewerId,
                        principalTable: "Reviewer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProposalScoring",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApplicantThematicId = table.Column<int>(nullable: false),
                    ReviewerId = table.Column<int>(nullable: false),
                    TypeId = table.Column<int>(nullable: false),
                    TitleScore = table.Column<int>(nullable: true),
                    ExecutiveSummaryScore = table.Column<int>(nullable: true),
                    IntroductionBackgroundScore = table.Column<int>(nullable: true),
                    AimsScore = table.Column<int>(nullable: true),
                    StatementOfProblemScore = table.Column<int>(nullable: true),
                    ConceptualFrameworkScore = table.Column<int>(nullable: true),
                    ProjectGoalScore = table.Column<int>(nullable: true),
                    ProjectImpactScore = table.Column<int>(nullable: true),
                    LiteratureReviewScore = table.Column<int>(nullable: true),
                    ResearchMethodolyScore = table.Column<int>(nullable: true),
                    ProjectActivities = table.Column<int>(nullable: true),
                    TimeFrameScore = table.Column<int>(nullable: true),
                    ActivityIndicatorScore = table.Column<int>(nullable: true),
                    StudyLocationScore = table.Column<int>(nullable: true),
                    DataManagementScore = table.Column<int>(nullable: true),
                    EthicalScore = table.Column<int>(nullable: true),
                    MonitoringScore = table.Column<int>(nullable: true),
                    DisseminationStrategiesScore = table.Column<int>(nullable: true),
                    ResearchWorkToDateScore = table.Column<int>(nullable: true),
                    PreviousGrantScore = table.Column<int>(nullable: true),
                    GroupResearchScore = table.Column<int>(nullable: true),
                    BudgetJustificationScore = table.Column<int>(nullable: true),
                    AdditionalSourcesScore = table.Column<int>(nullable: true),
                    DateReviewed = table.Column<DateTime>(nullable: false),
                    Remark = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProposalScoring", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProposalScoring_ApplicantThematic_ApplicantThematicId",
                        column: x => x.ApplicantThematicId,
                        principalTable: "ApplicantThematic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProposalScoring_Reviewer_ReviewerId",
                        column: x => x.ReviewerId,
                        principalTable: "Reviewer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ReviewerThematicStrenght",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ReviewerId = table.Column<int>(nullable: false),
                    ThemeId = table.Column<int>(nullable: false),
                    Strenght = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReviewerThematicStrenght", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ReviewerThematicStrenght_Reviewer_ReviewerId",
                        column: x => x.ReviewerId,
                        principalTable: "Reviewer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ReviewerThematicStrenght_Theme_ThemeId",
                        column: x => x.ThemeId,
                        principalTable: "Theme",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Email = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    RoleId = table.Column<int>(nullable: false),
                    LastLogin = table.Column<DateTime>(nullable: false),
                    ReviewerId = table.Column<int>(nullable: false),
                    OneTimeValidator = table.Column<string>(nullable: true),
                    Token = table.Column<string>(nullable: true),
                    IsLoggedIn = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                    table.ForeignKey(
                        name: "FK_User_Reviewer_ReviewerId",
                        column: x => x.ReviewerId,
                        principalTable: "Reviewer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_User_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProposalAssessment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApplicantThematicId = table.Column<int>(nullable: false),
                    ReviewerId = table.Column<int>(nullable: false),
                    ApprovedBudget = table.Column<decimal>(nullable: true),
                    Approved = table.Column<bool>(nullable: false),
                    Score = table.Column<decimal>(nullable: false),
                    ProposalId = table.Column<int>(nullable: false),
                    ReasonForDissapproval = table.Column<string>(nullable: true),
                    Remark = table.Column<string>(nullable: true),
                    ProposalScoringId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProposalAssessment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProposalAssessment_ApplicantThematic_ApplicantThematicId",
                        column: x => x.ApplicantThematicId,
                        principalTable: "ApplicantThematic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProposalAssessment_Proposal_ProposalId",
                        column: x => x.ProposalId,
                        principalTable: "Proposal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProposalAssessment_ProposalScoring_ProposalScoringId",
                        column: x => x.ProposalScoringId,
                        principalTable: "ProposalScoring",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProposalAssessment_Reviewer_ReviewerId",
                        column: x => x.ReviewerId,
                        principalTable: "Reviewer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Budget",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CategoryId = table.Column<int>(nullable: false),
                    SessionId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    AmountDispensed = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Budget", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Budget_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Budget_Session_SessionId",
                        column: x => x.SessionId,
                        principalTable: "Session",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Budget_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CategoryAdministrator",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    DateTime = table.Column<DateTime>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryAdministrator", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CategoryAdministrator_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CategoryAdministrator_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FinalProposalAproval",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApprovedAmount = table.Column<decimal>(nullable: true),
                    ApplicantThematicId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    IsApproved = table.Column<bool>(nullable: false),
                    Remark = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FinalProposalAproval", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FinalProposalAproval_ApplicantThematic_ApplicantThematicId",
                        column: x => x.ApplicantThematicId,
                        principalTable: "ApplicantThematic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FinalProposalAproval_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AssessmentGuide",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    MaxScore = table.Column<decimal>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    ProposalAssessmentId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssessmentGuide", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AssessmentGuide_ProposalAssessment_ProposalAssessmentId",
                        column: x => x.ProposalAssessmentId,
                        principalTable: "ProposalAssessment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "AssessmentGuide",
                columns: new[] { "Id", "Active", "MaxScore", "Name", "ProposalAssessmentId" },
                values: new object[,]
                {
                    { 1, true, 1m, "Title", null },
                    { 2, true, 4m, "Executive Summary", null },
                    { 3, true, 10m, "Project Team", null },
                    { 4, true, 15m, "Project Profile", null },
                    { 5, true, 10m, "Justification for Project", null },
                    { 6, true, 25m, "Strategies for Implementation", null },
                    { 7, true, 5m, "Time Frame (Phases/Time Lines)", null },
                    { 8, true, 5m, "Environment/Other Social Issues", null },
                    { 9, true, 22m, "Resource Implications", null },
                    { 10, true, 3m, "Ethic Committee", null }
                });

            migrationBuilder.InsertData(
                table: "BudgetItem",
                columns: new[] { "Id", "Active", "Name" },
                values: new object[,]
                {
                    { 7, true, "Others/Misc" },
                    { 6, true, "Dissemination" },
                    { 5, true, "Travels" },
                    { 3, true, "Supplies/Consumables" },
                    { 2, true, "Equipment(List and Specify)" },
                    { 1, true, "Personnel/Allowances" },
                    { 4, true, "Data Collection and Analysis" }
                });

            migrationBuilder.InsertData(
                table: "Category",
                columns: new[] { "Id", "Active", "Code", "Description", "Name" },
                values: new object[,]
                {
                    { 1, true, "HSS", "Humanities and Social Sciences (HSS)", "Humanities and Social Sciences (HSS)" },
                    { 2, true, "SETI", "Science, Technology and Innovation (SETI)", "Science, Technology and Innovation (SETI)" },
                    { 3, true, "CC", "Cross-Cutting (CC)", "Cross-Cutting (CC)" }
                });

            migrationBuilder.InsertData(
                table: "DefaultMessage",
                columns: new[] { "Id", "Active", "Message", "ProposalApprove", "isProposal" },
                values: new object[] { 1, true, "Thank you for applying to the National Research Fund. Unfortunately, your team did not make the cut for this funding round.We received a large number of submissions this year,and we have spent the last few weeks reviewing over 1000 applications from some of the brightest minds across Nigeria.Sadly,taking the availablilty of funds into cognizance we are limited by the number of submissions we can accept.This was a really hard decision,and we hope that you do not get discouraged by it.Thanks again for taking the time out to apply, and we wish your team continued success as you keep building innovative and enterprising solutions to critical problems on the continent.", false, true });

            migrationBuilder.InsertData(
                table: "EventType",
                columns: new[] { "Id", "Active", "Name" },
                values: new object[,]
                {
                    { 7, true, "Assign Assessor - Full Proposal" },
                    { 10, true, "System Setup" },
                    { 9, true, "Approval/Rejection of Full Proposal" },
                    { 8, true, "Assessment of Full Proposal" },
                    { 6, true, "Set Budget" },
                    { 4, true, "Assessment of Concept Note" },
                    { 3, true, "Assign Assessor - Concept Note" },
                    { 2, true, "Proposal Submission" },
                    { 1, true, "Concept Note Submission" },
                    { 5, true, "Approval/Rejection of Concept Note" }
                });

            migrationBuilder.InsertData(
                table: "FundingSource",
                columns: new[] { "Id", "Active", "Name" },
                values: new object[,]
                {
                    { 1, true, "TetFund" },
                    { 2, true, "Institution" },
                    { 3, true, "Others" }
                });

            migrationBuilder.InsertData(
                table: "GeoPoliticalZone",
                columns: new[] { "Id", "Active", "Name" },
                values: new object[,]
                {
                    { 5, true, "South South" },
                    { 6, true, "South West" },
                    { 4, true, "South East" },
                    { 2, true, "North East" },
                    { 1, true, "North Central" },
                    { 3, true, "North West" }
                });

            migrationBuilder.InsertData(
                table: "InstitutionCategory",
                columns: new[] { "Id", "Active", "Name" },
                values: new object[,]
                {
                    { 1, true, "University" },
                    { 2, true, "Polytechnic" },
                    { 3, true, "College Of Education" }
                });

            migrationBuilder.InsertData(
                table: "MenuGroup",
                columns: new[] { "Id", "Active", "Name" },
                values: new object[,]
                {
                    { 11, true, "Researchers" },
                    { 12, true, "Research Proposals" },
                    { 10, true, "Bulletin Board" },
                    { 14, true, "User Manual" },
                    { 15, true, "Research Cycle Events" },
                    { 13, true, "Dashboard" },
                    { 9, true, "Budget" },
                    { 2, true, "Full Proposal" },
                    { 7, true, "Manage Menu" },
                    { 6, true, "Statistics" },
                    { 5, true, "Report" },
                    { 4, true, "Setup" },
                    { 3, true, "Assessor" },
                    { 8, true, "Profile" },
                    { 1, true, "Concept Note" }
                });

            migrationBuilder.InsertData(
                table: "Qualification",
                columns: new[] { "Id", "Active", "Name" },
                values: new object[,]
                {
                    { 8, true, "Others." },
                    { 7, true, "PhD." },
                    { 6, true, "OND." },
                    { 5, true, "MSc." },
                    { 2, true, "BSc." },
                    { 3, true, "HND." },
                    { 1, true, "B.A" },
                    { 4, true, "M.A" }
                });

            migrationBuilder.InsertData(
                table: "Rank",
                columns: new[] { "Id", "Active", "Name" },
                values: new object[,]
                {
                    { 4, true, "Senior Lecturer" },
                    { 3, true, "Reader/Associate Prof." },
                    { 5, true, "Principal Lecturer" },
                    { 1, true, "Chief Lecturer" },
                    { 2, true, "Professor" }
                });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "Active", "Name" },
                values: new object[,]
                {
                    { 1, true, "Reviewer" },
                    { 2, true, "Super-Admin" },
                    { 3, true, "Applicant" },
                    { 4, true, "Co-Applicant" },
                    { 5, true, "Co-Admin" },
                    { 6, true, "Admin" }
                });

            migrationBuilder.InsertData(
                table: "Score",
                columns: new[] { "Id", "Active", "Name" },
                values: new object[,]
                {
                    { 2, true, "Fail" },
                    { 3, true, "To Be Reconciled" },
                    { 1, true, "Pass" }
                });

            migrationBuilder.InsertData(
                table: "Sex",
                columns: new[] { "Id", "Active", "Name" },
                values: new object[,]
                {
                    { 1, true, "Male" },
                    { 2, true, "Female" }
                });

            migrationBuilder.InsertData(
                table: "Title",
                columns: new[] { "Id", "Active", "Name" },
                values: new object[,]
                {
                    { 1, true, "Prof." },
                    { 2, true, "Dr." },
                    { 3, true, "Mr." },
                    { 4, true, "Mrs." },
                    { 5, true, "Ms." },
                    { 6, true, "Miss." }
                });

            migrationBuilder.InsertData(
                table: "Type",
                columns: new[] { "Id", "Active", "Name" },
                values: new object[,]
                {
                    { 1, true, "ConceptNote" },
                    { 2, true, "Proposal" }
                });

            migrationBuilder.InsertData(
                table: "State",
                columns: new[] { "Id", "Active", "GeoPoliticalZoneId", "Name" },
                values: new object[,]
                {
                    { 26, true, 1, "Nassarawa" },
                    { 2, true, 2, "Adamawa" },
                    { 5, true, 2, "Bauchi" },
                    { 8, true, 2, "Borno" },
                    { 16, true, 2, "Gombe" },
                    { 35, true, 2, "Taraba" },
                    { 36, true, 2, "Yobe" },
                    { 18, true, 3, "Jigawa" },
                    { 19, true, 3, "Kaduna" },
                    { 20, true, 3, "Kano" },
                    { 21, true, 3, "Katsina" },
                    { 22, true, 3, "Kebbi" },
                    { 34, true, 3, "Sokoto" },
                    { 37, true, 3, "Zamfara" },
                    { 38, true, 3, "Nigeria" },
                    { 1, true, 4, "Abia" },
                    { 4, true, 4, "Anambra" },
                    { 11, true, 4, "Ebonyi" },
                    { 14, true, 4, "Enugu" },
                    { 3, true, 5, "Akwa-Ibom" },
                    { 6, true, 5, "Bayelsa" },
                    { 9, true, 5, "Cross-Rivers" },
                    { 10, true, 5, "Delta" },
                    { 12, true, 5, "Edo" },
                    { 17, true, 5, "Imo" },
                    { 33, true, 5, "Rivers" },
                    { 13, true, 6, "Ekiti" },
                    { 25, true, 6, "Lagos" },
                    { 28, true, 6, "Ogun" },
                    { 29, true, 6, "Ondo" },
                    { 32, true, 1, "Plateau" },
                    { 27, true, 1, "Niger" },
                    { 31, true, 6, "Oyo" },
                    { 24, true, 1, "Kwara" },
                    { 23, true, 1, "Kogi" },
                    { 15, true, 1, "FCT Abuja" },
                    { 7, true, 1, "Benue" },
                    { 30, true, 6, "Osun" }
                });

            migrationBuilder.InsertData(
                table: "Theme",
                columns: new[] { "Id", "Active", "CategoryId", "Code", "Description", "Name" },
                values: new object[,]
                {
                    { 12, true, 2, "GEO", "Geosciences", "Geosciences" },
                    { 11, true, 2, "WAS", "Water & Sanitation", "Water & Sanitation" },
                    { 25, true, 1, "SDW", "Social Development and Welfare", "Social Development and Welfare" },
                    { 24, true, 1, "GES", "Gender, Equity and Social Inclusion", "Gender, Equity and Social Inclusion" },
                    { 23, true, 1, "GPL", "Governance, Politics, Law and Ethics", "Governance, Politics, Law and Ethics" },
                    { 8, true, 1, "PAM", "Population and Migration", "Population and Migration" },
                    { 9, true, 1, "TSR", "Tourism, Sports and Recreation", "Tourism, Sports and Recreation" },
                    { 7, true, 1, "HCI", "History, Culture and Identities", "History, Culture and Identities" },
                    { 6, true, 1, "LLM", "Languages, Literatures and Media", "Languages, Literatures and Media" },
                    { 5, true, 1, "HST", "Humanities, Social Sciences, Technology and Business Interface", "Humanities, Social Sciences, Technology and Business Interface" },
                    { 2, true, 1, "EDG", "Economic Development and Globalisation", "Economic Development and Globalisation" },
                    { 10, true, 1, "EHC", "Education and Human Capital", "Education and Human Capital" },
                    { 13, true, 2, "SST", "Space Science and Technology", "Space Science and Technology" },
                    { 15, true, 2, "TRT", "Transport", "Transport" },
                    { 16, true, 2, "HSW", "Health and Social Welfare", "Health and Social Welfare" },
                    { 17, true, 2, "ICT", "IT, Computing & Telecommunications", "IT, Computing & Telecommunications" },
                    { 22, true, 2, "SAE", "Science & Engineering", "Science & Engineering" },
                    { 26, true, 2, "AFS", "Agriculture and Food Security", "Agriculture and Food Security" },
                    { 27, true, 2, "III", "Industry, Innovation and Infrastructure", "Industry, Innovation and Infrastructure" },
                    { 28, true, 2, "NRT", "Sustainable Use of Natural Resources and Terrestrial Ecosystems", "Sustainable Use of Natural Resources and Terrestrial Ecosystems" },
                    { 3, true, 3, "REG", "Resource Governance", "Resource Governance" },
                    { 4, true, 3, "EWC", "Entrepreneurship & Wealth Creation", "Entrepreneurship & Wealth Creation" },
                    { 18, true, 3, "BEC", "Blue Economy", "Blue Economy" },
                    { 19, true, 3, "STI", "Science,Technology and  Innovation System Management", "Science,Technology and  Innovation System Management" },
                    { 20, true, 3, "CAE", "Clean and Affordable Energy", "Clean and Affordable Energy" },
                    { 21, true, 3, "EHU", "Environment, Housing & Urban and Regional Development", "Environment, Housing & Urban and Regional Development" },
                    { 14, true, 2, "PAE", "Power and Energy", "Power and Energy" },
                    { 1, true, 1, "CDS", "National Integration, Conflict, Defence and Security", "National Integration, Conflict, Defence and Security" }
                });

            migrationBuilder.InsertData(
                table: "Institution",
                columns: new[] { "Id", "Active", "InstitutionCategoryId", "IsPublic", "Name", "StateId" },
                values: new object[,]
                {
                    { 21, true, 2, true, "Benue State Polytechnic", 7 },
                    { 3, true, 1, true, "University of Portharcourt", 33 },
                    { 289, true, 3, true, "Alvan Ikoku Federal College of Education, Owerri ", 17 },
                    { 176, true, 1, false, "Hezekiah University, Umudi ", 17 },
                    { 111, true, 1, true, "Eastern Palm University, Ogboko ", 17 },
                    { 73, true, 1, true, "Imo State University, Owerri ", 17 },
                    { 49, true, 1, true, "Federal University of Technology, Owerri", 17 },
                    { 25, true, 2, true, "Imo State Polytechnic", 17 },
                    { 5, true, 2, true, "Federal Polytechnic Nekede", 17 },
                    { 361, true, 3, true, "Edo State College of Education, Igueben	Igueben, Edo State ", 12 },
                    { 320, true, 3, true, "College of Education, Ekiadolor-Benin ", 12 },
                    { 271, true, 2, false, "Shaka Polytechnic, Edo State. ", 12 },
                    { 260, true, 2, false, "Lighthouse Polytechnic, Edo State ", 12 },
                    { 256, true, 2, false, "Kings Polytechnic,Edo State ", 12 },
                    { 201, true, 2, true, "Edo State Institute of Technology and Management, Usen ", 12 },
                    { 166, true, 1, false, "Wellspring University, Benin City ", 12 },
                    { 149, true, 1, false, "Samuel Adegboyega University, Ogwa ", 12 },
                    { 136, true, 1, false, "Igbinedion University Okada ", 12 },
                    { 92, true, 1, true, "Edo University, Iyamho ", 12 },
                    { 80, true, 1, true, "Ambrose Alli University, Ekpoma ", 12 },
                    { 8, true, 1, true, "Rivers State University", 33 },
                    { 48, true, 1, true, "Rivers State University, Port Harcourt ", 33 },
                    { 77, true, 1, true, "Ignatius Ajuru University of Education, Port Harcourt ", 33 },
                    { 159, true, 1, false, "PAMO University of Medical Sciences, Port-Harcourt ", 33 },
                    { 182, true, 1, false, "Eko University of Medical and Health Sciences, Ijanikin ", 25 },
                    { 143, true, 1, false, "Anchor University, Lagos ", 25 },
                    { 133, true, 1, false, "Caleb University, Imota ", 25 },
                    { 125, true, 1, false, "Pan-Atlantic University, Lagos ", 25 },
                    { 46, true, 1, true, "Lagos State University, Ojo ", 25 },
                    { 37, true, 1, true, "University of Lagos,Lagos ", 25 },
                    { 313, true, 3, true, "College of Education, Ikere-Ekiti ", 13 },
                    { 254, true, 2, false, "Interlink Polytechnic, Ado-Ekiti ", 13 },
                    { 242, true, 2, false, "Crown Polytechnic, Ado-Ekiti ", 13 },
                    { 43, true, 1, true, "University of Benin, Benin City ", 12 },
                    { 235, true, 2, false, "Ajayi PolytechnicIkere Ekiti,Ekiti State ", 13 },
                    { 54, true, 1, true, "Ekiti State University, Ado Ekiti ", 13 },
                    { 44, true, 1, true, "Federal University, Oye-Ekiti", 13 },
                    { 7, true, 2, true, "Federal Polytechnic, Ado", 13 },
                    { 343, true, 3, true, "Rivers College of Education, Rumuolumeni ", 33 },
                    { 306, true, 3, true, "Federal College of Education (Technical), Omoku,, Rivers State ", 33 },
                    { 301, true, 3, true, "Federal College of Education (Technical), Akoka ", 33 },
                    { 244, true, 2, false, "Eastern Polytechnic, Rivers State ", 33 },
                    { 226, true, 2, true, "Port-Harcourt Polytechnic, Rivers State. ", 33 },
                    { 210, true, 2, true, "Ken Sarowiwa Polytechnic, Bori ", 33 },
                    { 118, true, 1, false, "Afe Babalola University, Ado-Ekiti ", 13 },
                    { 213, true, 2, true, "Lagos State Polytechnic, Ikorodu ", 25 },
                    { 12, true, 1, false, "Benson Idahosa University", 12 },
                    { 333, true, 3, true, "Delta State College of Education, Agbor	Agbor, Delta State ", 10 },
                    { 195, true, 2, true, "Bayelsa State College of Arts and Science, Elebele ", 6 },
                    { 107, true, 1, true, "University of Africa, Toru-Orua ", 6 },
                    { 95, true, 1, true, "Niger Delta University, Wilberforce Island ", 6 },
                    { 9, true, 1, true, "Federal University Otuoke", 6 },
                    { 334, true, 3, true, "Akwa Ibom State College of Education,Afahansit, Akwa Ibom State. ", 3 },
                    { 284, true, 2, false, "Uyo City Polytechnic, Akwa Ibom State ", 3 },
                    { 282, true, 2, false, "Trinity Polytechnic Uyo,Akwa Ibom State ", 3 },
                    { 274, true, 2, false, "Sure Foundation Polytechnic, Akwa Ibom State ", 3 },
                    { 251, true, 2, false, "Heritage Polytechnic, Ikot Udota, Eket,Akwa Ibom State ", 3 },
                    { 194, true, 2, true, "Akwa Ibom State Polytechnic,Ikot-Ekpene ", 3 },
                    { 193, true, 2, true, "Akwa Ibom State College of Art & Science, Nung Ukim ", 3 },
                    { 146, true, 1, false, "Obong University, Obong Ntak ", 3 },
                    { 144, true, 1, false, "Ritman University, Ikot Ekpene ", 3 },
                    { 86, true, 1, true, "Akwa Ibom State University,Ikot Akpad ", 3 },
                    { 57, true, 1, true, "University of Uyo, Uyo ", 3 },
                    { 20, true, 2, true, "Akwa-Ibom State Polytechnic", 3 },
                    { 379, true, 3, false, "The College of Education, Nsukka, Enugu	 ", 14 },
                    { 378, true, 3, false, "Peaceland College of Education, Enugu	 ", 14 },
                    { 371, true, 3, false, "African Thinkers Community of Inquiry, Enugu ", 14 },
                    { 362, true, 3, true, "Isaac Jasper Boro COE, Sagbama	Sagbama, Yenogoa, Bayelsa Stat	 ", 6 },
                    { 23, true, 2, true, "The Polytechnic,Calabar", 9 },
                    { 78, true, 1, true, "University of Calabar, Calabar ", 9 },
                    { 90, true, 1, true, "Cross River University of Technology, Calabar ", 9 },
                    { 315, true, 3, true, "College of Education, Warri ", 10 },
                    { 302, true, 3, true, "Federal College of Education (Technical), Asaba ", 10 },
                    { 239, true, 2, false, "Calvary Polytechnic, Owa-OyibuDelta State ", 10 },
                    { 200, true, 2, true, "Delta State School of Marine Technology, Burutu ", 10 },
                    { 199, true, 2, true, "Delta State Polytechnic, Ogwashi-Uku ", 10 },
                    { 169, true, 1, false, "Edwin Clark University, Kiagbodo ", 10 },
                    { 162, true, 1, false, "Admiralty University of Nigeria, Ibusa ", 10 },
                    { 158, true, 1, false, "Novena University, Ogume ", 10 },
                    { 156, true, 1, false, "Western Delta University, Oghara ", 10 },
                    { 350, true, 3, true, "Delta State Coll. of Physical Education, Mosogar ", 10 },
                    { 151, true, 1, false, "Michael and Cecilia Ibru University, Agbara-Otor ", 10 },
                    { 85, true, 1, true, "Delta State University, Abraka ", 10 },
                    { 35, true, 1, true, "Federal University of Petroleum, Resources", 10 },
                    { 28, true, 3, true, "Federal College of Education, Asaba ", 10 },
                    { 24, true, 2, true, "Delta State Polytechnic", 10 },
                    { 360, true, 3, true, "Cross River State Coll. of Education, Akampa ", 9 },
                    { 295, true, 3, true, "Federal College of Education, Obudu, Cross River State ", 9 },
                    { 264, true, 2, false, "Nogak Polytechnic, Ikom,Cross Rivers State ", 9 },
                    { 207, true, 2, true, "Institute of Technology and Management (ITM),Ikom-Calabar ", 9 },
                    { 181, true, 1, false, "Arthur Jarvis University, Calabar ", 9 },
                    { 112, true, 1, true, "Nigerian Maritime University, Okerenkoko ", 10 },
                    { 369, true, 3, false, "OSISA Tech. Coll. of Education, Enugu ", 14 },
                    { 248, true, 2, false, "Grace Polytechnic, Surulere ", 25 },
                    { 258, true, 2, false, "Lagos City Polytechnic, Ikeja ", 25 },
                    { 286, true, 2, false, "Villanova Polytechnic, Osun State. ", 30 },
                    { 278, true, 2, false, "The Polytechnic Iresi, Osun State ", 30 },
                    { 277, true, 2, false, "The Polytechnic, Osun State. ", 30 },
                    { 253, true, 2, false, "Igbajo Polytechnic, Osun State ", 30 },
                    { 223, true, 2, true, "Osun State Polytechnic, Osun State ", 30 },
                    { 222, true, 2, true, "Osun State College of Technology, Osun State ", 30 },
                    { 160, true, 1, false, "Kings University, Odeomu ", 30 },
                    { 147, true, 1, false, "Oduduwa University, Ile Ife ", 30 },
                    { 140, true, 1, false, "Joseph Ayo Babalola University, Ikeji-Arakeji ", 30 },
                    { 139, true, 1, false, "Fountain University, Osogbo ", 30 },
                    { 126, true, 1, false, "Bowen University, Iwo ", 30 },
                    { 123, true, 1, false, "Redeemer's University, Ede ", 30 },
                    { 68, true, 1, true, "Osun State University, Oshogbo ", 30 },
                    { 39, true, 1, true, "Obafemi Awolowo University,Ile-Ife ", 30 },
                    { 13, true, 1, false, "Adeleke University", 30 },
                    { 288, true, 3, true, "Adeyemi College of Education, Ondo State ", 29 },
                    { 247, true, 2, false, "Global Polytechnic, Akure,Ondo State ", 29 },
                    { 237, true, 2, false, "Best Solution Polytechnic, Akure ", 29 },
                    { 228, true, 2, true, "Rufus Giwa Polytechnic, Owo ", 29 },
                    { 287, true, 2, false, "Wolex Polytechnic, Osun State ", 30 },
                    { 323, true, 3, true, "Osun State College of Education, Ilesa ", 30 },
                    { 347, true, 3, true, "College of Education, Ila-Orangun, Osun State ", 30 },
                    { 27, true, 3, true, "Federal College of Education, Oyo ", 31 },
                    { 330, true, 3, true, "Emmanuel Alayande College of Education (EACOED), Oyo ", 31 },
                    { 309, true, 3, true, "Federal College of Education (Special), Oyo ", 31 },
                    { 281, true, 2, false, "Tower Polytechnic, Ibadan. ", 31 },
                    { 269, true, 2, false, "Saf Polytechnic,P.O. Box 55 Iseyin Oyo State ", 31 },
                    { 252, true, 2, false, "Ibadan City Polytechnic, Ibadan Oyo State ", 31 },
                    { 238, true, 2, false, "Bolmor Polytechnic, Oyo State. ", 31 },
                    { 230, true, 2, true, "The Polytechnic Ibadan, Oyo State. ", 31 },
                    { 224, true, 2, true, "Oyo State College of Agriculture and Technology, Igbo Ora ", 31 },
                    { 221, true, 2, true, "Oke-Ogun Polytechnic, Shaki,Oyo State ", 31 },
                    { 172, true, 1, false, "Augustine University, Ilara ", 29 },
                    { 205, true, 2, true, "Ibarapa Polytechnic, Eruwa Oyo ", 31 },
                    { 187, true, 1, false, "Precious Cornerstone University, Ibadan ", 31 },
                    { 185, true, 1, false, "Atiba University, Oyo Town ", 31 },
                    { 179, true, 1, false, "Dominican University, Ibadan ", 31 },
                    { 153, true, 1, false, "Kola Daisi University, Ibadan ", 31 },
                    { 135, true, 1, false, "Lead City University, Ibadan ", 31 },
                    { 134, true, 1, false, "Ajayi Crowther University, Oyo Town ", 31 },
                    { 76, true, 1, true, "The Technical University, Ibadan ", 31 },
                    { 47, true, 1, true, "Ladoke Akintola University of Technology, Ogbomoso ", 31 },
                    { 38, true, 1, true, "University of Ibadan,Ibadan ", 31 },
                    { 190, true, 2, true, "Abraham Adesanya Polytechnic, Dogbolu/Akanran Ibadan Road, Atikori, Ijebu Igbo ", 31 },
                    { 255, true, 2, false, "Kalac Christal Polytechnic, Lagos State. ", 25 },
                    { 163, true, 1, false, "Wesley University of Science and Technology, Ondo City ", 29 },
                    { 102, true, 1, true, "Ondo State University of Science and Technology, Okitipupa ", 29 },
                    { 129, true, 1, false, "Bells University of Technology, Ota ", 28 },
                    { 120, true, 1, false, "Babcock University, Ilishan-Remo ", 28 },
                    { 117, true, 1, false, "Covenant University, Ota ", 28 },
                    { 113, true, 1, true, "Moshood Abiola University of Science and Technology, Abeokuta ", 28 },
                    { 58, true, 1, true, "Tai Solarin University of Education, Ijebu-Ode ", 28 },
                    { 45, true, 1, true, "Olabisi Onabanjo University, Ago Iwoye ", 28 },
                    { 33, true, 3, false, "Piaget College of Education, Abeokuta ", 28 },
                    { 16, true, 1, true, "Federal University of Agricultutre, Abeokuta", 28 },
                    { 11, true, 1, false, "Badcock University", 28 },
                    { 4, true, 2, true, "Federal Polytechnic Ilaro", 28 },
                    { 384, true, 2, true, "Yaba College of Technology, Lagos", 25 },
                    { 377, true, 3, false, "Corner Stone College of Education, lagos ", 25 },
                    { 370, true, 3, false, "St. Augustine Coll. of Education,Akoka, Lagos	 ", 25 },
                    { 367, true, 3, false, "Ansar-Ud-Deen College of Education, Isolo, Lagos State. ", 25 },
                    { 348, true, 3, true, "Michael Otedola Coll. of Prim. Education, Lagos ", 25 },
                    { 332, true, 3, true, "St. Augustine College of Education (Project Time), Lagos ", 25 },
                    { 329, true, 3, true, "Adeniran Ogunsanya College of Education, Otto/Ijanikin ", 25 },
                    { 280, true, 2, false, "Timeon Kairos Polytechnic, Lagos ", 25 },
                    { 268, true, 2, false, "Ronik Polytechnic, Lagos State. ", 25 },
                    { 132, true, 1, false, "Mountain Top University, Makogi Oba ", 28 },
                    { 145, true, 1, false, "Crescent University, Abeokuta ", 28 },
                    { 150, true, 1, false, "Crawford University, Faith City ", 28 },
                    { 167, true, 1, false, "Christopher University, Mowe ", 28 },
                    { 97, true, 1, true, "University of Medical Sciences, Ondo City ", 29 },
                    { 56, true, 1, true, "Adekunle Ajasin University, Akungba Akoko ", 29 },
                    { 41, true, 1, true, "Federal University of Technology, Akure", 29 },
                    { 15, true, 1, false, "Elizade University", 29 },
                    { 368, true, 3, false, "Yewa Central College of Education, Ayetoro, Ogun State.	 ", 28 },
                    { 312, true, 3, true, "Tai Solarin College of Education, Ijebu-Ode ", 28 },
                    { 290, true, 3, true, "Federal College of Education, Ogun State ", 28 },
                    { 272, true, 2, false, "Speedway Polytechnic, Ogun State ", 28 },
                    { 267, true, 2, false, "Redeemers College of Technology and Management, Ogun State", 28 },
                    { 141, true, 1, false, "Achievers University, Owo ", 29 },
                    { 257, true, 2, false, "Landmark Polytechnic, Ogun State ", 28 },
                    { 220, true, 2, true, "Ogun State Polytechnic, Ipokia ", 28 },
                    { 219, true, 2, true, "Ogun State Institute of Technology, IgbesaOba ", 28 },
                    { 215, true, 2, true, "Moshood Abiola Polytechnic, Abeokuta ", 28 },
                    { 203, true, 2, true, "Gateway Polytechnic, Saapade ", 28 },
                    { 198, true, 2, true, "D.S. Adegbenro ICT Polytechnic, Itori-Ewekoro ", 28 },
                    { 178, true, 1, false, "Hallmark University, Ijebu-Itele ", 28 },
                    { 173, true, 1, false, "Southwestern University, Nigeria ", 28 },
                    { 170, true, 1, false, "Chrisland University, Abeokuta ", 28 },
                    { 168, true, 1, false, "Mcpherson University, Seriki-Sotayo ", 28 },
                    { 234, true, 2, false, "Allover Central Polytechnic, Sango Ota ", 28 },
                    { 364, true, 3, false, "Institute of Ecumenical Education, (Thinkers Corner), Enugu ", 14 },
                    { 359, true, 3, true, "Enugu State Coll. of Education (T), Enugu ", 14 },
                    { 317, true, 3, true, "Osisatech College of Education, Enugu ", 14 },
                    { 87, true, 1, true, "Modibbo Adama University of Technology, Yola ", 2 },
                    { 18, true, 2, true, "Federal Polytechnic, Mubi", 2 },
                    { 17, true, 2, true, "Adamawa State Polytechnic", 2 },
                    { 383, true, 3, false, "ECWA College of Education, Jos  ", 32 },
                    { 310, true, 3, true, "College of Education,Gindiri, Plateau State. ", 32 },
                    { 297, true, 3, true, "Federal College of Education, Plateau State. ", 32 },
                    { 225, true, 2, true, "Plateau State Polytechnic, Barkin-Ladi ", 32 },
                    { 98, true, 1, true, "Plateau State University, Bokkos ", 32 },
                    { 42, true, 1, true, "University of Jos, Jos", 32 },
                    { 342, true, 3, true, "Niger State College of Education, Minna ", 27 },
                    { 294, true, 3, true, "Federal College of Education, Kontagora Niger State. ", 27 },
                    { 273, true, 2, false, "St. Mary Polytechnic, Niger State ", 27 },
                    { 217, true, 2, true, "Niger State Polytechnic, Zungeru ", 27 },
                    { 81, true, 1, true, "Ibrahim Badamasi Babangida University, Lapai ", 27 },
                    { 50, true, 1, true, "Federal University of Technology, Minna", 27 },
                    { 366, true, 3, false, "City College of Education, Mararaba,Nasarawa Stat ", 26 },
                    { 318, true, 3, true, "Nasarrawa State College of Education, Akwanga ", 26 },
                    { 263, true, 2, false, "Nacabs Polytechnic, Nasarawa State ", 26 },
                    { 233, true, 2, false, "Al-Hikma Polytechnic, Nasarawa State ", 26 },
                    { 104, true, 1, true, "Adamawa State University, Mubi ", 2 },
                    { 122, true, 1, false, "American University of Nigeria, Yola ", 2 },
                    { 192, true, 2, true, "Adamawa State Polytechnic, Yola ", 2 },
                    { 298, true, 3, true, "Fed. College of Educ. Yola ", 2 },
                    { 91, true, 1, true, "Federal University, Wukari", 35 },
                    { 89, true, 1, true, "Taraba State University, Jalingo ", 35 },
                    { 304, true, 3, true, "Federal College Education (Technical), Gombe ", 16 },
                    { 116, true, 1, true, "Gombe State University of Science and Technology ", 16 },
                    { 105, true, 1, true, "Gombe State University, Gombe ", 16 },
                    { 101, true, 1, true, "Federal University, Kashere ", 16 },
                    { 349, true, 3, true, "Kashim Ibrahim College of Educ., Maiduguri ", 8 },
                    { 338, true, 3, true, "Umar Ibn Ibrahim El-Kanemi College of Education, Science and Technology,Bama, Borno State. ", 8 },
                    { 331, true, 3, true, "College of Education,Waka BIU, Borno State ", 8 },
                    { 216, true, 2, true, "Nasarawa State Polytechnic, Nasarawa State. ", 26 },
                    { 227, true, 2, true, "Ramat Polytechnic, Maiduguri ", 8 },
                    { 66, true, 1, true, "University of Maiduguri, Maiduguri ", 8 },
                    { 22, true, 2, false, "Ramata Polytechnic", 8 },
                    { 376, true, 3, false, "Bauchi Institute of Arabic & Islamic Studies, Bauchi ", 5 },
                    { 337, true, 3, true, "College of Education, Azare, Bauchi State ", 5 },
                    { 191, true, 2, true, "Abubakar Tatari Ali Polytechnic, Bauchi ", 5 },
                    { 93, true, 1, true, "Bauchi State University,Gadau ", 5 },
                    { 51, true, 1, true, "Abubakar Tafawa Balewa University,Bauchi ", 5 },
                    { 19, true, 2, true, "Federal Polytechnic,Bauchi", 5 },
                    { 311, true, 3, true, "Adamawa State College of Education,Yola, Adamawa State ", 2 },
                    { 114, true, 1, true, "Borno State University, Maiduguri ", 8 },
                    { 164, true, 1, false, "Kwararafa University, Wukari ", 35 },
                    { 138, true, 1, false, "Bingham University,Auta Balifi ", 26 },
                    { 65, true, 1, true, "Nasarawa State University, Keffi ", 26 },
                    { 31, true, 3, true, "Federal College of Education, Okene ", 23 },
                    { 316, true, 3, true, "FCT College of Education Garki, FCT Abuja ", 15 },
                    { 243, true, 2, false, "Dorben Polytechnic, Abuja ", 15 },
                    { 240, true, 2, false, "Citi PolytechnicPlot 182, FCT Abuja ", 15 },
                    { 131, true, 1, false, "Veritas University, Abuja ", 15 },
                    { 130, true, 1, false, "Baze University, Abuja ", 15 },
                    { 127, true, 1, false, "African University of Science and Technology, Abuja ", 15 },
                    { 121, true, 1, false, "Nile University of Nigeria, Abuja ", 15 },
                    { 55, true, 1, true, "University of Abuja,Abuja ", 15 },
                    { 1, true, 1, true, "University of Abuja", 15 },
                    { 380, true, 3, false, "Unity College of Education, Auka Adoka, Benue ", 7 },
                    { 279, true, 2, false, "The Polytechnic, Benue State ", 7 },
                    { 246, true, 2, false, "Gboko Polytechnic, Gboko ", 7 },
                    { 245, true, 2, false, "Fidei Polytechnic, Benue State. ", 7 },
                    { 236, true, 2, false, "Ashi Polytechnic Anyiin, Benue State ", 7 },
                    { 196, true, 2, true, "Benue State Polytechnic, Ugbokolo ", 7 },
                    { 142, true, 1, false, "University of Mkar, Mkar ", 7 },
                    { 67, true, 1, true, "University of Agriculture, Makurdi", 7 },
                    { 59, true, 1, true, "Benue State University, Makurdi ", 7 },
                    { 82, true, 1, true, "Federal University, Lokoja", 23 },
                    { 94, true, 1, true, "Kogi State University, Anyigba ", 23 },
                    { 157, true, 1, false, "Salem University, Lokoja ", 23 },
                    { 211, true, 2, true, "Kogi State Polytechnic, Lokoja ", 23 },
                    { 382, true, 3, false, "Kinsey College of Education, Ilorin, Kwara State ", 24 },
                    { 375, true, 3, false, "College of Education, Offa, Kwara State	 ", 24 },
                    { 374, true, 3, false, "Muhyideen College of Education, Ilorin ", 24 },
                    { 327, true, 3, true, "College of Education (Technical) Lafiagi, Kwara State. ", 24 },
                    { 324, true, 3, true, "Kwara State College of Education, Ilorin ", 24 },
                    { 300, true, 3, true, "Nigerian Army School of Education,Ilorin, Kwara State ", 24 },
                    { 276, true, 2, false, "The Polytechnic, Kwara State ", 24 },
                    { 259, true, 2, false, "Lens Polytechnic, Kwara State ", 24 },
                    { 249, true, 2, false, "Graceland Polytechnic, Kwara State ", 24 },
                    { 99, true, 1, true, "Federal University, Lafia ", 26 },
                    { 212, true, 2, true, "Kwara State Polytechnic, Ilorin ", 24 },
                    { 171, true, 1, false, "Crown Hill University, Ilorin ", 24 },
                    { 124, true, 1, false, "Al-Hikmah University, Ilorin ", 24 },
                    { 119, true, 1, false, "Landmark University, Omu-Aran ", 24 },
                    { 52, true, 1, true, "Kwara State University, Ilorin ", 24 },
                    { 40, true, 1, true, "University of Ilorin,Ilorin", 24 },
                    { 363, true, 3, true, "Kogi State College of Education, Kabba	Kabba, Kogi State ", 23 },
                    { 335, true, 3, true, "Kogi State College of Education, Ankpa	Ankpa, Kogi state. ", 23 },
                    { 296, true, 3, true, "Federal College of Education, Okene, Kogi State ", 23 },
                    { 266, true, 2, false, "Prime Polytechnic, Jida Bassa, Ajaokuta, Kogi State ", 23 },
                    { 177, true, 1, false, "Summit University Offa,Offa ", 24 },
                    { 229, true, 2, true, "Taraba State Polytechnic, Suntai ", 35 },
                    { 339, true, 3, true, "College of Education,Jalingo, Taraba State. ", 35 },
                    { 103, true, 1, true, "Yobe State University, Damaturu ", 36 },
                    { 152, true, 1, false, "Paul University, Awka ", 4 },
                    { 137, true, 1, false, "Madonna University, Okija ", 4 },
                    { 83, true, 1, true, "Chukwuemeka Odumegwu Ojukwu University, Uli ", 4 },
                    { 60, true, 1, true, "Nnamdi Azikiwe University, Awka ", 4 },
                    { 6, true, 2, true, "Federal Polytechnic, Oko", 4 },
                    { 381, true, 3, false, "DIAMOND COLLEGE OF EDUCATION, ABA ", 1 },
                    { 373, true, 3, false, "Havard Wilson College of Education, Aba	, Abia State ", 1 },
                    { 346, true, 3, true, "College of Education, Arochukwu, Abia ", 1 },
                    { 285, true, 2, false, "Valley View Polytechnic, OhafiaAbia State ", 1 },
                    { 283, true, 2, false, "Uma Ukpai Polytechnic, Abia State ", 1 },
                    { 275, true, 2, false, "Temple Gate Polytechnic, Abia State. ", 1 },
                    { 241, true, 2, false, "Covenant Polytechnic, Aba ", 1 },
                    { 189, true, 2, true, "Abia State Polytechnic, Aba ", 1 },
                    { 183, true, 1, false, "Spiritan University, Nneochi ", 1 },
                    { 180, true, 1, false, "Clifford University, Ihie ", 1 },
                    { 165, true, 1, false, "Rhema University, Aba ", 1 },
                    { 154, true, 1, false, "Gregory University, Uturu ", 1 },
                    { 69, true, 1, true, "Abia State University, Uturu ", 1 },
                    { 64, true, 1, true, "Michael Okpara University of Agriculture, Umuahia ", 1 },
                    { 155, true, 1, false, "Tansian University, Umunya ", 4 },
                    { 184, true, 1, false, "Legacy University, Okija ", 4 },
                    { 250, true, 2, false, "Grundtvig Polytechnic, Oba Anambra ", 4 },
                    { 308, true, 3, true, "Federal College of Education (Technical), Umunze, Anambra State ", 4 },
                    { 291, true, 3, true, "Federal College of Education., Enugu State ", 14 },
                    { 265, true, 2, false, "Our Saviour Institute of Science, Agriculture & Technology, Enugu State. ", 14 },
                    { 262, true, 2, false, "Mater Dei PolytechnicUgwuoba TownOji River LGA Enugu State ", 14 },
                    { 261, true, 2, false, "Marist Polytechnic, Umuchigbo, Iji-Nike,Emene Enugu State ", 14 },
                    { 206, true, 2, true, "Institute of Management and Technology, Enugu ", 14 },
                    { 202, true, 2, true, "Enugu State Polytechnic, Iwollo ", 14 },
                    { 175, true, 1, false, "Evangel University, Akaeze Enugu ", 14 },
                    { 174, true, 1, false, "Coal City University, Enugu ", 14 },
                    { 161, true, 1, false, "Renaissance University, Enugu ", 14 },
                    { 26, true, 2, false, "Temple Gate Polytechnic", 1 },
                    { 128, true, 1, false, "Godfrey Okoye University, Ugwuomu-Nike ", 14 },
                    { 34, true, 3, false, "Our Saviour Institute of Science and Technology, Enugu ", 14 },
                    { 30, true, 3, true, "Federal College of Education, Eha-Amufu ", 14 },
                    { 14, true, 1, false, "Caritas University", 14 },
                    { 2, true, 1, true, "University of Nigeria,Nsukka", 14 },
                    { 314, true, 3, true, "Ebonyi State College of Education, Ikwo	Ikwo, Ebonyi State. ", 11 },
                    { 270, true, 2, false, "Savanah Institute of Technology, Ebonyi State ", 11 },
                    { 84, true, 1, true, "Ebonyi State University, Abakaliki ", 11 },
                    { 63, true, 1, true, "Alex Ekwueme Federal University, Ndufu-Alike", 11 },
                    { 328, true, 3, true, "Nwafor Orizu College of Education, Nsugbe ", 4 },
                    { 70, true, 1, true, "Enugu State University of Science and Technology, Enugu ", 14 },
                    { 10, true, 1, true, "Micheal Okpara University", 1 },
                    { 340, true, 3, true, "Zamfara State College of Education, Maru ", 37 },
                    { 305, true, 3, true, "Federal College of Education (Technical), Gusau, Zamfara State ", 37 },
                    { 75, true, 1, true, "Kano University of Science and Technology, Wudil ", 20 },
                    { 53, true, 1, true, "Bayero University Kano, Kano ", 20 },
                    { 29, true, 3, true, "Federal College of Education, Kano ", 20 },
                    { 385, true, 1, true, "Air Force Institute of Technology, Kaduna", 19 },
                    { 345, true, 3, true, "Jama'Atu College of Education (JACE), Kaduna ", 19 },
                    { 322, true, 3, true, "Kaduna State College of Education, Gidan-Waya, Kafanchan ", 19 },
                    { 299, true, 3, true, "Fed. College of Educ. Zaria ", 19 },
                    { 218, true, 2, true, "Nuhu Bamalli Polytechnic, Zaria ", 19 },
                    { 72, true, 1, true, "Kaduna State University, Kaduna ", 19 },
                    { 79, true, 1, true, "Yusuf Maitama Sule University Kano, Kano ", 20 },
                    { 36, true, 1, true, "Ahmadu Bello University,zaria	", 19 },
                    { 341, true, 3, true, "Jigawa State College of Education, Gumel ", 18 },
                    { 208, true, 2, true, "Jigawa State Polytechnic, Dutse ", 18 },
                    { 197, true, 2, true, "Binyaminu Usman Polytechnic, Hadejia,Jigawa State ", 18 },
                    { 106, true, 1, true, "Sule Lamido University, Kafin Hausa ", 18 },
                    { 74, true, 1, true, "Federal University, Dutse ", 18 },
                    { 321, true, 3, true, "College of Education, Gashua, Damaturu	Gashua, Yobe state. ", 36 },
                    { 307, true, 3, true, "Federal College of Education (Technical), Potiskum, Yobe State ", 36 },
                    { 214, true, 2, true, "Mai-Idris Alooma Polytechnic, Geidam ", 36 }
                });

            migrationBuilder.InsertData(
                table: "Institution",
                columns: new[] { "Id", "Active", "InstitutionCategoryId", "IsPublic", "Name", "StateId" },
                values: new object[,]
                {
                    { 109, true, 1, true, "Federal University, Gashua ", 36 },
                    { 32, true, 3, true, "Federal College of Education, Zaria ", 19 },
                    { 365, true, 3, false, "Delar College of Education Ibadan, Oyo State ", 31 },
                    { 186, true, 1, false, "Skyline University Nigeria,Kano ", 20 },
                    { 292, true, 3, true, "Federal College of Education, Kano ", 20 },
                    { 232, true, 2, true, "Zamfara State College of Arts and Science, Gusau ", 37 },
                    { 188, true, 2, true, "Abdu Gusau Polytechnic, Talata Mafara ", 37 },
                    { 115, true, 1, true, "Zamfara State University, Talata Mafara ", 37 },
                    { 108, true, 1, true, "Federal University, Gusau ", 37 },
                    { 344, true, 3, true, "Shehu shagari College of Education, Sokoto ", 34 },
                    { 231, true, 2, true, "Umaru Ali Shinkafi Polytechnic, Sokoto ", 34 },
                    { 100, true, 1, true, "Sokoto State University, Sokoto ", 34 },
                    { 61, true, 1, true, "Usmanu Danfodio University, Sokoto ", 34 },
                    { 336, true, 3, true, "Adamu Augie College of Education, Argungu, Kebbi State. ", 22 },
                    { 209, true, 2, true, "Kano State Polytechnic, Kano ", 20 },
                    { 110, true, 1, true, "Kebbi State University of Science and Technology, Aliero ", 22 },
                    { 325, true, 3, true, "College of Education, katsina-Ala ", 21 },
                    { 319, true, 3, true, "Isa Kaita College of Education, Dutsin-Ma ", 21 },
                    { 293, true, 3, true, "Federal College of Education, Katsina ", 21 },
                    { 204, true, 2, true, "Hassan Usman Katsina Polytechnic, Katsina ", 21 },
                    { 148, true, 1, false, "Al-Qalam University, Katsina ", 21 },
                    { 71, true, 1, true, "Federal University, Dutsin-Ma", 21 },
                    { 62, true, 1, true, "Umaru Musa Yar'Adua University, Katsina ", 21 },
                    { 326, true, 3, true, "Sa'adatu Rimi College of Education, Kumbotso, Kano ", 20 },
                    { 303, true, 3, true, "Federal College of Education (Technical), Bichi ", 20 },
                    { 96, true, 1, true, "Federal University, Birnin Kebbi ", 22 },
                    { 372, true, 3, false, "Muftau Olanihun College of Education, Ibadan ", 31 }
                });

            migrationBuilder.InsertData(
                table: "Reviewer",
                columns: new[] { "Id", "Active", "ApplicantReviewerId", "Email", "InstitutionId", "Name", "PhoneNo", "RoleId", "TitleId" },
                values: new object[] { 1, true, null, "Admin@gmail.com", 1, "Tetfund ICT", "08067486937", 6, 1 });

            migrationBuilder.InsertData(
                table: "Reviewer",
                columns: new[] { "Id", "Active", "ApplicantReviewerId", "Email", "InstitutionId", "Name", "PhoneNo", "RoleId", "TitleId" },
                values: new object[] { 2, true, null, "SuperAdmin@gmail.com", 1, "Director Tetfund ICT", "09906784223", 2, 1 });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "Active", "Email", "IsLoggedIn", "LastLogin", "OneTimeValidator", "Password", "ReviewerId", "RoleId", "Token", "UserName" },
                values: new object[] { 1, true, "Admin@gmail.com", false, new DateTime(2020, 6, 21, 6, 33, 27, 279, DateTimeKind.Utc).AddTicks(4537), null, "1234567", 1, 6, null, "Admin@gmail.com" });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "Active", "Email", "IsLoggedIn", "LastLogin", "OneTimeValidator", "Password", "ReviewerId", "RoleId", "Token", "UserName" },
                values: new object[] { 2, true, "SuperAdmin@gmail.com", false, new DateTime(2020, 6, 21, 6, 33, 27, 279, DateTimeKind.Utc).AddTicks(5480), null, "1234567", 2, 2, null, "SuperAdmin@gmail.com" });

            migrationBuilder.CreateIndex(
                name: "IX_Applicant_PersonId",
                table: "Applicant",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_Applicant_RoleId",
                table: "Applicant",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicantReviewer_ApplicantThematicId",
                table: "ApplicantReviewer",
                column: "ApplicantThematicId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicantReviewer_TypeId",
                table: "ApplicantReviewer",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicantSubmission_ApplicantThematicId",
                table: "ApplicantSubmission",
                column: "ApplicantThematicId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicantThematic_ApplicantId",
                table: "ApplicantThematic",
                column: "ApplicantId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicantThematic_ApprovalId",
                table: "ApplicantThematic",
                column: "ApprovalId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicantThematic_SessionId",
                table: "ApplicantThematic",
                column: "SessionId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicantThematic_ThemeId",
                table: "ApplicantThematic",
                column: "ThemeId");

            migrationBuilder.CreateIndex(
                name: "IX_Approval_ReviewerId",
                table: "Approval",
                column: "ReviewerId");

            migrationBuilder.CreateIndex(
                name: "IX_AssessmentGuide_ProposalAssessmentId",
                table: "AssessmentGuide",
                column: "ProposalAssessmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Budget_CategoryId",
                table: "Budget",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Budget_SessionId",
                table: "Budget",
                column: "SessionId");

            migrationBuilder.CreateIndex(
                name: "IX_Budget_UserId",
                table: "Budget",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_BudgetDetail_ApplicantThematicId",
                table: "BudgetDetail",
                column: "ApplicantThematicId");

            migrationBuilder.CreateIndex(
                name: "IX_BudgetDetail_BudgetItemId",
                table: "BudgetDetail",
                column: "BudgetItemId");

            migrationBuilder.CreateIndex(
                name: "IX_BudgetDetail_TypeId",
                table: "BudgetDetail",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_BulletinResponse_BulletinTopicId",
                table: "BulletinResponse",
                column: "BulletinTopicId");

            migrationBuilder.CreateIndex(
                name: "IX_BulletinResponse_PersonId",
                table: "BulletinResponse",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_BulletinTopic_ApplicantThematicId",
                table: "BulletinTopic",
                column: "ApplicantThematicId");

            migrationBuilder.CreateIndex(
                name: "IX_BulletinTopic_PersonId",
                table: "BulletinTopic",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryAdministrator_CategoryId",
                table: "CategoryAdministrator",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryAdministrator_UserId",
                table: "CategoryAdministrator",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ConceptNote_ApplicantThemeId",
                table: "ConceptNote",
                column: "ApplicantThemeId");

            migrationBuilder.CreateIndex(
                name: "IX_FinalProposalAproval_ApplicantThematicId",
                table: "FinalProposalAproval",
                column: "ApplicantThematicId");

            migrationBuilder.CreateIndex(
                name: "IX_FinalProposalAproval_UserId",
                table: "FinalProposalAproval",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Institution_InstitutionCategoryId",
                table: "Institution",
                column: "InstitutionCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Institution_StateId",
                table: "Institution",
                column: "StateId");

            migrationBuilder.CreateIndex(
                name: "IX_Menu_Menu_GroupId",
                table: "Menu",
                column: "Menu_GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_MenuInRole_MenuId",
                table: "MenuInRole",
                column: "MenuId");

            migrationBuilder.CreateIndex(
                name: "IX_MenuInRole_RoleId",
                table: "MenuInRole",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Person_InstitutionId",
                table: "Person",
                column: "InstitutionId");

            migrationBuilder.CreateIndex(
                name: "IX_Person_QualificationId",
                table: "Person",
                column: "QualificationId");

            migrationBuilder.CreateIndex(
                name: "IX_Person_RankId",
                table: "Person",
                column: "RankId");

            migrationBuilder.CreateIndex(
                name: "IX_Person_SexId",
                table: "Person",
                column: "SexId");

            migrationBuilder.CreateIndex(
                name: "IX_Person_StateId",
                table: "Person",
                column: "StateId");

            migrationBuilder.CreateIndex(
                name: "IX_Person_TitleId",
                table: "Person",
                column: "TitleId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectActivities_ApplicantThematicId",
                table: "ProjectActivities",
                column: "ApplicantThematicId");

            migrationBuilder.CreateIndex(
                name: "IX_Proposal_ApplicantThemeId",
                table: "Proposal",
                column: "ApplicantThemeId");

            migrationBuilder.CreateIndex(
                name: "IX_ProposalAssessment_ApplicantThematicId",
                table: "ProposalAssessment",
                column: "ApplicantThematicId");

            migrationBuilder.CreateIndex(
                name: "IX_ProposalAssessment_ProposalId",
                table: "ProposalAssessment",
                column: "ProposalId");

            migrationBuilder.CreateIndex(
                name: "IX_ProposalAssessment_ProposalScoringId",
                table: "ProposalAssessment",
                column: "ProposalScoringId");

            migrationBuilder.CreateIndex(
                name: "IX_ProposalAssessment_ReviewerId",
                table: "ProposalAssessment",
                column: "ReviewerId");

            migrationBuilder.CreateIndex(
                name: "IX_ProposalScoring_ApplicantThematicId",
                table: "ProposalScoring",
                column: "ApplicantThematicId");

            migrationBuilder.CreateIndex(
                name: "IX_ProposalScoring_ReviewerId",
                table: "ProposalScoring",
                column: "ReviewerId");

            migrationBuilder.CreateIndex(
                name: "IX_QuaterData_TimeFrameId",
                table: "QuaterData",
                column: "TimeFrameId");

            migrationBuilder.CreateIndex(
                name: "IX_ResearchCycleEvent_EventTypeId",
                table: "ResearchCycleEvent",
                column: "EventTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ResearchCycleEvent_SessionId",
                table: "ResearchCycleEvent",
                column: "SessionId");

            migrationBuilder.CreateIndex(
                name: "IX_Reviewer_ApplicantReviewerId",
                table: "Reviewer",
                column: "ApplicantReviewerId");

            migrationBuilder.CreateIndex(
                name: "IX_Reviewer_InstitutionId",
                table: "Reviewer",
                column: "InstitutionId");

            migrationBuilder.CreateIndex(
                name: "IX_Reviewer_RoleId",
                table: "Reviewer",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Reviewer_TitleId",
                table: "Reviewer",
                column: "TitleId");

            migrationBuilder.CreateIndex(
                name: "IX_ReviewerThematicStrenght_ReviewerId",
                table: "ReviewerThematicStrenght",
                column: "ReviewerId");

            migrationBuilder.CreateIndex(
                name: "IX_ReviewerThematicStrenght_ThemeId",
                table: "ReviewerThematicStrenght",
                column: "ThemeId");

            migrationBuilder.CreateIndex(
                name: "IX_State_GeoPoliticalZoneId",
                table: "State",
                column: "GeoPoliticalZoneId");

            migrationBuilder.CreateIndex(
                name: "IX_Team_ApplicantThematicId",
                table: "Team",
                column: "ApplicantThematicId");

            migrationBuilder.CreateIndex(
                name: "IX_Team_PersonId",
                table: "Team",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_Theme_CategoryId",
                table: "Theme",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_TimeFrame_ApplicantThematicId",
                table: "TimeFrame",
                column: "ApplicantThematicId");

            migrationBuilder.CreateIndex(
                name: "IX_User_ReviewerId",
                table: "User",
                column: "ReviewerId");

            migrationBuilder.CreateIndex(
                name: "IX_User_RoleId",
                table: "User",
                column: "RoleId");

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicantThematic_Approval_ApprovalId",
                table: "ApplicantThematic",
                column: "ApprovalId",
                principalTable: "Approval",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Applicant_Person_PersonId",
                table: "Applicant");

            migrationBuilder.DropForeignKey(
                name: "FK_Applicant_Role_RoleId",
                table: "Applicant");

            migrationBuilder.DropForeignKey(
                name: "FK_Reviewer_Role_RoleId",
                table: "Reviewer");

            migrationBuilder.DropForeignKey(
                name: "FK_ApplicantReviewer_ApplicantThematic_ApplicantThematicId",
                table: "ApplicantReviewer");

            migrationBuilder.DropTable(
                name: "AdminSettings");

            migrationBuilder.DropTable(
                name: "ApplicantSubmission");

            migrationBuilder.DropTable(
                name: "AssessmentGuide");

            migrationBuilder.DropTable(
                name: "Budget");

            migrationBuilder.DropTable(
                name: "BudgetDetail");

            migrationBuilder.DropTable(
                name: "BulletinResponse");

            migrationBuilder.DropTable(
                name: "CategoryAdministrator");

            migrationBuilder.DropTable(
                name: "ConceptNote");

            migrationBuilder.DropTable(
                name: "DefaultMessage");

            migrationBuilder.DropTable(
                name: "FinalProposalAproval");

            migrationBuilder.DropTable(
                name: "FundingSource");

            migrationBuilder.DropTable(
                name: "MenuInRole");

            migrationBuilder.DropTable(
                name: "ProjectActivities");

            migrationBuilder.DropTable(
                name: "Quarter");

            migrationBuilder.DropTable(
                name: "QuaterData");

            migrationBuilder.DropTable(
                name: "ResearchCycleEvent");

            migrationBuilder.DropTable(
                name: "ReviewerThematicStrenght");

            migrationBuilder.DropTable(
                name: "Score");

            migrationBuilder.DropTable(
                name: "Team");

            migrationBuilder.DropTable(
                name: "ProposalAssessment");

            migrationBuilder.DropTable(
                name: "BudgetItem");

            migrationBuilder.DropTable(
                name: "BulletinTopic");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "Menu");

            migrationBuilder.DropTable(
                name: "TimeFrame");

            migrationBuilder.DropTable(
                name: "EventType");

            migrationBuilder.DropTable(
                name: "Proposal");

            migrationBuilder.DropTable(
                name: "ProposalScoring");

            migrationBuilder.DropTable(
                name: "MenuGroup");

            migrationBuilder.DropTable(
                name: "Person");

            migrationBuilder.DropTable(
                name: "Qualification");

            migrationBuilder.DropTable(
                name: "Rank");

            migrationBuilder.DropTable(
                name: "Sex");

            migrationBuilder.DropTable(
                name: "Role");

            migrationBuilder.DropTable(
                name: "ApplicantThematic");

            migrationBuilder.DropTable(
                name: "Applicant");

            migrationBuilder.DropTable(
                name: "Approval");

            migrationBuilder.DropTable(
                name: "Session");

            migrationBuilder.DropTable(
                name: "Theme");

            migrationBuilder.DropTable(
                name: "Reviewer");

            migrationBuilder.DropTable(
                name: "Category");

            migrationBuilder.DropTable(
                name: "ApplicantReviewer");

            migrationBuilder.DropTable(
                name: "Institution");

            migrationBuilder.DropTable(
                name: "Title");

            migrationBuilder.DropTable(
                name: "Type");

            migrationBuilder.DropTable(
                name: "InstitutionCategory");

            migrationBuilder.DropTable(
                name: "State");

            migrationBuilder.DropTable(
                name: "GeoPoliticalZone");
        }
    }
}
