﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Grant.Web.Migrations
{
    public partial class activateReviewerTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ActiveReviewer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SessionId = table.Column<int>(nullable: false),
                    ReviewerId = table.Column<int>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActiveReviewer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ActiveReviewer_Reviewer_ReviewerId",
                        column: x => x.ReviewerId,
                        principalTable: "Reviewer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ActiveReviewer_Session_SessionId",
                        column: x => x.SessionId,
                        principalTable: "Session",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastLogin",
                value: new DateTime(2020, 7, 16, 12, 46, 28, 639, DateTimeKind.Utc).AddTicks(5276));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 2,
                column: "LastLogin",
                value: new DateTime(2020, 7, 16, 12, 46, 28, 639, DateTimeKind.Utc).AddTicks(6377));

            migrationBuilder.CreateIndex(
                name: "IX_ActiveReviewer_ReviewerId",
                table: "ActiveReviewer",
                column: "ReviewerId");

            migrationBuilder.CreateIndex(
                name: "IX_ActiveReviewer_SessionId",
                table: "ActiveReviewer",
                column: "SessionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActiveReviewer");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastLogin",
                value: new DateTime(2020, 6, 28, 10, 51, 17, 8, DateTimeKind.Utc).AddTicks(4708));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 2,
                column: "LastLogin",
                value: new DateTime(2020, 6, 28, 10, 51, 17, 8, DateTimeKind.Utc).AddTicks(5246));
        }
    }
}
