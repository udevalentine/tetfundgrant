﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Grant.Web.Migrations
{
    public partial class reconciliationTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsReconcilled",
                table: "ProposalAssessment",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsReconciled",
                table: "ApplicantReviewer",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "Reconciliation",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApplicantThematicId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    DateReconcilled = table.Column<DateTime>(nullable: false),
                    ReconcilliationAverageScore = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reconciliation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Reconciliation_ApplicantThematic_ApplicantThematicId",
                        column: x => x.ApplicantThematicId,
                        principalTable: "ApplicantThematic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Reconciliation_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastLogin",
                value: new DateTime(2020, 7, 27, 11, 41, 35, 927, DateTimeKind.Utc).AddTicks(5822));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 2,
                column: "LastLogin",
                value: new DateTime(2020, 7, 27, 11, 41, 35, 927, DateTimeKind.Utc).AddTicks(6722));

            migrationBuilder.CreateIndex(
                name: "IX_Reconciliation_ApplicantThematicId",
                table: "Reconciliation",
                column: "ApplicantThematicId");

            migrationBuilder.CreateIndex(
                name: "IX_Reconciliation_UserId",
                table: "Reconciliation",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Reconciliation");

            migrationBuilder.DropColumn(
                name: "IsReconcilled",
                table: "ProposalAssessment");

            migrationBuilder.DropColumn(
                name: "IsReconciled",
                table: "ApplicantReviewer");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastLogin",
                value: new DateTime(2020, 7, 25, 5, 44, 43, 625, DateTimeKind.Utc).AddTicks(2553));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 2,
                column: "LastLogin",
                value: new DateTime(2020, 7, 25, 5, 44, 43, 625, DateTimeKind.Utc).AddTicks(3101));
        }
    }
}
