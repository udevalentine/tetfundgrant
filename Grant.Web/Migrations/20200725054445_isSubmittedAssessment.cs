﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Grant.Web.Migrations
{
    public partial class isSubmittedAssessment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsSubmitted",
                table: "ProposalScoring",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastLogin",
                value: new DateTime(2020, 7, 25, 5, 44, 43, 625, DateTimeKind.Utc).AddTicks(2553));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 2,
                column: "LastLogin",
                value: new DateTime(2020, 7, 25, 5, 44, 43, 625, DateTimeKind.Utc).AddTicks(3101));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsSubmitted",
                table: "ProposalScoring");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastLogin",
                value: new DateTime(2020, 7, 16, 12, 46, 28, 639, DateTimeKind.Utc).AddTicks(5276));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 2,
                column: "LastLogin",
                value: new DateTime(2020, 7, 16, 12, 46, 28, 639, DateTimeKind.Utc).AddTicks(6377));
        }
    }
}
