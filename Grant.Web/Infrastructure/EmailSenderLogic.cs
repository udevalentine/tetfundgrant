﻿
using Microsoft.AspNetCore.Hosting;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Infrastructure
{
    public class EmailSenderLogic<T> : IEmailSender
    {
        private string _templateFilePath;
        private T _model;
        private IHostingEnvironment _hostingEnvironment;

        public void InitHostingEnvironment(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public EmailSenderLogic(string templateFilePath, T model)
        {
            _templateFilePath = templateFilePath;
            _model = model;
        }
        public void Send(MailGunModel message)
        {
            try
            {
                useMailgun(message, _templateFilePath);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Send(List<MailGunModel> messages)
        {
            foreach (MailGunModel message in messages)
            {
                Send(message);
            }
        }

        private IRestResponse useMailgun(MailGunModel message, string body)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            

            client.Authenticator =
                new HttpBasicAuthenticator("api",
                    "key-8540f3ef6a66cdaf8d9121f11c99aa6b");
            RestRequest request = new RestRequest();
            request.AddParameter("domain", "nrf.tetfund.gov.ng", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";

            request.AddParameter("from", "NRF <support@nrf.tetfund.gov.ng>");


            request.AddParameter("to", message.MessageTo);
            request.AddParameter("subject", message.Subject);
            request.AddParameter("html", body);
            request.Method = Method.POST;
            //return client.Execute(request);
            var response=client.Execute(request);
            return response;
        }
    }
}
