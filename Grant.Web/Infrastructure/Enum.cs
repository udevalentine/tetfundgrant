﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Infrastructure
{
    public class Enum
    {
    }
    public enum Types
    {
        ConceptNote = 1,
        Proposal = 2,
        
    }
    public enum EventTypes
    {
        ConceptNote = 1,
        Proposal = 2,
        AssignConceptNoteAssessor=3,
        ReviewConceptNote=4,
        AcceptConceptnote=5,
        SetBudgetParameter=6,
        AssignProposalAssessor=7,
        ReviewProposal=8,
        AcceptProposal=9

    }
    public enum Fundings
    {

        TetFunding = 1,
        InstitutionFunding = 2,
        OtherFunding = 3
    }
    public enum BudgetItems
    {
        PersonnelAllowances = 1,
        Equipment = 2,
        SuppliesConsumables = 3,
        DataCollectionandAnalysis = 4,
        Travel = 5,
        Dissemination = 6,
        Other = 7
    }
    public enum MessageType
    {
        signUp = 1,
        ConceptNoteSubmission = 2,
        ProposalSubmission = 3,
        ConceptNoteApproval = 4,
        ProposalApproval = 5,
        ConceptNoteRejection = 6,
        ProposalRejection = 7,
        ReviewerCreation = 8,
        ReviewerAssignment = 9,
        ReviewProposalAssignment = 10,
        ForgotPassword = 11,
        AddTeamMember=12,
        AddExistingCoResearcher=13,
        SendRegularMail=14,
        
    }

    public enum RoleType {
        Reviewer = 1,
        SuperAdmin = 2,
        Applicant = 3,
        Co_Applicant = 4,
            CoAdmin = 5,
        Admin = 6

    }
    public enum Roles
    {
        Reviewer=1,
        SuperAdmin = 2,
        Applicant=3,
        CoApplicant=4,
        CoAdmin=5,
        Admin=6
    }
}
