﻿using Castle.Core.Configuration;
using Grant.Web.Models;
using Grant.Web.Models.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.Extensions.Configuration;
using System.Net;
using System.Security.Claims;
using System.Net.Http;
using Newtonsoft.Json;

namespace Grant.Web.Infrastructure
{
    public class Utility
    {
        private readonly DataBaseContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;
        private Microsoft.Extensions.Configuration.IConfiguration _configuration;
        private string baseUrl= string.Empty;

        public Utility(DataBaseContext context, IHostingEnvironment hostingEnvironment, Microsoft.Extensions.Configuration.IConfiguration configuration)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
            baseUrl = _configuration.GetValue<string>("Url:onlineVerificationRoot");
        }
        public Applicant VerifyApplicantLoginDetail(string registrationNo, string password)
        {
            try
            {
                return _context.Applicant.Where(f => f.ApplicationNo == registrationNo && f.Password == password && f.Active && f.RoleId==(int)Roles.Applicant).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Applicant VerifyCoReseacherLoginDetail(string registrationNo, string password)
        {
            try
            {
                return _context.Applicant.Where(f => f.ApplicationNo == registrationNo && f.Password == password && f.Active && f.RoleId == (int)Roles.CoApplicant).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public User VerifyReviewerLoginDetail(string registrationNo, string password)
        {
            try
            {
                return _context.User.Where(f => f.Email == registrationNo && f.Password == password && f.Active && f.RoleId==(int)RoleType.Reviewer).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public User VerifyAdminLoginDetail(string username, string password)
        {
            try
            {
                return _context.User.Where(f => f.UserName == username && f.Password == password && f.Active && (f.RoleId == 2 || f.RoleId == 5 || f.RoleId==6)).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void RegisterNewApplicant(string name, string email, string phoneno)
        {
            try
            {

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void RegisterApplicant(Person person, Applicant applicant, int InstitutionId, int QualificationId, int TitleId, string otherQualification, int StateId, int SexId, RegistrationViewModel registrationViewModel)
        {
            try
            {
                person.Name=registrationViewModel.Surname + " " + registrationViewModel.FirstName + " " + registrationViewModel.Othername;
                using (TransactionScope scope = new TransactionScope())
                {
                    Qualification qualification = new Qualification();
                    var existingPerson = _context.Person.Where(d => d.Email == person.Email && d.PhoneNo == person.PhoneNo).FirstOrDefault();
                    if (otherQualification != null)
                    {
                        var formattedValue = otherQualification.Trim();
                        var existQualification = _context.Qualification.Where(f => f.Name == formattedValue).FirstOrDefault();
                        if (existQualification == null)
                        {
                            qualification.Name = formattedValue;
                            qualification.Active = false;
                            _context.Add(qualification);
                        }
                    }
                    if (existingPerson == null)
                    {
                        var currentDate = DateTime.UtcNow.AddHours(-8);
                        existingPerson = new Person()
                        {
                            DateCreated = currentDate,
                            Name = person.Name,
                            Email = person.Email,
                            PhoneNo = person.PhoneNo,
                            InstitutionId = InstitutionId,
                            QualificationId = qualification.Name != null ? qualification.Id : QualificationId,
                            TitleId = TitleId,
                            Age=person.Age,
                            SexId=SexId,
                            StateId=38,
                            DOB=person.DOB,
                            RankId=person.RankId
                        };
                        _context.Add(existingPerson);
                        _context.SaveChanges();
                    }
                    var existingApplicant = _context.Applicant.Where(f => f.ApplicationNo == person.Email && f.Password == applicant.Password).FirstOrDefault();
                    if (existingApplicant == null)
                    {
                        existingApplicant = new Applicant()
                        {
                            Active = true,
                            ApplicationNo = person.Email,
                            LastLogin = DateTime.UtcNow.AddHours(-8),
                            PersonId = existingPerson.Id,
                            Password = applicant.Password,
                            RoleId=(int)Roles.Applicant

                        };
                    }
                    _context.Add(existingApplicant);
                    _context.SaveChanges();
                    scope.Complete();
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void AddTeam(Person person, int applicantId, int InstitutionId, int QualificationId, string passporturl, string otherQualification, string oneTimeValidator,ApplicantThematic applicantThematic,string otherRank)
        {
            try
            {
                var currentDate = DateTime.UtcNow.AddHours(-8);
                using (TransactionScope scope = new TransactionScope())
                {
                    Qualification qualification = new Qualification();
                    if (otherQualification != null)
                    {
                        var formattedValue = otherQualification.Trim();
                        var existQualification = _context.Qualification.Where(f => f.Name == formattedValue).FirstOrDefault();
                        if (existQualification == null)
                        {
                            qualification.Name = formattedValue;
                            qualification.Active = false;
                            _context.Add(qualification);
                        }
                    }
                    Rank rank = new Rank();
                    if (otherRank != null)
                    {
                        var formattedRankValue = otherRank.Trim();
                        var existRank = _context.Rank.Where(f => f.Name == formattedRankValue).FirstOrDefault();
                        if (existRank == null)
                        {
                            rank.Name = formattedRankValue;
                            rank.Active = false;
                            _context.Add(rank);
                        }
                    }
                    var newperson = new Person()
                    {
                        DateCreated = currentDate,
                        Name = person.Name,
                        Email = person.Email,
                        PhoneNo = person.PhoneNo,
                        InstitutionId = InstitutionId,
                        QualificationId = qualification.Name != null ? qualification.Id : QualificationId,
                        PassportUrl = passporturl,
                        TitleId = person.TitleId,
                        SexId=person.SexId,
                        StateId=38,
                        RankId=rank.Name!=null?rank.Id:person.RankId
                    };
                    _context.Add(newperson);
                    _context.SaveChanges();
                    var newTeam = new Team()
                    {
                        Active = true,
                        PersonId = newperson.Id,
                        ApplicantThematicId = applicantThematic.Id
                    };
                    
                    _context.Add(newTeam);
                    _context.SaveChanges();
                    //create Login for TeamMember
                    
                    var newTeamApplicant = new Applicant()
                    {
                        Active = true,
                        ApplicationNo = person.Email,
                        DateVerified = DateTime.UtcNow.AddHours(-8),
                        IsVerified = false,
                        Password = "1234567",
                        Person = newperson,
                        RoleId =(int)Roles.CoApplicant,
                        OneTimeValidator=oneTimeValidator
                    };
                    _context.Add(newTeamApplicant);
                    _context.SaveChanges();
                    scope.Complete();
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void AddConceptNote(Applicant applicant, int ThemeId, string Title, decimal estimatedBudget)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                    ApplicantThematic existingApplicantThematic=_context.ApplicantThematic
                                                          .Where(f => f.ApplicantId == applicant.Id && f.SessionId== activeSessionForApplication.Id)
                                                          .Include(f => f.Theme)
                                                          .ThenInclude(f => f.Category)
                                                          .FirstOrDefault();
                    
                    if (existingApplicantThematic == null)
                    {
                        Theme selectedTheme = _context.Theme
                                                      .Include(t => t.Category)
                                                      .Where(t => t.Id == ThemeId)
                                                      .FirstOrDefault();

                        ApplicantThematic applicantThematic = new ApplicantThematic()
                        {
                            ApplicantId = applicant.Id,
                            DateApplied = DateTime.Now,
                            ThemeId = ThemeId,
                            Title = Title,
                            Session = activeSessionForApplication,
                            FileNo = GenerateUniqueFileNumber(selectedTheme.Category.Code, selectedTheme.Code,selectedTheme.Id)
                        };
                        _context.Add(applicantThematic);

                        ConceptNote conceptNote = new ConceptNote()
                        {
                            ApplicantThemeId = applicantThematic.Id,
                            ConceptNoteSubmittedBudget=estimatedBudget
                            //NoteUrl = filePath,
                            //ConceptNoteSummary = "",

                        };
                        _context.Add(conceptNote);
                    }
                    else
                    {
                        existingApplicantThematic.Title = Title;
                        existingApplicantThematic.ThemeId = ThemeId;
                        _context.Update(existingApplicantThematic);
                        var existingConceptNote=_context.ConceptNote.Where(t => t.ApplicantThemeId == existingApplicantThematic.Id).FirstOrDefault();
                        if (existingConceptNote == null)
                        {
                            ConceptNote conceptNote = new ConceptNote()
                            {
                                ApplicantThemeId = existingApplicantThematic.Id,
                                ConceptNoteSubmittedBudget=estimatedBudget
                            };
                            _context.Add(conceptNote);
                        }
                        else
                        {
                            existingConceptNote.ConceptNoteSubmittedBudget = estimatedBudget;
                            _context.Update(existingConceptNote);

                        }
                    }
                    
                    _context.SaveChanges();
                    scope.Complete();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void AddProposal(int ApplicantThematicId, string filePath, decimal budget)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {


                    Proposal proposal = new Proposal()
                    {
                        ApplicantThemeId = ApplicantThematicId,
                        ProposalUrl = filePath,
                        SubmittedBudget = budget

                    };
                    _context.Add(proposal);
                    _context.SaveChanges();
                    scope.Complete();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool CheckForFileExtension(string fileName)
        {
            try
            {
                var list = fileName.Split(".");
                if (list.Contains("pdf"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return false;
        }
        public void AddReviewer(string name, string phoneno, string email, string username, int TitleId, int InstitutionId,string guid)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {

                    Reviewer reviewer = new Reviewer()
                    {
                        Email = email,
                        Name = name,
                        PhoneNo = phoneno,
                        Active = false,
                        RoleId = 1,
                        TitleId = TitleId,
                        InstitutionId=InstitutionId
                    };
                    _context.Add(reviewer);
                    _context.SaveChanges();
                    User user = new User()
                    {
                        Active = false,
                        Email = email,
                        Password = "1234567",
                        ReviewerId = reviewer.Id,
                        RoleId = 1,
                        UserName = email,
                        OneTimeValidator=guid

                    };
                    _context.Add(user);
                    _context.SaveChanges();
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool ChangePassword(string currentPassword, string newPassword, string username, string role)
        {
            try
            {
                if (role == "Applicant" || role== "Co-Applicant")
                {
                    var applicant = _context.Applicant.Where(f => f.ApplicationNo == username && f.Password == currentPassword && f.Role.Name ==role).FirstOrDefault();
                    if (applicant != null)
                    {
                        applicant.Password = newPassword;
                        _context.Update(applicant);
                        _context.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    var user = _context.User.Where(f => f.UserName == username && f.Password == currentPassword).FirstOrDefault();
                    if (user != null)
                    {
                        user.Password = newPassword;
                        _context.Update(user);
                        _context.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            //return false;
        }
        public bool EmailExist(string email, int Id)
        {
            try
            {
                var record = _context.Reviewer.Where(f => f.Email == email).FirstOrDefault();
                if (record != null && record.Id != Id)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void SaveApplicationSubmission(ApplicantSubmission applicantSubmission, int applicantThematicId, int type, string url, ConceptNote conceptNote)
        {
            try
            {
                if (type == 2)
                {
                    var existApplicantSubmission = _context.ApplicantSubmission.Where(d => d.ApplicantThematicId == applicantThematicId && d.TypeId == type).FirstOrDefault();
                    if (existApplicantSubmission == null)
                    {
                        using (TransactionScope scope = new TransactionScope())
                        {
                            applicantSubmission.TypeId = type;
                            applicantSubmission.ApplicantThematicId = applicantThematicId;
                            _context.Add(applicantSubmission);
                           var existingProposal= _context.Proposal.Where(d => d.ApplicantThemeId == applicantThematicId).FirstOrDefault();
                            if (existingProposal == null)
                            {
                                Proposal proposal = new Proposal()
                                {
                                    ApplicantThemeId = applicantThematicId,
                                    ProposalUrl = url,
                                    SubmittedBudget = TotalProposalProposedBudget(applicantThematicId, type),
                                    Submitted = true

                                };
                                _context.Add(proposal);
                            }
                            else
                            {
                                existingProposal.ProposalUrl = url;
                                existingProposal.Submitted = true;
                                existingProposal.SubmittedBudget = TotalProposalProposedBudget(applicantThematicId, type);
                                _context.Update(existingProposal);
                            }
                            

                        }
                    }
                    else
                    {

                        _context.Update(existApplicantSubmission);
                        var proposal = _context.Proposal.Where(f => f.ApplicantThemeId == applicantThematicId).FirstOrDefault();
                        if (proposal != null)
                        {
                            proposal.SubmittedBudget = TotalProposalProposedBudget(applicantThematicId, type);
                            _context.Update(proposal);
                        }
                        else
                        {
                            Proposal newproposal = new Proposal()
                            {
                                ApplicantThemeId = applicantThematicId,
                                ProposalUrl = url,
                                Submitted = true,
                                SubmittedBudget = TotalProposalProposedBudget(applicantThematicId, type)
                            };
                            _context.Add(newproposal);
                        }
                    }

                    _context.SaveChanges();
                }
                else if (type == 1)
                {
                    var existApplicantSubmission = _context.ApplicantSubmission.Where(d => d.ApplicantThematicId == applicantThematicId && d.TypeId == type).FirstOrDefault();
                    if (existApplicantSubmission == null)
                    {
                        using (TransactionScope scope = new TransactionScope())
                        {
                            applicantSubmission.TypeId = type;
                            applicantSubmission.ApplicantThematicId = applicantThematicId;
                            _context.Add(applicantSubmission);

                           
                            var submittedConceptNote=_context.ConceptNote.Where(f => f.ApplicantThemeId == applicantThematicId).FirstOrDefault();
                            if (submittedConceptNote != null)
                            {
                                submittedConceptNote.ConceptNoteSubmittedBudget = conceptNote.ConceptNoteSubmittedBudget;
                                submittedConceptNote.Submitted = true;
                                _context.Update(submittedConceptNote);


                            }
                            else
                            {
                                ConceptNote newConceptNote = new ConceptNote()
                                {
                                    ApplicantThemeId = applicantThematicId,
                                    NoteUrl = url,
                                    ConceptNoteSubmittedBudget = conceptNote.ConceptNoteSubmittedBudget,
                                    Submitted = true


                                };
                                _context.Add(newConceptNote);
                            }
                            _context.SaveChanges();
                            scope.Complete();

                        }
                    }
                    else
                    {
                        using (TransactionScope scope = new TransactionScope())
                        {
                            existApplicantSubmission.TypeId = type;
                            existApplicantSubmission.Title = applicantSubmission.Title;
                            existApplicantSubmission.TimeFrame = applicantSubmission.TimeFrame;
                            existApplicantSubmission.ActivityIndicator = applicantSubmission.ActivityIndicator;
                            existApplicantSubmission.AdditionalSources = applicantSubmission.AdditionalSources;
                            existApplicantSubmission.Aims = applicantSubmission.Aims;
                            existApplicantSubmission.ApplicantThematicId = applicantThematicId;
                            existApplicantSubmission.BudgetJustification = applicantSubmission.BudgetJustification;
                            existApplicantSubmission.ConceptualFramework = applicantSubmission.ConceptualFramework;
                            existApplicantSubmission.DataManagement = applicantSubmission.DataManagement;
                            existApplicantSubmission.DisseminationStrategies = applicantSubmission.DisseminationStrategies;
                            existApplicantSubmission.Ethical = applicantSubmission.Ethical;
                            existApplicantSubmission.ExecutiveSummary = applicantSubmission.ExecutiveSummary;
                            existApplicantSubmission.GroupResearch = applicantSubmission.GroupResearch;
                            existApplicantSubmission.IntroductionBackground = applicantSubmission.IntroductionBackground;
                            existApplicantSubmission.LiteratureReview = applicantSubmission.LiteratureReview;
                            existApplicantSubmission.Monitoring = applicantSubmission.Monitoring;
                            existApplicantSubmission.PreviousGrant = applicantSubmission.PreviousGrant;
                            existApplicantSubmission.ProjectActivities = applicantSubmission.ProjectActivities;
                            existApplicantSubmission.ProjectGoal = applicantSubmission.ProjectGoal;
                            existApplicantSubmission.ProjectImpact = applicantSubmission.ProjectImpact;
                            existApplicantSubmission.ResearchMethodoly = applicantSubmission.ResearchMethodoly;
                            existApplicantSubmission.ResearchWorkToDate = applicantSubmission.ResearchWorkToDate;
                            existApplicantSubmission.StatementOfProblem = applicantSubmission.StatementOfProblem;
                            existApplicantSubmission.StudyLocation = applicantSubmission.StudyLocation;
                            existApplicantSubmission.ProposalTitle = applicantSubmission.ProposalTitle;
                            existApplicantSubmission.Innovations = applicantSubmission.Innovations;
                            existApplicantSubmission.ExpectedResult = applicantSubmission.ExpectedResult;
                            existApplicantSubmission.References = applicantSubmission.References;
                            existApplicantSubmission.ResearchQuestions = applicantSubmission.ResearchQuestions;
                            _context.Update(existApplicantSubmission);
                            var existconceptNote = _context.ConceptNote.Where(f => f.ApplicantThemeId == applicantThematicId).FirstOrDefault();
                            if (existconceptNote != null)
                            {
                                existconceptNote.ConceptNoteSubmittedBudget = conceptNote.ConceptNoteSubmittedBudget;
                                existconceptNote.Submitted = true;
                                _context.Update(existconceptNote);
                            }
                            _context.SaveChanges();
                            scope.Complete();
                        }

                    }

                    
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public decimal TotalProposalProposedBudget(int applicantThematicId, int type)
        {
            decimal total = 0m;
            try
            {
                total = _context.BudgetDetail.Where(f => f.ApplicantThematicId == applicantThematicId && f.TypeId == type).Sum(t => t.Total);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return total;
        }
        public int SumProposalScore(ProposalScoring scoring)
        {
            int total = 0;
            try
            {
                total = (int)((scoring.ActivityIndicatorScore != null ? scoring.ActivityIndicatorScore : 0) + (scoring.AdditionalSourcesScore != null ? scoring.AdditionalSourcesScore : 0) + (scoring.AimsScore != null ? scoring.AimsScore : 0) + (scoring.BudgetJustificationScore != null ? scoring.BudgetJustificationScore : 0) + (scoring.ConceptualFrameworkScore != null ? scoring.ConceptualFrameworkScore : 0)
                    + (scoring.DataManagementScore != null ? scoring.DataManagementScore : 0) + (scoring.DisseminationStrategiesScore != null ? scoring.DisseminationStrategiesScore : 0) + (scoring.EthicalScore != null ? scoring.EthicalScore : 0) + (scoring.ExecutiveSummaryScore != null ? scoring.ExecutiveSummaryScore : 0) + (scoring.GroupResearchScore != null ? scoring.GroupResearchScore : 0) + (scoring.IntroductionBackgroundScore != null ? scoring.IntroductionBackgroundScore : 0) + (scoring.LiteratureReviewScore != null ? scoring.LiteratureReviewScore : 0) + (scoring.MonitoringScore != null ? scoring.MonitoringScore : 0)
                    + (scoring.PreviousGrantScore != null ? scoring.PreviousGrantScore : 0) + (scoring.ProjectActivities != null ? scoring.ProjectActivities : 0) + (scoring.ProjectGoalScore != null ? scoring.ProjectGoalScore : 0) + (scoring.ProjectImpactScore != null ? scoring.ProjectImpactScore : 0) + (scoring.ResearchMethodolyScore != null ? scoring.ResearchMethodolyScore : 0) + (scoring.ResearchWorkToDateScore != null ? scoring.ResearchWorkToDateScore : 0) + (scoring.StatementOfProblemScore != null ? scoring.StatementOfProblemScore : 0) + (scoring.StudyLocationScore != null ? scoring.StudyLocationScore : 0) + (scoring.TimeFrameScore != null ? scoring.TimeFrameScore : 0)
                    + (scoring.TitleScore != null ? scoring.TitleScore : 0));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return total;
        }
        public void UpdateProposalAssessment(ProposalScoring proposalScoring, int applicantThematicId, int reviewerId, int type, decimal approvedBudget, bool approved, string remark, string reject, int proposalId)
        {
            try
            {
                var existingScoring = _context.ProposalScoring.Where(f => f.ApplicantThematicId == applicantThematicId && f.ReviewerId == reviewerId && f.TypeId == (int)Types.Proposal).FirstOrDefault();
                using (TransactionScope scope = new TransactionScope())
                {
                    if (existingScoring != null)
                    {
                        existingScoring.ActivityIndicatorScore = proposalScoring.ActivityIndicatorScore;
                        existingScoring.AdditionalSourcesScore = proposalScoring.AdditionalSourcesScore;
                        existingScoring.AimsScore = proposalScoring.AimsScore;
                        existingScoring.BudgetJustificationScore = proposalScoring.BudgetJustificationScore;
                        existingScoring.ConceptualFrameworkScore = proposalScoring.ConceptualFrameworkScore;
                        existingScoring.DataManagementScore = proposalScoring.DataManagementScore;
                        existingScoring.DisseminationStrategiesScore = proposalScoring.DisseminationStrategiesScore;
                        existingScoring.EthicalScore = proposalScoring.EthicalScore;
                        existingScoring.ExecutiveSummaryScore = proposalScoring.ExecutiveSummaryScore;
                        existingScoring.GroupResearchScore = proposalScoring.GroupResearchScore;
                        existingScoring.IntroductionBackgroundScore = proposalScoring.IntroductionBackgroundScore;
                        existingScoring.LiteratureReviewScore = proposalScoring.LiteratureReviewScore;
                        existingScoring.MonitoringScore = proposalScoring.MonitoringScore;
                        existingScoring.PreviousGrantScore = proposalScoring.PreviousGrantScore;
                        existingScoring.ProjectActivities = proposalScoring.ProjectActivities;
                        existingScoring.ProjectGoalScore = proposalScoring.ProjectGoalScore;
                        existingScoring.ProjectImpactScore = proposalScoring.ProjectImpactScore;
                        existingScoring.ResearchMethodolyScore = proposalScoring.ResearchMethodolyScore;
                        existingScoring.ResearchWorkToDateScore = proposalScoring.ResearchWorkToDateScore;
                        existingScoring.StatementOfProblemScore = proposalScoring.StatementOfProblemScore;
                        existingScoring.StudyLocationScore = proposalScoring.StudyLocationScore;
                        existingScoring.TimeFrameScore = proposalScoring.TimeFrameScore;
                        existingScoring.TitleScore = proposalScoring.TitleScore;
                        existingScoring.Remark = proposalScoring.Remark;
                        _context.Update(existingScoring);
                        var assessedProposal = _context.ProposalAssessment.Where(f => f.ApplicantThematicId == applicantThematicId && f.ReviewerId == reviewerId).FirstOrDefault();
                        if (assessedProposal != null)
                        {
                            assessedProposal.Score = SumProposalScore(proposalScoring);
                            assessedProposal.ApprovedBudget = approvedBudget;
                            assessedProposal.Remark = proposalScoring.Remark;
                            assessedProposal.ReasonForDissapproval = reject;
                            _context.Update(assessedProposal);
                        }
                    }
                    else
                    {
                        proposalScoring.ApplicantThematicId = applicantThematicId;
                        proposalScoring.ReviewerId = reviewerId;
                        proposalScoring.TypeId = (int)Types.Proposal;
                        proposalScoring.DateReviewed = DateTime.Now;
                        _context.Add(proposalScoring);
                        _context.SaveChanges();
                        ProposalAssessment proposalAssessment = new ProposalAssessment()
                        {
                            ApplicantThematicId = applicantThematicId,
                            Approved = approved,
                            ApprovedBudget = approvedBudget,
                            ProposalId = proposalId,
                            ProposalScoringId = proposalScoring.Id,
                            ReasonForDissapproval = reject,
                            ReviewerId = reviewerId,
                            Remark = proposalScoring.Remark,
                            Score = SumProposalScore(proposalScoring)
                        };
                        _context.Add(proposalAssessment);
                    }
                    _context.SaveChanges();
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void UpdateConceptNoteAssessment(ProposalScoring proposalScoring, int applicantThematicId, int reviewerId, int type, decimal? mark, bool isSubmitted)
        {
            try
            {
                var existingScoring = _context.ProposalScoring.Where(f => f.ApplicantThematicId == applicantThematicId && f.ReviewerId == reviewerId && f.TypeId == type).FirstOrDefault();
                using (TransactionScope scope = new TransactionScope())
                {
                    if (existingScoring != null)
                    {
                        existingScoring.ActivityIndicatorScore = proposalScoring.ActivityIndicatorScore;
                        existingScoring.AdditionalSourcesScore = proposalScoring.AdditionalSourcesScore;
                        existingScoring.AimsScore = proposalScoring.AimsScore;
                        existingScoring.BudgetJustificationScore = proposalScoring.BudgetJustificationScore;
                        existingScoring.ConceptualFrameworkScore = proposalScoring.ConceptualFrameworkScore;
                        existingScoring.DataManagementScore = proposalScoring.DataManagementScore;
                        existingScoring.DisseminationStrategiesScore = proposalScoring.DisseminationStrategiesScore;
                        existingScoring.EthicalScore = proposalScoring.EthicalScore;
                        existingScoring.ExecutiveSummaryScore = proposalScoring.ExecutiveSummaryScore;
                        existingScoring.GroupResearchScore = proposalScoring.GroupResearchScore;
                        existingScoring.IntroductionBackgroundScore = proposalScoring.IntroductionBackgroundScore;
                        existingScoring.LiteratureReviewScore = proposalScoring.LiteratureReviewScore;
                        existingScoring.MonitoringScore = proposalScoring.MonitoringScore;
                        existingScoring.PreviousGrantScore = proposalScoring.PreviousGrantScore;
                        existingScoring.ProjectActivities = proposalScoring.ProjectActivities;
                        existingScoring.ProjectGoalScore = proposalScoring.ProjectGoalScore;
                        existingScoring.ProjectImpactScore = proposalScoring.ProjectImpactScore;
                        existingScoring.ResearchMethodolyScore = proposalScoring.ResearchMethodolyScore;
                        existingScoring.ResearchWorkToDateScore = proposalScoring.ResearchWorkToDateScore;
                        existingScoring.StatementOfProblemScore = proposalScoring.StatementOfProblemScore;
                        existingScoring.StudyLocationScore = proposalScoring.StudyLocationScore;
                        existingScoring.TimeFrameScore = proposalScoring.TimeFrameScore;
                        existingScoring.TitleScore = proposalScoring.TitleScore;
                        existingScoring.Remark = proposalScoring.Remark;
                        existingScoring.IsSubmitted = isSubmitted;
                        existingScoring.DateReviewed = DateTime.Now;
                        _context.Update(existingScoring);
                        var assessedConceptNote = _context.ApplicantReviewer.Where(f => f.ApplicantThematicId == applicantThematicId && f.ReviewerId == reviewerId && f.TypeId == (int)Types.ConceptNote).FirstOrDefault();
                        if (assessedConceptNote != null)
                        {
                            assessedConceptNote.ConceptNoteMark = mark;
                            assessedConceptNote.DateReviewed = DateTime.Now;

                            _context.Update(assessedConceptNote);
                        }
                    }
                    else
                    {
                        proposalScoring.ApplicantThematicId = applicantThematicId;
                        proposalScoring.ReviewerId = reviewerId;
                        proposalScoring.TypeId = type;
                        proposalScoring.IsSubmitted = isSubmitted;
                        proposalScoring.DateReviewed = DateTime.Now;
                        _context.Add(proposalScoring);
                        var assessedConceptNote = _context.ApplicantReviewer.Where(f => f.ApplicantThematicId == applicantThematicId && f.ReviewerId == reviewerId && f.TypeId == (int)Types.ConceptNote).FirstOrDefault();
                        if (assessedConceptNote != null)
                        {
                            assessedConceptNote.ConceptNoteMark = mark;
                            assessedConceptNote.DateReviewed = DateTime.Now;
                            
                            _context.Update(assessedConceptNote);
                        }

                    }
                    _context.SaveChanges();
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AutoSaveApplicationSubmission(ApplicationSubmission applicantionSubmission, int applicantThematicId, int type, decimal? conceptNoteEditedbudget)
        {

            try
            {
                var existApplicantSubmission = _context.ApplicantSubmission.Where(d => d.ApplicantThematicId == applicantThematicId && d.TypeId == type).FirstOrDefault();

                if (existApplicantSubmission == null)
                {
                    using (TransactionScope scope = new TransactionScope())
                    {
                        ApplicantSubmission applicantSubmission = new ApplicantSubmission()
                        {
                            TypeId = type,
                            Title = applicantionSubmission.Title,
                            TimeFrame = applicantionSubmission.TimeFrame,
                            ActivityIndicator = applicantionSubmission.ActivityIndicator,
                            AdditionalSources = applicantionSubmission.AdditionalSources,
                            Aims = applicantionSubmission.Aims,
                            ApplicantThematicId = applicantThematicId,
                            BudgetJustification = applicantionSubmission.BudgetJustification,
                            ConceptualFramework = applicantionSubmission.ConceptualFramework,
                            DataManagement = applicantionSubmission.DataManagement,
                            DisseminationStrategies = applicantionSubmission.DisseminationStrategies,
                            Ethical = applicantionSubmission.Ethical,
                            ExecutiveSummary = applicantionSubmission.ExecutiveSummary,
                            GroupResearch = applicantionSubmission.GroupResearch,
                            IntroductionBackground = applicantionSubmission.IntroductionBackground,
                            LiteratureReview = applicantionSubmission.LiteratureReview,
                            Monitoring = applicantionSubmission.Monitoring,
                            PreviousGrant = applicantionSubmission.PreviousGrant,
                            ProjectActivities = applicantionSubmission.ProjectActivities,
                            ProjectGoal = applicantionSubmission.ProjectGoal,
                            ProjectImpact = applicantionSubmission.ProjectImpact,
                            ResearchMethodoly = applicantionSubmission.ResearchMethodoly,
                            ResearchWorkToDate = applicantionSubmission.ResearchWorkToDate,
                            StatementOfProblem = applicantionSubmission.StatementOfProblem,
                            StudyLocation = applicantionSubmission.StudyLocation,
                            ProposalTitle = applicantionSubmission.ProposalTitle,
                            Innovations = applicantionSubmission.Innovations,
                            ExpectedResult = applicantionSubmission.ExpectedResult,
                            References = applicantionSubmission.References,
                            ResearchQuestions = applicantionSubmission.ResearchQuestions,
                            TeamMates=applicantionSubmission.ResearchTeam

                        };


                        _context.Add(applicantSubmission);
                        if (type == 2)
                        {
                            Proposal proposal = new Proposal()
                            {
                                ApplicantThemeId = applicantThematicId,
                                //ProposalUrl = url,
                                SubmittedBudget = TotalProposalProposedBudget(applicantThematicId, type)

                            };
                        }
                        //else if (type == 1)
                        //{
                        //    var conceptNopte = _context.ConceptNote.Where(g => g.ApplicantThemeId == applicantThematicId).FirstOrDefault();
                        //    if (conceptNopte != null)
                        //    {
                        //        conceptNopte.ConceptNoteSubmittedBudget = TotalProposalProposedBudget(applicantThematicId, type);
                        //        _context.Update(conceptNopte);
                        //    }

                        //}

                       

                    }
                }
                else
                {
                    existApplicantSubmission.TypeId = type;
                    existApplicantSubmission.Title = applicantionSubmission.Title;
                    existApplicantSubmission.TimeFrame = applicantionSubmission.TimeFrame;
                    existApplicantSubmission.ActivityIndicator = applicantionSubmission.ActivityIndicator;
                    existApplicantSubmission.AdditionalSources = applicantionSubmission.AdditionalSources;
                    existApplicantSubmission.Aims = applicantionSubmission.Aims;
                    existApplicantSubmission.ApplicantThematicId = applicantThematicId;
                    existApplicantSubmission.BudgetJustification = applicantionSubmission.BudgetJustification;
                    existApplicantSubmission.ConceptualFramework = applicantionSubmission.ConceptualFramework;
                    existApplicantSubmission.DataManagement = applicantionSubmission.DataManagement;
                    existApplicantSubmission.DisseminationStrategies = applicantionSubmission.DisseminationStrategies;
                    existApplicantSubmission.Ethical = applicantionSubmission.Ethical;
                    existApplicantSubmission.ExecutiveSummary = applicantionSubmission.ExecutiveSummary;
                    existApplicantSubmission.GroupResearch = applicantionSubmission.GroupResearch;
                    existApplicantSubmission.IntroductionBackground = applicantionSubmission.IntroductionBackground;
                    existApplicantSubmission.LiteratureReview = applicantionSubmission.LiteratureReview;
                    existApplicantSubmission.Monitoring = applicantionSubmission.Monitoring;
                    existApplicantSubmission.PreviousGrant = applicantionSubmission.PreviousGrant;
                    existApplicantSubmission.ProjectActivities = applicantionSubmission.ProjectActivities;
                    existApplicantSubmission.ProjectGoal = applicantionSubmission.ProjectGoal;
                    existApplicantSubmission.ProjectImpact = applicantionSubmission.ProjectImpact;
                    existApplicantSubmission.ResearchMethodoly = applicantionSubmission.ResearchMethodoly;
                    existApplicantSubmission.ResearchWorkToDate = applicantionSubmission.ResearchWorkToDate;
                    existApplicantSubmission.StatementOfProblem = applicantionSubmission.StatementOfProblem;
                    existApplicantSubmission.StudyLocation = applicantionSubmission.StudyLocation;
                    existApplicantSubmission.ProposalTitle = applicantionSubmission.ProposalTitle;
                    existApplicantSubmission.Innovations = applicantionSubmission.Innovations;
                    existApplicantSubmission.ExpectedResult = applicantionSubmission.ExpectedResult;
                    existApplicantSubmission.References = applicantionSubmission.References;
                    existApplicantSubmission.ResearchQuestions = applicantionSubmission.ResearchQuestions;
                    existApplicantSubmission.TeamMates = applicantionSubmission.ResearchTeam;
                    _context.Update(existApplicantSubmission);

                    if (conceptNoteEditedbudget != null)
                    {
                        var conceptNopte = _context.ConceptNote.Where(g => g.ApplicantThemeId == applicantThematicId).FirstOrDefault();
                        if (conceptNopte != null)
                        {
                            conceptNopte.ConceptNoteSubmittedBudget = (decimal)conceptNoteEditedbudget;
                            _context.Update(conceptNopte);
                        }
                    }
                }

                _context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void SendMail(string receiverMail, string Name,int type )
        {
            string message = "";
            string subject = "";
            switch (type)
            {
                case 1:
                    message = "Thank You for Signing Up for National Research Fund Application";
                    subject = "NRF Sign-Up";
                    break;
                case 2:
                    message = "This is to acknowledge that your Concept Note has been received." +  

"The review process has begun and would go on for the next couple of weeks.By the end of December 2019, you would be informed of the status of your application." +

"If your submission is approved, you would be duly notified by us during this period."+

"As a result of the number of submissions we have received, we may not be able to give individual feedback for each application." + 

"Thank you once again for applying";
                    subject = "NRF Concept Note Submission";
                    break;
                case 3:
                    message = "This is to acknowledge that your Proposal has been received." +

"The review process has begun and would go on for the next couple of weeks.By the end of January 2020, you would be informed of the status of your application." +

"If your submission is approved, you would be duly notified by us during this period." +

"As a result of the number of submissions we have received, we may not be able to give individual feedback for each application." +

"Thank you once again for applying";
                    subject = "NRF Proposal Submission";
                    break;
                default:
                    message = "";
                    break;
            }


            MailGunModel mailGunModel = new MailGunModel();
            mailGunModel.Body = message;
            mailGunModel.DisplayName = "TetFund";
            mailGunModel.MessageFrom = "noreply@lloydant.com";
            mailGunModel.MessageTo = receiverMail;
            mailGunModel.Subject = subject;
            mailGunModel.Name = Name;
           // mailGunModel.Link = "http://97.74.6.243/tetfundgrant";
            mailGunModel.Link = "http://localhost";
            Email email = new Email();
            email.SendEmail(mailGunModel,type);
        }

        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead("http://google.com/generate_204"))
                    return true;
            }
            catch
            {
                return false;
            }
        }

        public void SendEmailValidationEmail(string email, string subject, string message, string randomGuuid)
        {
            try
            {
                if (CheckForInternetConnection())
                {
                    //string verifyUrl = string.format("http://localhost/Home/ValidateUser?guid={0}", randomGuuid);
                    string verifyUrl = string.Format("http://97.74.6.243/tetfundgrant/Home/ValidateUser?guid={0}", randomGuuid);

                   
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public void SendEMail(string email,  string name, int type, string message, string subject, string teamName,string randomGuuid,string researchTitle )
        {
            baseUrl = string.Empty;
            baseUrl = _configuration.GetValue<string>("Url:onlineVerificationRoot");
            try
            {

                var password = 1234567;
                if (CheckForInternetConnection())
                {
                    var encrypt = Encrypt(email);
                    var verifyLink = baseUrl + "Home/VerifyAccount?encryptData=" + encrypt;
                    switch (type)
                    {
                        case 1:
                            subject = "NRF Registration Successful!";
                            message = "Your account as a Principal Investigator (PI) has been successfully created;"
                                + "Kindly click on the button below to verify your account.";
                            break;
                        case 2:
                            message = "Your Concept Note with title: " +researchTitle + " has been successfully submitted.<br><br>" +
        "You will be duly notified if your Concept note is approved.";

                            subject = "CONCEPT NOTE SUBMITTED";
                            break;
                        case 3:
                            message = "This is to acknowledge that your Full Proposal with title: "+researchTitle +" has been received.<br><br>" +

        "Review of your Proposal is now in progress and would go on for the next couple of weeks. You will be duly notified of your Application status if your Proposal is approved.<br><br>" +

        "Please note that due to the number of submissions received, we may not be able to give individual feedback on each application.<br><br>"
        + "Thank you once again for applying.";

                            subject = "FULL PROPOSAL SUBMITTED";
                            break;
                        case 4:
                            message = "Your Concept Note titled: " + researchTitle+"has been approved! <br><br>" +

        "Kindly log into your account to submit your Full proposal.";
                            subject = "CONCEPT NOTE APPROVAL";
                            break;
                        case 5:
                            subject = "FULL PROPOSAL APPROVED";
                            message = "Congratulations! <br><br>"
                                + "We are pleased to inform you that your Full Proposal titled: " + researchTitle + " has been approved.<br><br>"
                                + "Further details on the next line of action will be communicated in a few days. ";
                            break;
                        case 8:
                            baseUrl = string.Format("{0}Home/CompleteRegistration?guid={1}", baseUrl, randomGuuid);
                            subject = "REVIEWER STATUS";
                            message = string.Format("Congratulations!<br><br>Please be informed that you have been added as an assessor on the Electronic Document Management System  of the TETFund National Research Fund. <br>Your username is: {0}. Kindly proceed to complete your registration by clicking the link below", email);
                            break;
                        case 9:
                            subject = "REVIEWER ASSIGNED";
                            message = string.Format("Dear {0}, <br><br> You've been assigned to consider a concept note", email);
                        break;
                        case 10:
                            subject = "REVIEWER ASSIGNED";
                            message = string.Format("Dear {0}, <br><br> You've been assigned to consider a proposal", email);
                        break;
                        case 11:
                            
                            string verifyPasswordUrl = string.Format("{0}Home/ValidateUser?guid={1}", baseUrl, randomGuuid);

                            StringBuilder btn = new StringBuilder();
                            btn.Append(string.Format("<a href='{0}'>Reset Password</a>", verifyPasswordUrl));

                            subject = "CHANGE YOUR PASSWORD";
                            message = string.Format("Please click on the link below to reset your password <br><br>{0}", btn.ToString());
                            break;
                        case 12:
                            baseUrl = string.Format("{0}Home/ChangePassword?guid={1}", baseUrl, randomGuuid);


                            subject = "CO-RESEARCHER";
                            message = string.Format("This is to inform you that you have been added as a Co-Researcher to: {0} Team, research title: " + researchTitle + ". <br> <br> Please click on the link below to access your Account.", teamName);
                            break;
                        case 13:
                            baseUrl = string.Format("{0}Home/index", baseUrl);

                            subject = "CO-RESEARCHER";
                            message = string.Format("You have just been added as a research team member on: {0} Team, research title: " + researchTitle + ". <br> <br> Please click on the link below to create a password and access your profile.", teamName);
                            break;
               
                        default:
                            message = "";
                            break;
                    }

                    MailGunModel mailGunModel = new MailGunModel();
                    mailGunModel.Name = name;
                    mailGunModel.Subject = subject;
                    mailGunModel.Link = type == 1 ? verifyLink : baseUrl;
                    mailGunModel.MessageTo = email ?? "support@lloydant.com";
                    mailGunModel.Body = message;
                    Email emailObj = new Email();
                    var template = emailObj.FormatHtmlTemplate(mailGunModel, type);
                    EmailSenderLogic<MailGunModel> receiptEmailSenderLogic = new EmailSenderLogic<MailGunModel>(template, mailGunModel);

                    receiptEmailSenderLogic.Send(mailGunModel);
                }   
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string Encrypt(string encrypData)
        {
            string data = "";
            try
            {
                string CharData = "";
                string ConChar = "";
                for (int i = 0; i < encrypData.Length; i++)
                {
                    CharData = Convert.ToString(encrypData.Substring(i, 1));
                    ConChar = char.ConvertFromUtf32(char.ConvertToUtf32(CharData, 0) + 115);
                    data = data + ConChar;
                }
            }
            catch (Exception)
            {
                data = "1";
                return data;
            }
            return data;
        }

        public static string Decrypt(string encrypData)
        {
            string data = "";
            try
            {
                string CharData = "";
                string ConChar = "";
                for (int i = 0; i < encrypData.Length; i++)
                {
                    CharData = Convert.ToString(encrypData.Substring(i, 1));
                    ConChar = char.ConvertFromUtf32(char.ConvertToUtf32(CharData, 0) - 115);
                    data = data + ConChar;
                }
            }
            catch (Exception)
            {
                data = "1";
                return data;
            }
            return data;
        }

        public string GenerateUniqueFileNumber(string categoryCode, string thematicCode,int themeId)
        {
            try
            {
                string fileNo = string.Empty;
                var applicationSubmission = _context.ApplicantThematic.Where(f=>f.Session.ActiveForApplication && f.ThemeId==themeId).LastOrDefault();
                if (applicationSubmission != null)
                {
                    var splittedFileNo=applicationSubmission.FileNo.Split('/');
                    var lastSerialNo=Convert.ToInt32(splittedFileNo[3]);
                    string lastIndexWithPadding = (lastSerialNo + 1).ToString();
                    string lastIndex = lastIndexWithPadding.PadLeft(5, '0');
                    fileNo = string.Format("NRF/{0}/{1}/{2}", categoryCode, thematicCode, lastIndex);
                }
                else
                {
                    fileNo = string.Format("NRF/{0}/{1}/00001", categoryCode, thematicCode);
                }
                return fileNo;
            }
            catch(Exception ex) { throw ex; }
        }
        
        //public string GenerateUniqueApplicationFileNo()
        //{
        //    try
        //    {
        //        string str = "0123456789ABCDEF";

        //        string code = "";
        //        var activeApplicationSession = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
        //        if (activeApplicationSession != null)
        //        {
        //            //string sessionString = activeApplicationSession.Name;
        //            //code = "NRF/" + sessionString +"/" + Shuffle(str).Substring(0, 5).ToUpper();
        //           var FileNo= _context.ApplicantThematic.Where(f => f.SessionId == activeApplicationSession.Id)
        //                .Include(f => f.Theme)
        //                .ThenInclude(f => f.Category)
        //                .Select(f=> new {
        //                    f.FileNo,
        //                    f.Theme
        //                }).LastOrDefault();
        //            if (FileNo != null)
        //            {
        //                int idx = FileNo.FileNo.LastIndexOf('/');

        //                if (idx != -1)
        //                {
        //                    var lastSubString=FileNo.FileNo.Substring(idx + 1);
        //                    if (lastSubString != null)
        //                    {
        //                        var nextNo = "0000" + (Convert.ToInt32(lastSubString) + 1);
        //                        code = string.Format("NRF/{0}/{1}/RESEC/{2}", FileNo.Theme.Category.Code, FileNo.Theme.Code, nextNo);
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                code = string.Format("NRF/{0}/{1}/RESEC/00001", FileNo.Theme.Category.Code, FileNo.Theme.Code);
        //            }
        //        }

        //        return code.ToUpper();
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //}
        public string Shuffle(string str)
        {
            char[] array = str.ToCharArray();
            Random rng = new Random();
            int n = array.Length;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                var value = array[k];
                array[k] = array[n];
                array[n] = value;
            }
            return new string(array);
        }

        public StatisticsViewModel ApplicantGraphicalReport()
        {
            try
            {
                StatisticsViewModel model = new StatisticsViewModel();
                //-----------------Applicants By state Of Origin(Begins)---------------------------//
                var stateOfOriginCount = _context.Applicant
                                                  .Include(p => p.Person)
                                                  .ThenInclude(p => p.State)
                                                  .Where(f=>f.RoleId==(int)Roles.Applicant)
                                                  .GroupBy(p => p.Person.State.Name)
                                                  .Select(p => new StatisticsValueObj() {
                                                      Count = p.Count(),
                                                      Name = p.Key
                                                  })
                                                  .ToList();
                if (stateOfOriginCount?.Count() > 0)
                {
                    model.MembersByStateOfOriginCount.AddRange(stateOfOriginCount);
                }
                //-----------------Applicants By state Of Origin(Ends)---------------------------//

                //-----------------Applicants By state Of Institution(Begins)---------------------------//
                var stateOfInstitutionCount = _context.Applicant
                                                      .Include(p => p.Person)
                                                      .ThenInclude(p => p.Institution)
                                                      .Include(p => p.Person.Institution.State)
                                                      .Where(f => f.RoleId == (int)Roles.Applicant)
                                                      .GroupBy(p => p.Person.Institution.State.Name)
                                                      .Select(p => new StatisticsValueObj()
                                                      {
                                                          Count = p.Count(),
                                                          Name = p.Key
                                                      })
                                                      .ToList();

                if (stateOfInstitutionCount?.Count() > 0)
                {
                    model.MembersByStateOfInstitutionCount.AddRange(stateOfInstitutionCount);
                }
                //-----------------Applicants By state Of Institution(Ends)---------------------------//

                //-----------------Applicants By Gender(Begins)---------------------------//
                var applicantsByGender = _context.Applicant
                                                 .Include(p => p.Person)
                                                 .ThenInclude(p => p.Sex)
                                                 .Where(p => p.RoleId == (int)Roles.Applicant)
                                                 .GroupBy(p => p.Person.Sex.Name)
                                                 .Select(p => new StatisticsValueObj()
                                                 {
                                                     Count = p.Count(),
                                                     Name = p.Key
                                                 })
                                                 .ToList();

                if (applicantsByGender?.Count() > 0)
                {
                    model.GenderCount.AddRange(applicantsByGender);
                }
                //-----------------Applicants By Gender(Ends)---------------------------//

                //-----------------Applicants By Institution(Begins)---------------------------//
                var applicantsByInstitutionCount = _context.Applicant
                                                       .Include(p => p.Person)
                                                       .ThenInclude(p => p.Institution)
                                                       .Where(f => f.RoleId == (int)Roles.Applicant)
                                                       .GroupBy(p => p.Person.Institution.Name)
                                                       .Select(p => new StatisticsValueObj()
                                                       {
                                                           Count = p.Count(),
                                                           Name = p.Key
                                                       })
                                                       .ToList();

                if (applicantsByInstitutionCount?.Count() > 0)
                {
                    model.InstitutionAttendedCount.AddRange(applicantsByInstitutionCount);
                }
                //-----------------Applicants By Institution(Ends)---------------------------//

                //-----------------Applicants By Geo-Political-Zone(Begins)---------------------------//
                var applicantsByGeoPoliticalZoneCount = _context.Applicant
                                                                .Include(p => p.Person)
                                                                .ThenInclude(p => p.State)
                                                                .Include(p => p.Person.State.GeoPoliticalZone)
                                                                .Where(f => f.RoleId == (int)Roles.Applicant)
                                                                .GroupBy(p => p.Person.State.GeoPoliticalZone.Name)
                                                                .Select(p => new StatisticsValueObj()
                                                                {
                                                                    Name = p.Key,
                                                                    Count = p.Count()
                                                                })
                                                                .ToList();

                if (applicantsByGeoPoliticalZoneCount?.Count() > 0)
                {
                    model.GeoPoliticalZoneCount.AddRange(applicantsByGeoPoliticalZoneCount);
                }
                //-----------------Applicants By Geo-Political-Zone(Ends)---------------------------//

                //-----------------Applicants By Types Of Grant(Begins)---------------------------//
                var applicantsByGrantTypeCount = _context.ApplicantThematic
                                                         .Include(p => p.Theme)
                                                         .ThenInclude(p => p.Category)

                                                         .GroupBy(p => p.Theme.Category.Name)
                                                          .Select(p => new StatisticsValueObj()
                                                          {
                                                              Name = p.Key,
                                                              Count = p.Count()
                                                          })
                                                           .ToList();

                if (applicantsByGrantTypeCount?.Count() > 0)
                {
                    model.TypeOfGrantCount.AddRange(applicantsByGrantTypeCount);
                }
                //-----------------Applicants By Types Of Grant(Ends)---------------------------//

                //-----------------Applicants By Age Range(Begins)---------------------------//
                var applicantCountByDOB = _context.Applicant
                                             .Include(p => p.Person)
                                             .Where(f => f.RoleId == (int)Roles.Applicant)
                                             .GroupBy(p => p.Person.DOB.Year)
                                             .ToList();

                if (applicantCountByDOB?.Count() > 0)
                {
                    foreach(var item in applicantCountByDOB)
                    {
                        var ageRange = ConvertYearToRange((DateTime.Now.Year - item.Key));
                        StatisticsValueObj statisticsValueObjAgeRange = new StatisticsValueObj()
                        {
                            Name = ageRange,
                            Count = item.Count()
                        };

                        model.AgeCount.Add(statisticsValueObjAgeRange);
                    }
                    //Re-group the applicant by their Age
                    model.AgeCount = model.AgeCount
                                          .GroupBy(p => p.Name)
                                          .Select(p => new StatisticsValueObj() {
                                              Count = p.Count(),
                                              Name = p.Key
                                          })
                                          .ToList();
                }


                //-----------------Applicants By Age Range(Ends)---------------------------//

                ////---------------Applicants By ApplicantThematic where Approved(Begins)-------------//
                //var approvedThemes = _context.Approval
                //                             .Include(af => af.ApplicantThematic)
                //                             .GroupBy(af => af.Approved)
                //                             .Select(af => new StatisticsValueObj() {
                //                                 Count = af.Count(),
                //                                 Name = af.Key ? "True" : "False"
                //                             })
                //                             .ToList();
                ////---------------Applicants By ApplicantThematic where Approved(Ends)-------------//

                //---------------Applicants By ApplicantThematic where Approved(Ends)-------------//
                var applicantThematicFromThematicAreas = _context.ApplicantThematic
                                                                 .Include(at => at.Theme)
                                                                 .GroupBy(af => af.Theme.Name)
                                                                 .Select(af => new StatisticsValueObj()
                                                                 {
                                                                     Name = af.Key,
                                                                     Count = af.Count()
                                                                 })
                                                                 .ToList();

                model.ConceptNoteByThematicAreasCount = applicantThematicFromThematicAreas;
                //---------------Applicants By ApplicantThematic where Approved(Ends)-------------//


                //---------------Applicant Thematic by the Total Concept Notes and Total Approved Notes(Begins)----//
                var totalApplicantThematicListCount = _context.ApplicantThematic.Count();
                var totalThematicObj = new StatisticsValueObj()
                {
                    Name = "Total Concept Notes",
                    Count = totalApplicantThematicListCount
                };

                var totalApprovedConceptNotesCount = _context.Approval
                                                        .Where(a => a.Approved)
                                                        .Count();
                var totalApprovedThematicObj = new StatisticsValueObj();
                totalApprovedThematicObj.Name = "Approved Concept Notes";
                totalApprovedThematicObj.Count = totalApprovedConceptNotesCount;

                model.TotalAndApprovedApplicantThematicCount = new List<StatisticsValueObj>();
                model.TotalAndApprovedApplicantThematicCount.Add(totalThematicObj);
                model.TotalAndApprovedApplicantThematicCount.Add(totalApprovedThematicObj);
                //---------------Applicant Thematic by the Total Concept Notes and Total Approved Notes(Ends)----//

                return model;
            }
            catch(Exception ex) { throw ex; }
        }

        public string ConvertYearToRange(int year)
        {
            try
            {
                string range = string.Empty;
                switch (year)
                {
                    case int n when (n <= 10 && n >= 0):
                        range = "0 - 10 Years";
                        break;
                    case int n when (n <= 20 && n > 10):
                        range = "11 - 20 Years";
                        break;
                    case int n when (n <= 30 && n > 20):
                        range = "21 - 30 Years";
                        break;
                    case int n when (n <= 40 && n > 30):
                        range = "31 - 40 Years";
                        break;
                    case int n when (n <= 50 && n > 40):
                        range = "41 - 50 Years";
                        break;
                    case int n when (n <= 60 && n > 50):
                        range = "51 - 60 Years";
                        break;
                    case int n when (n <= 70 && n > 60):
                        range = "61 - 70 Years";
                        break;
                    case int n when (n <= 80 && n > 70):
                        range = "71 - 80 Years";
                        break;
                    case int n when (n <= 90 && n > 80):
                        range = "81 - 90 Years";
                        break;
                    case int n when (n <= 100 && n > 90):
                        range = "91 - 100 Years";
                        break;
                    case int n when (n <= 110 && n > 100):
                        range = "101 - 110 Years";
                        break;
                    case int n when (n <= 120 && n > 110):
                        range = "111 - 120 Years";
                        break;
                    case int n when (n <= 130 && n > 120):
                        range = "121 - 130 Years";
                        break;
                    default:
                        range = "Undefined";
                        break;
                }

                return range;
            }
            catch (Exception ex) { throw ex; }
        }
        public BulletinTopic CreateBulletinTopic (BulletinTopic bulletinTopic, Person person, ApplicantThematic applicantThematic)
        {
            BulletinTopic bulletinTopicCreated = new BulletinTopic();
            try
            {
               var applicant= _context.Applicant.Where(f => f.PersonId == person.Id).FirstOrDefault();
                if (bulletinTopic != null && applicant?.Id>0)
                {
                    bulletinTopicCreated.Person = person;
                    bulletinTopicCreated.Topic = bulletinTopic.Topic;
                    bulletinTopicCreated.DateCreated = DateTime.Now;
                    bulletinTopicCreated.Active = true;
                    bulletinTopicCreated.ApplicantThematic = applicantThematic;
                    if (applicant.Role.Id == (int)Roles.Applicant)
                    {
                        //bulletinTopicCreated.Applicant = applicant;
                    }
                    else
                    {
                       var team= _context.Team.Where(f => f.PersonId == person.Id).FirstOrDefault();
                        if (team?.Id > 0)
                        {
                            //bulletinTopicCreated.Applicant = team.Applicant;
                        }
                    }
                    _context.Add(bulletinTopicCreated);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return bulletinTopicCreated;
        }
        public List<BulletinChatResponse> LoadBulletinChatBoard(Person person, long topicId)
        {
            List<BulletinChatResponse> BulletinChatResponses = new List<BulletinChatResponse>();
            try
            {
                var responses = _context.BulletinResponse.Where(f => f.BulletinTopicId == topicId).OrderBy(g => g.ResponseDate).ToList();
                if (responses?.Count > 0)
                {
                    foreach (var item in responses)
                    {
                        BulletinChatResponse bulletinChatResponse = new BulletinChatResponse();
                        bulletinChatResponse.Sender = item.Person.ApplicantFullName;
                        bulletinChatResponse.Response = item.ResponseMessage;
                        bulletinChatResponse.DateSent = item.ResponseDate.ToString("U");
                        bulletinChatResponse.ActiveSender = item.PersonId == person.Id ? true : false;
                        bulletinChatResponse.Topic = item.BulletinTopic.Topic;
                        bulletinChatResponse.FilePath = item.Upload;
                        bulletinChatResponse.FileName = item.FileName;
                        BulletinChatResponses.Add(bulletinChatResponse);

                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return BulletinChatResponses;
        }
        public void SaveChatResponse(long bulletinTopicId, Person person, string response, string attachement,string fileName)
        {
            try
            {
                if(bulletinTopicId>0 && person?.Id>0 && response != null)
                {
                    BulletinResponse bulletinResponse = new BulletinResponse()
                    {
                        BulletinTopicId = bulletinTopicId,
                        ResponseDate = DateTime.Now,
                        Active = true,
                        PersonId = person.Id,
                        ResponseMessage = response,
                        Upload=attachement,
                        FileName=fileName

                    };
                    _context.Add(bulletinResponse);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public string ConcatonateProposalSubmission(long applicantThematicId)
        {
            try
            {
                StringBuilder fullProposal = new StringBuilder();
                var submittedProposal=_context.ApplicantSubmission.Where(f => f.ApplicantThematicId == applicantThematicId && f.TypeId==(int)Types.Proposal).FirstOrDefault();
                if (submittedProposal?.Id > 0)
                {
                    
                    fullProposal.Append(submittedProposal.ProposalTitle);
                    fullProposal.Append(submittedProposal.ExecutiveSummary);
                    fullProposal.Append(submittedProposal.IntroductionBackground);
                    fullProposal.Append(submittedProposal.StatementOfProblem);
                    fullProposal.Append(submittedProposal.Aims);
                    fullProposal.Append(submittedProposal.LiteratureReview);
                    fullProposal.Append(submittedProposal.ResearchMethodoly);
                    fullProposal.Append(submittedProposal.TimeFrame);
                    fullProposal.Append(submittedProposal.References);
                }
                return fullProposal.ToString();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<Role> GetRoles()
        {
            try
            {
                return _context.Role.ToList();
                                        
            }
            catch(Exception ex) { throw ex; }
        }

        public List<User> GetAdminUsers()
        {
            try
            {
                return _context.User
                               .Where(r => r.RoleId == (int)RoleType.Admin)
                               .Include(r => r.Reviewer)
                               .ToList();

            }
            catch (Exception ex) { throw ex; }
        }

        public List<Institution> GetInstitutions()
        {
            try
            {
                return _context.Institution
                                .Include(i => i.InstitutionCategory) 
                                .ToList();

            }
            catch (Exception ex) { throw ex; }
        }

        public Institution GetInstitutionById(int Id)
        {
            try
            {
                return _context.Institution
                                .Where(i => i.Id == Id)
                                .FirstOrDefault();
            }
            catch(Exception ex) { throw ex; }
        }

        public Institution GetInstitutionByNameAndState(string name, int stateId)
        {
            try
            {
                return _context.Institution
                               .Where(i => i.Name == name && i.StateId == stateId)
                               .FirstOrDefault();
            }
            catch(Exception ex) { throw ex; }
        }

        public User GetUserByUsername(string username)
        {
            try
            {
                return _context.User
                               .Where(u => u.UserName == username)
                               .FirstOrDefault();
            }
            catch(Exception ex) { throw ex; }
        }

        public List<SelectListItem> GetCategorySL()
        {
            try
            {
                List<SelectListItem> CategoryList = new List<SelectListItem>();
                var categories = _context.Category
                                     .Where(s => s.Active)
                                     .Select(s => new SelectListItem()
                                     {
                                         Text = s.Name,
                                         Value = s.Id.ToString()
                                     })
                                     .ToList();

                if (categories != null && categories.Count() > 0)
                {
                    CategoryList.Add(new SelectListItem() { Value = "", Text = "----Select Thematic Category----" });
                    CategoryList.AddRange(categories);
                }

                return CategoryList;
            }
            catch(Exception ex) { throw ex; }
        }

        public List<SelectListItem> GetTitleSL()
        {
            try
            {
                List<SelectListItem> TitleList = new List<SelectListItem>();
                var titles = _context.Title
                                     .Where(s => s.Active)
                                     .Select(s => new SelectListItem()
                                     {
                                         Text = s.Name,
                                         Value = s.Id.ToString()
                                     })
                                     .ToList();

                if (titles?.Count() > 0)
                {
                    TitleList.Add(new SelectListItem() { Value = "", Text = "----Select Title----" });
                    TitleList.AddRange(titles);
                }

                return TitleList;
            }
            catch (Exception ex) { throw ex; }
        }

        public List<SelectListItem> GetStateSL()
        {
            try
            {
                List<SelectListItem> StateList = new List<SelectListItem>();
                var states = _context.State
                                     .Where(s => s.Active)
                                     .Select(s => new SelectListItem()
                                     {
                                         Text = s.Name,
                                         Value = s.Id.ToString()
                                     })
                                     .ToList();

                if (states?.Count() > 0)
                {
                    StateList.Add(new SelectListItem() { Value = "", Text = "----Select State----" });
                    StateList.AddRange(states);
                }

                return StateList;
            }
            catch(Exception ex) { throw ex; }
        }

        public List<SelectListItem> GetInstitutionCategorySL()
        {
            try
            {
                List<SelectListItem> InstitutionCategoryList = new List<SelectListItem>();
                var categories = _context.InstitutionCategory
                                          .Where(s => s.Active)
                                          .Select(i => new SelectListItem()
                                          {
                                              Text = i.Name,
                                              Value = i.Id.ToString()
                                          })
                                          .ToList();
                
                if (categories?.Count() > 0)
                {
                    InstitutionCategoryList.Add(new SelectListItem() { Value = "", Text = "----Select Institution Category----" });
                    InstitutionCategoryList.AddRange(categories);
                }

                return InstitutionCategoryList;
            }
            catch (Exception ex) { throw ex; }
        }

        public List<SelectListItem> GetAdminRoleSL(bool isNotSupeAdmin)
        {
            try
            {
                List<SelectListItem> RoleList = new List<SelectListItem>();
                var roles = new List<SelectListItem>();
                if (isNotSupeAdmin)
                {
                    roles= _context.Role
                                    .Where(s => s.Active && s.Name.Contains("Co-Admin"))
                                    .Select(r => new SelectListItem()
                                    {
                                        Text = r.Name,
                                        Value = r.Id.ToString()
                                    })
                                    .ToList();
                }
                else
                {
                    roles = _context.Role
                                    .Where(s => s.Active && s.Name.Contains("Admin"))
                                    .Select(r => new SelectListItem()
                                    {
                                        Text = r.Name,
                                        Value = r.Id.ToString()
                                    })
                                    .ToList();
                }
                    

                if (roles?.Count() > 0)
                {
                    RoleList.Add(new SelectListItem() { Value = "", Text = "----Select Role----" });
                    RoleList.AddRange(roles);
                    
                }

                return RoleList;
            }
            catch(Exception ex) { throw ex; }
        }

        public List<SelectListItem> GetSessionSL()
        {
            try
            {
                List<SelectListItem> SessionList = new List<SelectListItem>();
                var sessions = _context.Session
                                    .Where(s => s.Active)
                                    .Select(r => new SelectListItem()
                                    {
                                        Text = r.Name,
                                        Value = r.Id.ToString()
                                    })
                                    .ToList();

                if (sessions?.Count() > 0)
                {
                    SessionList.Add(new SelectListItem() { Value = "", Text = "----Select Research Cycle----" });
                    SessionList.AddRange(sessions);
                }

                return SessionList;
            }
            catch (Exception ex) { throw ex; }
        }

        public List<SelectListItem> GetEventTypeSL()
        {
            try
            {
                List<SelectListItem> EventTypeList = new List<SelectListItem>();
                var eventTypes = _context.EventType
                                    .Where(s => s.Active)
                                    .Select(r => new SelectListItem()
                                    {
                                        Text = r.Name,
                                        Value = r.Id.ToString()
                                    })
                                    .ToList();

                if (eventTypes?.Count() > 0)
                {
                    EventTypeList.Add(new SelectListItem() { Value = "", Text = "----Select Event Type----" });
                    EventTypeList.AddRange(eventTypes);
                }

                return EventTypeList;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool CheckResearchCycleEvent(int eventTypeId)
        {
            try
            {
                TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("W. Central Africa Standard Time");
                bool validCycle = false;
                var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                if (activeSessionForApplication?.Id > 0)
                {
                    var eventRecord = _context.ResearchCycleEvent
                                         .Where(r => r.EventTypeId == eventTypeId && r.SessionId == activeSessionForApplication.Id && r.Active)
                                         .FirstOrDefault();
                    if (eventRecord != null)
                    {
                        DateTime startDate;
                        DateTime endDate;
                        startDate = eventRecord.StartDate;
                        endDate = eventRecord.EndDate;
                        startDate = TimeZoneInfo.ConvertTimeFromUtc(startDate, cstZone);
                        endDate = TimeZoneInfo.ConvertTimeFromUtc(endDate, cstZone);
                        if (DateTime.UtcNow.AddHours(1) >= startDate && DateTime.UtcNow.AddHours(1) <= endDate)
                        {
                            validCycle = true;
                        }
                    }
                }
               

                return validCycle;
            }
            catch(Exception ex) { throw ex; }
        }
        public List<PrincipalInvestigator> AllPI()
        {
            List<PrincipalInvestigator> principalInvestigatorList = new List<PrincipalInvestigator>();
            try
            {
                var allPI=_context.ApplicantThematic.Where(f=>f.Applicant.RoleId==(int)Roles.Applicant).ToList();
                if (allPI?.Count > 0)
                {
                    foreach(var item in allPI)
                    {
                        PrincipalInvestigator principalInvestigator = new PrincipalInvestigator();
                        principalInvestigator.ApplicantThematic = item;

                        principalInvestigatorList.Add(principalInvestigator);
                    }
                    
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return principalInvestigatorList;
        }
        public ReviewerThematicStrenght GetAssessorsExpertCategory(int reviewerId)
        {
            try
            {
                return _context.ReviewerThematicStrenght.Where(f => f.ReviewerId == reviewerId).FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public PICronologicalOrder PIChronologicalOrder(Applicant applicant, int applicantThematicId)
        {
            PICronologicalOrder pICronologicalOrder = new PICronologicalOrder();
            try
            {
                if(applicantThematicId==0 && applicant?.Id > 0)
                {
                    var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                    if (activeSessionForApplication?.Id > 0 && applicant?.Id > 0)
                    {
                        var conceptNote = _context.ConceptNote.Where(f => f.ApplicantThematic.ApplicantId == applicant.Id && f.ApplicantThematic.SessionId == activeSessionForApplication.Id).FirstOrDefault();
                        var proposal = _context.Proposal.Where(f => f.ApplicantThematic.ApplicantId == applicant.Id && f.ApplicantThematic.SessionId == activeSessionForApplication.Id).FirstOrDefault();
                        if (conceptNote?.Id > 0)
                        {
                            var conceptAwaitingApproval = _context.Approval.Where(f => f.ApplicantThematicId == conceptNote.ApplicantThemeId && f.IsProposal == false).FirstOrDefault();
                            if (conceptNote.Submitted && proposal == null)
                            {
                                if (conceptAwaitingApproval?.Id > 0 && conceptAwaitingApproval.Approved)
                                {
                                    pICronologicalOrder.Current = "Concept Note Submitted";
                                    pICronologicalOrder.Next = "Yet to submit Proposal";
                                }
                                else
                                {
                                    pICronologicalOrder.Current = "Concept Note Submitted";
                                    pICronologicalOrder.Next = "Concept Note Awaiting Approval";
                                }

                            }
                            else if (conceptNote.Submitted && proposal?.Id > 0)
                            {
                                var proposalAwaitingApproval = _context.Approval.Where(f => f.ApplicantThematicId == conceptNote.ApplicantThemeId && f.IsProposal == false).FirstOrDefault();
                                if (proposal.Submitted && proposalAwaitingApproval == null)
                                {
                                    pICronologicalOrder.Current = "Full Proposal Submitted";
                                    pICronologicalOrder.Next = "Full Proposal Awaiting Approval";
                                }
                                else if (proposal.Submitted && proposalAwaitingApproval?.Approved == true)
                                {
                                    pICronologicalOrder.Current = "Full Proposal Approved";
                                    pICronologicalOrder.Next = "Awaitng Budget Disbursment";
                                }
                            }
                        }
                        else
                        {
                            pICronologicalOrder.Current = "Registration Completed";
                            pICronologicalOrder.Next = "Awaitng Concept Note Submission";
                        }

                    }
                    else
                    {
                        pICronologicalOrder.Current = "No Active Session";
                        pICronologicalOrder.Next = "No Active Session";
                    }
                }
                else if(applicant.Id > 0 && applicantThematicId > 0)
                {
                        var conceptNote = _context.ConceptNote.Where(f => f.ApplicantThematic.Id==applicantThematicId).FirstOrDefault();
                        var proposal = _context.Proposal.Where(f => f.ApplicantThematic.Id==applicantThematicId).FirstOrDefault();
                        if (conceptNote?.Id > 0)
                        {
                            var conceptAwaitingApproval = _context.Approval.Where(f => f.ApplicantThematicId == conceptNote.ApplicantThemeId && f.IsProposal == false).FirstOrDefault();
                            if (conceptNote.Submitted && proposal == null)
                            {
                                if (conceptAwaitingApproval?.Id > 0 && conceptAwaitingApproval.Approved)
                                {
                                    pICronologicalOrder.Current = "Concept Note Submitted";
                                    pICronologicalOrder.Next = "Yet to submit Proposal";
                                }
                                else
                                {
                                    pICronologicalOrder.Current = "Concept Note Submitted";
                                    pICronologicalOrder.Next = "Concept Note Awaiting Approval";
                                }

                            }
                            else if (conceptNote.Submitted && proposal?.Id > 0)
                            {
                                var proposalAwaitingApproval = _context.Approval.Where(f => f.ApplicantThematicId == conceptNote.ApplicantThemeId && f.IsProposal == false).FirstOrDefault();
                                if (proposal.Submitted && proposalAwaitingApproval == null)
                                {
                                    pICronologicalOrder.Current = "Full Proposal Submitted";
                                    pICronologicalOrder.Next = "Full Proposal Awaiting Approval";
                                }
                                else if (proposal.Submitted && proposalAwaitingApproval?.Approved == true)
                                {
                                    pICronologicalOrder.Current = "Full Proposal Approved";
                                    pICronologicalOrder.Next = "Awaitng Budget Disbursment";
                                }
                            }
                        }
                        else
                        {
                            pICronologicalOrder.Current = "Registration Completed";
                            pICronologicalOrder.Next = "Awaitng Concept Note Submission";
                        }

                }
               

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return pICronologicalOrder;
        }

        //Get existing Application for active cycle
        public bool HasSubmitted(int type, int applicantThematicId)
        {
            bool hasSubmitted = false;
            try
            {
                var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
                if (type == (int)Types.ConceptNote && activeSessionForApplication?.Id>0)
                {
                    var submittedConceptNote=_context.ConceptNote.Where(g => g.ApplicantThemeId == applicantThematicId && g.ApplicantThematic.SessionId == activeSessionForApplication.Id && g.Submitted).FirstOrDefault();
                    if (submittedConceptNote?.Id > 0)
                    {
                        hasSubmitted = true;
                    }

                }
                else if (type == (int)Types.Proposal && activeSessionForApplication?.Id > 0)
                {
                    var submittedProposal = _context.Proposal.Where(g => g.ApplicantThemeId == applicantThematicId && g.ApplicantThematic.SessionId == activeSessionForApplication.Id && g.Submitted).FirstOrDefault();
                    if (submittedProposal?.Id > 0)
                    {
                        hasSubmitted = true;
                    }

                }

            }
            catch (Exception ex) { throw ex; }
            return hasSubmitted;
        }
        public PITimeLine PITimeLineStatus(Applicant applicant)
        {
            PITimeLine pITimeLine = new PITimeLine();
            var activeSessionForApplication = _context.Session.Where(f => f.ActiveForApplication).LastOrDefault();
            if (activeSessionForApplication?.Id > 0 && applicant?.Id>0)
            {
                var applicantThematic=_context.ApplicantThematic.Where(f => f.SessionId == activeSessionForApplication.Id && f.ApplicantId == applicant.Id).FirstOrDefault();
                if (applicantThematic == null)
                    return pITimeLine;
                var conceptNote=_context.ConceptNote.Where(f => f.ApplicantThemeId == applicantThematic.Id).FirstOrDefault();
                if (conceptNote?.Id > 0)
                {
                    if (!conceptNote.Submitted)
                        return pITimeLine;
                    pITimeLine.ConceptNote = true;
                    var conceptNotApproval=_context.Approval.Where(f => f.ApplicantThematicId == conceptNote.ApplicantThemeId && f.IsProposal==false).FirstOrDefault();
                    if (conceptNotApproval == null)
                        return pITimeLine;
                    pITimeLine.ConceptNoteApproval = true;
                    var proposal=_context.Proposal.Where(f => f.ApplicantThemeId == conceptNote.ApplicantThemeId).FirstOrDefault();
                    if (proposal == null)
                        return pITimeLine;
                    if (!proposal.Submitted)
                        return pITimeLine;
                    pITimeLine.Proposal = true;
                    var proposalApproval = _context.Approval.Where(f => f.ApplicantThematicId == conceptNote.ApplicantThemeId && f.IsProposal).FirstOrDefault();
                    if (proposalApproval == null)
                        return pITimeLine;
                    pITimeLine.ProposalApproval = true;
                }
            }
            return pITimeLine;
        }
        public String CreateSubmissionText(ApplicantSubmission applicantSubmission, string EstimatedBudget)
        {
            StringBuilder sb = new StringBuilder();
            if (applicantSubmission?.Id > 0)
            {
                sb.Append(applicantSubmission.ApplicantThematic.FileNo);
                sb.Append("/*");
                sb.AppendLine(applicantSubmission.ApplicantThematic.Title);
                sb.Append("/*");
                sb.AppendLine(applicantSubmission.ApplicantThematic.Theme.Name);
                sb.Append("/*");
                sb.AppendLine(applicantSubmission.ApplicantThematic.Theme.Category.Name);

                if (!String.IsNullOrEmpty(EstimatedBudget))
                {
                    sb.Append("/*");
                    
                    sb.AppendLine(EstimatedBudget);
                    sb.Append("/*");
                }
                if (applicantSubmission.IntroductionBackground != null)
                {
                    sb.Append("/*");
                    sb.Append("BACKGROUND OF RESEARCH");
                    sb.Append("*/");
                    sb.AppendLine(applicantSubmission.IntroductionBackground);
                    sb.Append("/*");
                }
                if (applicantSubmission.StatementOfProblem != null)
                {
                    sb.Append("/*");
                    sb.Append("STATEMENT OF THE PROBLEM");
                    sb.Append("*/");
                    sb.AppendLine(applicantSubmission.StatementOfProblem);
                    sb.Append("/*");
                }
                if (applicantSubmission.Aims != null)
                {
                    sb.Append("/*");
                    sb.Append("OBJECTIVE OF THE RESEARCH");
                    sb.Append("*/");
                    sb.AppendLine(applicantSubmission.Aims);
                    sb.Append("/*");
                }
                if (applicantSubmission.ResearchQuestions != null)
                {
                    sb.Append("/*");
                    sb.Append("RESEARCH QUESTION");
                    sb.Append("*/");
                    sb.AppendLine(applicantSubmission.ResearchQuestions);
                    sb.Append("/*");
                }
                if (applicantSubmission.LiteratureReview != null)
                {
                    sb.Append("/*");
                    sb.Append("LITERATURE REVIEW");
                    sb.Append("*/");
                    sb.Append(applicantSubmission.LiteratureReview);
                    sb.Append("/*");
                }
                if (applicantSubmission.ResearchMethodoly != null)
                {
                    sb.Append("/*");
                    sb.Append("RESEARCH METHODOLOGY");
                    sb.Append("*/");
                    sb.Append(applicantSubmission.ResearchMethodoly);
                    sb.Append("/*");
                }
                if (applicantSubmission.ExpectedResult != null)
                {
                    sb.Append("/*");
                    sb.Append("EXPECTED RESULT");
                    sb.Append("*/");
                    sb.Append(applicantSubmission.ExpectedResult);
                    sb.Append("/*");
                }
                if (applicantSubmission.Innovations != null)
                {
                    sb.Append("/*");
                    sb.Append("INNOVATION");
                    sb.Append("*/");
                    sb.AppendLine(applicantSubmission.Innovations);
                    sb.Append("/*");
                }
               
                
                if (applicantSubmission.References != null)
                {
                    sb.Append("/*");
                    sb.Append("REFERENCES");
                    sb.Append("*/");
                    sb.AppendLine(applicantSubmission.References);
                    sb.Append("/*");
                }
                if (applicantSubmission.TeamMates != null)
                {
                    sb.Append("/*");
                    sb.Append("RESEARCH TEAM");
                    sb.Append("*/");
                    sb.AppendLine(applicantSubmission.TeamMates);
                    sb.Append("/*");
                }

            }
            return sb.ToString();
        }
        public bool IsAssessmentSubmitted(int applicantThematicId, int type, int reviewerId)
        {
            var assessment=_context.ProposalScoring.Where(f => f.ApplicantThematicId == applicantThematicId && f.ReviewerId == reviewerId && f.TypeId == (int)Types.ConceptNote && f.IsSubmitted).FirstOrDefault();
            if (assessment?.Id > 0)
                return true;
            return false;

        }


        //Dropdowns
    }
}
