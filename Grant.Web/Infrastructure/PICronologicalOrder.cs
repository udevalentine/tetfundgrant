﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Infrastructure
{
    public class PICronologicalOrder
    {
        public string Current { get; set; }
        public string Next { get; set; }
    }
    public class PITimeLine
    {
        public bool ConceptNote { get; set; }
        public bool Proposal { get; set; }
        public bool ConceptNoteApproval { get; set; }
        public bool ProposalApproval { get; set; }
    }
}
