﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Infrastructure
{
    public interface IFluentEmailLogic
    {
        Task SendMail(MailGunModel sendEmailDto, string template);
        Task EmailFormatter(MailGunModel sendEmailDto,int type);
    }
}
