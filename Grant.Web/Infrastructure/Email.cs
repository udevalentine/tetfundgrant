﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using MailKit.Net.Smtp;
using MimeKit;
using System.Text;
using System.Threading.Tasks;
using Grant.Web.Models;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using System.Reflection;
using System.IO;

namespace Grant.Web.Infrastructure
{
    public class Email
    {
        private string MailGunUsername = "postmaster@sandbox16a5b9e673954f75b873cfe5a380ebcf.mailgun.org";//postmaster@sandbox390c7518ae214a8eb50528e5f3869f8b.mailgun.org
        private string MailGunPassword = "c61691e8b166d2c1537b24229067fe67-aa4b0867-936f824c";//94b493490f8676d50a64db81fad02052




        public void SendEmail(MailGunModel model,int MessageType)
        {
            try
            {
                // Compose a message
                MimeMessage mail = new MimeMessage();
                mail.From.Add(new MailboxAddress(model.DisplayName, model.MessageFrom));
                mail.To.Add(new MailboxAddress(model.Subject, model.MessageTo));
                mail.Subject = model.Subject;
                mail.Body = new TextPart("html")
                {
                    Text = FormatHtmlTemplate(model, MessageType)
                };

                // Send it!
                using (var client = new SmtpClient())
                {
                    // XXX - Should this be a little different?
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect("smtp.mailgun.org", 587, false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(MailGunUsername, MailGunPassword);

                    client.Send(mail);
                    client.Disconnect(true);
                }
            }
            catch (Exception ex) { throw ex; }
        }
        
        public string FormatHtmlTemplate(MailGunModel mailGunModel, int MessageType)
        {
            StringBuilder st = new StringBuilder();
            st.Append("<section style='width: 100%; background: whitesmoke; padding: 120px 10px;font-family: helvetica;'>");
            st.Append("<div style='width: 70%; background-color: #fff; padding: 0;margin: 0 auto;'>");
            st.Append("<header style='width: 100%; min-height:120px;text-align: center;padding-top: 50px;background: #007D53;COLOR:#FFF;'>");
            //st.Append("<img src='http://97.74.6.243/tetfundgrant/images/tetfundlogo.png' alt='tetfund logo' style='width: auto; height: 50px;box-shadow: 2px 1px 15px #333;'>");
            st.Append("<img src='https://tetfund.gov.ng/images/new-logo.png' alt='tetfund logo' style='width: auto; height: 50px;box-shadow: 2px 1px 15px #333;'>");
            st.Append("<h1 style='font-size: 30px;text-shadow:2px 3px #333;'>NATIONAL RESEARCH FUND</h1>");
            st.Append("</header>");
            st.Append("<section style='width: 100%; background: #fff;padding-bottom: 25px;'>");
            st.Append("<article style='padding: 5% 10%;font-size:16px;'>");
            if (MessageType == 1)
            {
                //optional: Sign-Up
                st.Append("<p style='text-align: center;font-style: normal;font-size:16px;'>");
                st.Append(mailGunModel.Body);
                st.Append("</p><br>");
                //optional: Sign-Up
            }
            else
            {
                //optional: Concept Note
                st.Append("Hi ");
                st.Append(mailGunModel.Name);
                st.Append(",<br>");
                st.Append("<p style='text-align: justify;font-style: normal;'>");
                st.Append(mailGunModel.Body);
                st.Append("</p><br>");
                //optional: Concept Note
            }

            if (!string.IsNullOrEmpty(mailGunModel.Link))
            {
                if (MessageType == 1)
                {
                    //optional: Link
                    st.Append("<div style='width: 100%;text-align: center;'>");
                    st.Append("<a href='" + mailGunModel.Link + "'");
                    st.Append("style='background: #13aa52;padding: 15px 20px;text-decoration:none;color:#fff;border-radius: 5px;box-shadow: 2px 1px 6px #333'>");
                    st.Append("Verify Account");
                    st.Append("</a>");
                    st.Append("</div>");
                    //optional: Link
                }
                else
                {
                    //optional: Link
                    st.Append("<div style='width: 100%;text-align: center;'>");
                    st.Append("<a href='" + mailGunModel.Link + "'");
                    st.Append("style='background: #13aa52;padding: 15px 20px;text-decoration:none;color:#fff;border-radius: 5px;box-shadow: 2px 1px 6px #333'>");
                    st.Append("Visit Site");
                    st.Append("</a>");
                    st.Append("</div>");
                    //optional: Link
                }
            }

            st.Append("</article>");
            st.Append("<footer style='width: 100%;'>");
            st.Append("<div style='width:80%;border-top: 2px solid #eee;margin: 0 auto;text-align: center;padding: 10px 15px;'>");
            st.Append("<img src='https://portal.abiastateuniversity.edu.ng/Content/images/lloydant.png' alt='lloydantlogo' style='width: 30px; height: 30px;box-shadow: 2px 1px 15px #333;'>&nbsp;");
            st.Append("<a href='http://lloydant.com' style='font-size: 12px;text-decoration: none;color:#000;'>Powered by LloydAnt</a>");
            st.Append("</div>");
            st.Append("</footer>");
            st.Append("</section>");
            st.Append("</div>");

            return st.ToString();
            //return string.Format(st.ToString(), mailGunModel.Name, mailGunModel.Body, mailGunModel.Link);
        }
        private string ParseTemplate(string path)
        {
            string result;
            var assembly = Assembly.GetExecutingAssembly();
            using (var stream = assembly.GetManifestResourceStream(path))
            using (var reader = new StreamReader(stream))
            {
                result = reader.ReadToEnd();
            }

            return result;
        }


    }
    public class MailGunModel
    {
        /// <summary>
        /// Readable name that tells which School or service is Sending the message E.g LLoydant or NAU
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// can be any email email addr as long as it ends with a valid domain name
        /// E.G VALID: admin@lloydant.com
        /// INVALID lloydant@admin.com
        /// </summary>
        [Required]
        [DataType(DataType.EmailAddress)]
        public string MessageFrom { get; set; }

        public string Subject { get; set; }

        [Required]
        public string Body { get; set; }

        public string Link { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string MessageTo { get; set; }
        public string Name { get; set; }
        public string SenderEmail { get; set; } = "info@nrf.lloydant.com";
        public string SenderName { get; set; } = "NRF";
    }
}
