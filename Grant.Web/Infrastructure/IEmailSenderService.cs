﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grant.Web.Infrastructure
{
    interface IEmailSenderService
    {
        void Send(EmailMessage message);
        void Send(List<EmailMessage> messages);
    }
}
