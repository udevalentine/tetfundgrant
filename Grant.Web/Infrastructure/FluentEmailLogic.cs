﻿using FluentEmail.Core;
using FluentEmail.Mailgun;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Grant.Web.Infrastructure
{
    public class FluentEmailLogic: IFluentEmailLogic
    {
        string _template = "Grant.Web.Infrastructure.EmailTemplate.EmailTemplate.cshtml";

        private readonly IFluentEmail _email;
        private readonly IConfiguration _configuration;

        public FluentEmailLogic([FromServices]IFluentEmail email, IConfiguration configuration)
        {
            _email = email;
            _configuration = configuration;
            var sender = new MailgunSender(_configuration.GetValue<string>("MailGun:domain"),
                                           _configuration.GetValue<string>("MailGun:apiKey"));
            _email.Sender = sender;

        }

        public async Task SendMail(MailGunModel sendEmailDto, string template)
        {
            if (!string.IsNullOrEmpty(template))
            {
                _template = template;
            }

            var parsedTemplate = ParseTemplate(_template);
            if (!string.IsNullOrEmpty(parsedTemplate))
            {
                try
                {
                    await _email
                   .SetFrom(sendEmailDto.SenderEmail, sendEmailDto.SenderName)
                   .To(sendEmailDto.MessageTo)
                   .Subject(sendEmailDto.Subject)
                   .UsingTemplate(parsedTemplate, sendEmailDto)
                   .SendAsync();
        }
                catch (Exception ex)
                {
                    throw ex;
                }

}

        }

        private string ParseTemplate(string path)
        {
            string result;
            var assembly = Assembly.GetExecutingAssembly();
            using (var stream = assembly.GetManifestResourceStream(path))
            using (var reader = new StreamReader(stream))
            {
                result = reader.ReadToEnd();
            }

            return result;
        }
        public async Task EmailFormatter(MailGunModel sendEmailDto, int type)
        {

            switch (type)
            {
                case 13:
                    sendEmailDto.Subject = "Researcher";
                    sendEmailDto.Body = "You have just been added as a research team member on: {0} Team. <br> <br> Please click on the link below to create a password and access your profile.";
                    break;
                //case 1:
                //    sendEmailDto.Body = string.Format("You have been added as an E-Instructor on " + sendEmailDto.InstitutionName + ", E-Learning Platform .Please, kindly verify your email to complate your registration.");
                //    sendEmailDto.Subject = "Sign-Up Notification";
                //    sendEmailDto.Link = "http://localhost:3000/VerifyEmail?guid=" + sendEmailDto.VerificationGuid + "&type=" + sendEmailDto.VerificationCategory;
                //    break;
                //case 2:
                //    sendEmailDto.Body = string.Format("You have Successfully sign up your Institution, on E-Learning Nigeria.Please, kindly verify your sign-up  to continue with the setup.");
                //    sendEmailDto.Subject = "Sign-Up Notification";
                //    sendEmailDto.Link = "http://localhost:3000/VerifyEmail?guid=" + sendEmailDto.VerificationGuid + "&type=" + sendEmailDto.VerificationCategory;
                //    break;
                //case 3:
                //    sendEmailDto.Body = string.Format("You have Successfully sign up as an E-Student on " + sendEmailDto.InstitutionName + ", E-Learning Platform .Please, kindly verify your email to login.");
                //    sendEmailDto.Subject = "Sign-Up Notification";
                //    sendEmailDto.Link = "http://localhost:3000/VerifyEmail?guid=" + sendEmailDto.VerificationGuid + "&type=" + sendEmailDto.VerificationCategory;
                //    break;
                default:
                    break;
            }


            await SendMail(sendEmailDto, _template);
        }
    }
}
