﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Grant.Web.Infrastructure
{
    public class WriteToPDF
    {
        private IHostingEnvironment _hostingEnvironment;
        public WriteToPDF(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        //protected FileResult GeneratePDF()
        //{
        //    using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
        //    {
        //        Document document = new Document(PageSize.A4, 10, 10, 10, 10);

        //        PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
        //        document.Open();

        //        Chunk chunk = new Chunk("This is from chunk. ");
        //        document.Add(chunk);

        //        Phrase phrase = new Phrase("This is from Phrase.");
        //        document.Add(phrase);

        //        Paragraph para = new Paragraph("This is from paragraph.");
        //        document.Add(para);

        //        string text = @"you are successfully created PDF file.";
        //        Paragraph paragraph = new Paragraph();
        //        paragraph.SpacingBefore = 10;
        //        paragraph.SpacingAfter = 10;
        //        paragraph.Alignment = Element.ALIGN_LEFT;
        //        paragraph.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.GREEN);
        //        paragraph.Add(text);
        //        document.Add(paragraph);

        //        document.Close();
        //        byte[] bytes = memoryStream.ToArray();
        //        memoryStream.Close();



        //        return File(memoryStream, "application/pdf", strPDFFileName);


        //        //HttpResponse.Response.Clear();
        //        //Response.ContentType = "application/pdf";

        //        //string pdfName = "User";
        //        //Response.AddHeader("Content-Disposition", "attachment; filename=" + pdfName + ".pdf");
        //        //Response.ContentType = "application/pdf";
        //        //Response.Buffer = true;
        //        //Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
        //        //Response.BinaryWrite(bytes);
        //        //Response.End();
        //        //Response.Close();
        //    }
        //}
        //public FileResult CreatePdf()
        //{
        //    MemoryStream workStream = new MemoryStream();
        //    StringBuilder status = new StringBuilder("");
        //    DateTime dTime = DateTime.Now;
        //    //file name to be created   
        //    string strPDFFileName = string.Format("SamplePdf" + dTime.ToString("yyyyMMdd") + "-" + ".pdf");
        //    Document doc = new Document();
        //    doc.SetMargins(2, 2, 2, 2);
        //    //doc.SetMargins(0 f, 0 f, 0 f, 0 f);
        //    //file will created in this path  

        //    var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "ApplicationDownload");

        //    string filePath = Path.Combine(uploads, strPDFFileName);

        //    //string strAttachment = Server.MapPath("~/Downloadss/" + strPDFFileName);


        //    PdfWriter.GetInstance(doc, workStream).CloseStream = false;
        //    doc.Open();

        //    //Add Content to PDF   
        //    doc.Add();

        //    // Closing the document  
        //    doc.Close();

        //    byte[] byteInfo = workStream.ToArray();
        //    workStream.Write(byteInfo, 0, byteInfo.Length);
        //    workStream.Position = 0;

        //    return FileResult(workStream, "application/pdf", strPDFFileName);

        //}
    }
}
